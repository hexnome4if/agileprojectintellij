<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:variable name="LOGISTIX">./src/main/resources/icons/logistix.png</xsl:variable>
    <xsl:variable name="GOFORWARD">./src/main/resources/icons/goForward.png</xsl:variable>
    <xsl:variable name="TURNLEFT">./src/main/resources/icons/turnLeft.png</xsl:variable>
    <xsl:variable name="TURNRIGHT">./src/main/resources/icons/turnRight.png</xsl:variable>

    <xsl:template match="/">
        <html>
            <head>
                <title>
                    <xsl:text>Tournée </xsl:text>
                </title>
                <link rel="stylesheet" type="text/css" href="feuilleDeRoute.css"/>
            </head>
            <body style="background-color:white; font-family:sans-serif;">
                <div class="container">
                    <h1>
                        LogistiX
                    </h1>
                    <img id="logo">
                        <xsl:attribute name="src">
                            <xsl:value-of select='$LOGISTIX'/>
                        </xsl:attribute>
                    </img>
                    <h2>
                        <xsl:text>Durée de la tournée : </xsl:text>
                        <xsl:variable name="seconds" select="//dureeTournee"/>
                        <xsl:if test="floor($seconds div 3600)=0">
                            <xsl:if test="floor($seconds div 60)=0">
                                <xsl:value-of select="format-number($seconds mod 60, '00 s')"/>
                            </xsl:if>
                            <xsl:if test="floor($seconds div 60)>0">
                                <xsl:value-of select="format-number(floor($seconds div 60) mod 60, '00 min ')"/>
                                <xsl:value-of select="format-number($seconds mod 60, '00 s')"/>
                            </xsl:if>
                        </xsl:if>
                        <xsl:if test="floor($seconds div 3600)>0">
                            <xsl:value-of select="format-number(floor($seconds div 3600), '00 h ')"/>
                            <xsl:value-of select="format-number(floor($seconds div 60) mod 60, '00 min ')"/>
                            <xsl:value-of select="format-number($seconds mod 60, '00 s')"/>
                        </xsl:if>

                        <!--<xsl:value-of select="//dureeTournee"/>-->
                        <!--<xsl:text>s</xsl:text>-->
                    </h2>
                    <xsl:apply-templates select="//ItineraireHorodate"/>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="//ItineraireHorodate">
        <h3>
            <xsl:text>Livraison n°</xsl:text>
            <xsl:value-of select="count(preceding-sibling::ItineraireHorodate)+1"/>
        </h3>
        <h4>
            <xsl:text>
                Origine
            </xsl:text>
            <xsl:if test="./itineraire/livraisonOrigine/id=0">
                <xsl:text>: Entrepôt</xsl:text>
            </xsl:if>
        </h4>
        <table border="3" width="90%" align="center">
            <tr>
                <td style="width:34%">
                    <strong>Adresse</strong>
                </td>
                <td style="width:33%">
                    <strong>x</strong>
                </td>
                <td style="width:33%">
                    <strong>y</strong>
                </td>
            </tr>
            <tr>
                <td style="width:34%">
                    <xsl:value-of select="./itineraire/livraisonOrigine/adresse/id"/>
                </td>
                <td style="width:33%">
                    <xsl:value-of select="./itineraire/livraisonOrigine/adresse/coord/coordX"/>
                </td>
                <td style="width:33%">
                    <xsl:value-of select="./itineraire/livraisonOrigine/adresse/coord/coordY"/>
                </td>
            </tr>
        </table>
        <h4>
            <xsl:text>
                Destination
            </xsl:text>
            <xsl:if test="./itineraire/livraisonArrivee/id=0">
                <xsl:text>: Entrepôt</xsl:text>
            </xsl:if>
        </h4>
        <table border="3" width="90%" align="center">
            <tr>
                <td style="width:34%">
                    <strong>Adresse</strong>
                </td>
                <td style="width:33%">
                    <strong>x</strong>
                </td>
                <td style="width:33%">
                    <strong>y</strong>
                </td>
            </tr>
            <tr>
                <td style="width:34%">
                    <xsl:value-of select="./itineraire/livraisonArrivee/adresse/id"/>
                </td>
                <td style="width:33%">
                    <xsl:value-of select="./itineraire/livraisonArrivee/adresse/coord/coordX"/>
                </td>
                <td style="width:33%">
                    <xsl:value-of select="./itineraire/livraisonArrivee/adresse/coord/coordY"/>
                </td>
            </tr>
        </table>
        <xsl:if test="count(preceding-sibling::ItineraireHorodate)+1 &lt; count(//ItineraireHorodate)">
            <h4>
                <xsl:text>Contrainte livraison : </xsl:text>
                <xsl:value-of select="./plageLivraison/heureDeb/heures"/>
                <xsl:text>h</xsl:text>
                <xsl:value-of select="format-number(./plageLivraison/heureDeb/minutes, '00')"/>
                <!--<xsl:text>:</xsl:text>-->
                <!--<xsl:value-of select="./plageLivraison/heureDeb/secondes"/>-->
                <xsl:text> - </xsl:text>
                <xsl:value-of select="./plageLivraison/heureFin/heures"/>
                <xsl:text>h</xsl:text>
                <xsl:value-of select="format-number(./plageLivraison/heureFin/minutes, '00')"/>
                <!--<xsl:text>:</xsl:text>-->
                <!--<xsl:value-of select="./plageLivraison/heureFin/secondes"/>-->
            </h4>
            <h4>
                <xsl:text>Heure effective de livraison : </xsl:text>
                <xsl:value-of select="./heureDeLivraison/heures"/>
                <xsl:text>h</xsl:text>
                <xsl:value-of select="format-number(./heureDeLivraison/minutes, '00')"/>
                <!--<xsl:text>:</xsl:text>-->
                <!--<xsl:value-of select="./heureDeLivraison/secondes"/>-->
            </h4>
            <h4>
                <xsl:text>Durée effective de livraison : </xsl:text>
                <!--<xsl:value-of select="./itineraire/livraisonArrivee/dureeLivraisonSec"/>-->
                <xsl:variable name="seconds" select="./itineraire/livraisonArrivee/dureeLivraisonSec"/>
                <xsl:if test="floor($seconds div 3600)=0">
                    <xsl:if test="floor($seconds div 60)=0">
                        <xsl:value-of select="format-number($seconds mod 60, '00 s')"/>
                    </xsl:if>
                    <xsl:if test="floor($seconds div 60)>0">
                        <xsl:value-of select="format-number(floor($seconds div 60) mod 60, '00 min ')"/>
                        <xsl:value-of select="format-number($seconds mod 60, '00 s')"/>
                    </xsl:if>
                </xsl:if>
                <xsl:if test="floor($seconds div 3600)>0">
                    <xsl:value-of select="format-number(floor($seconds div 3600), '00 h ')"/>
                    <xsl:value-of select="format-number(floor($seconds div 60) mod 60, '00 min ')"/>
                    <xsl:value-of select="format-number($seconds mod 60, '00 s')"/>
                </xsl:if>
                <!--<xsl:text>s</xsl:text>-->
            </h4>
        </xsl:if>
        <xsl:if test="count(preceding-sibling::ItineraireHorodate)+1=count(//ItineraireHorodate)">
            <h4>
                <xsl:text>Heure de retour à l'Entrepôt : </xsl:text>
                <xsl:value-of select="./heureDeLivraison/heures"/>
                <xsl:text>h</xsl:text>
                <xsl:value-of select="./heureDeLivraison/minutes"/>
                <!--<xsl:text>:</xsl:text>-->
                <!--<xsl:value-of select="./heureDeLivraison/secondes"/>-->
            </h4>
        </xsl:if>
        <h4>Chemin à suivre :</h4>
        <h5 style="margin-left:4%">
            <xsl:text>Temps de parcours du chemin : </xsl:text>
            <!--<xsl:value-of select="./itineraire/dureeItineraire"/>-->
            <xsl:variable name="seconds" select="./itineraire/dureeItineraire"/>
            <xsl:if test="floor($seconds div 3600)=0">
                <xsl:if test="floor($seconds div 60)=0">
                    <xsl:value-of select="format-number($seconds mod 60, '00 s')"/>
                </xsl:if>
                <xsl:if test="floor($seconds div 60)>0">
                    <xsl:value-of select="format-number(floor($seconds div 60) mod 60, '00 min ')"/>
                    <xsl:value-of select="format-number($seconds mod 60, '00 s')"/>
                </xsl:if>
            </xsl:if>
            <xsl:if test="floor($seconds div 3600)>0">
                <xsl:value-of select="format-number(floor($seconds div 3600), '00 h ')"/>
                <xsl:value-of select="format-number(floor($seconds div 60) mod 60, '00 min ')"/>
                <xsl:value-of select="format-number($seconds mod 60, '00 s')"/>
            </xsl:if>
            <!--<xsl:text>s</xsl:text>-->
        </h5>

        <xsl:apply-templates select="./itineraire/listeTroncons">
        </xsl:apply-templates>

        <br/>
        <hr/>
    </xsl:template>

    <xsl:template match="//listeTroncons">
        <table id="mainTable" border="1" width="95%" align="center">
            <tr>
                <th>
                    <strong>Rue</strong>
                </th>
                <th>
                    <strong>Distance</strong>
                </th>
                <th>
                    <strong>Durée</strong>
                </th>
                <th>
                    <strong>Direction suivante</strong>
                </th>
            </tr>

            <xsl:for-each select="./troncon">
                <xsl:sort select="position()" data-type="number" order="descending"/>
                <xsl:variable name="curNomRue" select="./nomRue"/>
                <xsl:if test="not($curNomRue=following-sibling::*/nomRue)">
                    <tr height="50px">
                        <td>
                            <xsl:value-of select="$curNomRue"/>
                        </td>
                        <td>
                            <xsl:variable name="longueur" select="sum(../troncon[nomRue=$curNomRue]/longueur)"/>
                            <xsl:value-of select='format-number( round(100*$longueur) div 100 ,"##0.0" )'/>
                            <xsl:text> m</xsl:text>
                        </td>
                        <td>
                            <xsl:variable name="duree" select="sum(../troncon[nomRue=$curNomRue]/dureeParcours)"/>
                            <xsl:if test="floor($duree div 3600)=0">
                                <xsl:if test="floor($duree div 60)=0">
                                    <xsl:value-of select="format-number($duree mod 60, '00 s')"/>
                                </xsl:if>
                                <xsl:if test="floor($duree div 60)>0">
                                    <xsl:value-of select="format-number(floor($duree div 60) mod 60, '00 min ')"/>
                                    <xsl:value-of select="format-number($duree mod 60, '00 s')"/>
                                </xsl:if>
                            </xsl:if>
                            <xsl:if test="floor($duree div 3600)>0">
                                <xsl:value-of select="format-number(floor($duree div 3600), '00 h ')"/>
                                <xsl:value-of select="format-number(floor($duree div 60) mod 60, '00 min ')"/>
                                <xsl:value-of select="format-number($duree mod 60, '00 s')"/>
                            </xsl:if>
                        </td>
                        <td>
                            <p>
                                <xsl:variable name="posDeb"
                                              select="count(../troncon[not(./preceding-sibling::node()/nomRue=$curNomRue)])-1"/>
                                <xsl:variable name="nbIntersections"
                                              select="count(../troncon[nomRue=$curNomRue])-1"></xsl:variable>
                                <xsl:variable name="posFin" select="$nbIntersections + $posDeb"/>
                                <xsl:variable name="nbIntersectionsLeft"
                                              select="count(../../listeAngles/angle[text() &gt; 20 and position() &gt;= $posDeb and position() &lt;= ($posFin)])"></xsl:variable>
                                <xsl:variable name="nbIntersectionsRight"
                                              select="count(../../listeAngles/angle[text() &lt; -20 and position() &gt;= $posDeb and position() &lt;= ($posFin)])"></xsl:variable>
                                <xsl:variable name="angle" select="../../listeAngles/angle[$posDeb]"/>

                                <!-- DEBUG
                                <xsl:value-of select="$posDeb"></xsl:value-of>
                                eee
                                <xsl:value-of select="$nbIntersections"></xsl:value-of>
                                fff
                                <xsl:value-of select="$nbIntersectionsRight"></xsl:value-of>
                                dddd
                                <xsl:value-of select="$nbIntersectionsLeft"></xsl:value-of>
                                aaaa
                                <xsl:value-of select="$posFin"></xsl:value-of>
                                bbbb
                                <xsl:value-of select="$angle"></xsl:value-of>
                                -->

                                <span><!--
                                    <xsl:choose>
                                        <xsl:when test="count(preceding-sibling::*[not(nomRue=$curNomRue)])=0">Vous êtes
                                            arrivés.
                                        </xsl:when>
                                        <xsl:when test="$angle &gt; -20 and $angle &lt; 20">Continuer tout droit.
                                        </xsl:when>
                                        <xsl:when test="$nbIntersectionsRight &lt;= 1 and $nbIntersectionsLeft &lt;= 1">
                                            Prendre la 1ère
                                        </xsl:when>
                                        <xsl:when test="$nbIntersectionsLeft &lt;= 1 and $angle &lt;= -20">Prendre la
                                            1ère
                                        </xsl:when>
                                        <xsl:when test="$nbIntersectionsRight &lt;= 1 and $angle &gt;= 20">Prendre la
                                            1ère
                                        </xsl:when>

                                        <xsl:otherwise>
                                            Prendre la
                                            <xsl:choose>
                                                <xsl:when test="$angle &lt;= 160">
                                                    <xsl:value-of select="$nbIntersectionsLeft"/>
                                                </xsl:when>
                                                <xsl:when test="$angle &gt;= 200">
                                                    <xsl:value-of select="$nbIntersectionsRight"/>
                                                </xsl:when>
                                            </xsl:choose>
                                            ème
                                        </xsl:otherwise>

                                    </xsl:choose>-->
                                    <xsl:choose>
                                        <xsl:when test="count(preceding-sibling::*[not(nomRue=$curNomRue)])=0">Vous êtes
                                            arrivés.
                                        </xsl:when>
                                        <xsl:when test="$angle &gt; -20 and $angle &lt; 20">Continuer tout droit.
                                        </xsl:when>
                                        <xsl:when test="$angle &lt;= -20">
                                            <xsl:text>Tourner à droite.</xsl:text>
                                        </xsl:when>
                                        <xsl:when test="$angle &gt;= 20">
                                            <xsl:text>Tourner à gauche.</xsl:text>
                                        </xsl:when>
                                    </xsl:choose>
                                </span>
                                <xsl:choose>
                                    <xsl:when test="count(preceding-sibling::*[not(nomRue=$curNomRue)])=0"></xsl:when>
                                    <xsl:when test="$angle &gt; -20 and $angle &lt; 20">
                                        <img>
                                            <xsl:attribute name="src">
                                                <xsl:value-of select='$GOFORWARD'/>
                                            </xsl:attribute>
                                        </img>
                                    </xsl:when>
                                    <xsl:when test="$angle &lt;= -20">
                                        <img>
                                            <xsl:attribute name="src">
                                                <xsl:value-of select='$TURNRIGHT'/>
                                            </xsl:attribute>
                                        </img>
                                    </xsl:when>
                                    <xsl:when test="$angle &gt;= 20">
                                        <img>
                                            <xsl:attribute name="src">
                                                <xsl:value-of select='$TURNLEFT'/>
                                            </xsl:attribute>
                                        </img>
                                    </xsl:when>
                                </xsl:choose>
                            </p>
                        </td>
                    </tr>
                    <tr class="noColorTr">
                        <td colspan="4">
                            <table id="detail" border="1" width="95%" align="center">
                                <tr>
                                    <td colspan="4">
                                        <strong>
                                            <center>Détail du parcours</center>
                                        </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Adresse</strong>
                                    </td>
                                    <td>
                                        <strong>Longueur (m)</strong>
                                    </td>
                                    <td>
                                        <strong>Durée (s)</strong>
                                    </td>
                                    <td>
                                        <strong>Coordonnées</strong>
                                    </td>
                                </tr>
                                <xsl:apply-templates select="../troncon[nomRue=$curNomRue]">
                                    <xsl:sort select="position()" data-type="number" order="descending"/>
                                </xsl:apply-templates>
                            </table>
                        </td>
                    </tr>
                </xsl:if>
            </xsl:for-each>
        </table>
    </xsl:template>

    <xsl:template match="//troncon">
        <tr>
            <td>
                <xsl:value-of select="./origine/id"/>
            </td>
            <td>
                <xsl:variable name="longueur" select="./longueur"/>
                <xsl:value-of select='format-number( round(100*$longueur) div 100 ,"##0.0" )'/>
            </td>
            <td>
                <xsl:value-of select="./dureeParcours"/>
            </td>
            <td>
                <xsl:text>x : </xsl:text>
                <xsl:value-of select="./origine/coord/coordX"/>
                <xsl:text>&#x3000;</xsl:text>
                <xsl:text>y : </xsl:text>
                <xsl:value-of select="./origine/coord/coordY"/>
            </td>
        </tr>
    </xsl:template>
</xsl:stylesheet>
