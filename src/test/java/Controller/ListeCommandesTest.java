package Controller;

import Controller.Commandes.Commande;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ListeCommandesTest {
    @Test
    void getNbUndoableCmd() {
        assertEquals(0, listCmd.getNbUndoableCmd());
        assertEquals(0, listCmd.getNbRedoableCmd());

        //On créé la classe de test (définie comme InnerClasse)
        Commande testCmd = new CmdTest();
        try {
            listCmd.ajouterCmd(testCmd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(1, listCmd.getNbUndoableCmd());
        assertEquals(0, listCmd.getNbRedoableCmd());
        try {
            listCmd.undoCmd();
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(0, listCmd.getNbUndoableCmd());
        assertEquals(1, listCmd.getNbRedoableCmd());
        try {
            listCmd.redo();
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(1, listCmd.getNbUndoableCmd());
        assertEquals(0, listCmd.getNbRedoableCmd());
        try {
            listCmd.annule();
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(0, listCmd.getNbUndoableCmd());
        assertEquals(0, listCmd.getNbRedoableCmd());
    }

    @Test
    void getNbRedoableCmd() {
        assertEquals(0, listCmd.getNbUndoableCmd());
        //On créé la classe de test (définie comme InnerClasse)
        Commande testCmd = new CmdTest();
        try {
            listCmd.ajouterCmd(testCmd);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertEquals(1, listCmd.getNbUndoableCmd());
    }

    ListeCommandes listCmd;
    @BeforeEach
    void setUp() {
        listCmd = new ListeCommandes();
    }

    @AfterEach
    void tearDown() {
    }

    /**
     * Test l'ajout d'une commande à la liste des commande. (Est-elle ajouté à la liste des commande ?)
     */
    @Test
    void ajouterCmd_AjoutValide() {
        assertEquals(0, listCmd.getCommandeListeLength());

        //On créé la classe de test (définie comme InnerClasse)
        Commande testCmd = new CmdTest();
        try {
            listCmd.ajouterCmd(testCmd);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(1, listCmd.getCommandeListeLength());
    }

    /**
     * Test l'execution d'une commande lors de son ajout à la liste des commandes. (Est-elle executée ?)
     */
    @Test
    void ajouterCmd_ExecutionEffectue() {
        assertEquals(0, listCmd.getCommandeListeLength());

        //On créé la classe de test (définie comme InnerClasse)
        myBool myBool = new myBool();
        CmdTestTrueBool testCmd = new CmdTestTrueBool(myBool);
        try {
            listCmd.ajouterCmd(testCmd);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(true, myBool.mybool);
    }

    /**
     * Test l'execution du undo d'une commande lors de la demande à la liste des commandes. (Est-elle défaite ?)
     */
    @Test
    void undoCmd_ExecutionEffectue() {
        assertEquals(0, listCmd.getCommandeListeLength());

        //On créé les classes de test (définie comme InnerClasse)
        myBool myBool = new myBool();
        CmdTestTrueBool testCmd = new CmdTestTrueBool(myBool);
        try {
            listCmd.ajouterCmd(testCmd);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(true, myBool.mybool);

        try {
            listCmd.undoCmd();
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(false, myBool.mybool);
    }

    /**
     * Test l'execution du undo d'une commande lors de la demande de l'annulation. (Est-elle défaite ?)
     */
    @Test
    void annule_ExecutionEffectue() {
        assertEquals(0, listCmd.getCommandeListeLength());

        //On créé les classes de test (définie comme InnerClasse)
        myBool myBool = new myBool();
        //On execute une commande (false -> true)
        CmdTestTrueBool testCmd = new CmdTestTrueBool(myBool);
        try {
            listCmd.ajouterCmd(testCmd);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(true, myBool.mybool);

        //On défait la commande.
        try {
            listCmd.annule();
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(false, myBool.mybool);
    }

    /**
     * Test l'impossibilité de faire un redo en cas
     */
    @Test
    void annule_redoImpossible() {
        assertEquals(0, listCmd.getCommandeListeLength());

        //On créé les classes de test (définie comme InnerClasse)
        myBool myBool = new myBool();
        //On execute une commande (false -> true)
        CmdTestTrueBool testCmd = new CmdTestTrueBool(myBool);
        try {
            listCmd.ajouterCmd(testCmd);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(true, myBool.mybool);

        //On défait la commande
        try {
            listCmd.annule();
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(false, myBool.mybool);

        //On refait une commande (false -> false, car elle n'a pas du être re-executé)
        try {
            listCmd.redo();
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(false, myBool.mybool);
        assertEquals(0, listCmd.getCommandeListeLength());

    }

    /**
     * Test la ré-execution d'une commande lors de la re-execution (Est-ce qu'une commande est refaite ?)
     */
    @Test
    void redo_ExecutionEffectue() {
        assertEquals(0, listCmd.getCommandeListeLength());

        //On créé les classes de test (définie comme InnerClasse)
        myBool myBool = new myBool();
        //On execute une commande (false -> true)
        CmdTestTrueBool testCmd = new CmdTestTrueBool(myBool);
        try {
            listCmd.ajouterCmd(testCmd);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(true, myBool.mybool);

        //On défait la commande
        try {
            listCmd.undoCmd();
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(false, myBool.mybool);

        //On refait une commande (false -> true)
        try {
            listCmd.redo();
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(true, myBool.mybool);
    }

    @Test
    void reset() {
        assertEquals(0, listCmd.getCommandeListeLength());

        //On créé les classes de test (définie comme InnerClasse)
        //On execute une commande (false -> true)
        Commande testCmd = new CmdTest();
        try {
            listCmd.ajouterCmd(testCmd);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(1, listCmd.getCommandeListeLength());

        //On execute une commande (false -> true)
        try {
            listCmd.ajouterCmd(testCmd);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(2, listCmd.getCommandeListeLength());

        //On refait une commande (false -> false, car elle n'a pas du être re-executé)
        try {
            listCmd.ajouterCmd(testCmd);
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertEquals(3, listCmd.getCommandeListeLength());

        listCmd.reset();
        assertEquals(0, listCmd.getCommandeListeLength());
    }


    private static class CmdTest implements Commande {
        public CmdTest( ) {
        }

        public void doCmd() {
        }

        public void undoCmd() {
        }
    }

    private static class myBool{
        boolean mybool = false;
    }

    private static class CmdTestTrueBool implements Commande {
        public myBool mybool;
        public CmdTestTrueBool(myBool bool) {
            mybool = bool;
        }

        public void doCmd() {
            mybool.mybool = true;
        }

        public void undoCmd() {
            mybool.mybool = false;
        }
    }
}