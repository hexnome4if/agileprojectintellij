package ApplicationException;

import javafx.application.Application;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ApplicationExceptionTest {

    /**
     * Test si on catch une Application warning avec une ApplicationException
     */
    @Test
    void ApplicationWarning() {
        double  i = 2;

        try{
            throw new ApplicationWarning("Nom de l'erreur");
        } catch (ApplicationException e){
                i = 0;
                assertEquals("Nom de l'erreur",e.getMessage());
        }

        assertEquals(0, i);
    }

    /**
     * Test si on catch une Application error avec une ApplicationException
     */
    @Test
    void ApplicationError() {
        double  i = 2;

        try{
            throw new ApplicationError("Nom de l'erreur");
        } catch (ApplicationException e){
            i = 0;
            assertEquals("Nom de l'erreur",e.getMessage());
        }

        assertEquals(0, i);
    }

    /**
     * Test si on catch une Application warning avec une ApplicationException
     */
    @Test
    void ApplicationWarning_NoString() {
        double  i = 2;

        try{
            throw new ApplicationWarning();
        } catch (ApplicationException e){
            i = 0;
            assertEquals(null, e.getMessage());
        }

        assertEquals(0, i);
    }

    /**
     * Test si on catch une Application error avec une ApplicationException
     */
    @Test
    void ApplicationError_NoString() {
        double  i = 2;

        try{
            throw new ApplicationError();
        } catch (ApplicationException e){
            i = 0;
            assertEquals(null,e.getMessage());
        }

        assertEquals(0, i);
    }
}