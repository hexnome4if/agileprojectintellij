package Util;

import junit.framework.TestCase;
import org.junit.jupiter.api.Test;

public class UniformSpeedTest extends TestCase {
    @Test
    public void testCalculTemps() throws Exception {
        UniformSpeed us = new UniformSpeed();
        double longueur,temps,vitesse;
        vitesse=15000.0/3600.0;

        //Test 1 : longueur nulle
        longueur = 0.0;
        temps=longueur/vitesse;
        assertEquals("distance nulle",temps,us.calculTemps(longueur),0.001);

        //Test 2 : longueur positive
        longueur = 140.0;
        temps=longueur/vitesse;
        assertEquals("distance positive = 140m",temps,us.calculTemps(longueur),0.001);

        //Test 3 : longueur positive
        longueur = 10.0;
        temps=longueur/vitesse;
        assertEquals("distance positive = 10m",temps,us.calculTemps(longueur),0.001);

        //Test 4 : longueur positive
        longueur = 256.0;
        temps=longueur/vitesse;
        assertEquals("distance positive = 256m",temps,us.calculTemps(longueur),0.001);

        //Test 5 : longueur négative
        longueur = -10.0;
        Exception e = null;
        try {
            us.calculTemps(longueur);
        }catch(Exception e2){
            e = new Exception(e2);
        }
        assertNotNull("distance négative",e);

    }

}