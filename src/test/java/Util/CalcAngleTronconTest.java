package Util;

import Modele.Coordonnes;
import Modele.Noeud;
import Modele.Troncon;
import junit.framework.TestCase;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class CalcAngleTronconTest extends TestCase {

    @Test
    public void testCalculAngle45() throws Exception {
        Coordonnes coord1 = new Coordonnes(1,1);
        Coordonnes coord2 = new Coordonnes(2,2);
        Noeud n1 = new Noeud(1,coord1);
        Noeud n2 = new Noeud(2,coord2);
        Troncon t = new Troncon(1L,2.0,"rueTest",3,n1,n2);
        double angle = CalcAngleTroncon.calculerAngle(t);
        assertEquals(45.0,angle);
    }

    @Test
    public void testCalculAngle90() throws Exception {
        Coordonnes coord1 = new Coordonnes(1,1);
        Coordonnes coord2 = new Coordonnes(1,2);
        Noeud n1 = new Noeud(1,coord1);
        Noeud n2 = new Noeud(2,coord2);
        Troncon t = new Troncon(1L,2.0,"rueTest",3,n1,n2);
        double angle = CalcAngleTroncon.calculerAngle(t);
        assertEquals(90.0,angle);
    }

    @Test
    public void testCalculAngle135() throws Exception {
        Coordonnes coord1 = new Coordonnes(-1,1);
        Coordonnes coord2 = new Coordonnes(-2,2);
        Noeud n1 = new Noeud(1,coord1);
        Noeud n2 = new Noeud(2,coord2);
        Troncon t = new Troncon(1L,2.0,"rueTest",3,n1,n2);
        double angle = CalcAngleTroncon.calculerAngle(t);
        assertEquals(135.0,angle);
    }

    @Test
    public void testCalculAngleTronconInvalide() throws Exception {
        Troncon t = null;
        Exception e = assertThrows(Exception.class, () -> CalcAngleTroncon.calculerAngle(t));
    }
}