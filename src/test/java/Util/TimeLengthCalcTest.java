package Util;

import junit.framework.TestCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class TimeLengthCalcTest extends TestCase {
    TimeLengthCalc calculateurDuree;
    double longueur,temps,vitesse;
    
    @BeforeEach
    public void setUp(){
        calculateurDuree = new TimeLengthCalc();
        vitesse=15000.0/3600.0;
    }
    
    @Test
    public void testCalculDureeUniformeNulle() throws Exception {

        //Test 1 : longueur nulle
        longueur = 0.0;
        temps = longueur / vitesse;
        assertEquals("distance nulle", temps, calculateurDuree.calculDureeUniforme(longueur), 0.001);
    }

    @Test
    public void testCalculDureeUniformePositif1() throws Exception {
        //Test 2 : longueur positive
        longueur = 140.0;
        temps = longueur / vitesse;
        assertEquals("distance positive = 140m", temps, calculateurDuree.calculDureeUniforme(longueur), 0.001);
    }

    @Test
    public void testCalculDureeUniformePositif2() throws Exception {
        //Test 3 : longueur positive
        longueur = 10.0;
        temps = longueur / vitesse;
        assertEquals("distance positive = 10m", temps, calculateurDuree.calculDureeUniforme(longueur), 0.001);
    }

    @Test
    public void testCalculDureeUniformePositif3() throws Exception {
        //Test 4 : longueur positive
        longueur = 256.0;
        temps = longueur / vitesse;
        assertEquals("distance positive = 256m", temps, calculateurDuree.calculDureeUniforme(longueur), 0.001);
    }

    @Test
    public void testCalculDureeUniformeNegatif() throws Exception {
        //Test 5 : longueur négative
        Exception e = assertThrows(Exception.class, () -> calculateurDuree.calculDureeUniforme(-10.0));

    }

}