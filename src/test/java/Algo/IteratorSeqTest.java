package Algo;

import junit.framework.TestCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.TreeSet;

public class IteratorSeqTest extends TestCase {

    static TreeSet<Long> set_0;
    static TreeSet<Long> set_1;

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
        init_0();
        init_1();
    }

    public static void init_0(){
        set_0 = new TreeSet<>();
        set_0.add(3L);
        set_0.add(5L);
        set_0.add(135L);
        set_0.add(2L);
        set_0.add(156L);
    }
    public static void init_1(){
        set_1 = new TreeSet<Long>();
        set_1.add(3L);
        set_1.add(5L);
        set_1.add(135L);
        set_1.add(2L);
        set_1.add(156L);
        set_1.add(1562L);
        set_1.add(157L);
        set_1.add(16L);
        set_1.add(56L);
        set_1.add(6L);
        set_1.add(7L);
        set_1.add(12L);
        set_1.add(36L);
    }

    @Test
    public void testHasNext() throws Exception {
        testHasNext_0();
        testHasNext_1();
    }

    public static void testHasNext_0(){
        IteratorSeq it = new IteratorSeq(set_0);
        for(int i=0; i < 5; i++){
            assertTrue(it.hasNext());
            Long trash = it.next();
        }
        assertTrue(!it.hasNext());
    }

    public static void testHasNext_1(){
        IteratorSeq it = new IteratorSeq(set_1);
        for(int i=0; i < 13; i++){
            assertTrue(it.hasNext());
            Long trash = it.next();
        }
        assertTrue(!it.hasNext());
    }

    @Test
    public void testNext() throws Exception {
        testNext_0();
        testNext_1();
    }

    public static void testNext_0(){
        IteratorSeq it = new IteratorSeq((TreeSet<Long>)set_0.clone());

        TreeSet<Long> vus = new TreeSet<Long>();
        for(int i=0; i < set_0.size(); i++){
            assertTrue(it.hasNext());
            Long trash = it.next();
            vus.add(trash);
        }

        assertEquals(vus, set_0);

    }

    public static void testNext_1(){
        IteratorSeq it = new IteratorSeq((TreeSet<Long>) set_1.clone());

        TreeSet<Long> vus = new TreeSet<Long>();
        for(int i=0; i < set_1.size(); i++){
            assertTrue(it.hasNext());
            Long trash = it.next();
            vus.add(trash);
        }

        assertEquals(vus, set_1);

    }
}