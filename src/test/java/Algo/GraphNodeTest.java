package Algo;

import Modele.Coordonnes;
import Modele.Noeud;
import junit.framework.TestCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.DoubleBuffer;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class GraphNodeTest extends TestCase {
    static GraphNode g_0;
    static GraphNode g_1;
    static GraphNode g_2;
    static GraphNode g_3;

    static GraphNode g_0_d;
    static GraphNode g_1_d;
    static GraphNode g_2_d;
    static GraphNode g_3_d;

    @BeforeEach
    public void setUp() throws Exception{
        super.setUp();
        g_0 = new GraphNode(new Noeud(0, new Coordonnes(1,1)));
        g_1 = new GraphNode(new Noeud(1, new Coordonnes(10,3)));
        g_2 = new GraphNode(new Noeud(2, new Coordonnes(1,-1)));
        g_3 = new GraphNode(new Noeud(3, new Coordonnes(1,1)));

        g_0_d = new GraphNode(g_0.getNoeud(), 10D);
        g_1_d = new GraphNode(g_1.getNoeud(), 3D);
        g_2_d = new GraphNode(g_2.getNoeud(), 3.1D);
        g_3_d = new GraphNode(g_3.getNoeud(), 0);
    }

    @Test
    public void testGetNoeud() throws Exception {
        assertEquals(g_0.getNoeud().toString(), g_0_d.getNoeud().toString());
        assertEquals(g_1.getNoeud().toString(), g_1_d.getNoeud().toString());
        assertEquals(g_2.getNoeud().toString(), g_2_d.getNoeud().toString());
        assertEquals(g_3.getNoeud().toString(), g_3_d.getNoeud().toString());
        assertNotEquals(g_2.getNoeud().toString(), g_1_d.getNoeud().toString());
    }

    @Test
    public void testGetDistance() throws Exception {
        assertEquals(Double.MAX_VALUE, g_0.getDistance());
        assertEquals(Double.MAX_VALUE, g_2.getDistance());
        assertEquals(10D, g_0_d.getDistance());
        assertEquals(3D, g_1_d.getDistance());
        assertEquals(3.1D, g_2_d.getDistance());
        assertEquals(0D, g_3_d.getDistance());
    }

    @Test
    public void testSetDistance() throws Exception {
        assertEquals(Double.MAX_VALUE, g_0.getDistance());
        g_0.setDistance(12D);
        assertEquals(12D, g_0.getDistance());
        g_0.setDistance(0.111D);
        assertEquals(0.111D, g_0.getDistance());
        g_0.setDistance(-0.001D);
        assertEquals(Double.MAX_VALUE, g_0.getDistance());
        g_0.setDistance(0);
        assertEquals(0D, g_0.getDistance());
    }

    @Test
    public void testCompareTo() throws Exception {


        assertEquals(0, g_0.compareTo(g_0));
        assertEquals(0, g_0.compareTo(g_1));

        assertTrue(g_0.compareTo(g_0_d) > 0);
        assertTrue(g_0_d.compareTo(g_1_d) > 0);
        assertTrue(g_0_d.compareTo(g_2_d) > 0);
        assertTrue(g_0_d.compareTo(g_3_d) > 0);
        assertTrue(g_1_d.compareTo(g_2_d) < 0);
        assertTrue(g_1_d.compareTo(g_3_d) > 0);
        assertTrue(g_2_d.compareTo(g_3_d) > 0);


        assertTrue(g_2_d.compareTo(g_1_d) > 0);
        assertTrue(g_3_d.compareTo(g_1_d) < 0);
        assertTrue(g_3_d.compareTo(g_2_d) < 0);
    }

}