package Algo;

import ApplicationException.ApplicationError;
import Modele.*;
import junit.framework.TestCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.TreeMap;

public class DijkstraTest extends TestCase {

    static Dijkstra dijkstra_0;
    static Dijkstra dijkstra_1;

    @BeforeEach
    public void setUp() throws Exception {
        super.setUp();
        init_0();
        init_1();
    }

    public static  void init_0() throws Exception{
        /* Création d'une carte de ville */
            /* les intersections */
        ArrayList<Noeud> listNoeud_0 = new ArrayList<Noeud>();
        listNoeud_0.add(new Noeud(0, new Coordonnes(0, 0)));
        listNoeud_0.add(new Noeud(1, new Coordonnes(100, -10)));
        listNoeud_0.add(new Noeud(2, new Coordonnes(250, 120)));
        listNoeud_0.add(new Noeud(3, new Coordonnes(70, 140)));
        listNoeud_0.add(new Noeud(4, new Coordonnes(210, 500)));

            /* les troncons */
        ArrayList<Troncon> listTroncon_0 = new ArrayList<Troncon>();
        listTroncon_0.add(new Troncon(6L, 110, "Rue0bis", 110, listNoeud_0.get(0), listNoeud_0.get(1)));
        listTroncon_0.add(new Troncon(0L, 100, "Rue0", 100, listNoeud_0.get(0), listNoeud_0.get(1)));
        listTroncon_0.add(new Troncon(1L, 75, "Rue1", 75, listNoeud_0.get(0), listNoeud_0.get(3)));
        listTroncon_0.add(new Troncon(2L, 80, "Rue2", 80, listNoeud_0.get(1), listNoeud_0.get(2)));
        listTroncon_0.add(new Troncon(3L, 50, "Rue3", 30, listNoeud_0.get(3), listNoeud_0.get(2)));
        listTroncon_0.add(new Troncon(4L, 20, "Rue4", 20, listNoeud_0.get(2), listNoeud_0.get(4)));
        listTroncon_0.add(new Troncon(5L, 60, "Rue5", 60, listNoeud_0.get(3), listNoeud_0.get(4)));

        CarteVille cv_0 = new CarteVille(listTroncon_0, listNoeud_0);

        /* Création planLivraison */
            /* les livraisons */
        ArrayList<DemandeLivraison> livraisons_0 = new ArrayList<DemandeLivraison>();
        livraisons_0.add(new DemandeLivraison(0L, listNoeud_0.get(3), 100, new PlageHoraire(new Heure(32400), new Heure(36000)))); //3L
        livraisons_0.add(new DemandeLivraison(1L, listNoeud_0.get(4), 300, new PlageHoraire(new Heure(39600), new Heure(50000)))); //4L
        livraisons_0.add(new DemandeLivraison(2L, listNoeud_0.get(1), 50, new PlageHoraire(new Heure(45000), new Heure(55000))));  //1L
        livraisons_0.add(new DemandeLivraison(4L, listNoeud_0.get(1), 50, new PlageHoraire(new Heure(40000), new Heure(55000))));  //1L

            /* entrepot */
        Entrepot entrepot_0 = new Entrepot(3L, listNoeud_0.get(0), 0, new PlageHoraire(new Heure(0), new Heure(24 * 3600 - 1)));   //0L
        PlanLivraison pl_0 = new PlanLivraison(livraisons_0, entrepot_0);

        dijkstra_0 = new Dijkstra(cv_0, pl_0);
        return;
    }

    public static  void init_1() throws Exception{
        /* Création d'une carte de ville */
            /* les intersections */
        ArrayList<Noeud> listNoeud_1 = new ArrayList<Noeud>();
        listNoeud_1.add(new Noeud(0, new Coordonnes(0, 0)));
        listNoeud_1.add(new Noeud(1, new Coordonnes(100, 0)));
        listNoeud_1.add(new Noeud(2, new Coordonnes(100, -100)));
        listNoeud_1.add(new Noeud(3, new Coordonnes(0, -100)));
        listNoeud_1.add(new Noeud(4, new Coordonnes(-100, 0)));
        listNoeud_1.add(new Noeud(5, new Coordonnes(-100, -100)));

            /* les troncons */
        ArrayList<Troncon> listTroncon_1 = new ArrayList<Troncon>();
        listTroncon_1.add(new Troncon(11L, 5.1, "Rue34bis", 5, listNoeud_1.get(3), listNoeud_1.get(4)));
        listTroncon_1.add(new Troncon(0L, 4, "Rue01", 4, listNoeud_1.get(0), listNoeud_1.get(1)));
        listTroncon_1.add(new Troncon(1L, 2, "Rue10", 2, listNoeud_1.get(1), listNoeud_1.get(0)));
        listTroncon_1.add(new Troncon(2L, 6, "Rue12", 6, listNoeud_1.get(1), listNoeud_1.get(2)));
        listTroncon_1.add(new Troncon(3L, 6, "Rue21", 6, listNoeud_1.get(2), listNoeud_1.get(1)));
        listTroncon_1.add(new Troncon(4L, 12, "Rue02", 12, listNoeud_1.get(0), listNoeud_1.get(2)));
        listTroncon_1.add(new Troncon(5L, 8, "Rue13", 8, listNoeud_1.get(1), listNoeud_1.get(3)));
        listTroncon_1.add(new Troncon(6L, 14, "Rue23", 14, listNoeud_1.get(2), listNoeud_1.get(3)));
        listTroncon_1.add(new Troncon(7L, 4, "Rue34", 4, listNoeud_1.get(3), listNoeud_1.get(4)));
        listTroncon_1.add(new Troncon(8L, 10, "Rue43", 10, listNoeud_1.get(4), listNoeud_1.get(3)));
        listTroncon_1.add(new Troncon(9L, 6, "Rue54", 6, listNoeud_1.get(5), listNoeud_1.get(4)));
        listTroncon_1.add(new Troncon(10L, 20, "Rue53", 20, listNoeud_1.get(5), listNoeud_1.get(3)));
        listTroncon_1.add(new Troncon(11L, 4, "Rue04", 4, listNoeud_1.get(0), listNoeud_1.get(4)));

        CarteVille cv_1 = new CarteVille(listTroncon_1, listNoeud_1);

        /* Création planLivraison */
            /* les livraisons */
        ArrayList<DemandeLivraison> livraisons_1 = new ArrayList<DemandeLivraison>();
        livraisons_1.add(new DemandeLivraison(0L, listNoeud_1.get(3), 100, new PlageHoraire(new Heure(32400), new Heure(36000)))); //3L
        livraisons_1.add(new DemandeLivraison(1L, listNoeud_1.get(4), 300, new PlageHoraire(new Heure(39600), new Heure(50000)))); //4L
        livraisons_1.add(new DemandeLivraison(2L, listNoeud_1.get(1), 50, new PlageHoraire(new Heure(45000), new Heure(55000))));  //1L
        livraisons_1.add(new DemandeLivraison(3L, listNoeud_1.get(5), 50, new PlageHoraire(new Heure(45000), new Heure(55000))));  //5L

            /* entrepot */
        Entrepot entrepot_1 = new Entrepot(4L, listNoeud_1.get(0), 0, new PlageHoraire(new Heure(0), new Heure(24 * 3600 - 1)));   //0L
        PlanLivraison pl_1 = new PlanLivraison(livraisons_1, entrepot_1);

        dijkstra_1 = new Dijkstra(cv_1, pl_1);
        return;
    }
    @Test
    public void testSimpleDijkstra() throws Exception {
        testSimpleDijkstra_0();
        testSimpleDijkstra_1();
    }

    public static  void testSimpleDijkstra_0() throws Exception{
        testSimpleDijkstra_0_0();
        testSimpleDijkstra_0_1();
    }
    public static  void testSimpleDijkstra_0_0()throws Exception{
        TreeMap<Long, Itineraire> result0 = dijkstra_0.SimpleDijkstra(dijkstra_0.lieuLivraisons.get(3L));

        assertEquals(100, result0.get(2L).getDureeItineraire());
        assertEquals(75, result0.get(0L).getDureeItineraire());
        assertEquals(125, result0.get(1L).getDureeItineraire());
    }
    public static  void testSimpleDijkstra_0_1()throws Exception{
        TreeMap<Long, Itineraire> result0 = dijkstra_0.SimpleDijkstra(dijkstra_0.lieuLivraisons.get(2L));

        assertEquals(Integer.MAX_VALUE, result0.get(3L).getDureeItineraire());
        assertEquals(Integer.MAX_VALUE, result0.get(0L).getDureeItineraire());
        assertEquals(100, result0.get(1L).getDureeItineraire());
        assertEquals(0, result0.get(4L).getDureeItineraire());
    }



    public static  void testSimpleDijkstra_1() throws Exception{
        testSimpleDijkstra_1_0();
        testSimpleDijkstra_1_1();
        testSimpleDijkstra_1_2();
        testSimpleDijkstra_1_3();
        testSimpleDijkstra_1_4();
    }
    public static  void testSimpleDijkstra_1_0()throws Exception{
        TreeMap<Long, Itineraire> result1 = dijkstra_1.SimpleDijkstra(dijkstra_1.lieuLivraisons.get(4L));

        assertEquals(4, result1.get(2L).getDureeItineraire());
        assertEquals(12, result1.get(0L).getDureeItineraire());
        assertEquals(4, result1.get(1L).getDureeItineraire());
        assertEquals(Integer.MAX_VALUE, result1.get(3L).getDureeItineraire());
    }
    public static  void testSimpleDijkstra_1_1()throws Exception{
        TreeMap<Long, Itineraire> result1 = dijkstra_1.SimpleDijkstra(dijkstra_1.lieuLivraisons.get(2L));

        assertEquals(2, result1.get(4L).getDureeItineraire());
        assertEquals(8, result1.get(0L).getDureeItineraire());
        assertEquals(6, result1.get(1L).getDureeItineraire());
        assertEquals(Integer.MAX_VALUE, result1.get(3L).getDureeItineraire());
    }
    public static  void testSimpleDijkstra_1_2()throws Exception{
        TreeMap<Long, Itineraire> result1 = dijkstra_1.SimpleDijkstra(dijkstra_1.lieuLivraisons.get(0L));

        assertEquals(Integer.MAX_VALUE, result1.get(4L).getDureeItineraire());
        assertEquals(Integer.MAX_VALUE, result1.get(2L).getDureeItineraire());
        assertEquals(4, result1.get(1L).getDureeItineraire());
        assertEquals(Integer.MAX_VALUE, result1.get(3L).getDureeItineraire());
    }
    public static  void testSimpleDijkstra_1_3()throws Exception{
        TreeMap<Long, Itineraire> result1 = dijkstra_1.SimpleDijkstra(dijkstra_1.lieuLivraisons.get(1L));

        assertEquals(Integer.MAX_VALUE, result1.get(4L).getDureeItineraire());
        assertEquals(Integer.MAX_VALUE, result1.get(2L).getDureeItineraire());
        assertEquals(10, result1.get(0L).getDureeItineraire());
        assertEquals(Integer.MAX_VALUE, result1.get(3L).getDureeItineraire());
    }
    public static  void testSimpleDijkstra_1_4()throws Exception{
        TreeMap<Long, Itineraire> result1 = dijkstra_1.SimpleDijkstra(dijkstra_1.lieuLivraisons.get(3L));

        assertEquals(Integer.MAX_VALUE, result1.get(4L).getDureeItineraire());
        assertEquals(Integer.MAX_VALUE, result1.get(2L).getDureeItineraire());
        assertEquals(16, result1.get(0L).getDureeItineraire());
        assertEquals(6, result1.get(1L).getDureeItineraire());
    }


    @Test
    public void testCalculItineraire() throws ApplicationError {
        testCalculItineraire_0();
        testCalculItineraire_1();

    }

    public static void testCalculItineraire_0() throws ApplicationError {
        CarteVilleSimplifiee cvs_0 = dijkstra_0.CalculItineraires();
        assertEquals(dijkstra_0.lieuLivraisons.size(), cvs_0.getMatriceAdjacenceDindiceLivraison().size());
        for(DemandeLivraison l : dijkstra_0.lieuLivraisons.values()){
            assertEquals(dijkstra_0.lieuLivraisons.size()-1, cvs_0.getMatriceAdjacenceDindiceLivraison().get(l.getId()).size());
            testCalculItineraire_0_x(cvs_0, l);
        }

    }
    public static void testCalculItineraire_0_x(CarteVilleSimplifiee cvs, DemandeLivraison l) throws ApplicationError {
        TreeMap<Long, Itineraire> result0 = dijkstra_0.SimpleDijkstra(dijkstra_0.lieuLivraisons.get(l.getId()));

        for(DemandeLivraison i : dijkstra_0.lieuLivraisons.values()){
            try{
                assertEquals( result0.get(i.getId()).getDureeItineraire(), cvs.getMatriceAdjacenceDindiceLivraison().get(l.getId()).get(i.getId()).getDureeItineraire());
            }
            catch(NullPointerException e){
                assertEquals(i.getId(), l.getId());
            }
        }
    }

    public static void testCalculItineraire_1() throws ApplicationError {
        CarteVilleSimplifiee cvs_1 = dijkstra_1.CalculItineraires();
        assertEquals(dijkstra_1.lieuLivraisons.size(), cvs_1.getMatriceAdjacenceDindiceLivraison().size());
        for(DemandeLivraison l : dijkstra_1.lieuLivraisons.values()){
            assertEquals(dijkstra_1.lieuLivraisons.size()-1, cvs_1.getMatriceAdjacenceDindiceLivraison().get(l.getId()).size());
            testCalculItineraire_1_x(cvs_1, l);
        }

    }
    public static void testCalculItineraire_1_x(CarteVilleSimplifiee cvs, DemandeLivraison l) throws ApplicationError {
        TreeMap<Long, Itineraire> result1 = dijkstra_1.SimpleDijkstra(dijkstra_1.lieuLivraisons.get(l.getId()));

        for(DemandeLivraison i : dijkstra_1.lieuLivraisons.values()){
            try{
                assertEquals( result1.get(i.getId()).getDureeItineraire(), cvs.getMatriceAdjacenceDindiceLivraison().get(l.getId()).get(i.getId()).getDureeItineraire());
            }
            catch(NullPointerException e){
                assertEquals(i.getId(), l.getId());
            }
        }
    }

}