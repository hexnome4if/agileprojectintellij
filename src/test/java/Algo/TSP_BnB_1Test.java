package Algo;

import GestionIO.ParseurDemandeXML;
import GestionIO.ParseurPlanXML;
import Modele.*;
import junit.framework.TestCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class TSP_BnB_1Test extends TestCase {

    static TSP_BnB_1 tsp_0;
    static CarteVilleSimplifiee cvs_0;

    static TSP_BnB_1 tsp_1;
    static CarteVilleSimplifiee cvs_1;

    static TSP_BnB_1 tsp_1_timing;
    static CarteVilleSimplifiee cvs_1_timing;

    static TSP_BnB_1 tsp_small;
    static CarteVilleSimplifiee cvs_small;

    @BeforeEach
    protected void setUp(){
        init_0();
        init_1();
        init_1_timing();
        init_small();
    }

    public static void init_0(){
        Dijkstra dijkstra_0;
        /* Création d'une carte de ville */
            /* les intersections */
        ArrayList<Noeud> listNoeud_0 = new ArrayList<Noeud>();
        listNoeud_0.add(new Noeud(0, new Coordonnes(0, 0)));
        listNoeud_0.add(new Noeud(1, new Coordonnes(100, 0)));
        listNoeud_0.add(new Noeud(2, new Coordonnes(100, -100)));
        listNoeud_0.add(new Noeud(3, new Coordonnes(0, -100)));
        listNoeud_0.add(new Noeud(4, new Coordonnes(-100, 0)));
        listNoeud_0.add(new Noeud(5, new Coordonnes(-100, -100)));
        listNoeud_0.add(new Noeud(6, new Coordonnes(-50, 50)));

            /* les troncons */
        ArrayList<Troncon> listTroncon_0 = new ArrayList<Troncon>();
        listTroncon_0.add(new Troncon(11L, 5.1, "Rue34bis", 5, listNoeud_0.get(3), listNoeud_0.get(4)));
        listTroncon_0.add(new Troncon(0L, 4, "Rue01", 4, listNoeud_0.get(0), listNoeud_0.get(1)));
        listTroncon_0.add(new Troncon(1L, 2, "Rue10", 2, listNoeud_0.get(1), listNoeud_0.get(0)));
        listTroncon_0.add(new Troncon(2L, 6, "Rue12", 6, listNoeud_0.get(1), listNoeud_0.get(2)));
        listTroncon_0.add(new Troncon(3L, 6, "Rue21", 6, listNoeud_0.get(2), listNoeud_0.get(1)));
        listTroncon_0.add(new Troncon(4L, 12, "Rue02", 12, listNoeud_0.get(0), listNoeud_0.get(2)));
        listTroncon_0.add(new Troncon(5L, 8, "Rue13", 8, listNoeud_0.get(1), listNoeud_0.get(3)));
        listTroncon_0.add(new Troncon(6L, 14, "Rue23", 14, listNoeud_0.get(2), listNoeud_0.get(3)));
        listTroncon_0.add(new Troncon(7L, 4, "Rue34", 4, listNoeud_0.get(3), listNoeud_0.get(4)));
        listTroncon_0.add(new Troncon(8L, 10, "Rue43", 10, listNoeud_0.get(4), listNoeud_0.get(3)));
        listTroncon_0.add(new Troncon(9L, 6, "Rue54", 6, listNoeud_0.get(5), listNoeud_0.get(4)));
        listTroncon_0.add(new Troncon(10L, 20, "Rue53", 20, listNoeud_0.get(5), listNoeud_0.get(3)));
        listTroncon_0.add(new Troncon(11L, 4, "Rue04", 4, listNoeud_0.get(0), listNoeud_0.get(4)));
        listTroncon_0.add(new Troncon(12L, 7, "Rue46", 7, listNoeud_0.get(4), listNoeud_0.get(6)));
        listTroncon_0.add(new Troncon(13L, 2, "Rue60", 2, listNoeud_0.get(6), listNoeud_0.get(0)));

        CarteVille cv_0 = new CarteVille(listTroncon_0, listNoeud_0);

        /* Création planLivraison */
            /* les livraisons */

        try {
            ArrayList<DemandeLivraison> livraisons_0 = new ArrayList<DemandeLivraison>();
            livraisons_0.add(new DemandeLivraison(0L, listNoeud_0.get(3), 100, new PlageHoraire(new Heure(8, 0, 0), new Heure(24 * 3600 - 1))));
            livraisons_0.add(new DemandeLivraison(1L, listNoeud_0.get(4), 300, new PlageHoraire(new Heure(8, 0, 0), new Heure(24 * 3600 - 1))));
            livraisons_0.add(new DemandeLivraison(2L, listNoeud_0.get(1), 50, new PlageHoraire(new Heure(8, 0, 0), new Heure(24 * 3600 - 1))));

        /* entrepot */
            Entrepot entrepot_0 = new Entrepot(3L, listNoeud_0.get(0), 0, new PlageHoraire(new Heure(8, 0, 0), new Heure(24 * 3600 - 1)));
            PlanLivraison pl_0 = new PlanLivraison(livraisons_0, entrepot_0);
            dijkstra_0 = new Dijkstra(cv_0, pl_0);

            cvs_0 = dijkstra_0.CalculItineraires();
            tsp_0 = new TSP_BnB_1(cvs_0);
        }
        catch (Exception e){
            assertTrue(false);
        }



        return;

    }

    public static void init_1(){
        Dijkstra dijkstra_1;
        /* Création d'une carte de ville */
            /* les intersections */
        ArrayList<Noeud> listNoeud_1 = new ArrayList<Noeud>();
        listNoeud_1.add(new Noeud(0, new Coordonnes(0, 0)));
        listNoeud_1.add(new Noeud(1, new Coordonnes(10, 5)));
        listNoeud_1.add(new Noeud(2, new Coordonnes(20, 0)));
        listNoeud_1.add(new Noeud(3, new Coordonnes(30, 5)));
        listNoeud_1.add(new Noeud(4, new Coordonnes(50, 0)));
        listNoeud_1.add(new Noeud(5, new Coordonnes(45, -8)));
        listNoeud_1.add(new Noeud(6, new Coordonnes(30, -10)));
        listNoeud_1.add(new Noeud(7, new Coordonnes(8, -5)));

            /* les troncons */
        ArrayList<Troncon> listTroncon_1 = new ArrayList<Troncon>();
        listTroncon_1.add(new Troncon(0L, 8, "Rue01", 8, listNoeud_1.get(0), listNoeud_1.get(1)));
        listTroncon_1.add(new Troncon(1L, 9, "Rue10", 9, listNoeud_1.get(1), listNoeud_1.get(0)));
        listTroncon_1.add(new Troncon(2L, 2, "Rue12", 2, listNoeud_1.get(1), listNoeud_1.get(2)));
        listTroncon_1.add(new Troncon(3L, 4, "Rue21", 4, listNoeud_1.get(2), listNoeud_1.get(1)));
        listTroncon_1.add(new Troncon(4L, 5, "Rue31", 5, listNoeud_1.get(3), listNoeud_1.get(1)));
        listTroncon_1.add(new Troncon(5L, 11, "Rue13", 11, listNoeud_1.get(1), listNoeud_1.get(3)));
        listTroncon_1.add(new Troncon(6L, 3, "Rue32", 3, listNoeud_1.get(3), listNoeud_1.get(2)));
        listTroncon_1.add(new Troncon(7L, 13, "Rue24", 13, listNoeud_1.get(2), listNoeud_1.get(4)));
        listTroncon_1.add(new Troncon(8L, 9, "Rue43", 9, listNoeud_1.get(4), listNoeud_1.get(3)));
        listTroncon_1.add(new Troncon(9L, 13, "Rue42", 13, listNoeud_1.get(4), listNoeud_1.get(2)));
        listTroncon_1.add(new Troncon(10L, 3, "Rue45", 3, listNoeud_1.get(4), listNoeud_1.get(5)));
        listTroncon_1.add(new Troncon(11L, 2, "Rue56", 2, listNoeud_1.get(5), listNoeud_1.get(6)));
        listTroncon_1.add(new Troncon(12L, 6, "Rue46", 6, listNoeud_1.get(4), listNoeud_1.get(6)));
        listTroncon_1.add(new Troncon(13L, 5, "Rue64", 5, listNoeud_1.get(6), listNoeud_1.get(4)));
        listTroncon_1.add(new Troncon(14L, 6, "Rue67", 6, listNoeud_1.get(6), listNoeud_1.get(7)));
        listTroncon_1.add(new Troncon(15L, 3, "Rue76", 3, listNoeud_1.get(7), listNoeud_1.get(6)));
        listTroncon_1.add(new Troncon(16L, 5, "Rue27", 5, listNoeud_1.get(2), listNoeud_1.get(7)));
        listTroncon_1.add(new Troncon(17L, 3, "Rue72", 3, listNoeud_1.get(7), listNoeud_1.get(2)));
        listTroncon_1.add(new Troncon(18L, 4, "Rue07", 4, listNoeud_1.get(0), listNoeud_1.get(7)));

        CarteVille cv_1 = new CarteVille(listTroncon_1, listNoeud_1);

        /* Création planLivraison */
            /* les livraisons */

        try {
            ArrayList<DemandeLivraison> livraisons_1 = new ArrayList<DemandeLivraison>();
            livraisons_1.add(new DemandeLivraison(3L, listNoeud_1.get(1), 100, new PlageHoraire(new Heure(8, 0, 0), new Heure(24 * 3600 - 1))));
            livraisons_1.add(new DemandeLivraison(1L, listNoeud_1.get(4), 300, new PlageHoraire(new Heure(8, 0, 0), new Heure(24 * 3600 - 1))));
            livraisons_1.add(new DemandeLivraison(2L, listNoeud_1.get(7), 50, new PlageHoraire(new Heure(8, 0, 0), new Heure(24 * 3600 - 1))));

        /* entrepot */
            Entrepot entrepot_1 = new Entrepot(0L, listNoeud_1.get(0), 0, new PlageHoraire(new Heure(8, 0, 0), new Heure(24 * 3600 - 1)));
            PlanLivraison pl_1 = new PlanLivraison(livraisons_1, entrepot_1);
            dijkstra_1 = new Dijkstra(cv_1, pl_1);


            cvs_1 = dijkstra_1.CalculItineraires();
            tsp_1 = new TSP_BnB_1(cvs_1);

        }
        catch (Exception e){
            assertTrue(false);
        }



        return;
    }

    public static void init_small(){
        String cheminFichierCarte = "./src/main/resources/InputData/planLyonGrand.xml";
        String cheminFichierPlanLivraison = "./src/main/resources/InputData/DLgrand10.xml";

        ParseurPlanXML p_plan = new ParseurPlanXML();
        ParseurDemandeXML p_demande = new ParseurDemandeXML();
        p_plan.setFichierPlan(cheminFichierCarte);
        p_demande.setFichierDemande(cheminFichierPlanLivraison);
        try{
            CarteVille cv = p_plan.recupererPlanVille();
            PlanLivraison pl = p_demande.recupererPlanLivraison(cv);


            Dijkstra dijkstra_small = new Dijkstra(cv, pl);
            cvs_small = dijkstra_small.CalculItineraires();

            tsp_small = new TSP_BnB_1(cvs_small);

        }
        catch(Exception e){

        }
    }

    /**
     * Même graphe qu'init_1 mais les plages horaires des lieux de livraisons sont différentes (et modifient grandement la Tournee calculée)
     */
    public static void init_1_timing(){
        Dijkstra dijkstra_1;
        /* Création d'une carte de ville */
            /* les intersections */
        ArrayList<Noeud> listNoeud_1 = new ArrayList<Noeud>();
        listNoeud_1.add(new Noeud(0, new Coordonnes(0, 0)));
        listNoeud_1.add(new Noeud(1, new Coordonnes(10, 5)));
        listNoeud_1.add(new Noeud(2, new Coordonnes(20, 0)));
        listNoeud_1.add(new Noeud(3, new Coordonnes(30, 5)));
        listNoeud_1.add(new Noeud(4, new Coordonnes(50, 0)));
        listNoeud_1.add(new Noeud(5, new Coordonnes(45, -8)));
        listNoeud_1.add(new Noeud(6, new Coordonnes(30, -10)));
        listNoeud_1.add(new Noeud(7, new Coordonnes(8, -5)));

            /* les troncons */
        ArrayList<Troncon> listTroncon_1 = new ArrayList<Troncon>();
        listTroncon_1.add(new Troncon(0L, 8, "Rue01", 8, listNoeud_1.get(0), listNoeud_1.get(1)));
        listTroncon_1.add(new Troncon(1L, 9, "Rue10", 9, listNoeud_1.get(1), listNoeud_1.get(0)));
        listTroncon_1.add(new Troncon(2L, 2, "Rue12", 2, listNoeud_1.get(1), listNoeud_1.get(2)));
        listTroncon_1.add(new Troncon(3L, 4, "Rue21", 4, listNoeud_1.get(2), listNoeud_1.get(1)));
        listTroncon_1.add(new Troncon(4L, 5, "Rue31", 5, listNoeud_1.get(3), listNoeud_1.get(1)));
        listTroncon_1.add(new Troncon(5L, 11, "Rue13", 11, listNoeud_1.get(1), listNoeud_1.get(3)));
        listTroncon_1.add(new Troncon(6L, 3, "Rue32", 3, listNoeud_1.get(3), listNoeud_1.get(2)));
        listTroncon_1.add(new Troncon(7L, 13, "Rue24", 13, listNoeud_1.get(2), listNoeud_1.get(4)));
        listTroncon_1.add(new Troncon(8L, 9, "Rue43", 9, listNoeud_1.get(4), listNoeud_1.get(3)));
        listTroncon_1.add(new Troncon(9L, 13, "Rue42", 13, listNoeud_1.get(4), listNoeud_1.get(2)));
        listTroncon_1.add(new Troncon(10L, 3, "Rue45", 3, listNoeud_1.get(4), listNoeud_1.get(5)));
        listTroncon_1.add(new Troncon(11L, 2, "Rue56", 2, listNoeud_1.get(5), listNoeud_1.get(6)));
        listTroncon_1.add(new Troncon(12L, 6, "Rue46", 6, listNoeud_1.get(4), listNoeud_1.get(6)));
        listTroncon_1.add(new Troncon(13L, 5, "Rue64", 5, listNoeud_1.get(6), listNoeud_1.get(4)));
        listTroncon_1.add(new Troncon(14L, 6, "Rue67", 6, listNoeud_1.get(6), listNoeud_1.get(7)));
        listTroncon_1.add(new Troncon(15L, 3, "Rue76", 3, listNoeud_1.get(7), listNoeud_1.get(6)));
        listTroncon_1.add(new Troncon(16L, 5, "Rue27", 5, listNoeud_1.get(2), listNoeud_1.get(7)));
        listTroncon_1.add(new Troncon(17L, 3, "Rue72", 3, listNoeud_1.get(7), listNoeud_1.get(2)));
        listTroncon_1.add(new Troncon(18L, 4, "Rue07", 4, listNoeud_1.get(0), listNoeud_1.get(7)));

        CarteVille cv_1 = new CarteVille(listTroncon_1, listNoeud_1);

        /* Création planLivraison */
            /* les livraisons */

        try {
            ArrayList<DemandeLivraison> livraisons_1 = new ArrayList<DemandeLivraison>();
            livraisons_1.add(new DemandeLivraison(0L, listNoeud_1.get(1), 100, new PlageHoraire(new Heure(8, 0, 0), new Heure(24 * 3600 - 1))));
            livraisons_1.add(new DemandeLivraison(1L, listNoeud_1.get(4), 300, new PlageHoraire(new Heure(8, 1, 22), new Heure(24 * 3600 - 1))));
            livraisons_1.add(new DemandeLivraison(2L, listNoeud_1.get(7), 50, new PlageHoraire(new Heure(8, 0, 14), new Heure(24 * 3600 - 1))));

        /* entrepot */
            Entrepot entrepot_1 = new Entrepot(3L, listNoeud_1.get(0), 0, new PlageHoraire(new Heure(8, 0, 0), new Heure(24 * 3600 - 1)));
            PlanLivraison pl_1 = new PlanLivraison(livraisons_1, entrepot_1);
            dijkstra_1 = new Dijkstra(cv_1, pl_1);

            cvs_1_timing = dijkstra_1.CalculItineraires();
            tsp_1_timing = new TSP_BnB_1(cvs_1_timing);

        }
        catch (Exception e){
            assertTrue(false);
        }



        return;
    }
    @Test
    public void testChercheSolution() throws Exception {
        testChercheSolution_0();
        testChercheSolution_1();
        testChercheSolution_1_timing();
        testChercheSolution_small();
    }

    public static void testChercheSolution_0(){
        tsp_0.chercheSolution();
        try{
            assertEquals(4+8+4+7+2 + 450, tsp_0.getCoutSolution());
        }
        catch(Exception e){
            assertTrue(false);
        }
    }

    public static void testChercheSolution_1(){
        tsp_1.chercheSolution();
        try{
            assertEquals(35 + 450, tsp_1.getCoutSolution());
        }
        catch(Exception e){
            assertTrue(false);
        }
    }

    public static void testChercheSolution_1_timing(){
        tsp_1_timing.chercheSolution();
        try{
            assertEquals(496, tsp_1_timing.getCoutSolution());
        }
        catch(Exception e){
            assertTrue(false);
        }
    }

    public static void testChercheSolution_small() {
        long beg = System.currentTimeMillis();
        tsp_small.chercheSolution();
        long end  = System.currentTimeMillis();

        System.out.println("Temps total : " + ((float)(end-beg))/1000f + "s.");
        try {
            assertEquals(15043, tsp_small.getCoutSolution());
        } catch (Exception e) {
            assertTrue(false);
        }
    }
}