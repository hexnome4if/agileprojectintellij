package Logger;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InternalLoggerTest {
    @Test
    void getInstance() {
        //On test si, quand on récupère deux instances de suite, on récupère bien 2 fois la même
        InternalLogger myLogger1 = InternalLogger.getInstance();
        InternalLogger myLogger2 = InternalLogger.getInstance();
        assertEquals(myLogger1, myLogger2);
    }

    @Test
    void logSevere() {
        InternalLogger.getInstance().logSevere("TEST de log SEVERE", this);
    }

    @Test
    void logWarning() {
        InternalLogger.getInstance().logWarning("TEST de log WARNING", this);
    }

    @Test
    void logConfig() {
        InternalLogger.getInstance().logConfig("TEST de log CONFIG", this);
    }

}