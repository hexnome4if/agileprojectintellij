package Modele;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Heure_test {

    private Heure heureSecondes;
    private Heure heureTime;

    @BeforeEach
    void setUp() throws Exception {
        this.heureSecondes = new Heure(60002);
        this.heureTime = new Heure(16, 40, 5);
    }

    @AfterEach
    void tearDown() {
    }

    /*@Test
    void constructorInvalidTimeNegativeValues() {
        Heure uneHeure;
        Exception e = assertThrows(
                Exception.class,() -> uneHeure = new Heure(-312125));
    }*/

    @Test
    void setterInvalidTimeNegativeValues() {
        Exception e = assertThrows(
                Exception.class, () -> heureTime.setTimeFromSecondes(-1));
    }

    @Test
    void setTimeFromSecondes() throws Exception {
        int old = heureTime.getTotalSeconds();
        heureTime.setTimeFromSecondes(59240);
        assertEquals("Heure{16:27:20}", heureTime.toString());
        heureTime.setTimeFromSecondes(old);
    }

    @Test
    void heureTakesTotalSeconds() throws Exception {
        Heure heure = new Heure(3691);
        assertEquals("Heure{" +
                1 +
                ":" + 1 +
                ":" + 31 +
                '}', heure.toString());
    }

    @Test
    void getHeures() {
        assertEquals(16, heureSecondes.getHeures());
        assertEquals(16, heureTime.getHeures());
    }

    @Test
    void getMinutes() {
        assertEquals(40, heureSecondes.getMinutes());
        assertEquals(40, heureTime.getMinutes());
    }

    @Test
    void getSecondes() {
        assertEquals(2, heureSecondes.getSecondes());
        assertEquals(5, heureTime.getSecondes());
    }

    @Test
    void setHeuresOK() throws Exception {
        int old = heureTime.getHeures();
        heureTime.setHeures(4);
        assertEquals(4, heureTime.getHeures());
        heureTime.setHeures(old);
    }

    @Test
    void setHeuresNegative() throws Exception {
        int old = heureTime.getHeures();
        heureTime.setHeures(-5);
        assertEquals(0, heureTime.getHeures());
        heureTime.setHeures(old);
    }

    @Test
    void setHeuresToHigh() throws Exception {
        int old = heureTime.getHeures();
        heureTime.setHeures(25);
        assertEquals(23, heureTime.getHeures());
        heureTime.setHeures(old);
    }

    @Test
    void setMinutes() {
        int old = heureTime.getMinutes();
        heureTime.setMinutes(55);
        assertEquals(55, heureTime.getMinutes());
        heureTime.setMinutes(old);
    }

    @Test
    void setMinutesNegative() {
        int old = heureTime.getMinutes();
        heureTime.setMinutes(-7);
        assertEquals(0, heureTime.getMinutes());
        heureTime.setMinutes(old);
    }

    @Test
    void setMinutesTooHigh() {
        int old = heureTime.getMinutes();
        heureTime.setMinutes(61);
        assertEquals(59, heureTime.getMinutes());
        heureTime.setMinutes(old);
    }

    @Test
    void setSecondes() {
        int old = heureTime.getSecondes();
        heureTime.setSecondes(12);
        assertEquals(12, heureTime.getSecondes());
        heureTime.setSecondes(old);
    }

    @Test
    void setSecondesNegative() {
        int old = heureTime.getSecondes();
        heureTime.setSecondes(-16);
        assertEquals(0, heureTime.getSecondes());
        heureTime.setSecondes(old);
    }

    @Test
    void setSecondesTooHigh() {
        int old = heureTime.getSecondes();
        heureTime.setSecondes(62);
        assertEquals(59, heureTime.getSecondes());
        heureTime.setSecondes(old);
    }

    @Test
    void getTotalSeconds() {
        assertEquals(60005, heureTime.getTotalSeconds());
    }

    @Test
    void getFormattedTimeWithSeconds() {
        String value = heureSecondes.getFormattedTime(true);
        assertEquals("16h40m02s", value);
    }

    @Test
    void getFormattedTimeWithoutSeconds() {
        String value = heureSecondes.getFormattedTime(false);
        assertEquals("16h40", value);
    }

    @Test
    void formatValue() {
        assertEquals("02", heureSecondes.formatValue(2));
        assertEquals("12", heureSecondes.formatValue(12));
        assertEquals("00", heureSecondes.formatValue(-3));
    }

    @Test
    void isBeforeTrue() {
        assertEquals(true, heureSecondes.isBefore(heureTime));
    }

    @Test
    void isBeforeFalse() {

        assertEquals(false, heureTime.isBefore(heureSecondes));
    }

    @Test
    void normalise24() {
        assertEquals(0, heureSecondes.normalise24(-5));
        assertEquals(23, heureSecondes.normalise24(25));
        assertEquals(13, heureSecondes.normalise24(13));
    }

    @Test
    void normalise60() {
        assertEquals(0, heureSecondes.normalise60(-5));
        assertEquals(59, heureSecondes.normalise60(61));
        assertEquals(31, heureSecondes.normalise60(31));
    }

    @Test
    void testToString() {
        String expected = "Heure{16:40:5}";
        assertEquals(expected, heureTime.toString());
    }

    @Test
    void testClone() throws CloneNotSupportedException {
        Heure heureClone = heureSecondes.clone();

        assertEquals(heureClone.getTotalSeconds(), heureSecondes.getTotalSeconds());
        assertNotEquals(heureClone, heureSecondes);
    }

    @Test
    void equals() {

        Heure h = new Heure(13, 37, 0);

        assertTrue(h.equals(new Heure(13, 37, 0)));
        assertFalse(h.equals(new Heure(12, 15, 23)));
    }
}