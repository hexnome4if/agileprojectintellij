package Modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class Entrepot_test {
    private Entrepot entrepot;
    private Noeud adresse;
    private int dureeLivraisonSec;
    private PlageHoraire plageHoraire;

    @BeforeEach
    void setUp() {
        adresse = new Noeud(1, new Coordonnes(5, 6));
        dureeLivraisonSec = 150;
        Heure heureD = new Heure(28800);
        Heure heureF = new Heure(72000);
        plageHoraire = new PlageHoraire(heureD, heureF);
        entrepot = new Entrepot((long) 10, adresse, dureeLivraisonSec, plageHoraire);
    }


    @Test
    void getId() {

        assertEquals(new Long(10), entrepot.getId());
    }

    @Test
    void getAdresse() {
        assertEquals((new Noeud(1, new Coordonnes(5, 6))).toString(),
                entrepot.getAdresse().toString());
    }

    @Test
    void getDureeLivraisonSec() {
        assertEquals(150, entrepot.getDureeLivraisonSec());
    }

    @Test
    void getPlageLivraison() {
        assertEquals((new PlageHoraire(new Heure(28800), new Heure(72000))).toString()
                , entrepot.getPlageLivraison().toString());
    }

    @Test
    void setId() {
        entrepot.setId(new Long(99));
        assertEquals(new Long(99), entrepot.getId());
    }

    @Test
    void setAdresse() {
        Noeud newNoeud = new Noeud(new Long(55), new Coordonnes(10, 55));
        entrepot.setAdresse(newNoeud);
        assertEquals(newNoeud, entrepot.getAdresse());
    }

    @Test
    void setDureeLivraisonSec() {
        int newDureeLivraisonSec = 450;
        entrepot.setDureeLivraisonSec(newDureeLivraisonSec);
        assertEquals(newDureeLivraisonSec, entrepot.getDureeLivraisonSec());
    }

    @Test
    void setPlageLivraison() {
        Heure heureD = new Heure(99999);
        Heure heureF = new Heure(421556);
        PlageHoraire newPlageHoraire = new PlageHoraire(heureF, heureF);
        entrepot.setPlageLivraison(newPlageHoraire);
        assertEquals(newPlageHoraire, entrepot.getPlageLivraison());
    }

    @Test
    void testClone() throws CloneNotSupportedException {
        Entrepot entrepotClone = entrepot.clone();

        assertEquals(entrepotClone.getId(), entrepot.getId());
        assertEquals(entrepotClone.getAdresse().toString(), entrepot.getAdresse().toString());
        assertEquals(entrepotClone.getPlageLivraison().toString(), entrepot.getPlageLivraison().toString());
        assertEquals(entrepotClone.getDureeLivraisonSec(), entrepot.getDureeLivraisonSec());
        assertNotEquals(entrepotClone, entrepot);
    }

}