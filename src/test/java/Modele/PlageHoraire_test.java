package Modele;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class PlageHoraire_test {
    PlageHoraire unePlageHoraire;
    PlageHoraire unePlageHoraireValues;

    @BeforeEach
    void setUp() throws Exception {
        this.unePlageHoraire = new PlageHoraire();
    }

    @Test
    public void constructorWithOkParams() throws Exception {
        Heure heureD = new Heure(50035);
        Heure heureF = new Heure(50062);
        PlageHoraire PH = new PlageHoraire(heureD, heureF);

        String expected = "PlageHoraire{"
                + "heureDeb=Heure{13:53:55}, "
                + "heureFin=Heure{13:54:22}}";

        assertEquals(expected, PH.toString());
    }

    @Test
    public void constructorWithEqualsParams() throws Exception {
        Heure heureD = new Heure(50035);
        Heure heureF = new Heure(50035);
        PlageHoraire PH = new PlageHoraire(heureD, heureF);

        String expected = "PlageHoraire{"
                + "heureDeb=Heure{13:53:55}, "
                + "heureFin=Heure{13:53:55}}";

        assertEquals(expected, PH.toString());
    }

    @Test
    public void constructorWithIsBefore() throws Exception {
        Heure heureD = new Heure(50073);
        Heure heureF = new Heure(50072);
        PlageHoraire PH = new PlageHoraire(heureD, heureF);

        String expected = "PlageHoraire{"
                + "heureDeb=Heure{13:54:33}, "
                + "heureFin=Heure{13:54:33}}";

        assertEquals(expected, PH.toString());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getHeureDeb() {
        assertEquals("Heure{0:0:0}", this.unePlageHoraire.getHeureDeb().toString());
    }

    @Test
    void getHeureFin() {
        assertEquals("Heure{23:59:59}", this.unePlageHoraire.getHeureFin().toString());
    }

    @Test
    void setHeureDeb() throws Exception {
        Heure uneHeure = new Heure(60000);
        this.unePlageHoraire.setHeureDeb(uneHeure);
        assertEquals("Heure{16:40:0}", this.unePlageHoraire.getHeureDeb().toString());
    }

    @Test
    void setHeureFin() throws Exception {
        Heure uneHeure = new Heure(60000);
        this.unePlageHoraire.setHeureFin(uneHeure);
        assertEquals("Heure{16:40:0}", this.unePlageHoraire.getHeureFin().toString());
    }

    @Test
    void testToString() {
        String expected = "PlageHoraire{"
                + "heureDeb=Heure{0:0:0}, "
                + "heureFin=Heure{23:59:59}}";
        assertEquals(expected, this.unePlageHoraire.toString());
    }

    @Test
    void testClone() throws CloneNotSupportedException {
        PlageHoraire plageHoraireClone = unePlageHoraire.clone();

        assertEquals(plageHoraireClone.getHeureDeb().toString(), unePlageHoraire.getHeureDeb().toString());
        assertEquals(plageHoraireClone.getHeureFin().toString(), unePlageHoraire.getHeureFin().toString());

        assertNotEquals(plageHoraireClone, unePlageHoraire);

        assertNotEquals(plageHoraireClone.getHeureDeb(), unePlageHoraire.getHeureDeb());
        assertNotEquals(plageHoraireClone.getHeureFin(), unePlageHoraire.getHeureFin());

    }
}