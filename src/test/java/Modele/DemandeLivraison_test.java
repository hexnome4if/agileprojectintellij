package Modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class DemandeLivraison_test {
    private DemandeLivraison demandeLivraison;
    private Noeud adresse;
    private int dureeLivraisonSec;
    private PlageHoraire plageHoraire;

    @BeforeEach
    void setUp() {
        adresse = new Noeud(1, new Coordonnes(5, 6));
        dureeLivraisonSec = 150;
        Heure heureD = new Heure(28800);
        Heure heureF = new Heure(72000);
        plageHoraire = new PlageHoraire(heureD, heureF);
        demandeLivraison = new DemandeLivraison((long) 10, adresse, dureeLivraisonSec, plageHoraire);
    }

    @Test
    void testConstructeurSansId() {
        Heure heureD = new Heure(28800);
        Heure heureF = new Heure(72000);
        plageHoraire = new PlageHoraire(heureD, heureF);
        DemandeLivraison demandeLivraisonSansId = new DemandeLivraison(
                new Noeud((long) 1, new Coordonnes(5, 6)),
                150, plageHoraire
        );
        assertEquals(demandeLivraisonSansId.getAdresse().toString(), demandeLivraison.getAdresse().toString());
        assertEquals(demandeLivraisonSansId.getDureeLivraisonSec(), demandeLivraison.getDureeLivraisonSec());
        assertEquals(demandeLivraisonSansId.getPlageLivraison().toString(), demandeLivraison.getPlageLivraison().toString());
    }

    @Test
    void getId() {

        assertEquals(new Long(10), demandeLivraison.getId());
    }

    @Test
    void getAdresse() {
        assertEquals((new Noeud(1, new Coordonnes(5, 6))).toString(),
                demandeLivraison.getAdresse().toString());
    }

    @Test
    void getDureeLivraisonSec() {
        assertEquals(150, demandeLivraison.getDureeLivraisonSec());
    }

    @Test
    void getPlageLivraison() {
        assertEquals((new PlageHoraire(new Heure(28800), new Heure(72000))).toString()
                , demandeLivraison.getPlageLivraison().toString());
    }

    @Test
    void setId() {
        demandeLivraison.setId(new Long(99));
        assertEquals(new Long(99), demandeLivraison.getId());
    }

    @Test
    void setAdresse() {
        Noeud newNoeud = new Noeud(new Long(55), new Coordonnes(10, 55));
        demandeLivraison.setAdresse(newNoeud);
        assertEquals(newNoeud, demandeLivraison.getAdresse());
    }

    @Test
    void setDureeLivraisonSec() {
        int newDureeLivraisonSec = 450;
        demandeLivraison.setDureeLivraisonSec(newDureeLivraisonSec);
        assertEquals(newDureeLivraisonSec, demandeLivraison.getDureeLivraisonSec());
    }

    @Test
    void setPlageLivraison() {
        Heure heureD = new Heure(99999);
        Heure heureF = new Heure(421556);
        PlageHoraire newPlageHoraire = new PlageHoraire(heureF, heureF);
        demandeLivraison.setPlageLivraison(newPlageHoraire);
        assertEquals(newPlageHoraire, demandeLivraison.getPlageLivraison());
    }

    @Test
    void Testclone() throws CloneNotSupportedException {

        DemandeLivraison demandeLivraisonClone = demandeLivraison.clone();

        assertEquals(demandeLivraisonClone.getId(), demandeLivraison.getId());
        assertEquals(demandeLivraisonClone.getAdresse().toString(), demandeLivraison.getAdresse().toString());
        assertEquals(demandeLivraisonClone.getPlageLivraison().toString(), demandeLivraison.getPlageLivraison().toString());
        assertEquals(demandeLivraisonClone.getDureeLivraisonSec(), demandeLivraison.getDureeLivraisonSec());
        assertNotEquals(demandeLivraisonClone, demandeLivraison);
    }

}