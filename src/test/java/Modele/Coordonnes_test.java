package Modele;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class Coordonnes_test {
    static Coordonnes coord;

    @BeforeAll
    public static void setUp() {
        coord = new Coordonnes(5, 2);
    }

    @Test
    void getCoordX() {
        assertEquals(5, coord.getCoordX());
    }

    @Test
    void getCoordY() {
        assertEquals(2, coord.getCoordY());
    }

    @Test
    void setCoordX() {
        coord.setCoordX(3);
        assertEquals(3, coord.getCoordX());
        coord.setCoordX(5);
    }

    @Test
    void setCoordY() {
        coord.setCoordY(4);
        assertEquals(4, coord.getCoordY());
        coord.setCoordY(2);
    }

    @Test
    public void testToString() {
        String expected = "Coordonnes{" +
                "coordX=5" +
                ", coordY=2" +
                '}';
        assertEquals(expected, coord.toString());
    }

    @Test
    public void testClone() throws CloneNotSupportedException {
        Coordonnes coordonnesClone = coord.clone();
        assertEquals(coordonnesClone.getCoordX(), coord.getCoordX());
        assertEquals(coordonnesClone.getCoordY(), coord.getCoordY());

        assertNotEquals(coordonnesClone, coord);
    }

    @Test
    void testIsEquals() {
        Coordonnes coordonnes = new Coordonnes(5, 2);
        assertEquals(true, coord.isEqual(coordonnes));
    }
}