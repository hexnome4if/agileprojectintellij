package Modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class CarteVilleSimplifiee_test {
    CarteVilleSimplifiee carteVilleSimplifiee;


    public TreeMap<Long, TreeMap<Long, Itineraire>> init_0() {
        int[] coordonneesX_Entrepot_A = {0, 1, 4, 9};
        int[] coordonneesY_Entrepot_A = {0, 3, 7, 15};
        int[] coordonneesX_A_B = {9, 10, 7};
        int[] coordonneesY_A_B = {15, 10, 19};

        String[] nomRue_Entrepot_A = {"rue 0", "rue 1", "rue 3"};
        String[] nomRue_A_B = {"rue 4", "rue 5", "rue 6"};

        Coordonnes coordonneesEntrepot = new Coordonnes(0, 0);
        Coordonnes coordonneesA = new Coordonnes(9, 15);
        Coordonnes coordonneesB = new Coordonnes(7, 19);

        Noeud noeud_Entrepot = new Noeud(1, coordonneesEntrepot);
        Noeud noeud_A = new Noeud(2, coordonneesA);
        Noeud noeud_B = new Noeud(545, coordonneesB);

        ArrayList<Troncon> listeTroncon_Entrepot_A = new ArrayList<Troncon>();
        ArrayList<Troncon> listeTroncon_A_B = new ArrayList<Troncon>();

        for (int i = 0; i < coordonneesX_Entrepot_A.length - 1; i++) {
            listeTroncon_Entrepot_A.add(new Troncon((long) 10 + i, 10.0, nomRue_Entrepot_A[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_Entrepot_A[i], coordonneesY_Entrepot_A[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_Entrepot_A[i + 1], coordonneesY_Entrepot_A[i + 1]))
            ));
        }

        for (int i = 0; i < coordonneesX_A_B.length - 1; i++) {
            listeTroncon_A_B.add(new Troncon((long) 10 + i, 10.0, nomRue_A_B[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_A_B[i], coordonneesY_A_B[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_A_B[i + 1], coordonneesY_A_B[i + 1]))
            ));
        }

        Heure debutLivraison_Entrepot = new Heure(8 * 3600);
        Heure debutLivraison_A = new Heure(10 * 3600);
        Heure debutLivraison_B = new Heure(15 * 3600);

        Heure finLivraison_Entrepot = new Heure(20 * 3600);
        Heure finLivraison_A = new Heure(12 * 3600);
        Heure finLivraison_B = new Heure(18 * 3600);

        PlageHoraire plageHoraire_Entrepot = new PlageHoraire(debutLivraison_Entrepot, finLivraison_Entrepot);
        PlageHoraire plageHoraire_A = new PlageHoraire(debutLivraison_A, finLivraison_A);
        PlageHoraire plageHoraire_B = new PlageHoraire(debutLivraison_B, finLivraison_B);

        DemandeLivraison demandeLivraison_Entrepot = new DemandeLivraison(new Long(1), noeud_Entrepot, 0, plageHoraire_Entrepot);
        DemandeLivraison demandeLivraison_A = new DemandeLivraison(new Long(2), noeud_A, 100, plageHoraire_A);
        DemandeLivraison demandeLivraison_B = new DemandeLivraison(new Long(3), noeud_B, 15, plageHoraire_B);

        Itineraire itineraire_Entrepot_A = new Itineraire(listeTroncon_Entrepot_A, demandeLivraison_Entrepot, demandeLivraison_A, 1000);
        Itineraire itineraire_A_B = new Itineraire(listeTroncon_A_B, demandeLivraison_A, demandeLivraison_B, 9444);

        TreeMap<Long, TreeMap<Long, Itineraire>> listeTournee = new TreeMap<>();
        TreeMap<Long, Itineraire> treeMap_E_A = new TreeMap<>();
        TreeMap<Long, Itineraire> treeMap_A_B = new TreeMap<>();
        treeMap_E_A.put(demandeLivraison_A.getId(), itineraire_Entrepot_A);
        treeMap_A_B.put(demandeLivraison_B.getId(), itineraire_A_B);
        listeTournee.put(demandeLivraison_Entrepot.getId(), treeMap_E_A);
        listeTournee.put(demandeLivraison_A.getId(), treeMap_A_B);
        return listeTournee;
    }


    public TreeMap<Long, TreeMap<Long, Itineraire>> init_1() {
        int[] coordonneesX_Entrepot_A = {0, 1, 4, 9};
        int[] coordonneesY_Entrepot_A = {0, 3, 7, 15};
        int[] coordonneesX_A_C = {9, 10,};
        int[] coordonneesY_A_C = {15, 10};
        int[] coordonneesX_C_B = {9, 10, 7};
        int[] coordonneesY_C_B = {15, 10, 19};

        String[] nomRue_Entrepot_A = {"rue 0", "rue 1", "rue 3"};
        String[] nomRue_A_C = {"rue 4", "rue 6"};
        String[] nomRue_C_B = {"rue 9", "rue 14", "rue 46"};
        Coordonnes coordonneesEntrepot = new Coordonnes(0, 0);
        Coordonnes coordonneesA = new Coordonnes(9, 15);
        Coordonnes coordonneesB = new Coordonnes(7, 19);
        Coordonnes coordonneesC = new Coordonnes(10, 10);

        Noeud noeud_Entrepot = new Noeud(1, coordonneesEntrepot);
        Noeud noeud_A = new Noeud(2, coordonneesA);
        Noeud noeud_B = new Noeud(545, coordonneesB);
        Noeud noeud_C = new Noeud(754, coordonneesC);

        ArrayList<Troncon> listeTroncon_Entrepot_A = new ArrayList<Troncon>();
        ArrayList<Troncon> listeTroncon_A_C = new ArrayList<Troncon>();
        ArrayList<Troncon> listeTroncon_C_B = new ArrayList<Troncon>();

        for (int i = 0; i < coordonneesX_Entrepot_A.length - 1; i++) {
            listeTroncon_Entrepot_A.add(new Troncon((long) 10 + i, 10.0, nomRue_Entrepot_A[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_Entrepot_A[i], coordonneesY_Entrepot_A[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_Entrepot_A[i + 1], coordonneesY_Entrepot_A[i + 1]))
            ));
        }

        for (int i = 0; i < coordonneesX_A_C.length - 1; i++) {
            listeTroncon_A_C.add(new Troncon((long) 10 + i, 10.0, nomRue_A_C[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_A_C[i], coordonneesY_A_C[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_A_C[i + 1], coordonneesY_A_C[i + 1]))
            ));
        }

        for (int i = 0; i < coordonneesX_C_B.length - 1; i++) {
            listeTroncon_C_B.add(new Troncon((long) 10 + i, 10.0, nomRue_C_B[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_C_B[i], coordonneesY_C_B[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_C_B[i + 1], coordonneesY_C_B[i + 1]))
            ));
        }

        Heure debutLivraison_Entrepot = new Heure(8 * 3600);
        Heure debutLivraison_A = new Heure(10 * 3600);
        Heure debutLivraison_B = new Heure(15 * 3600);
        Heure debutLivraison_C = new Heure(15 * 3600);


        Heure finLivraison_Entrepot = new Heure(20 * 3600);
        Heure finLivraison_A = new Heure(12 * 3600);
        Heure finLivraison_B = new Heure(18 * 3600);
        Heure finLivraison_C = new Heure(18 * 3600);

        PlageHoraire plageHoraire_Entrepot = new PlageHoraire(debutLivraison_Entrepot, finLivraison_Entrepot);
        PlageHoraire plageHoraire_A = new PlageHoraire(debutLivraison_A, finLivraison_A);
        PlageHoraire plageHoraire_B = new PlageHoraire(debutLivraison_B, finLivraison_B);
        PlageHoraire plageHoraire_C = new PlageHoraire(debutLivraison_C, finLivraison_C);

        DemandeLivraison demandeLivraison_Entrepot = new DemandeLivraison(new Long(1), noeud_Entrepot, 0, plageHoraire_Entrepot);
        DemandeLivraison demandeLivraison_A = new DemandeLivraison(new Long(2), noeud_A, 100, plageHoraire_A);
        DemandeLivraison demandeLivraison_B = new DemandeLivraison(new Long(3), noeud_B, 15, plageHoraire_B);
        DemandeLivraison demandeLivraison_C = new DemandeLivraison(new Long(4), noeud_C, 15, plageHoraire_C);

        Itineraire itineraire_Entrepot_A = new Itineraire(listeTroncon_Entrepot_A, demandeLivraison_Entrepot, demandeLivraison_A, 1000);
        Itineraire itineraire_A_C = new Itineraire(listeTroncon_A_C, demandeLivraison_A, demandeLivraison_C, 9444);
        Itineraire itineraire_C_B = new Itineraire(listeTroncon_C_B, demandeLivraison_C, demandeLivraison_B, 9444);


        TreeMap<Long, TreeMap<Long, Itineraire>> listeTournee = new TreeMap<>();
        TreeMap<Long, Itineraire> treeMap_E_A = new TreeMap<>();
        TreeMap<Long, Itineraire> treeMap_A_C = new TreeMap<>();
        TreeMap<Long, Itineraire> treeMap_C_B = new TreeMap<>();
        treeMap_E_A.put(demandeLivraison_A.getId(), itineraire_Entrepot_A);
        treeMap_A_C.put(demandeLivraison_C.getId(), itineraire_A_C);
        treeMap_C_B.put(demandeLivraison_B.getId(), itineraire_C_B);

        listeTournee.put(demandeLivraison_Entrepot.getId(), treeMap_E_A);
        listeTournee.put(demandeLivraison_A.getId(), treeMap_A_C);
        listeTournee.put(demandeLivraison_C.getId(), treeMap_C_B);
        return listeTournee;
    }


    @BeforeEach
    void setUp() {
        Coordonnes coordonnees_Entrepot = new Coordonnes(0, 0);
        // public Entrepot(Long id, Noeud adresse, int dureeLivraisonSec, PlageHoraire plageLivraison) {

        PlageHoraire plageHoraire_Entrepot = new PlageHoraire(new Heure(8 * 3600), new Heure(20 * 3600));
        Entrepot entrepot = new Entrepot(1L, new Noeud(1L, coordonnees_Entrepot), 0, plageHoraire_Entrepot);

        carteVilleSimplifiee = new CarteVilleSimplifiee(init_0(), entrepot);
    }


    @Test
    void getMatriceAdjacenceDindiceLivraison() {
        assertEquals(init_0().toString(), carteVilleSimplifiee.getMatriceAdjacenceDindiceLivraison().toString());
    }

    @Test
    void setMatriceAdjacenceDindiceLivraison() {
        carteVilleSimplifiee.setMatriceAdjacenceDindiceLivraison(init_1());
        assertEquals(init_1().toString(), carteVilleSimplifiee.getMatriceAdjacenceDindiceLivraison().toString());
    }

    @Test
    void getEntrepot() {
        Coordonnes coordonnees_Entrepot = new Coordonnes(0, 0);

        PlageHoraire plageHoraire_Entrepot = new PlageHoraire(new Heure(8 * 3600), new Heure(20 * 3600));
        Entrepot entrepot = new Entrepot(1L, new Noeud(1L, coordonnees_Entrepot), 0, plageHoraire_Entrepot);

        assertEquals(entrepot.toString(), carteVilleSimplifiee.getEntrepot().toString());
    }

    @Test
    void setEntrepot() {
        Coordonnes coordonnees_Entrepot = new Coordonnes(9, 4);

        PlageHoraire plageHoraire_Entrepot = new PlageHoraire(new Heure(8 * 3600 + 30 * 60), new Heure(21 * 3600));
        Entrepot entrepot = new Entrepot(99L, new Noeud(19L, coordonnees_Entrepot), 0, plageHoraire_Entrepot);

        carteVilleSimplifiee.setEntrepot(entrepot);

        assertEquals(entrepot.toString(), carteVilleSimplifiee.getEntrepot().toString());
    }

    @Test
    void Testclone() throws CloneNotSupportedException {
        CarteVilleSimplifiee carteVilleSimplifieeClone = carteVilleSimplifiee.clone();
        assertEquals(carteVilleSimplifieeClone.toString(), carteVilleSimplifiee.toString());
        assertNotEquals(carteVilleSimplifieeClone, carteVilleSimplifiee);
    }
}