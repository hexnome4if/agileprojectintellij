package Modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class Troncon_test {
    Troncon troncon;

    @BeforeEach
    void setUp() {
        Noeud noeudOrigine = new Noeud(1, new Coordonnes(6, 7));
        Noeud noeudArrivee = new Noeud(2, new Coordonnes(8, 9));
        this.troncon = new Troncon(1L, 50, "Général de Gaulle", 200, noeudOrigine, noeudArrivee);
    }

    @Test
    void getId() {
        assertEquals((Long) 1L, this.troncon.getId());
    }

    @Test
    void getLongueur() {
        assertEquals(50, this.troncon.getLongueur());
    }

    @Test
    void getNomRue() {
        assertEquals("Général de Gaulle", this.troncon.getNomRue());
    }

    @Test
    void getDureeParcours() {
        assertEquals(200, this.troncon.getDureeParcours());
    }

    @Test
    void getOrigine() {
        Noeud unNoeud = new Noeud(1, new Coordonnes(6, 7));
        assertEquals(unNoeud.getCoord().getCoordX(), this.troncon.getOrigine().getCoord().getCoordX());
        assertEquals(unNoeud.getCoord().getCoordY(), this.troncon.getOrigine().getCoord().getCoordY());
        assertEquals(unNoeud.getId(), this.troncon.getOrigine().getId());
    }

    @Test
    void getArrivee() {
        Noeud unNoeud = new Noeud(2, new Coordonnes(8, 9));
        assertEquals(unNoeud.getCoord().getCoordX(), this.troncon.getArrivee().getCoord().getCoordX());
        assertEquals(unNoeud.getCoord().getCoordY(), this.troncon.getArrivee().getCoord().getCoordY());
        assertEquals(unNoeud.getId(), this.troncon.getArrivee().getId());
    }

    @Test
    void setId() {
        this.troncon.setId(2L);
        assertEquals((Long) 2L, this.troncon.getId());
    }

    @Test
    void setLongueur() {
        this.troncon.setLongueur(60);
        assertEquals(60, this.troncon.getLongueur());
    }

    @Test
    void setNomRue() {
        this.troncon.setNomRue("Général Leclerc");
        assertEquals("Général Leclerc", this.troncon.getNomRue());
    }

    @Test
    void setDureeParcours() {
        this.troncon.setDureeParcours(500);
        assertEquals(500, this.troncon.getDureeParcours());
    }

    @Test
    void setOrigine() {
        Noeud unNoeud = new Noeud(3, new Coordonnes(10, 11));
        this.troncon.setOrigine(unNoeud);
        assertEquals(unNoeud, this.troncon.getOrigine());
    }

    @Test
    void setArrivee() {
        Noeud unNoeud = new Noeud(1, new Coordonnes(12, 13));
        this.troncon.setArrivee(unNoeud);
        assertEquals(unNoeud, this.troncon.getArrivee());
    }


    @Test
    void testClone() throws CloneNotSupportedException {
        Troncon tronconClone = troncon.clone();

        assertEquals(tronconClone.getOrigine(), tronconClone.getOrigine());
        assertEquals(tronconClone.getArrivee(), tronconClone.getArrivee());
        assertEquals(tronconClone.getDureeParcours(), tronconClone.getDureeParcours());
        assertEquals(tronconClone.getLongueur(), tronconClone.getLongueur());
        assertEquals(tronconClone.getNomRue(), tronconClone.getNomRue());
        assertEquals(tronconClone.getId(), tronconClone.getId());
        assertNotEquals(tronconClone, troncon);
    }

    void testIsEqual() {
        Troncon tronconEqual;
        Noeud noeudOrigine = new Noeud(1, new Coordonnes(6, 7));
        Noeud noeudArrivee = new Noeud(2, new Coordonnes(8, 9));
        this.troncon = new Troncon(1L, 50, "Général de Gaulle", 200, noeudOrigine, noeudArrivee);

    }

}