package Modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class ItineraireHorodate_Test {
    ItineraireHorodate itineraireHorodate;

    @BeforeEach
    void setUp() {
        Troncon troncon;
        int[] coordonneesXA = {0, 1, 2, 3};
        int[] coordonneesYA = {10, 11, 12, 13};
        int[] coordonneesXB = {5, 6, 7, 8};
        int[] coordonneesYB = {15, 16, 17, 18};
        int[] longeur = {20, 21, 22, 23};
        int[] dureeParcours = {30, 31, 32, 33};
        String[] nomRues = {"rue 0", "rue 1", "rue 2", "rue 3"};
        int dureeItineraire = 15;
        Noeud adresse = new Noeud(1, new Coordonnes(5, 6));
        int dureeLivraisonSec = 150;

        Heure heureDebutItineraire = new Heure(28800);
        Heure heureFinItineraire = new Heure(72000);
        Heure heureLivraison = new Heure(95426);
        PlageHoraire plageHoraire = new PlageHoraire(heureDebutItineraire, heureFinItineraire);

        DemandeLivraison depot = new DemandeLivraison((long) 10, adresse, dureeLivraisonSec, plageHoraire);
        DemandeLivraison livraisonOrigine = depot;
        DemandeLivraison livraisonArrivee = depot;

        ArrayList<Troncon> listTroncon = new ArrayList<Troncon>();

        for (int i = 0; i < coordonneesXA.length; i++) {
            Noeud noeudOrigine = new Noeud(i, new Coordonnes(coordonneesXA[i], coordonneesYA[i]));
            Noeud noeudArrivee = new Noeud(i + coordonneesXA.length, new Coordonnes(coordonneesXB[i], coordonneesYB[i]));
            troncon = new Troncon((long) i, longeur[i], nomRues[i], dureeParcours[i], noeudOrigine, noeudArrivee);
            listTroncon.add(troncon);
        }
        Itineraire itineraire = new Itineraire(listTroncon, livraisonOrigine, livraisonArrivee, dureeItineraire);

        itineraireHorodate = new ItineraireHorodate(itineraire, heureLivraison, plageHoraire);
    }

    @Test
    void getItineraire() {
        Troncon troncon;
        int[] coordonneesXA = {0, 1, 2, 3};
        int[] coordonneesYA = {10, 11, 12, 13};
        int[] coordonneesXB = {5, 6, 7, 8};
        int[] coordonneesYB = {15, 16, 17, 18};
        int[] longeur = {20, 21, 22, 23};
        int[] dureeParcours = {30, 31, 32, 33};
        String[] nomRues = {"rue 0", "rue 1", "rue 2", "rue 3"};
        int dureeItineraire = 15;
        Noeud adresse = new Noeud(1, new Coordonnes(5, 6));
        int dureeLivraisonSec = 150;

        Heure heureDebutItineraire = new Heure(28800);
        Heure heureFinItineraire = new Heure(72000);
        Heure heureLivraison = new Heure(95426);
        PlageHoraire plageHoraire = new PlageHoraire(heureDebutItineraire, heureFinItineraire);

        DemandeLivraison depot = new DemandeLivraison((long) 10, adresse, dureeLivraisonSec, plageHoraire);
        DemandeLivraison livraisonOrigine = depot;
        DemandeLivraison livraisonArrivee = depot;

        ArrayList<Troncon> listTroncon = new ArrayList<Troncon>();

        for (int i = 0; i < coordonneesXA.length; i++) {
            Noeud noeudOrigine = new Noeud(i, new Coordonnes(coordonneesXA[i], coordonneesYA[i]));
            Noeud noeudArrivee = new Noeud(i + coordonneesXA.length, new Coordonnes(coordonneesXB[i], coordonneesYB[i]));
            troncon = new Troncon((long) i, longeur[i], nomRues[i], dureeParcours[i], noeudOrigine, noeudArrivee);
            listTroncon.add(troncon);
        }
        Itineraire itineraire = new Itineraire(listTroncon, livraisonOrigine, livraisonArrivee, dureeItineraire);
        assertEquals(itineraire.toString(), itineraireHorodate.getItineraire().toString());
    }

    @Test
    void setItineraire() {
        Troncon troncon;
        int[] coordonneesXA = {0, 1};
        int[] coordonneesYA = {10, 11};
        int[] coordonneesXB = {5, 6};
        int[] coordonneesYB = {15, 16};
        int[] longeur = {20, 21};
        int[] dureeParcours = {30, 31};
        String[] nomRues = {"rue 0", "rue 1"};
        ArrayList<Troncon> listTroncon = new ArrayList<Troncon>();
        int dureeItineraire = 15;
        Noeud adresse = new Noeud(1, new Coordonnes(77, 132));
        int dureeLivraisonSec = 123456;
        Heure heureDebutItineraire = new Heure(9612);
        Heure heureFinItineraire = new Heure(96120);
        Heure heureLivraison = new Heure(96120);
        PlageHoraire plageHoraire = new PlageHoraire(heureDebutItineraire, heureFinItineraire);

        DemandeLivraison depot = new DemandeLivraison((long) 10, adresse, dureeLivraisonSec, plageHoraire);
        DemandeLivraison livraisonOrigine = depot;
        DemandeLivraison livraisonArrivee = depot;

        for (int i = 0; i < coordonneesXA.length; i++) {
            Noeud noeudOrigine = new Noeud(i, new Coordonnes(coordonneesXA[i], coordonneesYA[i]));
            Noeud noeudArrivee = new Noeud(i + coordonneesXA.length, new Coordonnes(coordonneesXB[i], coordonneesYB[i]));
            troncon = new Troncon((long) i, longeur[i], nomRues[i], dureeParcours[i], noeudOrigine, noeudArrivee);
            listTroncon.add(troncon);
        }
        Itineraire itineraire = new Itineraire(listTroncon, livraisonOrigine, livraisonArrivee, dureeItineraire);

        itineraireHorodate.setItineraire(itineraire);
        assertEquals(itineraire.toString(), itineraireHorodate.getItineraire().toString());
    }

    @Test
    void getHeureDeLivraison() {
        Heure heureLivraison = new Heure(95426);
        assertEquals(heureLivraison.toString(), itineraireHorodate.getHeureDeLivraison().toString());
    }

    @Test
    void setHeureDeLivraison() {
        Heure heureLivraison = new Heure(113456);

        assertEquals(heureLivraison.toString(), itineraireHorodate.getHeureDeLivraison().toString());
    }

    @Test
    void getPlageLivraison() {
    }

    @Test
    void setPlageLivraison() {
    }

    @Test
    void TestClone() throws CloneNotSupportedException {
        ItineraireHorodate itineraireHorodateClone = itineraireHorodate.clone();
        assertEquals(itineraireHorodateClone.getItineraire().toString(), itineraireHorodate.getItineraire().toString());
        assertEquals(itineraireHorodateClone.getHeureDeLivraison().toString(), itineraireHorodate.getHeureDeLivraison().toString());
        assertEquals(itineraireHorodateClone.getPlageLivraison().toString(), itineraireHorodate.getPlageLivraison().toString());

        assertNotEquals(itineraireHorodateClone.getItineraire(), itineraireHorodate.getItineraire());
        assertNotEquals(itineraireHorodateClone.getHeureDeLivraison(), itineraireHorodate.getHeureDeLivraison());
        assertNotEquals(itineraireHorodateClone.getPlageLivraison(), itineraireHorodate.getPlageLivraison());


    }
}