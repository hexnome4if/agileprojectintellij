package Modele;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class CarteVille_test {
    static CarteVille carteVille;

    static CarteVille initialisateurCarteVille() {
        ArrayList<Noeud> noeuds = new ArrayList<Noeud>();
        Noeud noeud1 = new Noeud(1, new Coordonnes(5, 6));
        Noeud noeud2 = new Noeud(2, new Coordonnes(8, 8));
        Noeud noeud3 = new Noeud(3, new Coordonnes(1, 2));
        Noeud noeud4 = new Noeud(4, new Coordonnes(3, 4));
        noeuds.add(noeud1);
        noeuds.add(noeud2);
        noeuds.add(noeud3);
        noeuds.add(noeud4);
        ArrayList<Troncon> troncons = new ArrayList<Troncon>();
        troncons.add(
                new Troncon(1L, 50, "aaa",
                        300,
                        noeud1, noeud2));
        troncons.add(
                new Troncon(2L, 20, "bbb",
                        400,
                        noeud2, noeud3));
        troncons.add(
                new Troncon(3L, 30, "ccc",
                        500,
                        noeud3, noeud4));
        return new CarteVille(troncons, noeuds);
    }

    @BeforeAll
    static void setUp() {

        carteVille = initialisateurCarteVille();
    }


    @Test
    void getListTroncon() {
        String expected = "["
                + "Troncon"
                + "{id=1, longueur=50.0, nomRue='aaa', dureeParcours=300, "
                + "origine=Noeud{id=1, coord=Coordonnes{coordX=5, coordY=6}}, "
                + "arrivee=Noeud{id=2, coord=Coordonnes{coordX=8, coordY=8}}}, "
                + "Troncon"
                + "{id=2, longueur=20.0, nomRue='bbb', dureeParcours=400, "
                + "origine=Noeud{id=2, coord=Coordonnes{coordX=8, coordY=8}}, "
                + "arrivee=Noeud{id=3, coord=Coordonnes{coordX=1, coordY=2}}}, "
                + "Troncon"
                + "{id=3, longueur=30.0, nomRue='ccc', dureeParcours=500, "
                + "origine=Noeud{id=3, coord=Coordonnes{coordX=1, coordY=2}}, "
                + "arrivee=Noeud{id=4, coord=Coordonnes{coordX=3, coordY=4}}}"
                + "]";
        assertEquals(expected, carteVille.getListTroncon().toString());
    }

    @Test
    void setListTroncon() {
    }

    @Test
    void getListNoeud() {
        String expected =
                "[Noeud{id=1, coord=Coordonnes{coordX=5, coordY=6}}, "
                        + "Noeud{id=2, coord=Coordonnes{coordX=8, coordY=8}}, "
                        + "Noeud{id=3, coord=Coordonnes{coordX=1, coordY=2}}, "
                        + "Noeud{id=4, coord=Coordonnes{coordX=3, coordY=4}}]";
        assertEquals(expected, carteVille.getListNoeud().toString());
    }

    @Test
    void setListNoeud() {

    }

    @Test
    public void testToString() {
        String expected =
                "CarteVille{listTroncon=[Troncon"
                        + "{id=1, longueur=50.0, nomRue='aaa', dureeParcours=300, "
                        + "origine=Noeud{id=1, coord=Coordonnes{coordX=5, coordY=6}}, "
                        + "arrivee=Noeud{id=2, coord=Coordonnes{coordX=8, coordY=8}}}, "
                        + "Troncon"
                        + "{id=2, longueur=20.0, nomRue='bbb', dureeParcours=400, "
                        + "origine=Noeud{id=2, coord=Coordonnes{coordX=8, coordY=8}}, "
                        + "arrivee=Noeud{id=3, coord=Coordonnes{coordX=1, coordY=2}}}, "
                        + "Troncon"
                        + "{id=3, longueur=30.0, nomRue='ccc', dureeParcours=500, "
                        + "origine=Noeud{id=3, coord=Coordonnes{coordX=1, coordY=2}}, "
                        + "arrivee=Noeud{id=4, coord=Coordonnes{coordX=3, coordY=4}}}"
                        + "], listNoeud=["
                        + "Noeud{id=1, coord=Coordonnes{coordX=5, coordY=6}}, "
                        + "Noeud{id=2, coord=Coordonnes{coordX=8, coordY=8}}, "
                        + "Noeud{id=3, coord=Coordonnes{coordX=1, coordY=2}}, "
                        + "Noeud{id=4, coord=Coordonnes{coordX=3, coordY=4}}]}";
        assertEquals(expected, carteVille.toString());
    }

    @Test
    void testClone() throws CloneNotSupportedException {
        CarteVille carteVilleCLone = carteVille.clone();
        assertEquals(carteVilleCLone.getListNoeud(), carteVille.getListNoeud());
        assertEquals(carteVilleCLone.getListTroncon(), carteVille.getListTroncon());

        assertNotEquals(carteVilleCLone, carteVille);
    }
}