package Modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class Itineraire_test {
    private Itineraire itineraire;
    private ArrayList<Troncon> listTroncon = new ArrayList<Troncon>();
    private DemandeLivraison livraisonOrigine;
    private DemandeLivraison livraisonArrivee;
    private int dureeItineraire;
    @BeforeEach
    void setUp() {
        Troncon troncon;
        int[] coordonneesXA = {0, 1, 2, 3};
        int[] coordonneesYA = {10, 11, 12, 13};

        int[] coordonneesXB = {5, 6, 7, 8};
        int[] coordonneesYB = {15, 16, 17, 18};

        int[] longeur = {20, 21, 22, 23};
        int[] dureeParcours = {30, 31, 32, 33};
        String[] nomRues = {"rue 0", "rue 1", "rue 2", "rue 3"};
        dureeItineraire = 15;
        Noeud adresse = new Noeud(1, new Coordonnes(5, 6));
        int dureeLivraisonSec = 150;
        Heure heureD = new Heure(28800);
        Heure heureF = new Heure(72000);
        PlageHoraire plageHoraire = new PlageHoraire(heureD, heureF);
        DemandeLivraison depot = new DemandeLivraison((long) 10, adresse, dureeLivraisonSec, plageHoraire);
        livraisonOrigine = depot;
        livraisonArrivee = depot;
        for (int i = 0; i < coordonneesXA.length; i++) {
            Noeud noeudOrigine = new Noeud(i, new Coordonnes(coordonneesXA[i], coordonneesYA[i]));
            Noeud noeudArrivee = new Noeud(i + coordonneesXA.length, new Coordonnes(coordonneesXB[i], coordonneesYB[i]));
            troncon = new Troncon((long) i, longeur[i], nomRues[i], dureeParcours[i], noeudOrigine, noeudArrivee);
            listTroncon.add(troncon);
        }
        itineraire = new Itineraire(listTroncon, livraisonOrigine, livraisonArrivee, dureeItineraire);

    }

    @Test
    void getListTroncon() {
        Troncon troncon;
        ArrayList<Troncon> listeTronconB = new ArrayList<Troncon>();
        int[] coordonneesXA = {0, 1, 2, 3};
        int[] coordonneesYA = {10, 11, 12, 13};
        int[] longeurA = {20, 21, 22, 23};
        int[] dureeParcoursA = {30, 31, 32, 33};
        int[] coordonneesXB = {5, 6, 7, 8};
        int[] coordonneesYB = {15, 16, 17, 18};
        String[] nomRues = {"rue 0", "rue 1", "rue 2", "rue 3"};
        for (int i = 0; i < coordonneesXA.length; i++) {
            Noeud noeudOrigine = new Noeud(i, new Coordonnes(coordonneesXA[i], coordonneesYA[i]));
            Noeud noeudArrivee = new Noeud(i + coordonneesXA.length, new Coordonnes(coordonneesXB[i], coordonneesYB[i]));
            troncon = new Troncon((long) i, longeurA[i], nomRues[i], dureeParcoursA[i], noeudOrigine, noeudArrivee);
            listeTronconB.add(troncon);
        }
        assertEquals(listeTronconB.toString(), itineraire.getListTroncon().toString());
    }


    @Test
    void setListTroncon() {
        Troncon troncon;
        ArrayList<Troncon> listeTronconB = new ArrayList<Troncon>();
        int[] coordonneesXA = {55, 771, 992};
        int[] coordonneesYA = {0, 1, 1};
        int[] longeurA = {20, 215, 22};
        int[] dureeParcoursA = {30, 32, 33};
        int[] coordonneesXB = {6, 7, 8};
        int[] coordonneesYB = {15, 17, 18};
        String[] nomRues = {"rue update 1", "rue update 2", "rue update 3"};
        for (int i = 0; i < coordonneesXA.length; i++) {
            Noeud noeudOrigine = new Noeud(i, new Coordonnes(coordonneesXA[i], coordonneesYA[i]));
            Noeud noeudArrivee = new Noeud(i + coordonneesXA.length, new Coordonnes(coordonneesXB[i], coordonneesYB[i]));
            troncon = new Troncon((long) i, longeurA[i], nomRues[i], dureeParcoursA[i], noeudOrigine, noeudArrivee);
            listeTronconB.add(troncon);
        }
        itineraire.setListTroncon(listeTronconB);
        assertEquals(listeTronconB.toString(), itineraire.getListTroncon().toString());
    }

    @Test
    void getLivraisonOrigine() {
        Noeud adresse = new Noeud(1, new Coordonnes(5, 6));
        int dureeLivraisonSec = 150;
        Heure heureD = new Heure(28800);
        Heure heureF = new Heure(72000);
        PlageHoraire plageHoraire = new PlageHoraire(heureD, heureF);
        DemandeLivraison depot = new DemandeLivraison((long) 10, adresse, dureeLivraisonSec, plageHoraire);
        assertEquals(depot.toString(), itineraire.getLivraisonOrigine().toString());
    }

    @Test
    void setLivraisonOrigine() {
        Noeud adresse = new Noeud(1, new Coordonnes(945, 457));
        int dureeLivraisonSec = 444;
        Heure heureD = new Heure(451315);
        Heure heureF = new Heure(4511554);
        PlageHoraire plageHoraire = new PlageHoraire(heureD, heureF);
        DemandeLivraison depot = new DemandeLivraison((long) 99, adresse, dureeLivraisonSec, plageHoraire);
        itineraire.setLivraisonOrigine(depot);
        assertEquals(depot, itineraire.getLivraisonOrigine());
    }

    @Test
    void getLivraisonArrivee() {
        Noeud adresse = new Noeud(1, new Coordonnes(5, 6));
        int dureeLivraisonSec = 150;
        Heure heureD = new Heure(28800);
        Heure heureF = new Heure(72000);
        PlageHoraire plageHoraire = new PlageHoraire(heureD, heureF);
        DemandeLivraison depot = new DemandeLivraison((long) 10, adresse, dureeLivraisonSec, plageHoraire);
        assertEquals(depot.toString(), itineraire.getLivraisonArrivee().toString());
    }

    @Test
    void setLivraisonArrivee() {
        Noeud adresse = new Noeud(1, new Coordonnes(945, 457));
        int dureeLivraisonSec = 444;
        Heure heureD = new Heure(451315);
        Heure heureF = new Heure(4511554);
        PlageHoraire plageHoraire = new PlageHoraire(heureD, heureF);
        DemandeLivraison depot = new DemandeLivraison((long) 99, adresse, dureeLivraisonSec, plageHoraire);
        itineraire.setLivraisonArrivee(depot);
        assertEquals(depot, itineraire.getLivraisonArrivee());
    }


    @Test
    void TestClone() throws CloneNotSupportedException {
        Itineraire itineraireClone = itineraire.clone();

        assertEquals(itineraireClone.getDureeItineraire(), itineraire.getDureeItineraire());
        assertEquals(itineraireClone.getLivraisonArrivee().toString(), itineraire.getLivraisonArrivee().toString());
        assertEquals(itineraireClone.getLivraisonOrigine().toString(), itineraire.getLivraisonOrigine().toString());
        assertEquals(itineraireClone.getListTroncon(), itineraire.getListTroncon());

        assertNotEquals(itineraireClone, itineraire);
        assertNotEquals(itineraireClone.getLivraisonArrivee(), itineraire.getLivraisonArrivee());
        assertNotEquals(itineraireClone.getLivraisonOrigine(), itineraire.getLivraisonOrigine());
    }
}