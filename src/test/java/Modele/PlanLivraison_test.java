package Modele;

import ApplicationException.ApplicationError;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class PlanLivraison_test {
    PlanLivraison planLivraison;

    @BeforeEach
    void setUp() {

        DemandeLivraison.resetIdClass();
        ArrayList<DemandeLivraison> listeLivraison = new ArrayList<DemandeLivraison>();
        Entrepot entrepot;
        Noeud noeudEntrepot = new Noeud((long) 1, new Coordonnes(1, 1));
        Noeud noeudA = new Noeud((long) 2, new Coordonnes(100, 2));
        Noeud noeudB = new Noeud((long) 3, new Coordonnes(54, 97));

        int dureeLivraisonSecEntrepot = 0;
        int dureeLivraisonSecA = 10;
        int dureeLivraisonSecB = 20;
        PlageHoraire plageHoraireEntrepot = new PlageHoraire(new Heure(7 * 3600),
                new Heure(20 * 3600));
        PlageHoraire plageHoraireA = new PlageHoraire(new Heure(8 * 3600 + 30 * 60),
                new Heure(10 * 3600));
        PlageHoraire plageHoraireB = new PlageHoraire(new Heure(10 * 3600 + 30 * 60),
                new Heure(13 * 3600));

        entrepot = new Entrepot((long) 0, noeudEntrepot, dureeLivraisonSecEntrepot, plageHoraireEntrepot);
        DemandeLivraison demandeLivraisonA = new DemandeLivraison((long) 1, noeudA, dureeLivraisonSecA, plageHoraireA);
        DemandeLivraison demandeLivraisonB = new DemandeLivraison((long) 2, noeudB, dureeLivraisonSecB, plageHoraireB);
        listeLivraison.add(demandeLivraisonA);
        listeLivraison.add(demandeLivraisonB);
        planLivraison = new PlanLivraison(listeLivraison, entrepot);
    }


    @Test
    void getListeLivraison() {
        ArrayList<DemandeLivraison> listeLivraison = new ArrayList<DemandeLivraison>();

        Noeud noeudA = new Noeud((long) 2, new Coordonnes(100, 2));
        Noeud noeudB = new Noeud((long) 3, new Coordonnes(54, 97));

        int dureeLivraisonSecA = 10;
        int dureeLivraisonSecB = 20;
        PlageHoraire plageHoraireA = new PlageHoraire(new Heure(8 * 3600 + 30 * 60),
                new Heure(10 * 3600));
        PlageHoraire plageHoraireB = new PlageHoraire(new Heure(10 * 3600 + 30 * 60),
                new Heure(13 * 3600));


        DemandeLivraison.resetIdClass();

        DemandeLivraison demandeLivraisonA = new DemandeLivraison((long) 1, noeudA, dureeLivraisonSecA, plageHoraireA);
        DemandeLivraison demandeLivraisonB = new DemandeLivraison((long) 2, noeudB, dureeLivraisonSecB, plageHoraireB);
        listeLivraison.add(demandeLivraisonA);
        listeLivraison.add(demandeLivraisonB);
        assertEquals(listeLivraison.toString(), planLivraison.getListeLivraison().toString());
    }

    @Test
    void setListeLivraison() {
        ArrayList<DemandeLivraison> listeLivraison = new ArrayList<DemandeLivraison>();
        Noeud noeudA = new Noeud((long) 99, new Coordonnes(123, 2));
        Noeud noeudB = new Noeud((long) 15, new Coordonnes(5, 947));

        int dureeLivraisonSecA = 1;
        int dureeLivraisonSecB = 0;
        PlageHoraire plageHoraireA = new PlageHoraire(new Heure(9 * 3600 + 30 * 60),
                new Heure(10 * 3600));
        PlageHoraire plageHoraireB = new PlageHoraire(new Heure(10 * 3600 + 35 * 60),
                new Heure(13 * 3600 + 15 * 60));

        DemandeLivraison demandeLivraisonA = new DemandeLivraison(noeudA, dureeLivraisonSecA, plageHoraireA);
        DemandeLivraison demandeLivraisonB = new DemandeLivraison(noeudB, dureeLivraisonSecB, plageHoraireB);
        listeLivraison.add(demandeLivraisonA);
        listeLivraison.add(demandeLivraisonB);
        planLivraison.setListeLivraison(listeLivraison);
        assertEquals(listeLivraison.toString(), planLivraison.getListeLivraison().toString());

    }

    @Test
    void getEntrepot() {
        Entrepot entrepot;

        Noeud noeudEntrepot = new Noeud((long) 1, new Coordonnes(1, 1));

        int dureeLivraisonSecEntrepot = 0;

        PlageHoraire plageHoraireEntrepot = new PlageHoraire(new Heure(7 * 3600),
                new Heure(20 * 3600));

        entrepot = new Entrepot((long) 0, noeudEntrepot, dureeLivraisonSecEntrepot, plageHoraireEntrepot);

        assertEquals(entrepot.toString(), planLivraison.getEntrepot().toString());
    }

    @Test
    void setEntrepot() {
        Entrepot entrepot;

        Noeud noeudEntrepot = new Noeud((long) 9, new Coordonnes(99, 97));

        int dureeLivraisonSecEntrepot = 0;

        PlageHoraire plageHoraireEntrepot = new PlageHoraire(new Heure(8 * 3600),
                new Heure(21 * 3600));

        entrepot = new Entrepot((long) 15, noeudEntrepot, dureeLivraisonSecEntrepot, plageHoraireEntrepot);

        planLivraison.setEntrepot(entrepot);
        assertEquals(entrepot.toString(), planLivraison.getEntrepot().toString());


    }


    @Test
    void findIndexOf_True() throws ApplicationError {
        ArrayList<DemandeLivraison> listeLivraison = new ArrayList<DemandeLivraison>();
        Noeud noeudA = new Noeud((long) 2, new Coordonnes(100, 2));
        Noeud noeudB = new Noeud((long) 3, new Coordonnes(54, 97));
        int dureeLivraisonSecA = 10;
        int dureeLivraisonSecB = 20;
        PlageHoraire plageHoraireA = new PlageHoraire(new Heure(8 * 3600 + 30 * 60),
                new Heure(10 * 3600));
        PlageHoraire plageHoraireB = new PlageHoraire(new Heure(10 * 3600 + 30 * 60),
                new Heure(13 * 3600));
        DemandeLivraison demandeLivraisonA = new DemandeLivraison((long) 1, noeudA, dureeLivraisonSecA, plageHoraireA);
        DemandeLivraison demandeLivraisonB = new DemandeLivraison((long) 2, noeudB, dureeLivraisonSecB, plageHoraireB);
        listeLivraison.add(demandeLivraisonA);
        listeLivraison.add(demandeLivraisonB);
        assertEquals(listeLivraison.indexOf(demandeLivraisonA), planLivraison.findIndexOf(demandeLivraisonA));
    }


    @Test
    void findIndexOf_False() throws ApplicationError {
        Noeud noeudA = new Noeud((long) 9, new Coordonnes(3, 2));
        int dureeLivraisonSecA = 10;
        PlageHoraire plageHoraireA = new PlageHoraire(new Heure(8 * 3600 + 30 * 60),
                new Heure(15 * 3600));
        DemandeLivraison demandeLivraisonA = new DemandeLivraison((long) 15, noeudA, dureeLivraisonSecA, plageHoraireA);

        assertEquals(0, planLivraison.findIndexOf(demandeLivraisonA));
    }


    @Test
    void ajouterDemandeLivraison() throws ApplicationError {
        Noeud noeudA = new Noeud((long) 9, new Coordonnes(3, 2));
        int dureeLivraisonSecA = 10;
        PlageHoraire plageHoraireA = new PlageHoraire(new Heure(8 * 3600 + 30 * 60),
                new Heure(15 * 3600));
        DemandeLivraison demandeLivraisonA = new DemandeLivraison((long) 15, noeudA, dureeLivraisonSecA, plageHoraireA);
        int size = planLivraison.getListeLivraison().size();
        planLivraison.ajouterDemandeLivraison(demandeLivraisonA);
        assertEquals(size, planLivraison.findIndexOf(demandeLivraisonA));
    }

    @Test
    void supprimerDemandeLivraison() throws ApplicationError {
        Noeud noeudA = new Noeud((long) 99, new Coordonnes(123, 2));
        int dureeLivraisonSecA = 1;
        PlageHoraire plageHoraireA = new PlageHoraire(new Heure(9 * 3600 + 30 * 60),
                new Heure(10 * 3600));
        DemandeLivraison demandeLivraisonA = new DemandeLivraison((long) 2, noeudA, dureeLivraisonSecA, plageHoraireA);
        //supprimerDemandeLivraison(DemandeLivraison demandeASupprimer)
        planLivraison.supprimerDemandeLivraison(demandeLivraisonA);
        assertEquals(0, planLivraison.findIndexOf(demandeLivraisonA));
    }

    @Test
    void modifierDemandeLivraison() throws ApplicationError {
        Noeud noeudA = new Noeud((long) 99, new Coordonnes(100, 2));
        int dureeLivraisonSecA = 10;
        PlageHoraire plageHoraireA = new PlageHoraire(new Heure(9 * 3600),
                new Heure(11 * 3600));
        DemandeLivraison demandeLivraisonA = new DemandeLivraison((long) 2, noeudA, dureeLivraisonSecA, plageHoraireA);
        //modifierDemandeLivraison(DemandeLivraison demandeAModifier)
        planLivraison.modifierDemandeLivraison(demandeLivraisonA);
        int index = planLivraison.findIndexOf(demandeLivraisonA);
        assertEquals(demandeLivraisonA.toString(),
                planLivraison.getListeLivraison().get(index).toString());
    }

    @Test
    void TestClone() throws CloneNotSupportedException {
        PlanLivraison plageHoraireClone = planLivraison.clone();

        assertEquals(plageHoraireClone.toString(), planLivraison.toString());
        assertNotEquals(plageHoraireClone, planLivraison);
    }
}

