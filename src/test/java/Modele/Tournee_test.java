package Modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class Tournee_test {
    Tournee tournee;

    public ArrayList<ItineraireHorodate> init_0() {
        int[] coordonneesX_Entrepot_A = {0, 1, 4, 9};
        int[] coordonneesY_Entrepot_A = {0, 3, 7, 15};
        int[] coordonneesX_A_B = {9, 10, 7};
        int[] coordonneesY_A_B = {15, 10, 19};

        String[] nomRue_Entrepot_A = {"rue 0", "rue 1", "rue 3"};
        String[] nomRue_A_B = {"rue 4", "rue 5", "rue 6"};

        Coordonnes coordonneesEntrepot = new Coordonnes(0, 0);
        Coordonnes coordonneesA = new Coordonnes(9, 15);
        Coordonnes coordonneesB = new Coordonnes(7, 19);

        Noeud noeud_Entrepot = new Noeud(1, coordonneesEntrepot);
        Noeud noeud_A = new Noeud(2, coordonneesA);
        Noeud noeud_B = new Noeud(545, coordonneesB);

        ArrayList<Troncon> listeTroncon_Entrepot_A = new ArrayList<Troncon>();
        ArrayList<Troncon> listeTroncon_A_B = new ArrayList<Troncon>();

        for (int i = 0; i < coordonneesX_Entrepot_A.length - 1; i++) {
            listeTroncon_Entrepot_A.add(new Troncon((long) 10 + i, 10.0, nomRue_Entrepot_A[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_Entrepot_A[i], coordonneesY_Entrepot_A[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_Entrepot_A[i + 1], coordonneesY_Entrepot_A[i + 1]))
            ));
        }

        for (int i = 0; i < coordonneesX_A_B.length - 1; i++) {
            listeTroncon_A_B.add(new Troncon((long) 10 + i, 10.0, nomRue_A_B[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_A_B[i], coordonneesY_A_B[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_A_B[i + 1], coordonneesY_A_B[i + 1]))
            ));
        }

        Heure debutLivraison_Entrepot = new Heure(8 * 3600);
        Heure debutLivraison_A = new Heure(10 * 3600);
        Heure debutLivraison_B = new Heure(15 * 3600);

        Heure heureDeLivraison_A = new Heure(11 * 3600);
        Heure heureDeLivraison_B = new Heure(16 * 3600);

        Heure finLivraison_Entrepot = new Heure(20 * 3600);
        Heure finLivraison_A = new Heure(12 * 3600);
        Heure finLivraison_B = new Heure(18 * 3600);

        PlageHoraire plageHoraire_Entrepot = new PlageHoraire(debutLivraison_Entrepot, finLivraison_Entrepot);
        PlageHoraire plageHoraire_A = new PlageHoraire(debutLivraison_A, finLivraison_A);
        PlageHoraire plageHoraire_B = new PlageHoraire(debutLivraison_B, finLivraison_B);

        DemandeLivraison demandeLivraison_Entrepot = new DemandeLivraison(new Long(1), noeud_Entrepot, 0, plageHoraire_Entrepot);
        DemandeLivraison demandeLivraison_A = new DemandeLivraison(new Long(2), noeud_A, 100, plageHoraire_A);
        DemandeLivraison demandeLivraison_B = new DemandeLivraison(new Long(3), noeud_B, 15, plageHoraire_B);

        Itineraire itineraire_Entrepot_A = new Itineraire(listeTroncon_Entrepot_A, demandeLivraison_Entrepot, demandeLivraison_A, 1000);
        Itineraire itineraire_A_B = new Itineraire(listeTroncon_A_B, demandeLivraison_A, demandeLivraison_B, 9444);

        ItineraireHorodate itineraireHorodate_Entrepot_A = new ItineraireHorodate(itineraire_Entrepot_A
                , heureDeLivraison_A, plageHoraire_A);

        ItineraireHorodate itineraireHorodate_A_B = new ItineraireHorodate(itineraire_A_B
                , heureDeLivraison_B, plageHoraire_B);

        ArrayList<ItineraireHorodate> listeTournee = new ArrayList<ItineraireHorodate>();
        //public Tournee(ArrayList<ItineraireHorodate> listeTournee) {
        listeTournee.add(itineraireHorodate_Entrepot_A);
        listeTournee.add(itineraireHorodate_A_B);
        return listeTournee;
    }

    public ArrayList<ItineraireHorodate> init_1() {
        int[] coordonneesX_Entrepot_A = {0, 1, 4, 0};
        int[] coordonneesY_Entrepot_A = {0, 4, 7, 15};
        int[] coordonneesX_A_B = {0, 110, 7};
        int[] coordonneesY_A_B = {15, 10, 19};

        String[] nomRue_Entrepot_A = {"rue 10", "rue 21", "rue 13"};
        String[] nomRue_A_B = {"rue 44", "rue 59", "rue 64"};

        Coordonnes coordonneesEntrepot = new Coordonnes(0, 0);
        Coordonnes coordonneesA = new Coordonnes(0, 15);
        Coordonnes coordonneesB = new Coordonnes(7, 19);

        Noeud noeud_Entrepot = new Noeud(0, coordonneesEntrepot);
        Noeud noeud_A = new Noeud(10, coordonneesA);
        Noeud noeud_B = new Noeud(54, coordonneesB);

        ArrayList<Troncon> listeTroncon_Entrepot_A = new ArrayList<Troncon>();
        ArrayList<Troncon> listeTroncon_A_B = new ArrayList<Troncon>();

        for (int i = 0; i < coordonneesX_Entrepot_A.length - 1; i++) {
            listeTroncon_Entrepot_A.add(new Troncon((long) 10 + i, 10.0, nomRue_Entrepot_A[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_Entrepot_A[i], coordonneesY_Entrepot_A[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_Entrepot_A[i + 1], coordonneesY_Entrepot_A[i + 1]))
            ));
        }

        for (int i = 0; i < coordonneesX_A_B.length - 1; i++) {
            listeTroncon_A_B.add(new Troncon((long) 10 + i, 10.0, nomRue_A_B[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_A_B[i], coordonneesY_A_B[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_A_B[i + 1], coordonneesY_A_B[i + 1]))
            ));
        }

        Heure debutLivraison_Entrepot = new Heure(8 * 3600);
        Heure debutLivraison_A = new Heure(10 * 3600);
        Heure debutLivraison_B = new Heure(15 * 3600);

        Heure heureDeLivraison_A = new Heure(11 * 3600);
        Heure heureDeLivraison_B = new Heure(16 * 3600);

        Heure finLivraison_Entrepot = new Heure(20 * 3600);
        Heure finLivraison_A = new Heure(12 * 3600);
        Heure finLivraison_B = new Heure(18 * 3600);

        PlageHoraire plageHoraire_Entrepot = new PlageHoraire(debutLivraison_Entrepot, finLivraison_Entrepot);
        PlageHoraire plageHoraire_A = new PlageHoraire(debutLivraison_A, finLivraison_A);
        PlageHoraire plageHoraire_B = new PlageHoraire(debutLivraison_B, finLivraison_B);

        DemandeLivraison demandeLivraison_Entrepot = new DemandeLivraison(new Long(1), noeud_Entrepot, 0, plageHoraire_Entrepot);
        DemandeLivraison demandeLivraison_A = new DemandeLivraison(new Long(2), noeud_A, 100, plageHoraire_A);
        DemandeLivraison demandeLivraison_B = new DemandeLivraison(new Long(3), noeud_B, 15, plageHoraire_B);

        Itineraire itineraire_Entrepot_A = new Itineraire(listeTroncon_Entrepot_A, demandeLivraison_Entrepot, demandeLivraison_A, 1000);
        Itineraire itineraire_A_B = new Itineraire(listeTroncon_A_B, demandeLivraison_A, demandeLivraison_B, 9444);

        ItineraireHorodate itineraireHorodate_Entrepot_A = new ItineraireHorodate(itineraire_Entrepot_A
                , heureDeLivraison_A, plageHoraire_A);

        ItineraireHorodate itineraireHorodate_A_B = new ItineraireHorodate(itineraire_A_B
                , heureDeLivraison_B, plageHoraire_B);

        ArrayList<ItineraireHorodate> listeTournee = new ArrayList<ItineraireHorodate>();
        //public Tournee(ArrayList<ItineraireHorodate> listeTournee) {
        listeTournee.add(itineraireHorodate_Entrepot_A);
        listeTournee.add(itineraireHorodate_A_B);
        return listeTournee;
    }


    public ArrayList<ItineraireHorodate> init_2() {
        int[] coordonneesX_Entrepot_A = {0, 1, 4, 9};
        int[] coordonneesY_Entrepot_A = {0, 3, 7, 15};
        int[] coordonneesX_A_C = {9, 10,};
        int[] coordonneesY_A_C = {15, 10};
        int[] coordonneesX_C_B = {9, 10, 7};
        int[] coordonneesY_C_B = {15, 10, 19};

        String[] nomRue_Entrepot_A = {"rue 0", "rue 1", "rue 3"};
        String[] nomRue_A_C = {"rue 4", "rue 6"};
        String[] nomRue_C_B = {"rue 9", "rue 14", "rue 46"};
        Coordonnes coordonneesEntrepot = new Coordonnes(0, 0);
        Coordonnes coordonneesA = new Coordonnes(9, 15);
        Coordonnes coordonneesB = new Coordonnes(7, 19);
        Coordonnes coordonneesC = new Coordonnes(10, 10);

        Noeud noeud_Entrepot = new Noeud(1, coordonneesEntrepot);
        Noeud noeud_A = new Noeud(2, coordonneesA);
        Noeud noeud_B = new Noeud(545, coordonneesB);
        Noeud noeud_C = new Noeud(754, coordonneesC);

        ArrayList<Troncon> listeTroncon_Entrepot_A = new ArrayList<Troncon>();
        ArrayList<Troncon> listeTroncon_A_C = new ArrayList<Troncon>();
        ArrayList<Troncon> listeTroncon_C_B = new ArrayList<Troncon>();

        for (int i = 0; i < coordonneesX_Entrepot_A.length - 1; i++) {
            listeTroncon_Entrepot_A.add(new Troncon((long) 10 + i, 10.0, nomRue_Entrepot_A[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_Entrepot_A[i], coordonneesY_Entrepot_A[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_Entrepot_A[i + 1], coordonneesY_Entrepot_A[i + 1]))
            ));
        }

        for (int i = 0; i < coordonneesX_A_C.length - 1; i++) {
            listeTroncon_A_C.add(new Troncon((long) 10 + i, 10.0, nomRue_A_C[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_A_C[i], coordonneesY_A_C[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_A_C[i + 1], coordonneesY_A_C[i + 1]))
            ));
        }

        for (int i = 0; i < coordonneesX_C_B.length - 1; i++) {
            listeTroncon_C_B.add(new Troncon((long) 10 + i, 10.0, nomRue_C_B[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_C_B[i], coordonneesY_C_B[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_C_B[i + 1], coordonneesY_C_B[i + 1]))
            ));
        }

        Heure debutLivraison_Entrepot = new Heure(8 * 3600);
        Heure debutLivraison_A = new Heure(10 * 3600);
        Heure debutLivraison_B = new Heure(15 * 3600);
        Heure debutLivraison_C = new Heure(15 * 3600);

        Heure heureDeLivraison_A = new Heure(11 * 3600);
        Heure heureDeLivraison_B = new Heure(16 * 3600);
        Heure heureDeLivraison_C = new Heure(16 * 3600);

        Heure finLivraison_Entrepot = new Heure(20 * 3600);
        Heure finLivraison_A = new Heure(12 * 3600);
        Heure finLivraison_B = new Heure(18 * 3600);
        Heure finLivraison_C = new Heure(18 * 3600);

        PlageHoraire plageHoraire_Entrepot = new PlageHoraire(debutLivraison_Entrepot, finLivraison_Entrepot);
        PlageHoraire plageHoraire_A = new PlageHoraire(debutLivraison_A, finLivraison_A);
        PlageHoraire plageHoraire_B = new PlageHoraire(debutLivraison_B, finLivraison_B);
        PlageHoraire plageHoraire_C = new PlageHoraire(debutLivraison_C, finLivraison_C);

        DemandeLivraison demandeLivraison_Entrepot = new DemandeLivraison(new Long(1), noeud_Entrepot, 0, plageHoraire_Entrepot);
        DemandeLivraison demandeLivraison_A = new DemandeLivraison(new Long(2), noeud_A, 100, plageHoraire_A);
        DemandeLivraison demandeLivraison_B = new DemandeLivraison(new Long(3), noeud_B, 15, plageHoraire_B);
        DemandeLivraison demandeLivraison_C = new DemandeLivraison(new Long(4), noeud_C, 15, plageHoraire_C);

        Itineraire itineraire_Entrepot_A = new Itineraire(listeTroncon_Entrepot_A, demandeLivraison_Entrepot, demandeLivraison_A, 1000);
        Itineraire itineraire_A_C = new Itineraire(listeTroncon_A_C, demandeLivraison_A, demandeLivraison_C, 9444);
        Itineraire itineraire_C_B = new Itineraire(listeTroncon_C_B, demandeLivraison_C, demandeLivraison_B, 9444);

        ItineraireHorodate itineraireHorodate_Entrepot_A = new ItineraireHorodate(itineraire_Entrepot_A
                , heureDeLivraison_A, plageHoraire_A);
        ItineraireHorodate itineraireHorodate_A_C = new ItineraireHorodate(itineraire_A_C
                , heureDeLivraison_C, plageHoraire_C);
        ItineraireHorodate itineraireHorodate_C_B = new ItineraireHorodate(itineraire_C_B
                , heureDeLivraison_B, plageHoraire_B);

        ArrayList<ItineraireHorodate> listeTournee = new ArrayList<ItineraireHorodate>();
        //public Tournee(ArrayList<ItineraireHorodate> listeTournee) {
        listeTournee.add(itineraireHorodate_Entrepot_A);
        listeTournee.add(itineraireHorodate_A_C);
        listeTournee.add(itineraireHorodate_C_B);
        return listeTournee;
    }

    @BeforeEach
    void setUp() {
        tournee = new Tournee(init_0());

    }

    @Test
    void getListeTournee() {
        ArrayList<ItineraireHorodate> listTournee = init_0();
        assertEquals(listTournee.toString(), tournee.getListeTournee().toString());
    }

    @Test
    void setListeTournee() {
        ArrayList<ItineraireHorodate> listetournee = init_1();
        tournee.setListeTournee(listetournee);
        assertEquals(listetournee.toString(), tournee.getListeTournee().toString());
    }

    @Test
    void ajouterItineraireHorodatee() {
        int[] coordonneesX_A_C = {9, 10,};
        int[] coordonneesY_A_C = {15, 10};
        int[] coordonneesX_C_B = {9, 10, 7};
        int[] coordonneesY_C_B = {15, 10, 19};

        String[] nomRue_A_C = {"rue 4", "rue 6"};
        String[] nomRue_C_B = {"rue 9", "rue 14", "rue 46"};

        Coordonnes coordonneesA = new Coordonnes(9, 15);
        Coordonnes coordonneesB = new Coordonnes(7, 19);
        Coordonnes coordonneesC = new Coordonnes(10, 10);

        Noeud noeud_A = new Noeud(2, coordonneesA);
        Noeud noeud_B = new Noeud(545, coordonneesB);
        Noeud noeud_C = new Noeud(754, coordonneesC);

        ArrayList<Troncon> listeTroncon_A_C = new ArrayList<Troncon>();
        ArrayList<Troncon> listeTroncon_C_B = new ArrayList<Troncon>();

        for (int i = 0; i < coordonneesX_A_C.length - 1; i++) {
            listeTroncon_A_C.add(new Troncon((long) 10 + i, 10.0, nomRue_A_C[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_A_C[i], coordonneesY_A_C[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_A_C[i + 1], coordonneesY_A_C[i + 1]))
            ));
        }

        for (int i = 0; i < coordonneesX_C_B.length - 1; i++) {
            listeTroncon_C_B.add(new Troncon((long) 10 + i, 10.0, nomRue_C_B[i], i,
                    new Noeud((long) i, new Coordonnes(coordonneesX_C_B[i], coordonneesY_C_B[i])),
                    new Noeud((long) i + 1, new Coordonnes(coordonneesX_C_B[i + 1], coordonneesY_C_B[i + 1]))
            ));
        }

        Heure debutLivraison_A = new Heure(10 * 3600);
        Heure debutLivraison_B = new Heure(15 * 3600);
        Heure debutLivraison_C = new Heure(15 * 3600);

        Heure heureDeLivraison_A = new Heure(11 * 3600);
        Heure heureDeLivraison_B = new Heure(16 * 3600);
        Heure heureDeLivraison_C = new Heure(16 * 3600);

        Heure finLivraison_A = new Heure(12 * 3600);
        Heure finLivraison_B = new Heure(18 * 3600);
        Heure finLivraison_C = new Heure(18 * 3600);

        PlageHoraire plageHoraire_A = new PlageHoraire(debutLivraison_A, finLivraison_A);
        PlageHoraire plageHoraire_B = new PlageHoraire(debutLivraison_B, finLivraison_B);
        PlageHoraire plageHoraire_C = new PlageHoraire(debutLivraison_C, finLivraison_C);

        DemandeLivraison demandeLivraison_A = new DemandeLivraison(new Long(2), noeud_A, 100, plageHoraire_A);
        DemandeLivraison demandeLivraison_B = new DemandeLivraison(new Long(3), noeud_B, 15, plageHoraire_B);
        DemandeLivraison demandeLivraison_C = new DemandeLivraison(new Long(4), noeud_B, 15, plageHoraire_C);

        Itineraire itineraire_A_C = new Itineraire(listeTroncon_A_C, demandeLivraison_A, demandeLivraison_C, 9444);
        Itineraire itineraire_C_B = new Itineraire(listeTroncon_C_B, demandeLivraison_C, demandeLivraison_B, 9444);

        ItineraireHorodate itineraireHorodate_A_C = new ItineraireHorodate(itineraire_A_C
                , heureDeLivraison_C, plageHoraire_C);
        ItineraireHorodate itineraireHorodate_C_B = new ItineraireHorodate(itineraire_C_B
                , heureDeLivraison_B, plageHoraire_B);

        ArrayList<ItineraireHorodate> listeTournee = init_2();
        tournee.ajouterItineraireHorodatee(2, itineraireHorodate_A_C, itineraireHorodate_C_B);
        assertEquals(listeTournee.toString(), tournee.getListeTournee().toString());
    }

    @Test
    void Testclone() throws CloneNotSupportedException {

        Tournee tourneeClone = tournee.clone();

        assertEquals(tourneeClone.toString(), tournee.toString());
        assertNotEquals(tourneeClone, tournee);


    }
}