package Modele;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class Noeud_test {
    Noeud noeud;

    @BeforeEach
    void setUp() {
        this.noeud = new Noeud(1, new Coordonnes(2, 3));
    }


    @Test
    void getId() {
        assertEquals((long) 1, noeud.getId());
    }

    @Test
    void getCoord() {
        assertEquals((new Coordonnes(2, 3)).toString(), noeud.getCoord().toString());
    }

    @Test
    void setId() {
        long newId = 99;
        noeud.setId(newId);
        assertEquals(newId, noeud.getId());

    }

    @Test
    void setCoord() {
        Coordonnes newCoordonnes = new Coordonnes(55, 99);
        noeud.setCoord(newCoordonnes);
        assertEquals(newCoordonnes.toString(), noeud.getCoord().toString());
    }

    @Test
    void testToString() {
        String expected = "Noeud{" +
                "id=1" +
                ", coord=Coordonnes{" +
                "coordX=2" +
                ", coordY=3" +
                "}}";
        assertEquals(expected, noeud.toString());
    }

    @Test
    void TestClone() throws CloneNotSupportedException {
        Noeud noeudClone = noeud.clone();

        assertEquals(noeudClone.getId(), noeud.getId());
        assertEquals(noeudClone.getCoord().toString(), noeud.getCoord().toString());

        assertNotEquals(noeudClone, noeud);
        assertNotEquals(noeudClone.getCoord(), noeud.getCoord());
    }
}