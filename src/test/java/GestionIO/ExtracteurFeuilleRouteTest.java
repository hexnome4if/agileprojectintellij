package GestionIO;

import Algo.Dijkstra;
import Algo.TSP_BnB_2;
import Modele.CarteVille;
import Modele.CarteVilleSimplifiee;
import Modele.PlanLivraison;
import Modele.Tournee;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class ExtracteurFeuilleRouteTest {
    static private CarteVilleSimplifiee cvs_small;
    static private TSP_BnB_2 tsp_small;

    private void saveFeuilleRoute() {
        String cheminFichierCarte = "./src/main/resources/InputData/planLyonGrand.xml";
        String cheminFichierPlanLivraison = "./src/main/resources/InputData/DLgrand10TW2.xml";

        ParseurPlanXML p_plan = new ParseurPlanXML();
        ParseurDemandeXML p_demande = new ParseurDemandeXML();
        p_plan.setFichierPlan(cheminFichierCarte);
        p_demande.setFichierDemande(cheminFichierPlanLivraison);
        try {
            CarteVille cv = p_plan.recupererPlanVille();
            PlanLivraison pl = p_demande.recupererPlanLivraison(cv);

            Dijkstra dijkstra_small = new Dijkstra(cv, pl);
            cvs_small = dijkstra_small.CalculItineraires();
            tsp_small = new TSP_BnB_2(cvs_small);
            tsp_small.chercheSolution();
            Tournee tournee = tsp_small.getSolution();

            ExtracteurFeuilleRoute extract = new ExtracteurFeuilleRoute();
            extract.creerFeuilleRoute(tournee);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void creerFeuilleRouteTest() {
        ArrayList<String> expectedContent = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get("./src/test/resources/GestionIOData/resultXML.xml"))) {
            stream.forEach(expectedContent::add);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.saveFeuilleRoute();
        String fileName = "";
        Date date = new Date();
        SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss");
        fileName = dt.format(date);
        fileName = fileName.replace(":", "-") + ".xml";

        ArrayList<String> actualContent = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(actualContent::add);
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertEquals(true, actualContent.equals(expectedContent));
    }

    @Test
    void setFichierExportAvecXMLTest(){
        ExtracteurFeuilleRoute extract = new ExtracteurFeuilleRoute();
        String fichier = "test.xml";
        extract.setFichierExport(fichier);
        assertEquals("test.xml",extract.getFichierExport());
    }

    @Test
    void setFichierExportSansXMLTest(){
        ExtracteurFeuilleRoute extract = new ExtracteurFeuilleRoute();
        String fichier = "test";
        extract.setFichierExport(fichier);
        assertEquals("test.xml",extract.getFichierExport());
    }

    @Test
    void getFichierExportTest(){
        ExtracteurFeuilleRoute extract = new ExtracteurFeuilleRoute();
        assertEquals("",extract.getFichierExport());
    }
}