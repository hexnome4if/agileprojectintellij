package GestionIO;

import ApplicationException.ApplicationError;
import ApplicationException.ApplicationException;
import Modele.CarteVille;
import Modele.Heure;
import Modele.PlanLivraison;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ParseurDemandeXMLTest {
    static private String filesPath = "./src/test/resources/GestionIOData/";
    static private ParseurPlanXML PP = new ParseurPlanXML();
    static private CarteVille carteVille;
    static private ParseurDemandeXML PL = new ParseurDemandeXML();

    @BeforeAll
    static void setUp() {
        PP.setFichierPlan(filesPath + "forTestsDemandesLivraisons_PlanVille.xml");
        try {
            carteVille = PP.recupererPlanVille();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<String> generateTestsList() {
        ArrayList<String> testsToCompute = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(filesPath + "TestsToCompute_PlanDeLivraison.txt"))) {
            stream.forEach(testsToCompute::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return testsToCompute;
    }

    @Test
    void PlanLivraison_FichierValide() throws ApplicationException {
        //DemandeLivraison.resetIdClass();
        PL.setFichierDemande(filesPath + "PlanLivraison_FichierValide.xml");
        PlanLivraison planLivraison = PL.recupererPlanLivraison(carteVille);
        String expected = "PlanLivraison{listeLivraison=[" +
                "DemandeLivraison{" +
                "id=1, " +
                "adresse=Noeud{id=4242803790, " +
                "coord=Coordonnes{coordX=27911, coordY=-15602}}, " +
                "dureeLivraisonSec=300, " +
                "plageLivraison=PlageHoraire{heureDeb=Heure{12:54:0}, heureFin=Heure{15:22:0}}}, " +
                "DemandeLivraison{" +
                "id=2, " +
                "adresse=Noeud{id=54803427, " +
                "coord=Coordonnes{coordX=27050, coordY=-16034}}, " +
                "dureeLivraisonSec=300, " +
                "plageLivraison=PlageHoraire{heureDeb=Heure{13:0:0}, heureFin=Heure{14:0:0}}}, " +
                "DemandeLivraison{" +
                "id=3, " +
                "adresse=Noeud{id=54803121, " +
                "coord=Coordonnes{coordX=20091, coordY=-23356}}, " +
                "dureeLivraisonSec=300, " +
                "plageLivraison=PlageHoraire{heureDeb=Heure{11:0:0}, heureFin=Heure{13:0:0}}}], " +
                "entrepot=DemandeLivraison{id=0, adresse=Noeud{id=26106489, coord=Coordonnes{coordX=27866, coordY=-15427}}, " +
                "dureeLivraisonSec=0, plageLivraison=PlageHoraire{heureDeb=Heure{8:0:0}, heureFin=Heure{20:0:0}}}}";
        assertEquals(expected, planLivraison.toString());
    }

    @Test
    void NullMap_FichierValide() throws ApplicationException {
        PL.setFichierDemande(filesPath + "PlanLivraison_FichierValide.xml");
        ApplicationError e = assertThrows(
                ApplicationError.class, () -> PL.recupererPlanLivraison(null)
        );
    }

    @Test
    void NullListeNoeuds_FichierValide() throws ApplicationException {
        PL.setFichierDemande(filesPath + "PlanLivraison_FichierValide.xml");
        CarteVille cv = new CarteVille(carteVille.getListTroncon(), null);
        ApplicationError e = assertThrows(
                ApplicationError.class, () -> PL.recupererPlanLivraison(cv)
        );
    }

    @Test
    void NullListeTroncons_FichierValide() throws ApplicationException {
        //DemandeLivraison.resetIdClass();
        PL.setFichierDemande(filesPath + "PlanLivraison_FichierValide.xml");
        CarteVille cv = new CarteVille(null, carteVille.getListNoeud());
        PlanLivraison planLivraison = PL.recupererPlanLivraison(cv);
        String expected = "PlanLivraison{listeLivraison=[" +
                "DemandeLivraison{" +
                "id=1, " +
                "adresse=Noeud{id=4242803790, " +
                "coord=Coordonnes{coordX=27911, coordY=-15602}}, " +
                "dureeLivraisonSec=300, " +
                "plageLivraison=PlageHoraire{heureDeb=Heure{12:54:0}, heureFin=Heure{15:22:0}}}, " +
                "DemandeLivraison{" +
                "id=2, " +
                "adresse=Noeud{id=54803427, " +
                "coord=Coordonnes{coordX=27050, coordY=-16034}}, " +
                "dureeLivraisonSec=300, " +
                "plageLivraison=PlageHoraire{heureDeb=Heure{13:0:0}, heureFin=Heure{14:0:0}}}, " +
                "DemandeLivraison{" +
                "id=3, " +
                "adresse=Noeud{id=54803121, " +
                "coord=Coordonnes{coordX=20091, coordY=-23356}}, " +
                "dureeLivraisonSec=300, " +
                "plageLivraison=PlageHoraire{heureDeb=Heure{11:0:0}, heureFin=Heure{13:0:0}}}], " +
                "entrepot=DemandeLivraison{id=0, adresse=Noeud{id=26106489, coord=Coordonnes{coordX=27866, coordY=-15427}}, " +
                "dureeLivraisonSec=0, plageLivraison=PlageHoraire{heureDeb=Heure{8:0:0}, heureFin=Heure{20:0:0}}}}";
        assertEquals(expected, planLivraison.toString());
    }

    @Test
    void extraireEntrepot_HeureIncorrecte_i() throws ApplicationException {
        PL.setFichierDemande(filesPath + "extraireEntrepot_HeureIncorrecte_i.xml");
        PlanLivraison planLivraison = PL.recupererPlanLivraison(carteVille);
        Heure heure = planLivraison.getEntrepot().getPlageLivraison().getHeureDeb();
        Heure expectedHeure = new Heure(0, 0, 0);
        assertEquals(expectedHeure.toString(), heure.toString());
    }

    @Test
    void extraireEntrepot_HeureIncorrecte_ii() throws ApplicationException {
        PL.setFichierDemande(filesPath + "extraireEntrepot_HeureIncorrecte_ii.xml");
        PlanLivraison planLivraison = PL.recupererPlanLivraison(carteVille);
        Heure heure = planLivraison.getEntrepot().getPlageLivraison().getHeureDeb();
        Heure expectedHeure = new Heure(23, 12, 15);
        assertEquals(expectedHeure.toString(), heure.toString());
    }

    @Test
    void extraireDemandesLivraison_DebutPlageIncorrect_i() throws ApplicationException {
        PL.setFichierDemande(filesPath + "extraireDemandesLivraison_DebutPlageIncorrect_i.xml");
        PlanLivraison planLivraison = PL.recupererPlanLivraison(carteVille);
        Heure heure = planLivraison.getListeLivraison().get(0).getPlageLivraison().getHeureDeb();
        Heure expectedHeure = new Heure(0, 54, 0);
        assertEquals(expectedHeure.toString(), heure.toString());
    }

    @Test
    void extraireDemandesLivraison_FinPlageIncorrect_ii() throws ApplicationException {
        PL.setFichierDemande(filesPath + "extraireDemandesLivraison_FinPlageIncorrecte_ii.xml");
        PlanLivraison planLivraison = PL.recupererPlanLivraison(carteVille);
        Heure heure = planLivraison.getListeLivraison().get(0).getPlageLivraison().getHeureFin();
        Heure expectedHeure = new Heure(23, 22, 0);
        assertEquals(expectedHeure.toString(), heure.toString());
    }

    @Test
    void extraireDemandesLivraison_PlageIncomplete_i() throws ApplicationException {
        PL.setFichierDemande(filesPath + "extraireDemandesLivraison_PlageIncomplete_i.xml");
        PlanLivraison planLivraison = PL.recupererPlanLivraison(carteVille);
        Heure heureDeb = planLivraison.getListeLivraison().get(0).getPlageLivraison().getHeureDeb();
        Heure expectedHeureDeb = new Heure(6, 0, 0);
        assertEquals(expectedHeureDeb.toString(), heureDeb.toString());
        Heure heureFin = planLivraison.getListeLivraison().get(0).getPlageLivraison().getHeureFin();
        Heure expectedHeureFin = new Heure(20, 0, 0);
        assertEquals(expectedHeureFin.toString(), heureFin.toString());
    }

    @Test
    void extraireDemandesLivraison_PlageIncomplete_ii() throws ApplicationException {
        PL.setFichierDemande(filesPath + "extraireDemandesLivraison_PlageIncomplete_ii.xml");
        PlanLivraison planLivraison = PL.recupererPlanLivraison(carteVille);
        Heure heureDeb = planLivraison.getListeLivraison().get(0).getPlageLivraison().getHeureDeb();
        Heure expectedHeureDeb = new Heure(6, 0, 0);
        assertEquals(expectedHeureDeb.toString(), heureDeb.toString());
        Heure heureFin = planLivraison.getListeLivraison().get(0).getPlageLivraison().getHeureFin();
        Heure expectedHeureFin = new Heure(15, 22, 0);
        assertEquals(expectedHeureFin.toString(), heureFin.toString());
    }

    @Test
    void extraireDemandesLivraison_PlageIncomplete_iii() throws ApplicationException {
        PL.setFichierDemande(filesPath + "extraireDemandesLivraison_PlageIncomplete_iii.xml");
        PlanLivraison planLivraison = PL.recupererPlanLivraison(carteVille);
        Heure heureDeb = planLivraison.getListeLivraison().get(0).getPlageLivraison().getHeureDeb();
        Heure expectedHeureDeb = new Heure(12, 54, 0);
        assertEquals(expectedHeureDeb.toString(), heureDeb.toString());
        Heure heureFin = planLivraison.getListeLivraison().get(0).getPlageLivraison().getHeureFin();
        Heure expectedHeureFin = new Heure(20, 0, 0);
        assertEquals(expectedHeureFin.toString(), heureFin.toString());
    }

    @Test
    void extraireDemandesLivraison_HeureFinAvantHeureDebut_ii() throws ApplicationException {
        PL.setFichierDemande(filesPath + "extraireDemandesLivraison_HeureFinAvantHeureDebut_ii.xml");
        PlanLivraison planLivraison = PL.recupererPlanLivraison(carteVille);
        Heure heureDeb = planLivraison.getListeLivraison().get(0).getPlageLivraison().getHeureDeb();
        Heure expectedHeureDeb = new Heure(12, 54, 0);
        assertEquals(expectedHeureDeb.toString(), heureDeb.toString());
        Heure heureFin = planLivraison.getListeLivraison().get(0).getPlageLivraison().getHeureFin();
        Heure expectedHeureFin = new Heure(12, 54, 0);
        assertEquals(expectedHeureFin.toString(), heureFin.toString());
    }

    @Test
    void extraireDemandesLivraison_NoLivraison() throws ApplicationException {
        PL.setFichierDemande(filesPath + "extraireDemandesLivraison_NoLivraison.xml");
        PlanLivraison planLivraison = PL.recupererPlanLivraison(carteVille);
        assertEquals(0, planLivraison.getListeLivraison().size());
    }

    private void genericTestMethod(String testToCompute) throws ApplicationException {
        PL.setFichierDemande(filesPath + testToCompute);
        ApplicationError e = assertThrows(
                ApplicationError.class, () -> PL.recupererPlanLivraison(carteVille)
        );
    }

    @TestFactory
    Stream<DynamicTest> testApplicationsErrors() {
        ArrayList<String> testsToCompute = generateTestsList();
        return testsToCompute.stream()
                .map(datum -> DynamicTest.dynamicTest(
                        datum,
                        () -> genericTestMethod(
                                datum)));
    }
}