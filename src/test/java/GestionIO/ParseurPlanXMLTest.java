package GestionIO;

import ApplicationException.ApplicationError;
import ApplicationException.ApplicationException;
import Modele.CarteVille;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ParseurPlanXMLTest {
    static private String filesPath = "./src/test/resources/GestionIOData/";
    static private ParseurPlanXML PP = new ParseurPlanXML();

    private ArrayList<String> generateTestsList() {
        ArrayList<String> testsToCompute = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(filesPath + "TestsToCompute_CarteVille.txt"))) {
            stream.forEach(testsToCompute::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return testsToCompute;
    }

    @Test
    void recupererPlanVille_FichierValide() throws ApplicationException {
        PP.setFichierPlan(filesPath + "CarteVille_FichierValide.xml");
        CarteVille carteVille = PP.recupererPlanVille();
        String expected = "CarteVille{listTroncon=[Troncon{id=1, " +
                "longueur=1.0, nomRue='rue test 1', dureeParcours=0, " +
                "origine=Noeud{id=1, coord=Coordonnes{coordX=1, coordY=-1}}, " +
                "arrivee=Noeud{id=2, coord=Coordonnes{coordX=2, coordY=-2}}}], " +
                "listNoeud=[Noeud{id=1, coord=Coordonnes{coordX=1, coordY=-1}}, " +
                "Noeud{id=2, coord=Coordonnes{coordX=2, coordY=-2}}]}";
        assertEquals(expected, carteVille.toString());
    }

    //TODO
    @Test
    void recupererNoeudCarte_FichierSansNoeud() throws ApplicationException {
        PP.setFichierPlan(filesPath + "recupererNoeudCarte_FichierSansNoeud.xml");
        ApplicationError e = assertThrows(
                ApplicationError.class, () -> PP.recupererPlanVille()
        );
    }

    @Test
    void recupererNoeudCarte_NoeudSansAttribut() throws ApplicationException {
        PP.setFichierPlan(filesPath + "recupererNoeudCarte_NoeudSansAttribut.xml");
        CarteVille carteVille = PP.recupererPlanVille();
        assertEquals(3, carteVille.getListNoeud().size());
    }

    @Test
    void recupererTronconsCarte_FichierSansTroncon() throws ApplicationException {
        PP.setFichierPlan(filesPath + "recupererTronconsCarte_FichierSansTroncon.xml");
        CarteVille carteVille = PP.recupererPlanVille();
        assertEquals(0, carteVille.getListTroncon().size());
    }

    @Test
    void recupererTronconsCarte_TronconSansAttributs() throws ApplicationException {
        PP.setFichierPlan(filesPath + "recupererTronconsCarte_TronconSansAttributs.xml");
        CarteVille carteVille = PP.recupererPlanVille();
        assertEquals(5, carteVille.getListTroncon().size());
    }

    @Test
    void recupererTronconsCarte_UnAttributErrone_iv() throws ApplicationException {
        PP.setFichierPlan(filesPath + "recupererTronconsCarte_UnAttributErrone_iv.xml");
        CarteVille carteVille = PP.recupererPlanVille();
        assertEquals("*51*", carteVille.getListTroncon().get(0).getNomRue());
    }

    @Test
    void recupererTronconsCarte_UnAttributManquant_iv() throws ApplicationException {
        PP.setFichierPlan(filesPath + "recupererTronconsCarte_UnAttributManquant_iv.xml");
        CarteVille carteVille = PP.recupererPlanVille();
        assertEquals("", carteVille.getListTroncon().get(0).getNomRue());
    }

    private void genericTestMethod(String testToCompute) throws ApplicationException {
        PP.setFichierPlan(filesPath + testToCompute);
        ApplicationError e = assertThrows(
                ApplicationError.class, () -> PP.recupererPlanVille()
        );
    }

    @TestFactory
    Stream<DynamicTest> testApplicationsErrors() {
        ArrayList<String> testsToCompute = generateTestsList();
        return testsToCompute.stream()
                .map(datum -> DynamicTest.dynamicTest(
                        datum,
                        () -> genericTestMethod(
                                datum)));
    }
}