package Modele;

public class Noeud implements Cloneable {

    /**
     * L'id de l'objet noeud
     */
    private long id;
    /**
     * Coordonnée du noeud
     */
    private Coordonnes coord;

    /**
     * Constructeur par default
     */
    private Noeud() {
    }

    /**
     * Constructeur Noeud
     *
     * @param id
     * @param coord
     */
    public Noeud(long id, Coordonnes coord) {
        this.id = id;
        this.coord = coord;
    }

    /**
     *
     * @return l'id du noeud
     */
    public long getId() {
        return id;
    }

    /**
     * Modifier l'id du noeud
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return les coordonnes du noeuds
     */
    public Coordonnes getCoord() {
        return coord;
    }

    /**
     * Modifier les coordonnés du noeud
     * @param coord
     */
    public void setCoord(Coordonnes coord) {
        this.coord = coord;
    }

    /**
     * @return L'objet en format Json
     */
    @Override
    public String toString() {
        return "Noeud{" +
                "id=" + id +
                ", coord=" + coord +
                '}';
    }

    /**
     *
     * @param noeud
     * @return si les noeuds sont égales : true
     *                            si non : false
     */
    public Boolean isEqual(Noeud noeud) {
        return this.id == noeud.getId() && this.coord.isEqual(noeud.getCoord());
    }

    /**
     *
     * @return un clone de l'objet
     * @throws CloneNotSupportedException
     */
    @Override
    public Noeud clone() throws CloneNotSupportedException {

        Noeud cloneNoeud = (Noeud) super.clone();

        cloneNoeud.id = id;
        cloneNoeud.coord = coord.clone();

        return cloneNoeud;
    }

}
