package Modele;

public class Heure implements Cloneable {
    /**
     * Heure du commencement des livraisons
     */
    final public static Heure DEBUT_JOURNEE = new Heure(6 * 60 * 60); //6h du matin

    /**
     * Heure de fin des livraisons
     */
    final public static Heure FIN_JOURNEE = new Heure(20 * 60 * 60); //20h

    /**
     * L'heure totale en seconde
     */
    private int totalSecondes;

    /**
     * Constructeur par default
     */
    private Heure() {
    }
    public Heure(int heures, int minutes, int secondes) {
        this.totalSecondes = normalise60(secondes) + normalise60(minutes) * 60 + normalise24(heures) * 3600;
    }

    /**
     * Constructeur Heure
     *
     * @param totalSeconds
     */
    public Heure(int totalSeconds) {
        if (totalSeconds < 0) {
            this.totalSecondes = 0;
        } else if (totalSeconds > 24 * 3600 - 1) {
            this.totalSecondes = 24 * 3600 - 1;
        } else {
            this.totalSecondes = totalSeconds;
        }
    }

    /**
     *
     * @return le nombre d'heures
     */
    public int getHeures() {
        return this.totalSecondes / 3600;
    }

    /**
     * Modifier le nombre d'heures
     * @param heures
     * @throws Exception
     */
    public void setHeures(int heures) throws Exception {

        heures = normalise24(heures);
        this.totalSecondes = this.totalSecondes % 3600 + heures * 3600;

    }

    /**
     *
     * @return le nombre de minutes
     */
    public int getMinutes() {
        int totalSeconds = this.totalSecondes;
        int heures = totalSeconds / 3600;
        totalSeconds = totalSeconds % 3600;
        int minutes = totalSeconds / 60;
        return minutes;
    }

    /**
     * Modifier le nombre de minutes
     * @param minutes
     */
    public void setMinutes(int minutes) {
        minutes = normalise60(minutes);
        int totalSecondes = this.totalSecondes;
        int nbHeures = totalSecondes / 3600;
        totalSecondes = totalSecondes % 3600;
        int nbMinutes = totalSecondes / 60;
        totalSecondes = totalSecondes % 60;
        int secondes = totalSecondes / 60;
        totalSecondes = secondes + minutes * 60 + nbHeures * 3600;

        this.totalSecondes = totalSecondes;
    }

    /**
     *
     * @return le nombre de secondes
     */
    public int getSecondes() {
        int secondes = this.totalSecondes;
        int heures = secondes / 3600;
        secondes = secondes % 3600;
        int minutes = secondes / 60;
        secondes = secondes % 60;
        return secondes;
    }

    /**
     * Modifier le nombre de secondes
     * @param secondes
     */
    public void setSecondes(int secondes) {
        int nbSecondes = normalise60(secondes);
        int totalSecondes = this.totalSecondes;
        int nbHeures = totalSecondes / 3600;
        totalSecondes = totalSecondes % 3600;
        int nbMinutes = totalSecondes / 60;
        this.totalSecondes = nbSecondes + nbMinutes * 60 + nbHeures * 3600;
    }

    /**
     * Modifier l'heure
     * @param totalSeconds
     * @throws Exception
     */
    public void setTimeFromSecondes(int totalSeconds) throws Exception {
        if (totalSeconds < 0) {
            throw new Exception("totalSeconds doit être supérieur à zéro");
        } else {
            this.totalSecondes = totalSeconds;
        }
    }

    /**
     * @param includeSeconds : true for format ..h..m..s,
     *                       false for format ..h..
     * @return formatted time
     */
    public String getFormattedTime(boolean includeSeconds) {
        String returnValue =
                formatValue(normalise24(this.getHeures()))
                        + "h" + formatValue(normalise60(this.getMinutes()));

        if (includeSeconds) {
            returnValue = returnValue
                    + "m" + formatValue(normalise60(this.getSecondes())) + "s";
        }

        return returnValue;
    }

    /**
     * @param value to format
     * @return formatted value :
     * value if superior to 10, 0x otherwise
     */
    protected String formatValue(int value) {
        if (value < 0) {
            value = 0;
        }
        return value > 10 ? String.valueOf(value) : "0" + value;
    }

    /**
     *
     * @return l'objet en format Json
     */
    @Override
    public String toString() {
        int secondes = this.totalSecondes;
        int heures = secondes / 3600;
        secondes = secondes % 3600;
        int minutes = secondes / 60;
        secondes = secondes % 60;
        return "Heure{" +
                heures +
                ":" + minutes +
                ":" + secondes +
                '}';
    }

    /**
     * @return time in seconds
     */
    public int getTotalSeconds() {
        return this.totalSecondes;
    }

    /**
     * @param heure : time to compare to
     * @return true if the instance is before or equal to the heure parameter
     */
    public boolean isBefore(Heure heure) {
        if (heure.getTotalSeconds() >= this.totalSecondes) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param value to normalise
     * @return 0 or 60 if outside, or value otherwise
     */
    protected int normalise60(int value) {
        if (value < 0) {
            return 0;
        } else if (value > 59) {
            return 59;
        } else {
            return value;
        }
    }

    /**
     * @param value to normalise
     * @return 0 or 24 if outside, or value otherwise
     */
    protected int normalise24(int value) {
        if (value < 0) {
            return 0;
        } else if (value > 23) {
            return 23;
        } else {
            return value;
        }
    }

    /**
     *
     * @param h
     * @return si les heures sont égales : true
     *                            si non : false
     */
    public boolean equals(Heure h) {
        return h.getTotalSeconds() == getTotalSeconds();
    }

    /**
     *
     * @return un clone de l'objet
     * @throws CloneNotSupportedException
     */
    @Override
    public Heure clone() throws CloneNotSupportedException {

        Heure cloneHeure = (Heure) super.clone();
        cloneHeure.totalSecondes = totalSecondes;

        return cloneHeure;
    }
}
