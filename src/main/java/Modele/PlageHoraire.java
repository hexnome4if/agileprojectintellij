package Modele;

public class PlageHoraire implements Cloneable{
    /**
     * Heure de debut de la plage horaire
     */
    private Heure heureDeb;
    /**
     * Heure de fin de la plage horaire
     */
    private Heure heureFin;

    /**
     * Constructeur par default
     */
    public PlageHoraire() {
        this.heureDeb = new Heure(0);
        this.heureFin = new Heure(23, 59, 59);
    }

    /**
     * Constructeur PlageHoraire
     *
     * @param heureDeb
     * @param heureFin
     */
    public PlageHoraire(Heure heureDeb, Heure heureFin) {
        // If heureFin is inferior to heureDeb, heureFin = heureDeb.
        this.heureDeb = heureDeb;
        if (heureFin.isBefore(heureDeb)) {
            this.heureFin = heureDeb;
        } else {
            this.heureFin = heureFin;
        }
    }

    /**
     *
     * @return Heure de début
     */
    public Heure getHeureDeb() {
        return heureDeb;
    }

    /**
     * Modifier heure de début
     * @param heureDeb
     */
    public void setHeureDeb(Heure heureDeb) {
        this.heureDeb = heureDeb;
    }

    /**
     * @return Heure de fin
     */
    public Heure getHeureFin() {
        return heureFin;
    }

    /**
     * Modifier heure de fin
     * @param heureFin
     */
    public void setHeureFin(Heure heureFin) {
        this.heureFin = heureFin;
    }

    /**
     * @return durée de la plage horaire
     */
    public int getDuree() {
        return this.heureFin.getTotalSeconds() - this.heureDeb.getTotalSeconds();
    }

    /**
     *
     * @param plageHoraire
     * @return si les PlageHoraire sont égales : true
     *                            si non : false
     */
    public boolean equals(PlageHoraire plageHoraire) {
        return plageHoraire.heureDeb.equals(heureDeb) && plageHoraire.heureFin.equals(heureFin);
    }

    /**
     *
     * @return L'objet en format Json
     */
    @Override
    public String toString() {
        return "PlageHoraire{" +
                "heureDeb=" + heureDeb +
                ", heureFin=" + heureFin +
                '}';
    }

    /**
     *
     * @return un clone de l'objet
     * @throws CloneNotSupportedException
     */
    @Override
    public PlageHoraire clone() throws CloneNotSupportedException {

        PlageHoraire clonePlageHoraire = (PlageHoraire) super.clone();

        clonePlageHoraire.heureDeb = heureDeb.clone();
        clonePlageHoraire.heureFin = heureFin.clone();

        return clonePlageHoraire;
    }
}
