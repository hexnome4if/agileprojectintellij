package Modele;

import java.util.TreeMap;

public class CarteVilleSimplifiee implements Cloneable{
    /**
     * La matrice d'adjacence simplifiée
     * Premier id : l'id de l'itineraire d'origine
     * Second id : l'id de l'itineraire d'arrivée
     */
    private TreeMap<Long, TreeMap<Long, Itineraire>> MatriceAdjacenceDindiceLivraison
            = new TreeMap<>();
    /**
     * l'Entrepot
     */
    private Entrepot entrepot;

    /**
     * Constructeur CarteVilleSimplifiee
     *
     * @param matriceAdjacenceDindiceLivraison
     * @param entrepot
     */
    public CarteVilleSimplifiee(TreeMap<Long, TreeMap<Long, Itineraire>> matriceAdjacenceDindiceLivraison, Entrepot entrepot) {
        MatriceAdjacenceDindiceLivraison = matriceAdjacenceDindiceLivraison;
        this.entrepot = entrepot;
    }

    /**
     *
     * @return MatriceAdjacenceDindiceLivraison
     */
    public TreeMap<Long, TreeMap<Long, Itineraire>> getMatriceAdjacenceDindiceLivraison() {
        return MatriceAdjacenceDindiceLivraison;
    }

    /**
     * Modifier matriceAdjacenceDindiceLivraison
     * @param matriceAdjacenceDindiceLivraison
     */
    public void setMatriceAdjacenceDindiceLivraison(TreeMap<Long, TreeMap<Long, Itineraire>> matriceAdjacenceDindiceLivraison) {
        MatriceAdjacenceDindiceLivraison = matriceAdjacenceDindiceLivraison;
    }

    /**
     *
     * @return entrepot
     */
    public Entrepot getEntrepot() {
        return entrepot;
    }

    public void setEntrepot(Entrepot entrepot) {
        this.entrepot = entrepot;
    }

    /**
     *
     * @return l'objet en format Json
     */
    @Override
    public String toString() {
        return "CarteVilleSimplifiee{" +
                "MatriceAdjacenceDindiceLivraison=" + MatriceAdjacenceDindiceLivraison +
                ", entrepot=" + entrepot +
                '}';
    }

    /**
     * @return un clone de l'objet
     * @throws CloneNotSupportedException
     */
    @Override
    public CarteVilleSimplifiee clone() throws CloneNotSupportedException {

        CarteVilleSimplifiee cloneCarteVilleSimplifiee = (CarteVilleSimplifiee) super.clone();

        cloneCarteVilleSimplifiee.entrepot = entrepot.clone();
        cloneCarteVilleSimplifiee.MatriceAdjacenceDindiceLivraison = (TreeMap<Long, TreeMap<Long, Itineraire>>) MatriceAdjacenceDindiceLivraison.clone();

        return cloneCarteVilleSimplifiee;
    }
}
