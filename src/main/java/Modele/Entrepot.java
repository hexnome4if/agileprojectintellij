package Modele;

public class Entrepot extends DemandeLivraison implements Cloneable{
    /**
     * Constructeur par default
     */
    private Entrepot() {
    }

    /**
     * Constructeur Entrepot
     *
     * @param id
     * @param adresse
     * @param dureeLivraisonSec
     * @param plageLivraison
     */
    public Entrepot(Long id, Noeud adresse, int dureeLivraisonSec, PlageHoraire plageLivraison) {
        super(id, adresse, dureeLivraisonSec, plageLivraison);
    }

    /**
     * Constructeur Entrepot sans gestion de l'id
     * @param adresse
     * @param dureeLivraisonSec
     * @param plageLivraison
     */
    public Entrepot(Noeud adresse, int dureeLivraisonSec, PlageHoraire plageLivraison) {
        super(adresse, dureeLivraisonSec, plageLivraison);
    }

    /**
     *
     * @return l'id de l'objet
     */
    @Override
    public Long getId() {
        return super.getId();
    }

    /**
     * Modifier l'id de l'objet
     * @param idDemandeLivraison
     */
    @Override
    public void setId(Long idDemandeLivraison) {
        super.setId(idDemandeLivraison);
    }

    /**
     *
     * @return l'adresse
     */
    @Override
    public Noeud getAdresse() {
        return super.getAdresse();
    }

    /**
     * Modifier l'adresse
     * @param adresse
     */
    @Override
    public void setAdresse(Noeud adresse) {
        super.setAdresse(adresse);
    }

    /**
     *
     * @return durée de livraison en secondes
     */
    @Override
    public int getDureeLivraisonSec() {
        return super.getDureeLivraisonSec();
    }

    /**
     * Modifier la durée de livraison en secondes
     * @param dureeLivraisonSec
     */
    @Override
    public void setDureeLivraisonSec(int dureeLivraisonSec) {
        super.setDureeLivraisonSec(dureeLivraisonSec);
    }

    /**
     *
     * @return la plage de livraison
     */
    @Override
    public PlageHoraire getPlageLivraison() {
        return super.getPlageLivraison();
    }

    /**
     * Modifier la plage de livraison
     * @param plageLivraison
     */
    @Override
    public void setPlageLivraison(PlageHoraire plageLivraison) {
        super.setPlageLivraison(plageLivraison);
    }

    /**
     *
     * @return l'objet en format Json
     */
    @Override
    public String toString() {
        return super.toString();
    }

    /**
     *
     * @return un clone de l'objet
     * @throws CloneNotSupportedException
     */
    @Override
    public Entrepot clone() throws CloneNotSupportedException {

        Entrepot cloneEntrepot = (Entrepot) super.clone();

        return cloneEntrepot;
    }

}
