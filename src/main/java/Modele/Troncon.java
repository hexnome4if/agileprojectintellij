package Modele;


import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;

public class Troncon implements Cloneable {
    /**
     * l'id de l'objet troncon
     */
    private Long id;
    /**
     * Longueur du troncon
     */
    private double longueur;
    /**
     * Nom de la rue
     */
    private String nomRue;
    /**
     * durée du parcours
     */
    private int dureeParcours;
    /**
     * Noeud d'origine
     */
    private Noeud origine;
    /**
     * Noeud de d'arrivée
     */
    private Noeud arrivee;

    /**
     * Constructeur par default
     */
    private Troncon() {
    }

    /**
     * Constructeur troncon
     *
     * @param id
     * @param longueur
     * @param nomRue
     * @param dureeParcours
     * @param origine
     * @param arrivee
     */
    public Troncon(Long id, double longueur, String nomRue, int dureeParcours, Noeud origine, Noeud arrivee) {
        this.id = id;
        this.longueur = longueur;
        this.nomRue = nomRue;
        this.dureeParcours = dureeParcours;
        this.origine = origine;
        this.arrivee = arrivee;
    }

    /**
     *
     * @return id du tronon
     */
    public Long getId() {
        return id;
    }

    /**
     * Modifier l'Id du troncon
     * @param id
     */
    @XmlElement
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return la longueur du tronçon
     */
    public double getLongueur() {
        return longueur;
    }

    /**
     * Modifier la longueur du troncon
     * @param longueur
     */
    @XmlElement
    public void setLongueur(double longueur) {
        this.longueur = longueur;
    }

    /**
     *
     * @return le nom de la true
     */
    public String getNomRue() {
        return nomRue;
    }

    /**
     * Modifier le nom de la rue
     * @param nomRue
     */
    @XmlElement
    public void setNomRue(String nomRue) {
        this.nomRue = nomRue;
    }

    /**
     *
     * @return la durée du parcours
     */
    public int getDureeParcours() {
        return dureeParcours;
    }

    /**
     * Modifier la durée du parcours
     * @param dureeParcours
     */
    @XmlElement
    public void setDureeParcours(int dureeParcours) {
        this.dureeParcours = dureeParcours;
    }

    /**
     *
     * @return le noeud d'origine
     */
    public Noeud getOrigine() {
        return origine;
    }

    /**
     * Modifier le noeud d'origine
     * @param origine
     */
    @XmlElement
    public void setOrigine(Noeud origine) {
        this.origine = origine;
    }

    /**
     *
     * @return le noeud d'arrivée
     */
    public Noeud getArrivee() {
        return arrivee;
    }

    /**
     * Modifier le noeud d'origine
     * @param arrivee
     */
    @XmlElement
    public void setArrivee(Noeud arrivee) {
        this.arrivee = arrivee;
    }

    /**
     *
     * @return L'objet en format Json
     */
    @Override
    public String toString() {
        return "Troncon{" +
                "id=" + id +
                ", longueur=" + longueur +
                ", nomRue='" + nomRue + '\'' +
                ", dureeParcours=" + dureeParcours +
                ", origine=" + origine +
                ", arrivee=" + arrivee +
                '}';
    }

    /**
     *
     * @param troncon
     * @return si les troncons sont égaux : true
     *                             si non : false
     */
    public Boolean isEqual(Troncon troncon) {
        return this.origine.isEqual(troncon.getOrigine())
                && this.arrivee.isEqual(troncon.getArrivee())
                && this.longueur == troncon.getLongueur()
                && this.nomRue.equals(troncon.getNomRue());
    }

    /**
     *
     * @param listeTroncons
     * @return true si le troncon est inclu dans la liste des troncon si non false
     */
    public Boolean isInTronconList(ArrayList<Troncon> listeTroncons) {
        for (int i = 0; i < listeTroncons.size(); i++) {
            if (this.isEqual(listeTroncons.get(i))) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @return un clone du troncon
     * @throws CloneNotSupportedException
     */
    @Override
    public Troncon clone() throws CloneNotSupportedException {

        Troncon cloneTroncon = (Troncon) super.clone();

        cloneTroncon.dureeParcours = dureeParcours;
        cloneTroncon.id = id;
        cloneTroncon.longueur = longueur;
        cloneTroncon.nomRue = nomRue;
        cloneTroncon.arrivee = arrivee.clone();
        cloneTroncon.origine = origine.clone();

        return cloneTroncon;
    }
}
