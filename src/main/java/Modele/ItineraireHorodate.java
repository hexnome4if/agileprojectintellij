package Modele;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ItineraireHorodate  implements Cloneable{
    private Itineraire itineraire;
    /**
     * Heure à laquelle on commence à livrer la marchandise.
     */
    private Heure heureDeLivraison;

    /**
     * Plage horaire que nous proposons pour la livraison
     */
    private PlageHoraire plageLivraison;

    /**
     * Constructeur par default
     */
    private ItineraireHorodate() {

    }

    /**
     * Constructeur ItineraireHorodate
     *
     * @param itineraire
     * @param heureDeLivraison
     * @param plageLivraison
     */
    public ItineraireHorodate(Itineraire itineraire, Heure heureDeLivraison, PlageHoraire plageLivraison) {
        this.itineraire = itineraire;
        this.heureDeLivraison = heureDeLivraison;
        this.plageLivraison = plageLivraison;
    }

    /**
     *
     * @return l'itineraire
     */
    public Itineraire getItineraire() {
        return itineraire;
    }

    /**
     * Modifier l'itineraire
     * @param  itineraire
     */
    @XmlElement
    public void setItineraire(Itineraire itineraire) {
        this.itineraire = itineraire;
    }

    /**
     *
     * @return l'heure de livraison
     */
    public Heure getHeureDeLivraison() {
        return heureDeLivraison;
    }

    /**
     * Modifier l'heure de livraison
     * @param heureDeLivraison
     */
    @XmlElement
    public void setHeureDeLivraison(Heure heureDeLivraison) {
        this.heureDeLivraison = heureDeLivraison;
    }

    /**
     *
     * @return Plage horaire proposé par le système
     */
    public PlageHoraire getPlageLivraison() {
        return plageLivraison;
    }

    /**
     * Modifier la plage horaire de l'itineraire horodaté
     * @param plageLivraison
     */
    @XmlElement
    public void setPlageLivraison(PlageHoraire plageLivraison) {
        this.plageLivraison = plageLivraison;
    }

    /**
     *
     * @return L'objet en format Json
     */
    @Override
    public String toString() {
        return "ItineraireHorodate{" +
                "itineraire=" + itineraire +
                ", heureDeLivraison=" + heureDeLivraison +
                ", plageLivraison=" + plageLivraison +
                '}';
    }

    /**
     *
     * @return un clone de l'objet
     * @throws CloneNotSupportedException
     */
    @Override
    public ItineraireHorodate clone() throws CloneNotSupportedException {

        ItineraireHorodate cloneItineraireHorodate = (ItineraireHorodate) super.clone();
        cloneItineraireHorodate.heureDeLivraison = heureDeLivraison.clone();
        cloneItineraireHorodate.itineraire = itineraire.clone();
        cloneItineraireHorodate.plageLivraison = plageLivraison.clone();

        return cloneItineraireHorodate;
    }
}
