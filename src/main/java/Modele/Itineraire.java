package Modele;

import ApplicationException.ApplicationError;
import Util.CalcAngleTroncon;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;

public class Itineraire  implements Cloneable{
    /**
     * Liste des tronçons entre les deux livraisons
     */
    private ArrayList<Troncon> listTroncon = new ArrayList<Troncon>();
    /**
     * Livraison d'origine
     */
    private DemandeLivraison livraisonOrigine;
    /**
     * Livraison d'arrivée
     */
    private DemandeLivraison livraisonArrivee;

    /**
     * Durée pour aller de l'origine à l'arrivée
     */
    private int dureeItineraire;

    /**
     *
     */
    private ArrayList<Double> angles = new ArrayList<Double>();
    /**
     *
     */
    private ArrayList<String> rueOrigine = new ArrayList<String>();
    /**
     *
     */
    private ArrayList<String> rueDestination = new ArrayList<String>();

    /**
     * Constructeur par default
     */
    private Itineraire() {
    }

    /**
     * Constructeur
     *
     * @param listTroncon
     * @param livraisonOrigine
     * @param livraisonArrivee
     * @param dureeItineraire
     */
    public Itineraire(ArrayList<Troncon> listTroncon, DemandeLivraison livraisonOrigine, DemandeLivraison livraisonArrivee, int dureeItineraire) {
        this.listTroncon = listTroncon;
        this.livraisonOrigine = livraisonOrigine;
        this.livraisonArrivee = livraisonArrivee;
        this.dureeItineraire = dureeItineraire;
    }

    /**
     *
     * @return la liste des tronçons
     */
    @XmlElementWrapper(name = "listeTroncons")
    @XmlElement(name = "troncon")
    public ArrayList<Troncon> getListTroncon() {
        return listTroncon;
    }

    /**
     * Modifier la liste des tronçons
     * @param listTroncon
     */
    public void setListTroncon(ArrayList<Troncon> listTroncon) {
        this.listTroncon = listTroncon;
    }

    /**
     *
     * @return la demande de livraison d'origine
     */
    public DemandeLivraison getLivraisonOrigine() {
        return livraisonOrigine;
    }

    /**
     * Modifier la demande de livraison d'origine
     * @param livraisonOrigine
     */
    public void setLivraisonOrigine(DemandeLivraison livraisonOrigine) {
        this.livraisonOrigine = livraisonOrigine;
    }

    /**
     *
     * @return La demande de livraison d'arrivée
     */
    public DemandeLivraison getLivraisonArrivee() {
        return livraisonArrivee;
    }

    /**
     * Modifier la livraison d'arrivée
     * @param livraisonArrivee
     */
    public void setLivraisonArrivee(DemandeLivraison livraisonArrivee) {
        this.livraisonArrivee = livraisonArrivee;
    }

    /**
     *
     * @return La durée du trajet de l'itinéraire
     */
    public int getDureeItineraire() {
        return dureeItineraire;
    }

    /**
     * Modifier la durée du trajet de l'itinéraire
     * @param dureeItineraire
     */
    public void setDureeItineraire(int dureeItineraire) {
        this.dureeItineraire = dureeItineraire;
    }


    @XmlElementWrapper(name="listeAngles")
    @XmlElement(name="angle")
    public ArrayList<Double> getAngles() {
        return angles;
    }

    public void setAngles(ArrayList<Double> angles) {
        this.angles = angles;
    }

    @XmlElementWrapper(name="listeRuesOrigine")
    @XmlElement(name="rueOrigine")
    public ArrayList<String> getRueOrigine() {
        return rueOrigine;
    }

    public void setRueOrigine(ArrayList<String> rueOrigine) {
        this.rueOrigine = rueOrigine;
    }

    @XmlElementWrapper(name="listeRuesDestination")
    @XmlElement(name="rueDestination")
    public ArrayList<String> getRueDestination() {
        return rueDestination;
    }

    public void setRueDestination(ArrayList<String> rueDestination) {
        this.rueDestination = rueDestination;
    }

    /**
     *
     * @return String L'objet en format Json
     */
    @Override
    public String toString() {
        return "Itineraire{" +
                "listTroncon=" + listTroncon +
                ", livraisonOrigine=" + livraisonOrigine +
                ", livraisonArrivee=" + livraisonArrivee +
                ", dureeItineraire=" + dureeItineraire +
                '}';
    }

    /**
     *
     * @return un clone de l'objet
     * @throws CloneNotSupportedException
     */
    @Override
    public Itineraire clone() throws CloneNotSupportedException {

        Itineraire cloneItineraire = (Itineraire) super.clone();
        cloneItineraire.listTroncon = (ArrayList<Troncon>) listTroncon.clone();
        cloneItineraire.livraisonArrivee = livraisonArrivee.clone();
        cloneItineraire.livraisonOrigine = livraisonOrigine.clone();
        cloneItineraire.dureeItineraire = dureeItineraire;
        //Angles
        cloneItineraire.angles = angles;
        cloneItineraire.rueOrigine = rueOrigine;
        cloneItineraire.rueDestination = rueDestination;

        return cloneItineraire;
    }

    /**
     * Permet de calculer l'ensemble des angles entre chaque tronçon de l'itinéraire
     * Renseigne les attributs angles,rueOrigine et rueDestination de la classe
     * @throws ApplicationError : erreur provenant de la méthode calculerAngle de la classe CalcAngleTroncon.
     */
    public void calculerAnglesItineraire() throws ApplicationError {
        if(this.listTroncon.size()>1) {
            Troncon t1;
            Troncon t2;
            String nomRueOrigine;
            String nomRueDestination;
            double angle1;
            double angle2;
            double angleTroncons;
            for (int i = 0; i < this.listTroncon.size() - 1; i++) {
                t1 = this.listTroncon.get(i);
                t2 = this.listTroncon.get(i + 1);
                nomRueOrigine = t1.getNomRue();
                nomRueDestination = t2.getNomRue();
                angle1 = CalcAngleTroncon.calculerAngle(t1);
                angle2 = CalcAngleTroncon.calculerAngle(t2);
                angleTroncons = angle2 - angle1;
                angleTroncons += 360;
                angleTroncons = angleTroncons % 360;
                if (angleTroncons > 180) {
                    angleTroncons = angleTroncons - 360;
                }

                this.angles.add(angleTroncons);
                this.rueOrigine.add(nomRueOrigine);
                this.rueDestination.add(nomRueDestination);
            }
        }
    }
}