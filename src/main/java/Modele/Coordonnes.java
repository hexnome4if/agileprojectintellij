package Modele;

public class Coordonnes implements Cloneable {
    /**
     * Coordonnée X
     */
    private int coordX;
    /**
     * Coordonnée Y
     */
    private int coordY;

    /**
     * Constructeur Coordonnes par default
     */
    private Coordonnes() {
    }

    /**
     * Constructeur Coordonnes
     *
     * @param coordX
     * @param coordY
     */
    public Coordonnes(int coordX, int coordY) {
        this.coordX = coordX;
        this.coordY = coordY;
    }

    /**
     *
     * @return coordX
     */
    public int getCoordX() {
        return coordX;
    }

    /**
     * Modifier coordX
     * @param coordX
     */
    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    /**
     * @return coordY
     */
    public int getCoordY() {
        return coordY;
    }

    /**
     * Modifier coordY
     * @param coordY
     */
    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }

    /**
     * @return l'objet en format Json
     */
    @Override
    public String toString() {
        return "Coordonnes{" +
                "coordX=" + coordX +
                ", coordY=" + coordY +
                '}';
    }

    /**
     *  Verifier que deux points sont égaux
     * @param coordonnes
     * @return true si les points sont égaux, false si non
     */
    public Boolean isEqual(Coordonnes coordonnes) {
        return this.coordX == coordonnes.getCoordX() && this.coordY == coordonnes.getCoordY();
    }

    /**
     *
     * @return un clone de l'objet
     * @throws CloneNotSupportedException
     */
    @Override
    public Coordonnes clone() throws CloneNotSupportedException {

        Coordonnes cloneCoordonnes = (Coordonnes) super.clone();

        cloneCoordonnes.coordX = coordX;
        cloneCoordonnes.coordY = coordY;

        return cloneCoordonnes;
    }

}
