package Modele;

import java.util.ArrayList;

public class CarteVille implements Cloneable{
    /**
     * La liste des tronçons
     */
    private ArrayList<Troncon> listTroncon = new ArrayList<Troncon>();

    /**
     * La liste des noeuds
     */
    private ArrayList<Noeud> listNoeud = new ArrayList<Noeud>();

    /**
     * Constructeur carte ville
     *
     * @param listTroncon
     * @param listNoeud
     */
    public CarteVille(ArrayList<Troncon> listTroncon, ArrayList<Noeud> listNoeud) {
        this.listTroncon = listTroncon;
        this.listNoeud = listNoeud;
    }

    /**
     *
     * @return la liste des tronçons
     */
    public ArrayList<Troncon> getListTroncon() {
        return listTroncon;
    }

    /**
     * Modifier la liste des tronçons
     * @param listTroncon
     */
    public void setListTroncon(ArrayList<Troncon> listTroncon) {
        this.listTroncon = listTroncon;
    }

    /**
     *
     * @return la liste des noeuds
     */
    public ArrayList<Noeud> getListNoeud() {
        return listNoeud;
    }

    /**
     * Modifier la liste des noeuds
     * @param listNoeud
     */
    public void setListNoeud(ArrayList<Noeud> listNoeud) {
        this.listNoeud = listNoeud;
    }

    /**
     *
     * @param id
     * @return le noeud ayant pour identifiant id ou null si il n'y a aucun noeud remplissant cette condition
     */
    public Noeud getNoeudByID(Long id) {

        for (Noeud n : getListNoeud()) {
            if (n.getId() == id) {
                return n;
            }
        }
        return null;
    }

    /**
     *
     * @return l'objet en format Json
     */
    @Override
    public String toString() {
        return "CarteVille{" +
                "listTroncon=" + listTroncon +
                ", listNoeud=" + listNoeud +
                '}';
    }

    /**
     *
     * @return un clone de l'objet
     * @throws CloneNotSupportedException
     */
    @Override
    public CarteVille clone() throws CloneNotSupportedException {

        CarteVille cloneCarteVille = (CarteVille) super.clone();

        cloneCarteVille.listNoeud =  (ArrayList<Noeud>) listNoeud.clone();
        cloneCarteVille.listTroncon = (ArrayList<Troncon>) listTroncon.clone();

        return cloneCarteVille;
    }
}
