package Modele;

public class DemandeLivraison implements Cloneable{
    /**
     * L'id de la classe
     */
    private static Long idClass = new Long(1);

    /**
     * L'id de l'objet
     */
    private Long id;

    /**
     * l'adresse de la livraison
     */
    private Noeud adresse;

    /**
     * la durée durant laquelle le livreur livre
     */
    private int dureeLivraisonSec;

    /**
     * Plage de livraison donnée par le client
     */
    private PlageHoraire plageLivraison;

    /**
     * Constructeur par default
     */
    protected DemandeLivraison() {
    }

    /**
     * Constructeur DemandeLivraison sans gestion de l'id
     *
     * @param adresse
     * @param dureeLivraisonSec
     * @param plageLivraison
     */
    public DemandeLivraison(Noeud adresse, int dureeLivraisonSec, PlageHoraire plageLivraison) {
        this.id = idClass;
        this.idClass++;
        this.adresse = adresse;
        this.dureeLivraisonSec = dureeLivraisonSec;
        this.plageLivraison = plageLivraison;
    }

    /**
     * Constructeur DemandeLivraison
     * @param id
     * @param adresse
     * @param dureeLivraisonSec
     * @param plageLivraison
     */
    public DemandeLivraison(Long id, Noeud adresse, int dureeLivraisonSec, PlageHoraire plageLivraison) {
        this.id = id;
        if (id > this.idClass) {
            this.idClass = id;
        }
        this.adresse = adresse;
        this.dureeLivraisonSec = dureeLivraisonSec;
        this.plageLivraison = plageLivraison;
    }

    /**
     * Reset l'IdClass à 1
     */
    public static void resetIdClass() {
        idClass = 1L;
    }

    /**
     *
     * @return l'id de la class
     */
    public Long getIdClass() {
        return idClass;
    }

    /**
     *
     * @return l'id de l'objet
     */
    public Long getId() {
        return id;
    }

    /**
     * Modifier l'id de l'objet
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return l'adresse
     */
    public Noeud getAdresse() {
        return adresse;
    }

    /**
     * Modifier l'adresse de l'objet
     *
     * @param adresse
     */
    public void setAdresse(Noeud adresse) {
        this.adresse = adresse;
    }

    /**
     *
     * @return la plage de livraison
     */
    public PlageHoraire getPlageLivraison() {
        return plageLivraison;
    }

    /**
     * modifer la plage de livraison
     *
     * @param plageLivraison
     */
    public void setPlageLivraison(PlageHoraire plageLivraison) {
        this.plageLivraison = plageLivraison;
    }

    /**
     *
     * @return durée de la livraison en secondes
     */
    public int getDureeLivraisonSec() {
        return dureeLivraisonSec;
    }

    /**
     * Modifier la durée de livraison en secondes
     * @param dureeLivraisonSec
     */
    public void setDureeLivraisonSec(int dureeLivraisonSec) {
        this.dureeLivraisonSec = dureeLivraisonSec;
    }

    /**
     *
     * @return l'objet en format Json
     */
    @Override
    public String toString() {
        return "DemandeLivraison{" +
                "id=" + id +
                ", adresse=" + adresse +
                ", dureeLivraisonSec=" + dureeLivraisonSec +
                ", plageLivraison=" + plageLivraison +
                '}';
    }

    /**
     *
     * @return un clone de l'objet
     * @throws CloneNotSupportedException
     */
    @Override
    public DemandeLivraison clone() throws CloneNotSupportedException {

        DemandeLivraison cloneDemandeLivraison = (DemandeLivraison) super.clone();

        cloneDemandeLivraison.adresse = adresse.clone();
        cloneDemandeLivraison.plageLivraison = plageLivraison.clone();
        cloneDemandeLivraison.id = id;
        cloneDemandeLivraison.dureeLivraisonSec = dureeLivraisonSec;

        return cloneDemandeLivraison;
    }
}
