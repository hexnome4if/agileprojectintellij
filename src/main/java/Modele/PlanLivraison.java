package Modele;

import ApplicationException.ApplicationError;

import java.util.ArrayList;

public class PlanLivraison implements Cloneable{
    /**
     * Liste des demandes de livraison
     */
    private ArrayList<DemandeLivraison> listeLivraison = new ArrayList<DemandeLivraison>();
    /**
     * L'entrepot
     */
    private Entrepot entrepot;

    /**
     * Constructeur PlanLivraison
     *
     * @param listeLivraison
     * @param entrepot
     */
    public PlanLivraison(ArrayList<DemandeLivraison> listeLivraison, Entrepot entrepot) {
        this.listeLivraison = listeLivraison;
        this.entrepot = entrepot;
    }

    /**
     *
     * @return la liste des livraison
     */
    public ArrayList<DemandeLivraison> getListeLivraison() {
        return listeLivraison;
    }

    /**
     * Modifier la liste des livraisons
     * @param listeLivraison
     */
    public void setListeLivraison(ArrayList<DemandeLivraison> listeLivraison) {
        this.listeLivraison = listeLivraison;
    }

    /**
     *
     * @return l'entrepot
     */
    public Entrepot getEntrepot() {
        return entrepot;
    }

    /**
     * Modifier l'entrepot
     * @param entrepot
     */
    public void setEntrepot(Entrepot entrepot) {
        this.entrepot = entrepot;
    }

    /**
     * Ajouter une demande de livraison
     * @param demandeAAjouter demande de livraison à ajouter
     */
    public void ajouterDemandeLivraison(DemandeLivraison demandeAAjouter) {

        this.listeLivraison.add(demandeAAjouter);
    }

    /**
     * Modifier la demande de livraison
     * @param demandeAModifier demande de livraison à modifier
     * @throws ApplicationError
     */
    public void modifierDemandeLivraison(DemandeLivraison demandeAModifier) throws ApplicationError {
        supprimerDemandeLivraison(demandeAModifier);
        ajouterDemandeLivraison(demandeAModifier);
    }

    /**
     * Supprimer la demande de livraison
     * @param demandeASupprimer demande de livraison à supprimer
     * @throws ApplicationError
     */
    public void supprimerDemandeLivraison(DemandeLivraison demandeASupprimer) throws ApplicationError {
        listeLivraison.remove(findIndexOf(demandeASupprimer));
    }

    /**
     * Trouvez l'indice de livraison
     * @param demandeATrouver
     * @return true si la demande de livraison a été trouvé, false si non
     * @throws ApplicationError
     */
    public int findIndexOf(DemandeLivraison demandeATrouver) throws ApplicationError {
        for (DemandeLivraison demandeCourant : listeLivraison) {
            if (demandeCourant.getId() == demandeATrouver.getId()) {
                //La demande a été trouvée
                return listeLivraison.indexOf(demandeCourant);
            }
        }
        return 0;
    }

    /**
     *
     * @return L'objet en format Json
     */
    @Override
    public String toString() {
        return "PlanLivraison{" +
                "listeLivraison=" + listeLivraison +
                ", entrepot=" + entrepot +
                '}';
    }

    /**
     *
     * @return un clone de l'objet
     * @throws CloneNotSupportedException
     */
    @Override
    public PlanLivraison clone() throws CloneNotSupportedException {

        PlanLivraison clonePlanLivraison = (PlanLivraison) super.clone();

        clonePlanLivraison.entrepot = entrepot.clone();
        clonePlanLivraison.listeLivraison = (ArrayList<DemandeLivraison>) listeLivraison.clone();

        return clonePlanLivraison;
    }
}
