package Modele;

import Logger.InternalLogger;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Tournee implements Cloneable {
    /**
     * Liste des itineraires horodatees
     */
    private ArrayList<ItineraireHorodate> listeTournee = new ArrayList<ItineraireHorodate>();
    /**
     * Durée totale de la tournée
     */
    private int dureeTournee;

    /**
     * Constructeur par default
     */
    private Tournee() {
    }

    /**
     * Constructeur
     *
     * @param listeTournee
     */
    public Tournee(ArrayList<ItineraireHorodate> listeTournee) {
        this.listeTournee = listeTournee;
        calculDureeTournee();
    }

    /**
     *
     * @return la liste des itineraires horodates
     */
    @XmlElementWrapper(name = "listeTournee")
    @XmlElement(name = "ItineraireHorodate")
    public List<ItineraireHorodate> getListeTournee() {
        return listeTournee;
    }

    /**
     * Modifier la liste des itineraires horodates
     * @param listeTournee
     */
    public void setListeTournee(ArrayList<ItineraireHorodate> listeTournee) {
        this.listeTournee = listeTournee;
        calculDureeTournee();
    }

    /**
     *
     * @return la durée de la tournee
     */
    @XmlElement
    public int getDureeTournee() {
        return dureeTournee;
    }

    private void calculDureeTournee(){
        //Heure de fin - Heure de départ
        if(listeTournee.size() > 0){
            dureeTournee = listeTournee.get(listeTournee.size()-1).getHeureDeLivraison().getTotalSeconds() - listeTournee.get(0).getItineraire().getLivraisonOrigine().getPlageLivraison().getHeureDeb().getTotalSeconds();
        } else {
            dureeTournee = 0;
        }

    }

    /**
     *
     * @param id l'id d'une demande de livraison
     * @return la demande de livraison ayant pour identifiant id
     */
    public DemandeLivraison getDemandeByID(Long id) {
        for (ItineraireHorodate i : listeTournee) {
            if (i.getItineraire().getLivraisonArrivee().getId().equals(id)) {
                return i.getItineraire().getLivraisonArrivee();
            }
        }
        InternalLogger.getInstance().logWarning("Pas de demande trouvée avec cet ID : " + id.toString() + ".", this);
        return null;
    }

    /**
     *
     * @param idCurrentDemande
     * @return l'id de la demande précédente
     */
    public Long getPreviousDemandeLivraisonById(Long idCurrentDemande) {

        // ***** On cherche la la demande de livraison qui était avant la livraison retirée *****
        for (ItineraireHorodate iti : this.getListeTournee()) {

            //On trouve la livraison d'arrivée de l'itinéraire qui nous intéresse
            if (iti.getItineraire().getLivraisonArrivee().getId() == idCurrentDemande) {
                //On récupère la livraison qui était l'origine
                return iti.getItineraire().getLivraisonOrigine().getId();
            }
        }
        InternalLogger.getInstance().logWarning("Pas de demande trouvée avec cet ID : " + idCurrentDemande.toString() + ".", this);
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Tournee{" +
                "listeTournee=" + listeTournee +
                "dureeTournee=" + dureeTournee +
                '}';
    }

    /**
     *
     * @return un clone de la tournee
     * @throws CloneNotSupportedException
     */
    @Override
    public Tournee clone() throws CloneNotSupportedException {
        Tournee cloneTournee = (Tournee) super.clone();
        cloneTournee.dureeTournee = dureeTournee;
        cloneTournee.listeTournee = (ArrayList<ItineraireHorodate> ) listeTournee.clone();
        return cloneTournee;
    }

    /**
     * Remplacer l'itineraire horodate à l'Index index par les 2 itinéraires horodates
     * @param index
     * @param itineraireHorodateA
     * @param itineraireHorodateB
     * @return la liste des itineraires horodates avec l'ajout
     */
    public ArrayList<ItineraireHorodate> ajouterItineraireHorodatee(int index, ItineraireHorodate itineraireHorodateA, ItineraireHorodate itineraireHorodateB) {
        ArrayList<ItineraireHorodate> newListeTournee = new ArrayList<>();

        for (int i = 0; i < index - 1; i++) {
            newListeTournee.add(this.listeTournee.get(i));
        }
        newListeTournee.add(itineraireHorodateA);
        newListeTournee.add(itineraireHorodateB);

        for (int i = index; i < listeTournee.size(); i++) {
            newListeTournee.add(this.listeTournee.get(i));
        }
        this.listeTournee = newListeTournee;
        return newListeTournee;
    }
}
