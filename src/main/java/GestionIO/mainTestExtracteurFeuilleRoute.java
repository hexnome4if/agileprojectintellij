package GestionIO;

import Algo.Dijkstra;
import Algo.TSP_BnB_2;
import Modele.CarteVille;
import Modele.CarteVilleSimplifiee;
import Modele.PlanLivraison;
import Modele.Tournee;

public class mainTestExtracteurFeuilleRoute {
    public static void main(String[] args) {
        CarteVilleSimplifiee cvs_small;
        TSP_BnB_2 tsp_small_2;

        String cheminFichierCarte = "./src/main/resources/InputData/planLyonMoyen.xml";
        String cheminFichierPlanLivraison = "./src/main/resources/InputData/DLmoyen5TW1.xml";

        ParseurPlanXML p_plan = new ParseurPlanXML();
        ParseurDemandeXML p_demande = new ParseurDemandeXML();
        p_plan.setFichierPlan(cheminFichierCarte);
        p_demande.setFichierDemande(cheminFichierPlanLivraison);
        try {
            CarteVille cv = p_plan.recupererPlanVille();
            PlanLivraison pl = p_demande.recupererPlanLivraison(cv);

            Dijkstra dijkstra_small = new Dijkstra(cv, pl);
            cvs_small = dijkstra_small.CalculItineraires();

            tsp_small_2 = new TSP_BnB_2(cvs_small);
            tsp_small_2.chercheSolution();
            Tournee tournee = tsp_small_2.getSolution();

            for(int i = 0;i<tournee.getListeTournee().size();i++){
                tournee.getListeTournee().get(i).getItineraire().calculerAnglesItineraire();
            }

            ExtracteurFeuilleRoute extract = new ExtracteurFeuilleRoute();
            extract.creerFeuilleRoute(tournee);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
