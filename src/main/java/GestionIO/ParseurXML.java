package GestionIO;

import Modele.CarteVille;
import Modele.PlanLivraison;


/**
 * Classe qui permet de récupérer la carte de la ville et le plan de livraison à partir
 * de deux fichiers XML qui seront parsés.
 */
public class ParseurXML {
    private ParseurPlanXML parseurPlan;
    private ParseurDemandeXML parseurDemande;

    public ParseurXML(){
        parseurPlan = new ParseurPlanXML();
        parseurDemande = new ParseurDemandeXML();
    }

    /**
     * Permet de charger le chemin du plan de la ville.
     *
     * @param cheminFichier : chemin absolu ou relatif du fichier contenant le plan de la ville.
     */
    public void chargerFichierPlan(String cheminFichier){
        parseurPlan.setFichierPlan(cheminFichier);
    }

    /**
     * Permet de charger le chemin du plan de livraison.
     *
     * @param cheminFichier : chemin absolu ou relatif du fichier contenant le plan de livraison.
     */
    public void chargerFichierDemande(String cheminFichier){
        parseurDemande.setFichierDemande(cheminFichier);
    }

    /**
     * Permet de parser le fichier XML renseigné précedemment à l'aide de la méthode
     * {@link ParseurXML#chargerFichierPlan(String)}.
     *
     * @return la carte ville contenant la liste des noeuds et des tronçons.
     * @throws Exception : renvoie une exception de type personnalise dans le
     *                              cas où une erreur survienne lors du parsing du fichier.
     */
    public CarteVille obtenirCarteVille() throws Exception {
        return parseurPlan.recupererPlanVille();
    }

    /**
     * Permet de parser le fichier XML renseigné précedemment à l'aide de la méthode
     * {@link ParseurXML#chargerFichierDemande(String)}.
     * @param carte : carteVille récupéré grâce à la méthode {@link ParseurXML#obtenirCarteVille()}.
     * @return le plan de livraison contenant la liste des demandes de livraison
     * et l'entrepôt.
     * @throws Exception : renvoie une exception de type personnalisé dans le
     * cas où une erreur survienne lors du parsing du fichier.
     */
    public PlanLivraison obtenirPlanLivraison(CarteVille carte) throws Exception {

        return parseurDemande.recupererPlanLivraison(carte);
    }

}

