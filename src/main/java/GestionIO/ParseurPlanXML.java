package GestionIO;

import ApplicationException.ApplicationError;
import ApplicationException.ApplicationException;
import Logger.InternalLogger;
import Modele.CarteVille;
import Modele.Coordonnes;
import Modele.Noeud;
import Modele.Troncon;
import Util.TimeLengthCalc;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

/**
 * Classe qui implémente l'algorithme de parsing d'un fichier représentant une carte d'une ville.
 *
 */
public class ParseurPlanXML {
    private String fichier;

    public ParseurPlanXML(){
        fichier = "";
    }

    /**
     * Permet de charger le chemin du plan de la ville.
     *
     * @param fichierPlan : chemin absolu ou relatif du fichier contenant le plan de la ville.
     */
    public void setFichierPlan(String fichierPlan){
        this.fichier = fichierPlan;
    }

    /**
     * Permet de parser le fichier XML renseigné précedemment à l'aide de la méthode
     * {@link ParseurPlanXML#setFichierPlan(String)}.
     *
     * @return la carte ville contenant la liste des noeuds et des tronçons.
     * @throws ApplicationException : renvoie une exception de type personnalisé dans le
     *                              cas où une erreur survienne lors du parsing du fichier.
     */
    public CarteVille recupererPlanVille() throws ApplicationException {
        CarteVille carte = null;
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        final Document document;
        try {
            final DocumentBuilder builder = factory.newDocumentBuilder();
            // Parsing du fichier à parser
            document = builder.parse(new File(fichier));

            //Récupère la version de XML
            String version = document.getXmlVersion();

            //Récupère l'encodage
            String encodage = document.getXmlEncoding();

            //Récupère s'il s'agit d'un document standalone
            Boolean standalone = document.getXmlStandalone();

            if(!version.equals("1.0") || !encodage.equals("UTF-8")  || standalone){
                throw new ApplicationError("Fichier carteVille invalide.");
            }

            final Element racine = document.getDocumentElement();

            if (racine.getNodeName() != "reseau") {
                throw new ApplicationError("Fichier carteVille : Le fichier XML est mal formé.");
            }

            final NodeList filsRacine = racine.getChildNodes();

            Set<Noeud> setNoeuds = recupererNoeudCarte(filsRacine);

            ArrayList listeNoeuds = new ArrayList(setNoeuds);

            ArrayList<Troncon> listeTroncons = recupererTronconsCarte(filsRacine,listeNoeuds);

            carte = new CarteVille(listeTroncons,listeNoeuds);
        } catch (final ParserConfigurationException e) {
            throw new ApplicationError("Fichier carteVille : Le fichier n'est pas un fichier XML bien formé.");
        } catch (final SAXException e) {
            throw new ApplicationError("Fichier carteVille : Le fichier n'est pas un fichier XML bien formé.");
        } catch (final IOException e) {
            throw new ApplicationError("Fichier carteVille : Le fichier n'est pas un fichier XML bien formé.");
        }

        return carte;
    }

    /**
     * Permet de récupérer le set des noeuds.
     *
     * @param filsRacine : noeuds à traiter.
     * @return un set contenant les noeuds.
     * @throws ApplicationException : renvoie une exception de type personnalisé dans le
     *                              cas où une erreur survienne lors du parsing du fichier.
     */
    protected Set<Noeud> recupererNoeudCarte(final NodeList filsRacine) throws ApplicationException {
        final int nbFilsRacine = filsRacine.getLength();

        Set<Noeud> setNoeuds = new TreeSet<Noeud>((Noeud a, Noeud b) -> {
            if (a.getId() < b.getId()) {
                return -1;
            } else if (a.getId() == b.getId()) {
                return 0;
            } else {
                return 1;
            }
        }
        );
        int taillePrecedente;
        for (int i = 0; i < nbFilsRacine; i++) {
            taillePrecedente = setNoeuds.size();
            if (filsRacine.item(i).getNodeType() == Node.ELEMENT_NODE) {
                final Element fils = (Element) filsRacine.item(i);
                if (fils.getNodeName() == "noeud") {
                    if(fils.getAttribute("id")!="" || fils.getAttribute("x") != "" || fils.getAttribute("y") != "") {
                        long id;
                        try {
                            id = Long.parseLong((fils.getAttribute("id")));
                        } catch (Exception e) {
                            throw new ApplicationError("Fichier carteVille invalide : l'id d'un noeud est absent ou invalide.");
                        }

                        if (id < 0.0) {
                            throw new ApplicationError("Fichier carteVille invalide : un id de noeud ne peut pas être négatif.");
                        }

                        int x;
                        try {
                            x = Integer.parseInt(fils.getAttribute("x"));
                        } catch (Exception e) {
                            throw new ApplicationError("Fichier carteVille invalide : la coordonnée x d'un noeud est absente ou invalide.");
                        }

                        int y;
                        try {
                            y = Integer.parseInt(fils.getAttribute("y"));
                        } catch (Exception e) {
                            throw new ApplicationError("Fichier carteVille invalide : la coordonnée y d'un noeud est absente ou invalide.");
                        }

                        Coordonnes coordonnees = new Coordonnes(y, -x);

                        Noeud noeud = new Noeud(id, coordonnees);
                        setNoeuds.add(noeud);
                        if (setNoeuds.size() == taillePrecedente) {
                            InternalLogger.getInstance().logWarning("Attention : doublon de noeud détecté.",this);
                        }
                    }
                }
            }
        }

        return setNoeuds;
    }

    /**
     * Permet de récupérer la liste des tronçons.
     * @param filsRacine : noeuds à traiter.
     * @return la liste des tronçons.
     * @throws ApplicationException : renvoie une exception de type personnalisé dans le
     * cas où une erreur survienne lors du parsing du fichier.
     */
    protected ArrayList<Troncon> recupererTronconsCarte(final NodeList filsRacine, ArrayList<Noeud> listeNoeuds) throws ApplicationException {
        final int nbFilsRacine = filsRacine.getLength();

        Long j=new Long(1);
        ArrayList<Troncon> listeTroncons = new ArrayList<Troncon>();

        for (int i = 0; i<nbFilsRacine; i++) {
            if (filsRacine.item(i).getNodeType() == Node.ELEMENT_NODE) {
                final Element fils = (Element) filsRacine.item(i);
                if (fils.getNodeName() == "troncon") {
                    // test si les attributs de la balise tronçon existent à faire
                    if(fils.getAttribute("longueur")!="" || fils.getAttribute("origine") != "" || fils.getAttribute("destination") != "" || fils.getAttribute("nomRue") != "") {
                        double longueur;
                        try {
                            longueur = Double.parseDouble(fils.getAttribute("longueur"));
                        } catch (Exception e) {
                            throw new ApplicationError("Fichier carteVille invalide : la longueur d'un troncon est absent ou invalide.");
                        }

                        TimeLengthCalc timeCalc = new TimeLengthCalc();
                        double dureeParcours = timeCalc.calculDureeUniforme(longueur);

                        long idOrigine;
                        try {
                            idOrigine = Long.parseLong(fils.getAttribute("origine"));
                        } catch (Exception e) {
                            throw new ApplicationError("Fichier carteVille invalide : l'origine d'un troncon est invalide ou non renseignée.");
                        }

                        long idDestination;
                        try {
                            idDestination = Long.parseLong(fils.getAttribute("destination"));
                        } catch (Exception e) {
                            throw new ApplicationError("Fichier carteVille invalide : la destination d'un troncon est invalide ou non renseignée.");
                        }

                        if(idOrigine<0.0 || idDestination<0.0){
                            throw new ApplicationError("Fichier carteVille invalide : id de noeuds d'un tronçon négatif.");
                        }else if(idOrigine!=idDestination) {
                            Noeud origine = null;
                            Noeud destination = null;
                            Boolean noeudOrigineTrouve = false;
                            Boolean noeudDestinationTrouve = false;
                            for (Noeud n : listeNoeuds) {
                                if (n.getId() == idOrigine) {
                                    origine = n;
                                    noeudOrigineTrouve = true;
                                } else if (n.getId() == idDestination) {
                                    destination = n;
                                    noeudDestinationTrouve = true;
                                } else if (noeudOrigineTrouve && noeudDestinationTrouve) {
                                    break;
                                }
                            }
                            if (!noeudOrigineTrouve || !noeudDestinationTrouve) {
                                throw new ApplicationError("Fichier carteVille invalide : un troncon référence des noeuds non existants.");

                            }

                            String nomRue = fils.getAttribute("nomRue");

                            Troncon troncon = new Troncon(j, longueur, nomRue, (int) dureeParcours, origine, destination);
                            if(troncon.isInTronconList(listeTroncons)){
                                InternalLogger.getInstance().logWarning("Fichier carteVille : Doublon de tronçon détecté", this);
                            }
                            listeTroncons.add(troncon);

                            j++;
                        }
                    }
                }
            }
        }
        return listeTroncons;
    }
}