package GestionIO;

import ApplicationException.ApplicationError;
import ApplicationException.ApplicationException;
import Modele.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Classe qui implémente l'algorithme de parsing d'un fichier de demandes de livraisons.
 *
 */
public class ParseurDemandeXML {
    private String fichier;

    public ParseurDemandeXML(){
        this.fichier="";
    }

    public void setFichierDemande(String fichierDemande){
        this.fichier=fichierDemande;
    }

    /**
     * Permet de parser le fichier XML renseigné précedemment à l'aide de la méthode
     * {@link ParseurDemandeXML#setFichierDemande(String)}.
     *
     * @return le plan de livraison contenant la liste des livraisons a effectuer et l'entrepôt de départ de la tournée
     * de livraison.
     * @throws ApplicationException : renvoie une exception de type personnalisé dans le
     *                              cas où une erreur survienne lors du parsing du fichier.
     */
    public PlanLivraison recupererPlanLivraison(CarteVille carte) throws ApplicationException {
        if (carte == null || carte.getListNoeud() == null) {
            throw new ApplicationError("La carteVille en entrée du traitement de la liste des livraison est invalide.");
        }

        PlanLivraison planLivraison = null;
        ArrayList<DemandeLivraison> demandesLivraison = null;
        Entrepot entrepot = null;
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        final Document document;
        try {
            final DocumentBuilder builder = factory.newDocumentBuilder();
            // Parsing du fichier à parser
            document = builder.parse(new File(fichier));

            //Récupère la version de XML
            String version = document.getXmlVersion();

            //Récupère l'encodage
            String encodage = document.getXmlEncoding();

            //Récupère s'il s'agit d'un document standalone
            Boolean standalone = document.getXmlStandalone();

            if(!version.equals("1.0") || !encodage.equals("UTF-8")  || standalone){
                throw new ApplicationError("Fichier planLivraison invalide.");
            }

            final Element racine = document.getDocumentElement();

            if (racine.getNodeName() != "demandeDeLivraisons") {
                throw new ApplicationError("Fichier planLivraison : Le fichier XML est mal formé.");
            }
            final NodeList filsRacine = racine.getChildNodes();

            entrepot = extraireEntrepot(filsRacine,carte);

            demandesLivraison = extraireDemandesLivraison(filsRacine,carte);

            planLivraison = new PlanLivraison(demandesLivraison,entrepot);

        }
        catch (final ParserConfigurationException e) {
            throw new ApplicationError("Fichier planLivraison : Le fichier n'est pas un fichier XML bien formé.");
        }
        catch (final SAXException e) {
            throw new ApplicationError("Fichier planLivraison : Le fichier n'est pas un fichier XML bien formé.");
        }
        catch (final IOException e) {
            throw new ApplicationError("Fichier planLivraison : Le fichier n'est pas un fichier XML bien formé.");
        }

        return planLivraison;
    }

    /**
     * Permet de récupérer l'entrepot servant de départ et arrivée de la tournée.
     *
     * @param filsRacine : noeuds à traiter.
     * @param carte : carte de la ville servant de repère pour les livraisons à faire
     * @return un Entrepot avec l'ensemble de ses informations.
     * @throws ApplicationException : renvoie une exception de type personnalisé dans le
     *                              cas où une erreur survienne lors du parsing du fichier.
     */
    private Entrepot extraireEntrepot(final NodeList filsRacine, CarteVille carte) throws ApplicationException {
        final int nbFilsRacine = filsRacine.getLength();

        Entrepot entrepot = null;
        Boolean entrepotTrouve = false;
        for (int i = 0; i<nbFilsRacine; i++) {
            if (filsRacine.item(i).getNodeType() == Node.ELEMENT_NODE) {
                final Element fils = (Element) filsRacine.item(i);
                if (fils.getNodeName() == "entrepot") {
                    if(entrepotTrouve){
                        throw new ApplicationError("Fichier planLivraison invalide : l'entrepot n'est pas unique.");
                    }else{
                        Noeud noeudEntrepot = null;
                        long idNoeud;
                        try {
                            idNoeud = Long.parseLong(fils.getAttribute("adresse"));
                        } catch (Exception e) {
                            throw new ApplicationError("Fichier planLivraison invalide : l'id de l'entrepôt est manquant ou invalide.");
                        }

                        if (idNoeud < 0.0) {
                            throw new ApplicationError("Fichier planLivraison invalide : l'id de l'entrepôt ne peut pas être négatif.");
                        }

                        ArrayList<Noeud> noeudsCarte = carte.getListNoeud();
                        for(int j=0;j<noeudsCarte.size();j++){
                            if(noeudsCarte.get(j).getId()==idNoeud){
                                noeudEntrepot=noeudsCarte.get(j);
                                break;
                            }
                        }
                        if(noeudEntrepot==null){
                            throw new ApplicationError("Fichier planLivraison invalide : entrepot situé à un endroit inconnu.");
                        }

                        PlageHoraire plageDebut = null;
                        String heureDepart = fils.getAttribute("heureDepart");
                        if(heureDepart==""){
                            throw new ApplicationError("Fichier planLivraison invalide : heure de départ de l'entrepot non définie.");
                        }else {
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                            try {
                                sdf.parse(heureDepart);
                            } catch (ParseException e) {
                                throw new ApplicationError("Fichier planLivraison invalide : format de l'heure de départ de l'entrepôt incorrect.");
                            }
                            String separateur = ":";
                            String[] heureDep = heureDepart.split(separateur);
                            int hDepart = Integer.parseInt(heureDep[0]);
                            int mDepart = Integer.parseInt(heureDep[1]);
                            int sDepart = Integer.parseInt(heureDep[2]);
                            Heure heureDebut = new Heure(hDepart, mDepart, sDepart);

                            plageDebut = new PlageHoraire(heureDebut, Heure.FIN_JOURNEE);
                        }

                        entrepot = new Entrepot(0L, noeudEntrepot, 0, plageDebut);

                    }
                }
            }
        }
        if (entrepot == null) {
            throw new ApplicationError("Fichier planLivraison invalide : entrepôt non renseigné.");
        }
        return entrepot;
    }

    /**
     * Permet de récupérer la liste des demandes de livraisons.
     *
     * @param filsRacine : noeuds à traiter.
     * @param carte : carte de la ville servant de repère pour les livraisons à faire
     * @return une liste de livraisons avec l'ensemble de leurs informations.
     * @throws ApplicationException : renvoie une exception de type personnalisé dans le
     *                              cas où une erreur survienne lors du parsing du fichier.
     */
    private ArrayList<DemandeLivraison> extraireDemandesLivraison(final NodeList filsRacine, CarteVille carte) throws ApplicationException {
        final int nbFilsRacine = filsRacine.getLength();

        ArrayList<DemandeLivraison> demandesLivraison = new ArrayList<DemandeLivraison>();

        // On reset l'id des demandes de livraison à 2 (affecté automatiquement)
        DemandeLivraison.resetIdClass();

        ArrayList<Noeud> noeudsCarte = carte.getListNoeud();
        for (int i = 0; i<nbFilsRacine; i++) {
            if (filsRacine.item(i).getNodeType() == Node.ELEMENT_NODE) {
                final Element fils = (Element) filsRacine.item(i);
                if (fils.getNodeName() == "livraison") {
                    Long duree = null;
                    if(fils.getAttribute("duree")==""){
                        throw new ApplicationError("Fichier planLivraison invalide : durée de livraison manquante.");
                    }else {
                        try {
                            duree = Long.valueOf(fils.getAttribute("duree"));
                        } catch (Exception e) {
                            throw new ApplicationError("Fichier planLivraison invalide : format de la durée de livraison invalide.");
                        }
                        if(duree<0L){
                            throw new ApplicationError("Fichier planLivraison invalide : la durée de livraison ne peut pas être négative.");
                        }
                    }

                    Noeud noeudLivraison = null;

                    long idNoeud;
                    try {
                        idNoeud = Long.parseLong(fils.getAttribute("adresse"));
                    } catch (Exception e) {
                        throw new ApplicationError("Fichier planLivraison invalide : adresse de livraison manquante ou invalide.");
                    }

                    for(int j=0;j<noeudsCarte.size();j++){
                        if(noeudsCarte.get(j).getId()==idNoeud){
                            noeudLivraison=noeudsCarte.get(j);
                            break;
                        }
                    }
                    if(noeudLivraison==null){
                        throw new ApplicationError("Fichier planLivraison invalide : adresse de livraison inconnue.");
                    }

                    PlageHoraire plageHoraire;
                    String plageDebut = fils.getAttribute("debutPlage");
                    String plageFin = fils.getAttribute("finPlage");
                    Heure heureDebut = Heure.DEBUT_JOURNEE;
                    Heure heureFin = Heure.FIN_JOURNEE;
                    String separateur = ":";
                    if(plageDebut!=""){
                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                        try {
                            sdf.parse(plageDebut);
                        } catch (ParseException e) {
                            throw new ApplicationError("Fichier planLivraison invalide : format de l'heure de départ de la livraison incorrect.");
                        }
                        String[] departLivraison = plageDebut.split(separateur);
                        int hDebut = Integer.parseInt(departLivraison[0]);
                        int mDebut = Integer.parseInt(departLivraison[1]);
                        int sDebut = Integer.parseInt(departLivraison[2]);
                        heureDebut = new Heure(hDebut,mDebut,sDebut);
                    }
                    if(plageFin!=""){
                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                        try {
                            sdf.parse(plageFin);
                        } catch (ParseException e) {
                            throw new ApplicationError("Fichier planLivraison invalide : format de l'heure de fin de la livraison incorrect.");
                        }
                        String[] finLivraison = plageFin.split(separateur);
                        int hFin = Integer.parseInt(finLivraison[0]);
                        int mFin = Integer.parseInt(finLivraison[1]);
                        int sFin = Integer.parseInt(finLivraison[2]);
                        heureFin = new Heure(hFin,mFin,sFin);
                    }

                    if(!heureDebut.isBefore(heureFin)){
                        throw new ApplicationError("Fichier planLivraison invalide : livraison avec heure de fin antérieure à heure de début.");
                    }
                    plageHoraire = new PlageHoraire(heureDebut, heureFin);

                    DemandeLivraison demandeLivraison = new DemandeLivraison(noeudLivraison, (int) (long) duree, plageHoraire);

                    demandesLivraison.add(demandeLivraison);
                }
            }
        }

        return demandesLivraison;
    }
}
