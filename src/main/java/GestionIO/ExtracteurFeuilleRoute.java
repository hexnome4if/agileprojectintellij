package GestionIO;

import Modele.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Classe qui implémente la création d'une feuille de route.
 *
 */
public class ExtracteurFeuilleRoute {

    private String fichierExport;

    public ExtracteurFeuilleRoute(){
        fichierExport="";
    }


    /**
     * Permet de créer le fichier XML d'export d'une tournée, renseigné précedemment à l'aide de la méthode
     * {@link ExtracteurFeuilleRoute#setFichierExport(String)}.
     * @param tournee
     */
    public void creerFeuilleRoute(Tournee tournee){
        String xslUrl = "./FDRStylesheet.xslt";
        String stringFichier;
        if(this.fichierExport=="") {
            Date date = new Date();
            SimpleDateFormat dt = new SimpleDateFormat("dd-MM-yyyy_hh-mm-ss");
            stringFichier = dt.format(date);
            stringFichier = stringFichier.replace(":", "-") + ".xml";
        }else{
            stringFichier=this.fichierExport;
        }

        try {
            JAXBContext context = JAXBContext.newInstance(Tournee.class);
            FileWriter fw = new FileWriter(stringFichier);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE); // To format XML
            marshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders",
                    "<?xml-stylesheet type='text/xsl' href=\"" +
                            xslUrl +
                            "\"?>");
            marshaller.marshal(tournee, fw);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de définir un nom pour le fichier d'export d'une tournée.
     * @param fichierExport
     */
    public void setFichierExport(String fichierExport){
        if(fichierExport.contains(".xml")){
            this.fichierExport=fichierExport;
        }else{
            this.fichierExport=fichierExport+".xml";
        }
    }


    /**
     * @return le nom de fichier a donner aux futurs exports si il n'est pas modifié.
     */
    public String getFichierExport(){
        return this.fichierExport;
    }
}
