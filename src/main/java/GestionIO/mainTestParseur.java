package GestionIO;

import Modele.CarteVille;
import Modele.PlanLivraison;

public class mainTestParseur {
    public static void main(String[] args) {
        ParseurXML p = new ParseurXML();
        System.out.println("debut parsing Carte de la ville : ");
        p.chargerFichierPlan("./src/main/resources/InputData/planLyonMoyen.xml");
        CarteVille carte = null;
        try {
            carte = p.obtenirCarteVille();
            System.out.println("nb tronçons = "+carte.getListTroncon().size());
            System.out.println("nb noeuds = "+carte.getListNoeud().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(carte.toString());

        System.out.println("debut parsing Plan Livraison : ");
        System.out.println();
        p.chargerFichierDemande("./src/main/resources/InputData/DLmoyen10TW3.xml");
        PlanLivraison planLivraison=null;
        try{
            planLivraison = p.obtenirPlanLivraison(new CarteVille(null,carte.getListNoeud()));
            System.out.println(planLivraison.getEntrepot().toString());
            System.out.println();
            System.out.println(planLivraison.getListeLivraison().toString());
            System.out.println("planLivraison"+planLivraison.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
