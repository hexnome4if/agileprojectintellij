package JFX;

import ControleurVue.ControleurFenetre;
import Controller.Controller;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainJFX extends Application {

    /** Permet de lancer l'application, et de générer la première fenêtre, depuis le contrôleur principal
     * @param stage : argument de lancement (inconnu)
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        Controller ctrl = new Controller();
        ControleurFenetre.setControleurPrincipal(ctrl);
        ctrl.launchApplication();
    }

    /** Main de l'application. Permet de lancer la fenêtre
     * @param args : arguments de lancement. (inconnus)
     */
    public static void main(String[] args) {
        launch(args);
    }
}