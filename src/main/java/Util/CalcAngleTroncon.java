package Util;

import ApplicationException.ApplicationError;
import Modele.Troncon;

/**
 * Classe abstraite implémentant une méthode de calcul d'angle absolu.
 */
public abstract class CalcAngleTroncon {

    private CalcAngleTroncon(){}

    /**
     * @param t : Tronçon dont on doit déterminer l'angle absolu
     * @return angle : l'angle correspondant au tronçon étudié
     * @throws ApplicationError en cas de Tronçon en input non renseigné au niveau des coordonnées.
     */
    public static double calculerAngle(Troncon t) throws ApplicationError{
        double angle;
        try {
            int x1 = t.getOrigine().getCoord().getCoordX();
            int x2 = t.getArrivee().getCoord().getCoordX();
            int y1 = t.getOrigine().getCoord().getCoordY();
            int y2 = t.getArrivee().getCoord().getCoordY();
            angle = Math.toDegrees(Math.atan2(y2 - y1, x2 - x1));

            if (angle < 0.0) {
                angle += 360.0;
            }
        }catch(Exception e){
            throw new ApplicationError("Calcul d'angle sur un troncon non valide");
        }

        return angle;
    }
}
