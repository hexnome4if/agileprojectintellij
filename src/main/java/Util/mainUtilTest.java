package Util;

import ApplicationException.ApplicationError;
import Modele.Coordonnes;
import Modele.Noeud;
import Modele.Troncon;

import java.util.ArrayList;

public class mainUtilTest {
    public static void main(String[] args) throws ApplicationError {
        TimeLengthCalc calculateur = new TimeLengthCalc();
        double longueur = 140.0;
        double temps = 0.0;
        try {
            temps = calculateur.calculDureeUniforme(longueur);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(temps);

        //Angles
        Coordonnes coord1 = new Coordonnes(1,-1);
        Coordonnes coord2 = new Coordonnes(2,-2);
        Noeud n1 = new Noeud(1,coord1);
        Noeud n2 = new Noeud(2,coord2);
        Troncon t = new Troncon(1L,2.0,"rueTest",3,n1,n2);
        double angle = CalcAngleTroncon.calculerAngle(t);
        System.out.println("angle = "+angle);
    }
}
