package Util;

import ApplicationException.ApplicationError;
import ApplicationException.ApplicationException;

/**
 * Classe implémentant le calcul de temps de parcours de tronçons dans le cas d'une vitesse de parcours uniforme
 */
public class UniformSpeed{
    private double vit;

    public UniformSpeed(){
        this.vit = (15.0f * 1000.0f)/(1.0f*3600.0f);
    }

    /**
     * @param longueur : longueur du tronçon à parcourir
     * @return le temps de parcours du tronçon
     * @throws ApplicationException : erreur en cas d'une ne correspondant pas à un double positif ou égal à 0.
     */
    public double calculTemps(double longueur) throws ApplicationException {
        if(longueur>=0.0) {
            return (longueur / this.vit);
        }else{
            throw new ApplicationError("Longueur d'un troncon négative, pas de calcul de durée possible.");
        }
    }
}
