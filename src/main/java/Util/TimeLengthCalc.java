package Util;

import ApplicationException.ApplicationException;

/**
 * Classe servant de façade pour les calculs de temps de parcours de tronçons
 */
public class TimeLengthCalc {
    private UniformSpeed vitesse;

    public TimeLengthCalc(){
        this.vitesse = new UniformSpeed();
    }

    /**
     * @param longueur : longueur du tronçon à parcourir
     * @return le temps de parcours du tronçon
     * @throws ApplicationException provenant de la méthode calculTemps de la classe UniformSpeed
     */
    public double calculDureeUniforme(double longueur) throws ApplicationException {
        return this.vitesse.calculTemps(longueur);
    }

}
