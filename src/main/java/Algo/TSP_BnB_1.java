package Algo;

import Modele.CarteVilleSimplifiee;
import Modele.Itineraire;

import java.util.Set;

/**
 * Amélioration de la méthode de bound de {@link TSP_BnB_0}.
 * Temps de résolution attendu plus faible, résultat identique.
 */
public class TSP_BnB_1 extends TSP_BnB_0 {

//    protected double minLenItineraire;


    public TSP_BnB_1(CarteVilleSimplifiee cvs) {
        super(cvs);

//        minLenItineraire = Double.MAX_VALUE;
//        for(Long beg : cvs.getMatriceAdjacenceDindiceLivraison().keySet()){
//            for(Long end : cvs.getMatriceAdjacenceDindiceLivraison().get(beg).keySet()){
//                minLenItineraire = Math.min(minLenItineraire, getItineraire(beg, end).getDureeItineraire());
//            }
//        }
    }

    /**
     * Bound admissible qui somme pour chaque {@link Modele.DemandeLivraison} non vue la longueur de l'{@link Itineraire} le plus court
     * qui l'a pour origine et les durées de Livraison de chacunes des {@link Modele.DemandeLivraison} restantes.
     * Complexité : O(n^2), n étant le nombre d'éléments dans nonVus
     * @param curr l'ID de la {@link Modele.DemandeLivraison} en cours
     * @param nonVus l'ensemble des IDs de {@link Modele.DemandeLivraison} pas encore traitées.
     * @return une estimation à priori du temps nécessaire restant pour terminer la tournée.
     */
    @Override
    protected double bound(Long curr, Set<Long> nonVus){

        double bound = 0;
        double min = Double.MAX_VALUE;
        for(Long next : nonVus){
            Itineraire trajet = getItineraire(curr, next);
            min = Math.min(min, trajet.getDureeItineraire());
            bound += trajet.getLivraisonArrivee().getDureeLivraisonSec();
        }
        bound += min;

        min = Double.MAX_VALUE;
        for(Long last : nonVus){
            min = Math.min(min, getItineraire(last, graphe.getEntrepot().getId()).getDureeItineraire());
        }
        bound += min;

        if (nonVus.size() > 1) {
            double max = 0;
            for (Long beg : nonVus) {
                min = Double.MAX_VALUE;
                for (Long end : nonVus) {
                    if (beg.equals(end)) {
                        continue;
                    }
                    min = Math.min(min, graphe.getMatriceAdjacenceDindiceLivraison().get(beg).get(end).getDureeItineraire());
                }
                bound += min;
                max = Math.max(max, min);
            }
            bound -= max;
        }
        return bound;

    }

}
