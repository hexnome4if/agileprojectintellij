package Algo;

import Modele.Itineraire;

import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Itérateur séquentiel : les éléments sont retournés dans l'ordre croissant des ID.
 */
public class IteratorSeq implements Iterator<Long> {

    /**
     * L'ensemble des valeurs que l'instance peut renvoyer à travers la méthode next.
     */
    private TreeSet<Long> nonVus;

    /**
     *
     * @param nonVus l'ensemble des {@link Modele.DemandeLivraison} pas encore traitées.
     */
    public IteratorSeq(TreeSet<Long> nonVus) {
        this.nonVus = (TreeSet<Long>) nonVus.clone();
    }

    public boolean hasNext() {
        return !nonVus.isEmpty();
    }

    /**
     *
     * @return le Long le plus petit encore dans l'ensemble des valeurs contenues dans nonVus
     */
    public Long next() {
        Long ret = nonVus.iterator().next();
        nonVus.remove(ret);
        return ret;
    }

}
