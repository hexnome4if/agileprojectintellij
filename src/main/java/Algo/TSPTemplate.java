package Algo;

import ApplicationException.ApplicationError;
import Logger.InternalLogger;
import Modele.*;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * Classe abstraite implémentant l'interface TSP.
 * Implémente des méthodes de modifications locales de la {@link Tournee} après le calcul de résolution du TSP.
 */
public abstract class TSPTemplate implements TSP {

    /**
     * Booleen de controle
     */
    boolean computed;
    /**
     * La solution trouvée au TSP.
     */
    Tournee solution;

    /**
     * Contient le graphe sur lequel la résolution du TSP repose
     */
    CarteVilleSimplifiee graphe;

    /**
     * Représentation matricielle du graphe pour l'optimisation des calculs durant le TSP
     */
    Itineraire[][] graphe_array;
    /**
     * Array de {@link DemandeLivraison} indexées par leur ID.
     */
    DemandeLivraison[] demande_array;

    /**
     * Initialise les attributs autre que solution.
     * @param cvs la {@link CarteVilleSimplifiee} contenant les graphe et l'{@link Entrepot} (voir attribut graphe)
     */
    public TSPTemplate(CarteVilleSimplifiee cvs){
        computed = false;
        solution = null;
        graphe = cvs;

        int size_max = cvs.getMatriceAdjacenceDindiceLivraison().size();

        demande_array = new DemandeLivraison[size_max];
        for (Itineraire i : graphe.getMatriceAdjacenceDindiceLivraison().get(graphe.getEntrepot().getId()).values()) {
            demande_array[(int) (long) i.getLivraisonArrivee().getId()] = i.getLivraisonArrivee();
        }
        demande_array[(int) (long) graphe.getEntrepot().getId()] = graphe.getEntrepot();
        graphe_array = new Itineraire[size_max][size_max];
        for (int i = 0; i < size_max; ++i) {

            for (int j = 0; j < size_max; ++j) {
                if (i == j) {
                    continue;
                }
                graphe_array[i][j] = cvs.getMatriceAdjacenceDindiceLivraison().get((long) i).get((long) j);
            }
        }
    }

    /**
     * Effectue les calculs nécessaires pour obtenir une solution optimale.
     */
    public abstract void chercheSolution();

    /**
     *
     * @return solution. Attention, null si pas de solution trouvée OU si pas encore calculée
     */
    public Tournee getSolution()throws ApplicationError{
        if(!computed){
            InternalLogger.getInstance().logWarning("Pas de solution encore calculée", this);
        }
        if(solution == null){
            InternalLogger.getInstance().logWarning("Pas de solution trouvée lors du dernier calcul", this);
            throw new ApplicationError("Pas de solution");
        }
        return solution;
    }
    /**
     *
     * @return solution.getDuree. Attention, -1 si pas de solution trouvée OU si pas encore calculée
     */
    public int getCoutSolution()throws ApplicationError{
        if(!computed){
            InternalLogger.getInstance().logWarning("Pas de solution encore calculée", this);
        }
        if(solution == null){
            InternalLogger.getInstance().logWarning("Pas de solution trouvée lors du dernier calcul", this);
            throw new ApplicationError("Pas de solution");
        }
        else{
            return solution.getDureeTournee();
        }
    }

    /** Methode qui permet d'ajouter une livraison à une tournée. La {@link DemandeLivraison} sera ajoutée à la {@link Tournee} solution
     * de manière à minimiser le temps total de la solution. La nouvelle solution n'est pas garantie optimale.
     * Complexité : O(n^2), n étant le nombre de {@link DemandeLivraison} dans la solution.
     * @param newDemandeLivraison : le nouvelle demande de livraison à ajouter.
     * @param nouvelleCarteVilleSimplifiee : La nouvelle carteVille sur laquelle on désire effectuer la modification
     * @throws ApplicationError si la {@link DemandeLivraison} ne peut être ajoutée.
     */
    public void ajouterLivraison(DemandeLivraison newDemandeLivraison, CarteVilleSimplifiee nouvelleCarteVilleSimplifiee) throws ApplicationError {
        setGraphe(nouvelleCarteVilleSimplifiee);

        Tournee nouvelle_tournee = null;


        int depart_entrepot = graphe.getEntrepot().getPlageLivraison().getHeureDeb().getTotalSeconds();

        /*
         * Boucle principale :
         *      indice_ajout = indice de l'itinéraire horodaté qui sera coupé pour insérer la nouvelle livraison
         *
         */
        for (int indice_ajout = 0; indice_ajout < solution.getListeTournee().size(); ++indice_ajout) {
            boolean canBeAdd = true;
            ArrayList<ItineraireHorodate> solution_new = new ArrayList<>();

            /* On ajoute les itinéraire précédents qui n'ont pas été modifiés */
            for (int i = 0; i < indice_ajout; ++i) {
                solution_new.add(solution.getListeTournee().get(i));
            }

            ItineraireHorodate curr_iti_h = solution.getListeTournee().get(indice_ajout);
            DemandeLivraison livraisonOrigine = curr_iti_h.getItineraire().getLivraisonOrigine();
            DemandeLivraison livraisonArrivee = curr_iti_h.getItineraire().getLivraisonArrivee();

            Itineraire itineraire_Origine_New = graphe.getMatriceAdjacenceDindiceLivraison().get(livraisonOrigine.getId()).get(newDemandeLivraison.getId());
            Itineraire itineraire_New_Arrivee = graphe.getMatriceAdjacenceDindiceLivraison().get(newDemandeLivraison.getId()).get(livraisonArrivee.getId());

            /* Heure de début de livraison à la nouvelle livraison */
            Heure h_arr_new;
            if (indice_ajout == 0) {
                h_arr_new = new Heure(Math.max(depart_entrepot + itineraire_Origine_New.getDureeItineraire(),
                        newDemandeLivraison.getPlageLivraison().getHeureDeb().getTotalSeconds())
                );
            } else {
                h_arr_new = new Heure(Math.max(solution.getListeTournee().get(indice_ajout - 1).getHeureDeLivraison().getTotalSeconds()
                                + itineraire_Origine_New.getDureeItineraire() + solution.getListeTournee().get(indice_ajout - 1).getItineraire().getLivraisonArrivee().getDureeLivraisonSec(),
                        newDemandeLivraison.getPlageLivraison().getHeureDeb().getTotalSeconds())
                );
            }




            /* On arrive pas à temps pour la fin de la plage de la nouvelle demande */
            if (h_arr_new.getTotalSeconds() > newDemandeLivraison.getPlageLivraison().getHeureFin().getTotalSeconds()) {
                continue;
            }

            /* On ajoute l'itinéraire à la solution */
            PlageHoraire pl_new = new PlageHoraire(new Heure(Math.max(newDemandeLivraison.getPlageLivraison().getHeureDeb().getTotalSeconds(), h_arr_new.getTotalSeconds() - 60 * 60)),
                    new Heure(Math.min(newDemandeLivraison.getPlageLivraison().getHeureFin().getTotalSeconds(), h_arr_new.getTotalSeconds() + 60 * 60)));
            ItineraireHorodate iti_ori_new_h = new ItineraireHorodate(itineraire_Origine_New, h_arr_new, pl_new);
            solution_new.add(iti_ori_new_h);

            /* heure de départ de la nouvelle livraison */
            int curr_time = h_arr_new.getTotalSeconds() + newDemandeLivraison.getDureeLivraisonSec();

            /* heure de début de livraison de la livraison d'arrivée */
            curr_time = Math.max(curr_time + itineraire_New_Arrivee.getDureeItineraire(),
                    curr_iti_h.getPlageLivraison().getHeureDeb().getTotalSeconds()
            );

            /* On arrive pas à temps pour la fin de la plage de la livraison d'arrivée */
            if (curr_time > curr_iti_h.getPlageLivraison().getHeureFin().getTotalSeconds()) {
                continue;
            }

            /* On ajoute l'itineraire new -> next à la solution */
            ItineraireHorodate iti_new_arr_h = new ItineraireHorodate(itineraire_New_Arrivee, new Heure(curr_time), curr_iti_h.getPlageLivraison());
            solution_new.add(iti_new_arr_h);

            /* heure de départ de la livraison d'arrivée */
            curr_time += livraisonArrivee.getDureeLivraisonSec();

            /*
             * Boucle de vérification des itineraires suivants
             */
            boolean can_be_added = true;

            for (int indice_verif = indice_ajout + 1; indice_verif < solution.getListeTournee().size(); ++indice_verif) {
                ItineraireHorodate verif_iti_h = solution.getListeTournee().get(indice_verif);

                curr_time = Math.max(curr_time + verif_iti_h.getItineraire().getDureeItineraire(),
                        verif_iti_h.getPlageLivraison().getHeureDeb().getTotalSeconds()
                );

                if (curr_time > verif_iti_h.getPlageLivraison().getHeureFin().getTotalSeconds()) {
                    can_be_added = false;
                    break;
                }

                /* On ajoute l'itinéraire à la solution */

                ItineraireHorodate iti_curr_h = new ItineraireHorodate(verif_iti_h.getItineraire(), new Heure(curr_time), verif_iti_h.getPlageLivraison());
                solution_new.add(iti_curr_h);

                curr_time += verif_iti_h.getItineraire().getLivraisonArrivee().getDureeLivraisonSec();


            }
            if (!can_be_added) {
                continue;
            }

            /* POST PROCESSING */

            for (int currIndex = 0, nextIndex = currIndex + 1; nextIndex < solution_new.size(); nextIndex++, currIndex++) {
                ItineraireHorodate curr = solution_new.get(currIndex);
                ItineraireHorodate next = solution_new.get(nextIndex);

                if (curr.getPlageLivraison().getHeureFin().getTotalSeconds() > next.getPlageLivraison().getHeureDeb().getTotalSeconds()) {
                    int median = (curr.getHeureDeLivraison().getTotalSeconds() + next.getHeureDeLivraison().getTotalSeconds()) / 2;
                    int diff = next.getHeureDeLivraison().getTotalSeconds() - curr.getHeureDeLivraison().getTotalSeconds();
                    curr.getPlageLivraison().setHeureFin(new Heure(median - (int) (diff * 0.05)));
                    next.getPlageLivraison().setHeureDeb(new Heure(median + (int) (diff * 0.05)));
                }
                // TODO : Le saumon ne doit pas sortir de la plage horaire !!!
            }

            Tournee solution_index_ajout = new Tournee(solution_new);
            if (nouvelle_tournee == null || solution_index_ajout.getDureeTournee() < nouvelle_tournee.getDureeTournee()) {
                nouvelle_tournee = solution_index_ajout;
            }

        }
        if(nouvelle_tournee == null){
            InternalLogger.getInstance().logWarning("Pas de solution trouvée pour ajouter la Livraison", this);
            throw new ApplicationError("Pas de solution trouvée pour ajouter la Livraison");
        }
        setTournee(nouvelle_tournee);
    }



    /** Methode qui permet de supprimer une {@link DemandeLivraison} de la solution. Pas de garantie d'optimalité de la nouvelle solution.
     * La demande livraison est supprimée de la solution et les {@link DemandeLivraison} avant et après sont reliées par l'{@link Itineraire} le plus court
     * les reliant.
     * Complexité : O(n), n étant le nombre de {@link DemandeLivraison} composant la solution.
     * @param aSupprimerLivraison : le nouvelle demande de livraison à supprimer.
     * @param nouvelleCarteVilleSimplifiee : La nouvelle carteVille sur laquelle on désire effectuer la modification
     */
    public void supprimerLivraison(DemandeLivraison aSupprimerLivraison, CarteVilleSimplifiee nouvelleCarteVilleSimplifiee) throws ApplicationError{
        setGraphe(nouvelleCarteVilleSimplifiee);
        ListIterator<ItineraireHorodate> i = solution.getListeTournee().listIterator();
        boolean found = false;

        /* Recherche de la demande à supprimer */
        while(i.hasNext()){
            if((i.next().getItineraire().getLivraisonArrivee().getId().equals(aSupprimerLivraison.getId()))){
                found = true;
                break;
            }
        }
        if(!found){
            InternalLogger.getInstance().logWarning("La livraison à supprimer n'existe pas.", this);
            throw new ApplicationError("La livraison à supprimer n'existe pas.");
        }
        else{
            ItineraireHorodate before = i.previous();

            /* Calcul de l'heure de départ de la livraison précédente */
            int departBefore = 0;
            if (before.getItineraire().getLivraisonOrigine().getId().equals(graphe.getEntrepot().getId())) {
                departBefore = graphe.getEntrepot().getPlageLivraison().getHeureDeb().getTotalSeconds();
            }
            else{
                departBefore = i.previous().getHeureDeLivraison().getTotalSeconds() + before.getItineraire().getLivraisonOrigine().getDureeLivraisonSec();
                i.next();
            }


            i.next();//déplacement
            /* Livraison après la livraison à supprimer */
            ItineraireHorodate after = i.next();


            //calcul de l'heure de livraison de la livraison qui suit : max entre debut de sa plage horaire et l'heure d'arrivée au point de livraison
            Heure debutL = new Heure(Math.max(
                    after.getPlageLivraison().getHeureDeb().getTotalSeconds(),
                    departBefore + graphe.getMatriceAdjacenceDindiceLivraison().get(before.getItineraire().getLivraisonOrigine().getId()).get(after.getItineraire().getLivraisonArrivee().getId()).getDureeItineraire())
            );

            if(debutL.getTotalSeconds() > after.getPlageLivraison().getHeureFin().getTotalSeconds()){
                InternalLogger.getInstance().logWarning("Incohérence des heures d'arrivée lors de la suppression d'un trajet !", this);
            }

            ItineraireHorodate newIt = new ItineraireHorodate(graphe.getMatriceAdjacenceDindiceLivraison().get(before.getItineraire().getLivraisonOrigine().getId()).get(after.getItineraire().getLivraisonArrivee().getId()),
                                        debutL, after.getPlageLivraison());

            Long idEnd = solution.getListeTournee().get(0).getItineraire().getLivraisonOrigine().getId();
            Long idCurr;
            ArrayList<ItineraireHorodate> nouvelleSolution = new ArrayList<>();

            int index = 0;
            boolean afterSuppr = false;

            do{
                idCurr = solution.getListeTournee().get(index).getItineraire().getLivraisonArrivee().getId();
                if(idCurr.equals(aSupprimerLivraison.getId())){
                    afterSuppr = true;
                    nouvelleSolution.add(newIt);

                    index += 2;
                }
                else{
                    if(!afterSuppr){
                        nouvelleSolution.add(solution.getListeTournee().get(index));
                    }
                    else{
                        Heure newDebutL = new Heure(Math.max(solution.getListeTournee().get(index).getPlageLivraison().getHeureDeb().getTotalSeconds(),
                                nouvelleSolution.get(nouvelleSolution.size() - 1).getHeureDeLivraison().getTotalSeconds() + nouvelleSolution.get(nouvelleSolution.size() - 1).getItineraire().getLivraisonArrivee().getDureeLivraisonSec() + solution.getListeTournee().get(index).getItineraire().getDureeItineraire()));
                        nouvelleSolution.add(new ItineraireHorodate(solution.getListeTournee().get(index).getItineraire(), newDebutL, solution.getListeTournee().get(index).getPlageLivraison()));
                    }
                    index ++;
                }

            }while(index < solution.getListeTournee().size() && !idCurr.equals(idEnd));

            setTournee(new Tournee(nouvelleSolution));
            setGraphe(nouvelleCarteVilleSimplifiee);
        }

    }

    /**
     * modifie l'attribut graphe
     * @param g la nouvelle {@link CarteVilleSimplifiee}
     */
    public void setGraphe(CarteVilleSimplifiee g) {
        graphe = g;
    }

    /**
     * modifie l'attribut solution
     * @param t la nouvelle {@link Tournee}
     */
    public void setTournee(Tournee t) {
        solution = t;
        if (t != null) {
            computed = true;
        } else {
            computed = false;
        }

    }
}
