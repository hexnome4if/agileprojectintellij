package Algo;

import ApplicationException.ApplicationError;
import Modele.*;

import java.util.*;

/**
 * Classe qui implémente l'algorithme de Dijkstra pour calculer les distances minimales entre les {@link DemandeLivraison}.
 */
public class Dijkstra extends AlgoGrapheComplet {

    public Dijkstra(CarteVille ville, PlanLivraison planLivraison){
        super(ville, planLivraison);


    }


    /**
     * Calcule à l'aide de l'agorithme de Dijkstra l'arbre des plus courts chemins enraciné au {@link Noeud} origine
     * Les {@link Itineraire} retourné sont valides dans les directions origine - destination
     * @param origine {@link DemandeLivraison} dont le {@link Noeud} servira de point de départ pour l'algorithme. Origine doit être une {@link DemandeLivraison} connue de l'instance.
     * @return Map des itinéraires optimaux calculés :
     *      key1 = ID de la {@link DemandeLivraison} destination de cet Itineraire, l'origine de cet Itineraire étant la {@link DemandeLivraison} origine passée en paramètre.
     *      Les seules clefs rentrées dans cette Map sont les IDs des {@link DemandeLivraison} (ou l'{@link Entrepot}) !
     */
    protected TreeMap<Long, Itineraire> SimpleDijkstra(DemandeLivraison origine) throws ApplicationError {

        HashSet<Long> vus = new HashSet<Long>();
        HashMap<Long, Long> tree = new HashMap<Long, Long>();
        PriorityQueue<GraphNode> queue = new PriorityQueue<GraphNode>();


        for (GraphNode n : intersections.values()) {
            n.setDistance(Double.MAX_VALUE);
        }
        intersections.get(origine.getAdresse().getId()).setDistance(0);

        GraphNode u = intersections.get(origine.getAdresse().getId());
        queue.add(u);
        tree.put(u.getNoeud().getId(), u.getNoeud().getId());

        while (!queue.isEmpty()) {

            u = queue.poll();
            if (u.getDistance() == Double.MAX_VALUE) break;

            if(listeAdj.containsKey(u.getNoeud().getId())) {
                for (Troncon t : listeAdj.get(u.getNoeud().getId()).values()) {
                    if (vus.contains(t.getArrivee().getId())) {
                        continue;
                    }
                    GraphNode v = intersections.get(t.getArrivee().getId());
                    final double alternateDist = u.getDistance() + t.getDureeParcours();
                    if (alternateDist < v.getDistance()) {
                        queue.remove(v);
                        v.setDistance(alternateDist);
                        tree.put(v.getNoeud().getId(), u.getNoeud().getId());
                        queue.add(v);
                    }
                }
            }
            vus.add(u.getNoeud().getId());
        }

        /* Post processing */
        TreeMap<Long, Itineraire> retIti = new TreeMap<Long, Itineraire>();

        for(DemandeLivraison livraison : lieuLivraisons.values()){
            if(livraison.getId() == origine.getId()){continue;}
            double longueur = Double.MAX_VALUE;
            GraphNode current = intersections.get(livraison.getAdresse().getId());
            ArrayList<Troncon> listTroncons = new ArrayList<Troncon>();
            /*
             * Si on a rencontré ce Noeud durant l'algorithme.
             * Sinon les valeurs par défaut sont conservées : MAX_VALUE en longueur de l'Itineraire et ArrayList vide
             */
            if(tree.containsKey(current.getNoeud().getId())){
                GraphNode next = intersections.get(tree.get(current.getNoeud().getId()));
                longueur = 0;
                while(current.getNoeud().getId() != next.getNoeud().getId()){
                    Troncon chemin = listeAdj.get(next.getNoeud().getId()).get(current.getNoeud().getId());
                    longueur += chemin.getDureeParcours();
                    listTroncons.add(chemin);

                    current = next;
                    next = intersections.get(tree.get(current.getNoeud().getId()));
                }
            }
            Itineraire itineraire = new Itineraire(listTroncons, origine, livraison, (int) longueur);
            itineraire.calculerAnglesItineraire();
            retIti.put(livraison.getId(), itineraire);

        }

        return retIti;
    }
    /**
     * Effectue le calcul de tous les Itineraires optimaux entre les points de Livraison en utilisant l'algorithme de Dijkstra.
     * @return Le graphe complet où les noeuds du graphe sont les points de livraison et les arêtes sont les itinéraires optimaux entre ces points.
     */
    public CarteVilleSimplifiee CalculItineraires() throws ApplicationError {
        TreeMap<Long, TreeMap<Long, Itineraire>> matrixItineraire = new TreeMap<Long, TreeMap<Long, Itineraire>>();
        for(DemandeLivraison livraison : lieuLivraisons.values()){
            TreeMap<Long, Itineraire> currentMap = SimpleDijkstra(livraison);
            matrixItineraire.put(livraison.getId(), currentMap);
        }
        CarteVilleSimplifiee cvs = new CarteVilleSimplifiee(matrixItineraire, entrepot);
        return cvs;
    }


}
