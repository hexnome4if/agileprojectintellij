package Algo;

import Modele.Coordonnes;
import Modele.Noeud;
import jdk.nashorn.internal.parser.TokenType;

/**
 * Classe encapsulant un {@link Noeud} et un double représentant une valeur attribuée au {@link Noeud}.
 * Implémente {@link Comparable} pour comparer à une autre instance de {@link GraphNode}.
 * Permet l'utilisation de {@link java.util.Collections} standard pour stocker et ordonner les {@link Noeud} selon leur valeur de distance.
 */
public class GraphNode implements Comparable<GraphNode> {

    private Noeud noeud;
    private double distance;


    public GraphNode (Noeud n){
        noeud = n;
        distance = Double.MAX_VALUE;
    }

    public GraphNode (Noeud n, double d){
        noeud = n;
        distance = d;
    }

    public Noeud getNoeud() {
        return noeud;
    }

    public double getDistance() {
        return distance;
    }

    /**
     * Modifie l'attribut distance
     * @param distance : une valeur négative sera remplacée par Double.MAX_VALUE.
     */
    public void setDistance(double distance) {
        if(distance < 0){
            this.distance = Double.MAX_VALUE;
        }
        else{
            this.distance = distance;
        }
    }

    @Override
    public String toString() {
        String beg = super.toString();
        return beg + "GraphNode{" +
                "distance=" + distance +
                '}';
    }

    /**
     * Comparaison de deux {@link GraphNode}
     * @param o : un autre {@link GraphNode}
     * @return la comparaison des distances de chacun des {@link GraphNode}
     */
    public int compareTo(GraphNode o) {
        return Double.compare(distance, o.distance);
    }
}
