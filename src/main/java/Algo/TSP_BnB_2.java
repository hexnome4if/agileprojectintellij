package Algo;

import Modele.CarteVilleSimplifiee;
import Modele.Itineraire;

import java.util.*;

/**
 * Amélioration de la méthode de bound de {@link TSP_BnB_1}.
 * Modification de l'{@link Iterator} utilisé ({@link IteratorSort}) ce qui permet d'avoir en moyenne des temps réduits.
 * Temps de résolution attendu plus faible, résultat identique.
 */
public class TSP_BnB_2 extends TSP_BnB_1 {
    public TSP_BnB_2(CarteVilleSimplifiee cvs) {
        super(cvs);
    }

    /**
     *
     * @param map le graphe sous forme de Maps
     * @param nodeCurr l'ID de la {@link Modele.DemandeLivraison} en cours
     * @param nonVus l'ensemble des IDs non vus encore.
     * @return Un {@link IteratorSort} sur les IDs contenus dans nonVus
     */
    @Override
    protected Iterator<Long> getIterator(TreeMap<Long, TreeMap<Long, Itineraire>> map, Long nodeCurr, TreeSet<Long> nonVus) {
        return new IteratorSort(graphe.getMatriceAdjacenceDindiceLivraison(), nodeCurr, nonVus);
    }

    /**
     * Bound admissible qui somme pour chaque {@link Modele.DemandeLivraison} non vue la longueur de l'{@link Itineraire} le plus court
     * qui l'a pour origine et les durées de Livraison de chacunes des {@link Modele.DemandeLivraison} restantes.
     *
     * Implémentation plus élégante que celle de {@link TSP_BnB_1}.
     *
     * Complexité : O(n^2), n étant le nombre d'éléments dans nonVus
     * @param currentNode l'ID de la {@link Modele.DemandeLivraison} en cours
     * @param unseen l'ensemble des IDs de {@link Modele.DemandeLivraison} pas encore traitées.
     * @return une estimation à priori du temps nécessaire restant pour terminer la tournée.
     */
    @Override
    protected double bound(Long currentNode, Set<Long> unseen) {
        List<Long> sources = new ArrayList<>(unseen);
        sources.add(currentNode);
        List<Long> destinations = new ArrayList<>(unseen);
        destinations.add(graphe.getEntrepot().getId());
        long bound = 0;
        for (Long source : sources) {
            int minPrice = Integer.MAX_VALUE;
            for (Long destination : destinations) {
                if (source.equals(destination)) {
                    continue;
                }
                if (getItineraire(source, destination).getDureeItineraire() < minPrice) {
                    minPrice = getItineraire(source, destination).getDureeItineraire();
                }
            }
            bound += minPrice;
        }
        for (Long destination : destinations) {
            bound += demande_array[(int) (long) destination].getDureeLivraisonSec();
        }
        return bound;
    }


    /**
     * Récupère l'{@link Itineraire} en utilisant la matrice pour être plus rapide.
     * @param beg ID de {@link Modele.DemandeLivraison} d'origine de l'{@link Itineraire}
     * @param end ID de {@link Modele.DemandeLivraison} de destination de l'{@link Itineraire}
     * @return l'{@link Itineraire} souhaité
     */
    @Override
    protected Itineraire getItineraire(Long beg, Long end) {
        return graphe_array[(int) (long) beg][(int) (long) end];
    }

}
