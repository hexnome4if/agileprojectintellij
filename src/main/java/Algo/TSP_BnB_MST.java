package Algo;

import Modele.CarteVilleSimplifiee;
import Modele.Itineraire;

import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;

/**
 * Résolution du TSP avec une heuristique de bound se basant sur le minimal spanning tree (MST) recouvrant les noeuds encore non vus.
 * La solution obtenue est garantie optimale dans la mesure où le MST est une heuristique admissible (non démontré mais affirmé
 * dans des références comme : Introduction to Algorithms 3rd Edition by Clifford Stein, Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest)
 *
 * Temps de calcul plus faible que pour {@link TSP_BnB_2}.
 */
public class TSP_BnB_MST extends TSP_BnB_2{
    public TSP_BnB_MST(CarteVilleSimplifiee cvs) {
        super(cvs);
    }


    /**
     * Algorithme de Prim utilisé pour calculer le MST adapté pour un digraph.
     * @param curr ID de la demande courante
     * @param nonVus Set des ID des demandes restantes à parcourir
     * @return borne inf du cout restant à parcourir
     */
    @Override
    protected double bound(Long curr, Set<Long> nonVus){
//        fonction prim(G, s)
//        pour tout sommet t
//          cout[t] := +∞
//          pred[t] := nil
//        cout[s] := 0
//        F := file de priorité contenant les sommets de G avec cout[.] comme priorité
//        tant que F ≠ vide
//            t := F.defiler
//            pour toute arête t--u
//                si cout[u] > poids(t--u)
//                    cout[u] := poids(t--u)
//                    pred[u] := t
//                    F.notifierDiminution(u)
//
//        retourner pred
        TreeSet<Long> nVus = new TreeSet<>(nonVus);
        PriorityQueue<GraphDemand> q = new PriorityQueue<>();
        GraphDemand[] m = new GraphDemand[this.graphe_array.length];
        for(Long i : nonVus){
            GraphDemand d = new GraphDemand(i);
            q.add(d);
            m[(int) (long) i] = d;
        }
        q.add(new GraphDemand(curr, 0));

        double bound = 0;

        while(!q.isEmpty()){
            GraphDemand d = q.poll();
            bound += d.cout;
            for (Long n : nVus) {
                if(d.id.equals(n)){continue;}
                GraphDemand g = m[(int) (long) n];
                double coutArc = getItineraire(d.id, n).getDureeItineraire();
                if(g.cout > coutArc){
                    q.remove(g);
                    g.cout = coutArc;
                    g.parent = d.id;
                    q.add(g);

                }
            }
            nVus.remove(d.id);

        }

        double min_e = Double.MAX_VALUE;

        for (Long i : nonVus) {
            GraphDemand d = m[(int) (long) i];
            bound += demande_array[(int) (long) d.id].getDureeLivraisonSec();
            min_e = Math.min(min_e, getItineraire(i, graphe.getEntrepot().getId()).getDureeItineraire());
        }

        bound += min_e;

        return bound;
    }

    class GraphDemand implements Comparable<GraphDemand>{
        private Long id;
        public double cout;
        public Long parent = null;

        public GraphDemand(Long i){
            id = i;
            cout = Double.MAX_VALUE;
        }

        public GraphDemand(Long i, double c){
            id = i;
            cout = c;
        }

        @Override
        public int compareTo(GraphDemand o) {
            return Double.compare(cout, o.cout);
        }
    }

    /**
     * Récupère l'{@link Itineraire} en utilisant la matrice pour être plus rapide.
     * @param beg ID de {@link Modele.DemandeLivraison} d'origine de l'{@link Itineraire}
     * @param end ID de {@link Modele.DemandeLivraison} de destination de l'{@link Itineraire}
     * @return l'{@link Itineraire} souhaité
     */
    @Override
    protected Itineraire getItineraire(Long beg, Long end) {
        return graphe_array[(int) (long) beg][(int) (long) end];
    }
}
