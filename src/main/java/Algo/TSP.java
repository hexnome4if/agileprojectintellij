package Algo;

import Modele.Tournee;

/**
 * Interface à implémenter pour une classe résolvant le problème du voyageur de commerce (TSP).
 * Pour obtenir une solution, il est nécessaire d'utiliser chercheSolution, puis getSolution.
 */
public interface TSP {

    /**
     * Effectue les calculs nécessaires à la résolution du TSP : peut être long. Optimal OU Approximatif en fonction de l'implémentation
     */
    void chercheSolution();

    /**
     *
     * @return La solution trouvée, ou Exception si aucune solution n'a été trouvée ou si la méthode chercheSolution n'a pas été appelée auparavant.
     */
    Tournee getSolution() throws Exception;

    /**
     *
     * @return Le coût en temps de la solution trouvée, ou Exception si aucune solution n'a été trouvée ou si la méthode chercheSolution n'a pas été appelée auparavant.
     */
    int getCoutSolution() throws Exception;


}
