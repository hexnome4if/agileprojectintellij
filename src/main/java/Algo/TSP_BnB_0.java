package Algo;

import Logger.InternalLogger;
import Modele.*;

import java.util.*;

/**
 * Première résolution du TSP par Branch and Bound avec bound triviale et itérateur séquentiel.
 */
public class TSP_BnB_0 extends TSPTemplate{
    /**
     * Temps maximal de calcul
     */
    final long TMP_MAX = 60*3*1000; // 3 minutes
    /**
     * début d'un calcul
     */
    protected long tmp_debut;

    /**
     * Solution provisoire potentiellement optimale
     */
    protected ArrayList<Long> solIndex;
    /**
     * cout de la solution provisoire
     */
    protected double coutSolIndex;

    /**
     * Initialise les attributs autre que les solutions.
     * @param cvs
     */
    public TSP_BnB_0(CarteVilleSimplifiee cvs) {
        super(cvs);
        coutSolIndex = Double.MAX_VALUE;
    }

    /**
     * Résolution du TSP par branch and bound. Recherche d'une solution optimale respectant les contraintes des clients.
     * Nécessaire d'utiliser cette méthode pour trouver une solution.
     * Modifie l'attribut solution si une solution optimale est trouvée
     */
    public void chercheSolution() {
        InternalLogger.getInstance().logConfig("TSP : Début du calcul de tournée optimisée", this);
        computed = false;

        coutSolIndex = Double.MAX_VALUE;

        TreeSet<Long> nonVus = new TreeSet<Long>();

        for(Itineraire i : graphe.getMatriceAdjacenceDindiceLivraison().get(graphe.getEntrepot().getId()).values()){
            nonVus.add(i.getLivraisonArrivee().getId());
        }

        ArrayList<Long> vus = new ArrayList<Long>();
        tmp_debut = System.currentTimeMillis();

        branchAndBound(graphe.getEntrepot().getId(), nonVus, vus, 0);//cout initial des vus ?

        if(computed){
            InternalLogger.getInstance().logConfig("TSP : Fin du calcul : résultat trouvé", this);
            solution = exportTournee();

        }
    }

    /**
     * Méthode de création d'une instance de {@link Tournee} à partir d'un ordre sur les {@link DemandeLivraison}.
     * Détermination des {@link PlageHoraire} à communiquer aux clients
     * @return la solution sous la forme d'une instance de {@link Tournee}
     */
    protected Tournee exportTournee(){
        InternalLogger.getInstance().logConfig("TSP : Début de l'export des résultat vers une Tournée", this);
        Long beg = graphe.getEntrepot().getId();
        Long end;

        double departEntrepot = graphe.getEntrepot().getPlageLivraison().getHeureDeb().getTotalSeconds();
        double dureeTournee = 0;
        ArrayList<ItineraireHorodate> planning = new ArrayList<ItineraireHorodate>(solIndex.size()+1);



        for(Long index : solIndex){
            end = index;
            Itineraire iti = graphe.getMatriceAdjacenceDindiceLivraison().get(beg).get(end);
            dureeTournee = Math.max(dureeTournee + iti.getDureeItineraire(), iti.getLivraisonArrivee().getPlageLivraison().getHeureDeb().getTotalSeconds() - graphe.getEntrepot().getPlageLivraison().getHeureDeb().getTotalSeconds());


            int arrivee = (int)(departEntrepot+dureeTournee);

            PlageHoraire comClient = new PlageHoraire(iti.getLivraisonArrivee().getPlageLivraison().getHeureDeb(), iti.getLivraisonArrivee().getPlageLivraison().getHeureFin());

            if(comClient.getHeureFin().getTotalSeconds() - comClient.getHeureDeb().getTotalSeconds() >= 2*60*60){ //plus de 2h de plage horaire
                comClient.setHeureDeb(new Heure(arrivee - 60*60));
                comClient.setHeureFin(new Heure(arrivee + 60*60));
            }
            else if(comClient.getHeureFin().getTotalSeconds() - comClient.getHeureDeb().getTotalSeconds() >= 60 * 60) { //entre 1h et 2h de plage horaire -> on conserve la même
                //nothing here to do
            }else if(comClient.getHeureFin().getTotalSeconds() - comClient.getHeureDeb().getTotalSeconds() < 60 * 60 ){ //moins d'une heure de plage horaire communiquée
                if(arrivee-comClient.getHeureDeb().getTotalSeconds() < comClient.getHeureFin().getTotalSeconds()-arrivee){
                    comClient.setHeureDeb(new Heure(comClient.getHeureFin().getTotalSeconds()-60*60));
                }
                else{
                    comClient.setHeureFin(new Heure(comClient.getHeureDeb().getTotalSeconds()+60*60));
                }
            }
            dureeTournee += iti.getLivraisonArrivee().getDureeLivraisonSec();
            planning.add(new ItineraireHorodate(iti, new Heure(arrivee), comClient));

            beg = end;
        }

        end = graphe.getEntrepot().getId();
        Itineraire iti = graphe.getMatriceAdjacenceDindiceLivraison().get(beg).get(end);
        dureeTournee += iti.getDureeItineraire();



        planning.add(new ItineraireHorodate(iti, new Heure((int)(departEntrepot + dureeTournee)), new PlageHoraire(new Heure((int) (departEntrepot+dureeTournee)), Heure.FIN_JOURNEE)));

        // POST PROCESSING


        for(int currIndex = 0, nextIndex = currIndex+1 ; nextIndex < planning.size() ; nextIndex++, currIndex++){
            ItineraireHorodate curr = planning.get(currIndex);
            ItineraireHorodate next = planning.get(nextIndex);

            // TODO : à vérifier s'il n'y a pas de conflits avec la boucle précédente qui entraineraient une sortie de la plage client ?
            if(curr.getPlageLivraison().getHeureFin().getTotalSeconds() - curr.getPlageLivraison().getHeureDeb().getTotalSeconds() < 60*60){
                curr.getPlageLivraison().setHeureFin(new Heure(curr.getPlageLivraison().getHeureDeb().getTotalSeconds() + 60*60));
            }

            if(curr.getPlageLivraison().getHeureFin().getTotalSeconds() > next.getPlageLivraison().getHeureDeb().getTotalSeconds()){
                int median = (curr.getHeureDeLivraison().getTotalSeconds() + next.getHeureDeLivraison().getTotalSeconds()) / 2;
                int diff = next.getHeureDeLivraison().getTotalSeconds() - curr.getHeureDeLivraison().getTotalSeconds();
                curr.getPlageLivraison().setHeureFin(new Heure(median - (int) (diff * 0.05)));
                next.getPlageLivraison().setHeureDeb(new Heure(median + (int) (diff * 0.05)));
            }
            // TODO : Le saumon ne doit pas sortir de la plage horaire !!!
        }


        Tournee ret = new Tournee(planning);

        InternalLogger.getInstance().logConfig("TSP : Fin de l'export des résultat vers une Tournée", this);
        return ret;
    }

    /**
     * Méthode récursive de BranchAndBound (BnB) pour trouver la solution optimale du TSP.
     * Assure l'optimalité de la solution (si elle existe) et réduit le temps de calcul en coupant l'arbre de recherche
     * lorsque la durée à priori d'une solution est plus grande que coutSolIndex.
     * @param nodeCurr ID de la {@link DemandeLivraison} en cours. Cette {@link DemandeLivraison} a déjà été effectuée
     *                 dans le contexte de cette méthode
     * @param nonVus Ensemble des IDs des {@link DemandeLivraison} restantes.
     * @param vus Array des IDs des {@link DemandeLivraison} déjà effectuées, triés dans l'ordre de visite.
     * @param coutVus Cout en temps du trajet provisoire comprenant le temps de trajet sur les vus, le temps d'attente
     *                potentiel des clients et le temps de livraison.
     */
    protected void branchAndBound(Long nodeCurr, TreeSet<Long> nonVus, ArrayList<Long> vus, double coutVus) {
        if(System.currentTimeMillis() - tmp_debut > TMP_MAX){return;}
        if(nonVus.size() == 0){
            coutVus += getItineraire(nodeCurr, graphe.getEntrepot().getId()).getDureeItineraire();
            if(coutVus < coutSolIndex){
                computed = true;
                solIndex =(ArrayList<Long>) vus.clone();
                coutSolIndex = coutVus;
//                System.out.println("Cout actuel : " + coutVus);

            }

        } else if(coutVus + bound(nodeCurr, nonVus) < coutSolIndex){
            Iterator<Long> it = getIterator(graphe.getMatriceAdjacenceDindiceLivraison(), nodeCurr, nonVus);
            double saveCoutVus = coutVus;
            while(it.hasNext()){
                Long nextNode = it.next();
                Itineraire trajet = getItineraire(nodeCurr, nextNode);

                /* check_i+1 = coutVus_i + duréeItineraire : Quand arrive-t-on chez le client ? */
                if(coutVus + trajet.getDureeItineraire() < trajet.getLivraisonArrivee().getPlageLivraison().getHeureFin().getTotalSeconds() -  graphe.getEntrepot().getPlageLivraison().getHeureDeb().getTotalSeconds()){


                    vus.add(nextNode);
                    nonVus.remove(nextNode);

                    /* coutVus_i+1 = coutVus_i + longTrajet + duréeLivraison OU (debutPlageHoraire - départEntrepot) + duréeLivraison */
                    coutVus = Math.max(coutVus + trajet.getDureeItineraire(),
                            trajet.getLivraisonArrivee().getPlageLivraison().getHeureDeb().getTotalSeconds() - graphe.getEntrepot().getPlageLivraison().getHeureDeb().getTotalSeconds()
                    );

                    coutVus += trajet.getLivraisonArrivee().getDureeLivraisonSec();

                    branchAndBound(nextNode, nonVus, vus, coutVus);

                    coutVus = saveCoutVus;
                    vus.remove(nextNode);
                    nonVus.add(nextNode);
                }
            }
        }
    }

    /**
     * Getter utilisant les Maps pour récupérer l'{@link Itineraire} souhaité
     * @param beg ID de {@link DemandeLivraison} d'origine de l'{@link Itineraire}
     * @param end ID de {@link DemandeLivraison} de destination de l'{@link Itineraire}
     * @return l'{@link Itineraire} souhaité
     */
    protected Itineraire getItineraire(Long beg, Long end) {
        return graphe.getMatriceAdjacenceDindiceLivraison().get(beg).get(end);

    }

    /**
     * Getter d'{@link Iterator} sur les ID contenus dans nonVus
     * @param map le graphe sous forme de Maps (inutilisé dans cette implémentation de la mathode)
     * @param nodeCurr l'ID de la {@link DemandeLivraison} en cours (inutilisé pour cette implémentation)
     * @param nonVus l'ensemble des IDs non vus encore.
     * @return
     */
    protected Iterator<Long> getIterator(TreeMap<Long, TreeMap<Long, Itineraire>> map, Long nodeCurr, TreeSet<Long> nonVus) {
        return new IteratorSeq(nonVus);
    }

    /**
     * Méthode d'évaluation de la longueur restante à parcourir à priori étant donné la liste des ID non vus
     * Implémentation triviale : return 0
     * @param curr inutilisé dans cette implémentation
     * @param nonVus inutilisé dans cette implémentation
     * @return 0
     */
    protected double bound(Long curr, Set<Long> nonVus){
        return 0;
    }
}
