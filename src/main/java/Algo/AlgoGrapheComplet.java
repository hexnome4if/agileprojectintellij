package Algo;

import ApplicationException.ApplicationError;
import Modele.*;

import java.util.HashMap;

/**
 * Classe abstraite qui déclare les méthodes qui serviront d'interface pour le controleur afin de calculer le graphe
 * complet des plus courts chemins entre les livraisons à partir du plan de la ville.
 * La méthode CalculItineraires permet de récupérer la {@link CarteVilleSimplifiee} représentant le {@link PlanLivraison} dans cette {@link CarteVille}
 * La méthode CalculNewItineraires permet de modifier le planLivraison et de calculer la nouvelle {@link CarteVilleSimplifiee}
 */
public abstract class AlgoGrapheComplet {

    /**
     * Map de Map de troncon :
     *      key1 = ID du Noeud Origine
     *      key2 = ID du Noeud Arrivee
     */
    protected HashMap<Long, HashMap<Long, Troncon>> listeAdj = new HashMap<Long, HashMap<Long, Troncon>>();

    /**
     * Map GraphNode :
     *      Key1 = ID du Noeud du GraphNode
     */
    protected HashMap<Long, GraphNode> intersections = new HashMap<Long, GraphNode>();

    /**
     * Map de DemandeLivraison :
     *      key1 = ID de la DemandeLivraison
     */
    protected HashMap<Long, DemandeLivraison> lieuLivraisons = new HashMap<Long, DemandeLivraison>();

    /**
     * L'Entrepot défini pour ce PlanLivraison
     */
    protected Entrepot entrepot;


    /**
     * Constructeur d'AlgoGrapheComplet : initialise les attributs utiles pour l'instance. Place les objets du Modele dans des Maps
     * pour les récuprer par index plus facilement ensuite.
     * @param ville La carte de la ville qui contient toutes les routes existantes sous forme d'intersections et de troncons
     * @param planLivraison Les demandes de livraisons que l'application doit organiser et mettre en ordre pour obtenir une tournée
     */
    public AlgoGrapheComplet(CarteVille ville, PlanLivraison planLivraison){
        entrepot = planLivraison.getEntrepot();
        for(Noeud inter : ville.getListNoeud()){
            intersections.put(inter.getId(), new GraphNode(inter));
        }
        for (Troncon troncon : ville.getListTroncon()) {
            if (!(intersections.containsKey(troncon.getOrigine().getId()) && intersections.containsKey(troncon.getArrivee().getId()))) {
                System.err.println("Troncon utilisant un Noeud inconnu de la liste des Noeuds, Ajout à la liste.");
                intersections.put(troncon.getOrigine().getId(), new GraphNode(troncon.getOrigine()));
                intersections.put(troncon.getArrivee().getId(), new GraphNode(troncon.getArrivee()));
            }
            if (!listeAdj.containsKey(troncon.getOrigine().getId())) {
                listeAdj.put(troncon.getOrigine().getId(), new HashMap<>());
            }

            if (!listeAdj.get(troncon.getOrigine().getId()).containsKey(troncon.getArrivee().getId())){//Le troncon n'est pas déjà contenu dans le graphe
                listeAdj.get(troncon.getOrigine().getId()).put(troncon.getArrivee().getId(), troncon);
            }else
            {
                if(listeAdj.get(troncon.getOrigine().getId()).get(troncon.getArrivee().getId()).getDureeParcours() > troncon.getDureeParcours()){
                    listeAdj.get(troncon.getOrigine().getId()).put(troncon.getArrivee().getId(), troncon);
                }
            }
        }

        for (DemandeLivraison livraison : planLivraison.getListeLivraison()){
            lieuLivraisons.put(livraison.getId(), livraison);
        }
        lieuLivraisons.put(entrepot.getId(), entrepot);

    }

    /**
     * Effectue le calcul de tous les Itineraires optimaux entre les points de Livraison. Différents algorithmes peuvent être implémentés pour répondre à cette problématique.
     * @return Le graphe complet où les noeuds du graphe sont les points de livraison et les arêtes sont les itinéraires optimaux entre ces points.
     */
    public abstract CarteVilleSimplifiee CalculItineraires() throws ApplicationError;

    /**
     *
     * @return l'{@link Entrepot} qui sert de point de départ et d'arrivée à la future {@link Tournee}
     */
    public Entrepot getEntrepot() {
        return entrepot;
    }

    /**
     * Modifie la liste des livraisons à faire en utilisant celle donnée en paramètre et calcule les plus courts chemins entre chacune des {@link DemandeLivraison}.
     * Inutile d'uiliser setPlanLivraison avant.
     * @param newPlanLivraison Nouveau {@link PlanLivraison} sur lequel seront basés les calculs
     * @return CarteVilleSimplifiee qui contient les {@link Itineraire} entre les {@link DemandeLivraison}.
     */
    public CarteVilleSimplifiee CalculNewItineraires(PlanLivraison newPlanLivraison) throws ApplicationError {
        setPlanLivraison(newPlanLivraison);
        return CalculItineraires();

    }

    /**
     * Modifie la liste des {@link DemandeLivraison} à effectuer : écrase celle en attribut pour la remplacer par le paramètre.
     * @param newPlanLivraison
     */
    public void setPlanLivraison(PlanLivraison newPlanLivraison){
        lieuLivraisons.clear();
        for (DemandeLivraison livraison : newPlanLivraison.getListeLivraison()){
            lieuLivraisons.put(livraison.getId(), livraison);
        }
        lieuLivraisons.put(newPlanLivraison.getEntrepot().getId(), newPlanLivraison.getEntrepot());

    }

}

