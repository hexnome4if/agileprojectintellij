package Algo;

import Modele.DemandeLivraison;
import Modele.Itineraire;

import java.util.*;

/**
 * Itérateur trié. Les valeurs retournées par next dépendent du sous-graphe des {@link DemandeLivraison} encore non traitées.
 */
public class IteratorSort implements Iterator<Long> {

    /**
     * Le graphe connectant les {@link DemandeLivraison}, il contient le sous-graphe qui nous intéresse.
     */
    private TreeMap<Long, TreeMap<Long, Itineraire>> graph;
    /**
     * ID de la {@link DemandeLivraison} en cours.
     */
    private Long curr;

    /**
     * Ensemble des IDs des {@link DemandeLivraison} pas encore traitées
     */
    private ArrayList<Long> nonVus;
    /**
     * Nombre d'IDs déjà retournés par next
     */
    private int indexNVus;

    /**
     *
     * @param map le graphe connectant les {@link DemandeLivraison} par des {@link Itineraire}
     * @param nodeCurr l'ID de la {@link DemandeLivraison} qui est notre point d'origine dans ce contexte.
     * @param nonVus l'ensemble des {@link DemandeLivraison} pas encore traitées
     */
    public IteratorSort(TreeMap<Long, TreeMap<Long, Itineraire>> map, Long nodeCurr, TreeSet<Long> nonVus) {
        graph = map;
        curr = nodeCurr;
        this.nonVus = new ArrayList<>(nonVus);

        // tri des noeuds non vus en fonction de la distance nodeCurr -> noeud
        Collections.sort(this.nonVus, (Long o1, Long o2) -> {
            Itineraire i = graph.get(o2).get(o1);
            DemandeLivraison d1 = i.getLivraisonOrigine();
            DemandeLivraison d2 = i.getLivraisonArrivee();
            if (d1.getPlageLivraison().getHeureFin().getTotalSeconds() != d2.getPlageLivraison().getHeureFin().getTotalSeconds()) {
                return (d1.getPlageLivraison().getHeureFin().getTotalSeconds() != d2.getPlageLivraison().getHeureFin().getTotalSeconds() ? -1 : 1);
            }
            double do1 = graph.get(curr).get(o1).getDureeItineraire();
            long l1 = (long) do1;
            double do2 = graph.get(curr).get(o2).getDureeItineraire();
            long l2 = (long) do2;
            return (l1 < l2 ? -1 : (l1==l2 ? 0 : 1));

        });
        indexNVus = 0;
    }

    @Override
    public boolean hasNext() {
        return !(indexNVus == nonVus.size());
    }

    /**
     *
     * @return l'ID de la {@link DemandeLivraison} dont la {@link Modele.PlageHoraire} se termine le plus tôt ou (en cas d'égalité)
     * la {@link DemandeLivraison} la plus proche de nodeCurr parmi les {@link DemandeLivraison} restantes.
     */
    @Override
    public Long next() {
        Long ret = nonVus.get(indexNVus);
        indexNVus ++;
        return ret;
    }
}
