package ApplicationException;

public class ApplicationWarning extends ApplicationException {
    public ApplicationWarning() {
        super();
    }

    public ApplicationWarning(String s) {
        super(s);
    }
}
