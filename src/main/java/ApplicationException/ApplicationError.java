package ApplicationException;

public class ApplicationError extends ApplicationException {
    public ApplicationError() {
        super();
    }

    public ApplicationError(String s) {
        super(s);
    }
}
