package ApplicationException;

public abstract class ApplicationException extends Exception {

    public ApplicationException() {
        super();
    }

    public ApplicationException(String s) {
        super(s);
    }
}
