package ControleurVue;

import javafx.scene.paint.Color;

public class Palette {

    /**
     * Une couleur rouge  pâle pour le dessin des durées de livraison
     */
    public static final Color LightGrey = Color.web("#e1e6e9");
    /**
     * Couleur grise identique à celle de fond utilisée par la fenêtre
     */
    public static final Color Background = Color.web("#f4f4f4");

    /**
     * Une couleur rouge  pâle pour le dessin des tronçons
     */
    public static final Color LightRed = Color.web("#ff6767");
    /**
     * Une couleur rouge foncé pour le dessin des points de livraison et flèches
     */
    public static final Color Red = Color.web("#D50000");

    public static final Color Black = Color.BLACK;

    public static final Color White = Color.WHITE;


}
