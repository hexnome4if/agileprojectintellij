package ControleurVue;

import Modele.DemandeLivraison;
import Modele.Entrepot;
import Modele.ItineraireHorodate;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

import static ControleurVue.ControleurCanvasCarte.TypeFocus.FOCUSDEMANDELIVRAISON;
import static ControleurVue.ControleurCanvasCarte.TypeFocus.FOCUSTOURNEE;

public class ControleurFenetreGestionTournee extends ControleurFenetre {

    /**
     * Spécialisation de la méthode d'ouverture de fenêtre
     */
    public static void ouvrir() {
        ControleurFenetre.creer("LogistiX - Gestion d'une tournée", "/fxml/GestionTournee.fxml");
    }

    /**
     * Spécialisation de la méthode de déclaration du contrôleur de fenêtre auprès du contrôleur principal
     */
    public void informerControleurPrincipal(){
        controleurPrincipal.setControleurFenetreGestionTournee(this);
    }

    /**
     * Item de menu "Charger plan de livraisons"
     */
    @FXML
    protected MenuItem menuChargerPlan;

    /**
     * Item de menu "Charger carte de la ville"
     */
    @FXML
    protected MenuItem menuChargerCarte;

    /**
     * Bouton e démarage "Charger plan de livraison"
     */
    @FXML
    protected Button buttonChargerPlanLivraison;

    /**
     * Bouton e démarage "Charger carte"
     */
    @FXML
    protected Button buttonChargerCarte;

    /**
     * Bouton "Rétablir la tournée"
     */
    @FXML
    protected Button buttonRetablirTournee;

    /**
     * Bouton "Exporter la tournée"
     */
    @FXML
    protected Button buttonExporterTournee;

    /**
     * Bouton "Calculer la tournée"
     */
    @FXML
    protected Button buttonCalculTournee;

    /**
     * Bouton "Ajouter livraison"
     */
    @FXML
    protected Button buttonAjouterLivraison;
    
    /**
     * Bouton "Modifier livraison"
     */
    @FXML
    protected Button buttonModifierLivraison;
    
    /**
     * Bouton "Supprimer livraison"
     */
    @FXML
    protected Button buttonSupprimerLivraison;


    /**
     * Bouton "Changement de focus"
     */
    @FXML
    protected Button buttonFocus;

    /**
     * Label contenant l'identifiant du noeud de la livraison
     */
    @FXML
    protected TextField fieldNumeroPointLivraison;

    /**
     * Champ texte contenant l'heure de la livraison sélectionnée
     */
    @FXML
    protected TextField fieldHeure;

    /**
     * Champ texte contenant les minutes de la livraison sélectionnée
     */
    @FXML
    protected TextField fieldMinute;

    /**
     * Canvas contenant la carte
     */
    @FXML
    protected Canvas canvasCarte;

    /**
     * Canvas contenant la timeline
     */
    @FXML
    protected Canvas canvasTimeline;

    /**
     * Label de l'état de l'application
     */
    @FXML
    protected Label labelEtat;

    /**
     * Item de menu "Calculer tournee"
     */
    @FXML
    protected MenuItem menuCalculerTournee;

    /**
     * Item de menu "Exporter tournee"
     */
    @FXML
    protected MenuItem menuExporterTournee;

    /**
     * Item de menu "Ajouter livraison"
     */
    @FXML
    protected MenuItem menuAjouterLivraison;

    /**
     * Item de menu "Supprimer livraison"
     */
    @FXML
    protected MenuItem menuSupprimerLivraison;

    /**
     * Item de menu "Modifier livraison"
     */
    @FXML
    protected MenuItem menuModifierLivraison;

    /**
     * Item de menu "Annuler" (undo)
     */
    @FXML
    protected MenuItem menuAnnulerModification;

    /**
     * Item de menu "Répéter" (redo)
     */
    @FXML
    protected MenuItem menuRepeterModification;

    /**
     * Référence vers le contrôleur de la carte
     */
    protected ControleurCanvasCarte controleurCanvasCarte;

    /**
     * Référence vers le contrôleur de la timeline
     */
    protected ControleurCanvasTimeline controleurCanvasTimeline;

    /**
     * Livraison actuellement sélectionnée
     */
    protected DemandeLivraison livraisonCourante;

    /**
     * Chemin vers le fichier XML de la carte
     */
    protected String cheminFichierCarte = null;
    /**
     *  Chemin vers le fichier XML des livraisons
     */
    protected String cheminFichierPlanLivraison = null;

    /**
     * Constructeur
     */
    public ControleurFenetreGestionTournee() {

        livraisonCourante = null;

    }

    /**
     * Action lors du clic sur "Charger carte de la ville..."
     */
    public void onMenuChargerCarteClicked() {
        // Chercher le chemin fichier
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(new Stage());
        if (file != null) {

            cheminFichierCarte = file.getAbsolutePath();

            controleurPrincipal.chargerPlan();
        }
    }

    /**
     * Action lors du clic sur "Charger plan de livraisons..."
     */
    public void onMenuChargerDemandesPlanLivraisonClicked() {
        // Chercher le chemin fichier
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(new Stage());
        if (file != null) {

            cheminFichierPlanLivraison = file.getAbsolutePath();

            controleurPrincipal.chargerDemandesLivraison();
        }


    }
    /**
     * Écouteur de fenêtre lors de l'appui sur les flèches du clavier
     */
    EventHandler<KeyEvent> onKeyReleased = new EventHandler<KeyEvent>() {

        @Override
        public void handle(KeyEvent event) {

            if (livraisonCourante == null) {
                return;
            }

            DemandeLivraison livraisonSuivante = null;
            DemandeLivraison livraisonPrecedente = null;

            for (ItineraireHorodate itineraireHorodate : controleurPrincipal.getTournee().getListeTournee()) {

                if (livraisonCourante.getId() == itineraireHorodate.getItineraire().getLivraisonOrigine().getId()) {
                    livraisonSuivante = itineraireHorodate.getItineraire().getLivraisonArrivee();
                }

                if (livraisonCourante.getId() == itineraireHorodate.getItineraire().getLivraisonArrivee().getId()) {
                    livraisonPrecedente = itineraireHorodate.getItineraire().getLivraisonOrigine();
                }

            }

            if (event.getCode().toString() == "LEFT" && livraisonPrecedente != null) {

                setLivraisonCourante(livraisonPrecedente);

            } else if (event.getCode().toString() == "RIGHT" && livraisonSuivante != null) {

                canvasCarte.requestFocus();
                setLivraisonCourante(livraisonSuivante);
            }

        }
    };

    /**
     * Action lors du clic sur l'item de menu "Supprimer livraison"
     */
    public void onMenuSupprimerLivraisonClicked() {
        onButtonLancerSupprimerClicked();
    }

    /**
     * Action lors du clic sur l'item de menu "Modifier livraison"
     */
    public void onMenuModifierLivraisonClicked() {
        onButtonLancerModificationClicked();
    }

    /**
     * Action lors du clic sur l'item de menu "Ajouter livraison"
     */
    public void onMenuAjouterLivraisonClicked() {
        onButtonLancerAjoutClicked();
    }

    /**
     * Action lors du clic sur l'item de menu "Répété" (redo)
     */
    public void onMenuRepeterModificationClicked() {
        controleurPrincipal.redo();
    }

    /**
     * Action lors du clic sur l'item de menu "Annuler" (undo)
     */
    public void onMenuAnnulerModificationClicked() {
        controleurPrincipal.undo();
    }

    /**
     * Action lors du clic sur l'item de menu "Exporter tournée"
     */
    public void onMenuExporterTourneeClicked() {
        onButtonExporterTourneeClicked();
    }

    /**
     * Action lors du clic sur l'item menu "Fermer"
     */
    public void onMenuFermerClicked() {
        fermerFenetre();
    }

    /**
     * Action lors du clic sur "Calculer la tournée"
     */
    public void onMenuCalculerTourneeClicked() {

        buttonCalculTournee.setText("Calculer la tournée : Chargement...");

        // Potentiellement demander le chemin fichier (fenêtre 'autotmatique')

        controleurPrincipal.calculerTournee();

        try {
            livraisonCourante = controleurPrincipal.getTournee().getListeTournee().get(0).getItineraire().getLivraisonOrigine();

            controleurCanvasCarte.injecterTroncons(controleurPrincipal.carteVille.getListTroncon());
            controleurCanvasCarte.injecterTournee(controleurPrincipal.getTournee());

        } catch (Exception e) {
            e.printStackTrace();
        }

        controleurCanvasTimeline.injecterTournee(controleurPrincipal.tournee);


        setLivraisonCourante(controleurPrincipal.getTournee().getListeTournee().get(0).getItineraire().getLivraisonOrigine());

        actualiseVue();
    }

    /**
     * Lors du clic sur le bouton de démarage "Charger carte"
     */
    public void onButtonChargerCarteClicked() {
        buttonChargerCarte.setText("Charger carte : Chargement...");
        onMenuChargerCarteClicked();
        actualiseVue();
    }

    /**
     * Lors du clic sur le bouton de démarage "Charger plan livraison"
     */
    public void onButtonChargerPlanLivraisonClicked() {
        buttonChargerPlanLivraison.setText("Charger plan de livraisons : Chargement...");
        onMenuChargerDemandesPlanLivraisonClicked();
        actualiseVue();
    }


    /**
     * Action lors du clic sur le bouton "Exporte tournée"
     */
    public void onButtonExporterTourneeClicked() {
        controleurPrincipal.exportTournee();
    }

    /**
     * Action lors du clic sur le bouton "Ajout livraison"
     */
    public void onButtonLancerAjoutClicked() {
        controleurPrincipal.ajouterLivraison();
    }

    /**
     * Action lors du clic sur le bouton "Modifier livraison"
     */
    public void onButtonLancerModificationClicked() {
        controleurPrincipal.modifierLivraison();
    }

    /**
     * Action lors du clic sur le bouton "Supprimer livraison"
     */
    public void onButtonLancerSupprimerClicked() {
        controleurPrincipal.supprimerLivraison();
    }

    /**
     * Action lors du clic sur le bouton "Rétablir la tournée"
     */
    public void onButtonLancerResetClicked() {
        controleurPrincipal.resetTournee();
    }

    /**
     * Action lors du clic sur le bouton "Dé/Focaliser"
     */
    public void onButtonFocusClicked() {
        ControleurCanvasCarte.TypeFocus typeFocus = controleurCanvasCarte.switchFocus();

        if(typeFocus == FOCUSTOURNEE) {
            buttonFocus.setText("Focaliser");
        } else if(typeFocus == FOCUSDEMANDELIVRAISON) {
            buttonFocus.setText("Défocaliser");
        }
    }

    /**
     * Initialisation de la fenêtre
     */
    @Override
    public void initialisation(){

        buttonFocus.setFocusTraversable(false);
        buttonAjouterLivraison.setFocusTraversable(false);
        buttonModifierLivraison.setFocusTraversable(false);
        buttonSupprimerLivraison.setFocusTraversable(false);
        buttonCalculTournee.setFocusTraversable(false);
        buttonRetablirTournee.setFocusTraversable(false);
        buttonExporterTournee.setFocusTraversable(false);
        fieldNumeroPointLivraison.setFocusTraversable(false);
        fieldHeure.setFocusTraversable(false);
        fieldMinute.setFocusTraversable(false);

        canvasCarte.requestFocus();

        controleurCanvasCarte = new ControleurCanvasCarte(this, canvasCarte);
        controleurCanvasTimeline = new ControleurCanvasTimeline(this, canvasTimeline);

        canvasCarte.getScene().setOnKeyReleased(onKeyReleased);

        labelEtat.setText("");

        actualiseVue();

    }

    /**
     * Ferme la fenêtre
     */
    public void fermerFenetre() {
        ((Stage) labelEtat.getScene().getWindow()).close();

    }

    /** Renvoit la livraison actuellement sélectionnée
     * @return La livraison actuellement sélectionnée
     */
    public DemandeLivraison getLivraisonCourante() {
        return livraisonCourante;
    }

    /** Actualise la vue de la fenêtre
     */
    public void actualiseVue() {

        //Grisement et visiblité des composants

        boolean carteChargee = controleurPrincipal.carteVille != null;
        boolean planCharge = controleurPrincipal.planLivraison != null;
        boolean applicationChargee = carteChargee && planCharge;
        boolean tourneeCalculee = controleurPrincipal.getTournee() != null;

        buttonChargerPlanLivraison.setText("Charger plan de livraisons : " + (planCharge ? "✓" : "✗"));
        buttonChargerCarte.setText("Charger carte : " + (carteChargee ? "✓" : "✗"));

        buttonChargerCarte.setVisible(!tourneeCalculee);
        buttonChargerPlanLivraison.setVisible(!tourneeCalculee);
        buttonChargerPlanLivraison.setDisable(!carteChargee);
        buttonCalculTournee.setVisible(!tourneeCalculee);
        buttonCalculTournee.setDisable(!applicationChargee);

        menuChargerCarte.setVisible(!tourneeCalculee);
        menuChargerPlan.setVisible(!tourneeCalculee);
        menuChargerPlan.setDisable(!carteChargee);

        menuCalculerTournee.setVisible(!tourneeCalculee);
        menuCalculerTournee.setDisable(!tourneeCalculee);
        menuExporterTournee.setDisable(!tourneeCalculee);

        menuRepeterModification.setDisable(!tourneeCalculee);
        menuAnnulerModification.setDisable(!tourneeCalculee);

        menuAjouterLivraison.setDisable(!tourneeCalculee);
        menuModifierLivraison.setDisable(!tourneeCalculee);
        menuSupprimerLivraison.setDisable(!tourneeCalculee);

        buttonRetablirTournee.setDisable(!tourneeCalculee);
        buttonExporterTournee.setDisable(!tourneeCalculee);

        buttonFocus.setDisable(!tourneeCalculee);

        buttonAjouterLivraison.setDisable(!tourneeCalculee);
        buttonModifierLivraison.setDisable(!tourneeCalculee);
        buttonSupprimerLivraison.setDisable(!tourneeCalculee);

        if (tourneeCalculee) {

            //Détérminer heure/minute de la demande de livraison courante

            fieldNumeroPointLivraison.setText(String.valueOf(livraisonCourante.getAdresse().getId()));

            ItineraireHorodate itineraireCourant = null;

            for (ItineraireHorodate itineraireHorodate : controleurPrincipal.getTournee().getListeTournee()) {
                if (itineraireHorodate.getItineraire().getLivraisonArrivee() == livraisonCourante) {
                    itineraireCourant = itineraireHorodate;
                }
            }

            if (itineraireCourant != null) {
                fieldHeure.setText(String.valueOf((itineraireCourant.getHeureDeLivraison().getHeures())));
                fieldMinute.setText(String.valueOf((itineraireCourant.getHeureDeLivraison().getMinutes())));
            }

            //Canvas

            controleurCanvasCarte.redessiner();
            controleurCanvasTimeline.redessiner();

            //Grisement des contrôles pour l'entrepôt

            boolean isEntrepot = livraisonCourante instanceof Entrepot;

            buttonModifierLivraison.setDisable(isEntrepot);
            buttonSupprimerLivraison.setDisable(isEntrepot);

            menuModifierLivraison.setDisable(isEntrepot);
            menuSupprimerLivraison.setDisable(isEntrepot);

            menuAnnulerModification.setDisable(controleurPrincipal.getNbUndoableCmd() == 0);
            menuRepeterModification.setDisable(controleurPrincipal.getNbRedoableCmd() == 0);

        }

    }

    /** Redéfinit la livraison actuellement sélectionée puis actualise la vue
     * @param _livraisonCourante Une référence vers la livraison à sélectionner
     */
    public void setLivraisonCourante(DemandeLivraison _livraisonCourante) {
        livraisonCourante = _livraisonCourante;

        controleurPrincipal.selectionerLivraison();
    }

    /** Renouvelle les données métiers dans les composants de la fenêtre
     */
    public void reinjecterDonnee() {

        livraisonCourante = controleurPrincipal.getLivraisonSelected();

        controleurCanvasCarte.injecterTroncons(controleurPrincipal.carteVille.getListTroncon());
        controleurCanvasCarte.injecterTournee(controleurPrincipal.getTournee());

        controleurCanvasTimeline.injecterTournee(controleurPrincipal.getTournee());

        actualiseVue();
    }

    /**
     * Affiche un message dans la barre d'état de l'application
     * @param message Le message à afficher
     */
    public void afficherMessageEtat(String message) {
        labelEtat.setText(message);
    }


    /**
     * Retourne le chemin du fichier de les troncons de la carte défini par l'utilisateur
     * @return Le chemin vers le fichier
     */
    public String getCheminFichierCarte() {
        return cheminFichierCarte;
    }

    /**
     * Retourne le chemin du fichier du plan de livraison défini par l'utilisateur
     * @return Le chemin vers le fichier
     */
    public String getCheminFichierPlanLivraison() {
        return cheminFichierPlanLivraison;
    }
}
