package ControleurVue;


import Modele.*;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.TextAlignment;

public class ControleurCanvasTimeline {

    /**
     * Durée en seconde d'un entrepôt
     */
    protected final int dureeEntrepot = 30 * 60;

    /**
     * Hauteur d'une graduation temporelle
     */
    protected final double hauteurGraduation;

    /**
     * Hauteur d'une boite d'une livraison
     */
    protected final double hauteurBoite;
    /**
     * Une référence vers le contrôleur de fenêtre de la gestion de tournée
     */
    protected ControleurFenetreGestionTournee controleurFenetreGestionTournee;
    /**
     * Le canvas de la timeline
     */
    protected Canvas canvas;
    /**
     * Le contexte de dessin sur le canvas de la timeline
     */
    protected GraphicsContext context;

    /**
     * La tournée à dessiner sur la timeline
     */
    protected Tournee tournee;

    /**
     * Heure à partir de laquelle on dessine la timeline
     */
    protected Heure debutTimeline;
    /**
     * Heure jusqu'à laquelle on dessine la timeline
     */
    protected Heure finTimeline;
    /**
     * L'action déclenchée au clic sur le canvas de la timeline
     */
    EventHandler<MouseEvent> onMouseClicked = new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent event) {

            //On vérifie si la souris est dans l'un des boites de la timeline

            for (ItineraireHorodate itineraireHorodate : tournee.getListeTournee()) {

                Heure debutLivraison = itineraireHorodate.getPlageLivraison().getHeureDeb();
                Heure finLivraison = itineraireHorodate.getPlageLivraison().getHeureFin();

                int debutBoite = getXFromInstant(debutLivraison);
                int finBoite = getXFromInstant(finLivraison);

                if (debutBoite <= event.getX() && event.getX() <= finBoite && event.getY() < hauteurBoite) {
                    controleurFenetreGestionTournee.setLivraisonCourante(itineraireHorodate.getItineraire().getLivraisonArrivee());
                    break;
                }

            }

        }
    };

    /**
     * Constructeur
     *
     * @param _controleurFenetreGestionTournee Le contrôleur de fenêtre de la gestion de tournée
     * @param _canvas                          Le canvas sur lequel on dessine la timeline
     */
    public ControleurCanvasTimeline(ControleurFenetreGestionTournee _controleurFenetreGestionTournee, Canvas _canvas) {

        controleurFenetreGestionTournee = _controleurFenetreGestionTournee;
        canvas = _canvas;
        context = canvas.getGraphicsContext2D();

        debutTimeline = new Heure(5, 30, 0);
        finTimeline = new Heure(20, 30, 0);

        hauteurGraduation = canvas.getHeight() - 17;
        hauteurBoite = hauteurGraduation - 6;

        canvas.setOnMouseClicked(onMouseClicked);

        redessiner();
    }

    /**
     * Injecte dans le contrôleur la tournée à dessiner
     * @param _tournee La tournée à dessiner
     */
    public void injecterTournee(Tournee _tournee) {
        tournee = _tournee;

        if (tournee == null) {
            return;
        }

        // On câle la plage horraire de la timeline de la première à la dernière livraison
        // On fixe à la volée la durée de l'entrepôt (pour éviter qu'il s'étendre jusqu'à l'heure de borne d'une journée)

        int nombreLivraisons = tournee.getListeTournee().size();

        PlageHoraire premiereLivraison = tournee.getListeTournee().get(0).getPlageLivraison();
        PlageHoraire derniereLivraison = null;

        if (nombreLivraisons == 1) {
            derniereLivraison = premiereLivraison;
        } else if (nombreLivraisons > 1) {

            try {
                derniereLivraison = tournee.getListeTournee().get(nombreLivraisons - 1).getPlageLivraison().clone();
                derniereLivraison.setHeureFin(new Heure(derniereLivraison.getHeureDeb().getTotalSeconds() + dureeEntrepot));
            } catch (CloneNotSupportedException e) {
            }

        }

        if (derniereLivraison != null) {
            debutTimeline = new Heure(premiereLivraison.getHeureDeb().getTotalSeconds() - dureeEntrepot);
            finTimeline = new Heure(derniereLivraison.getHeureFin().getTotalSeconds() + dureeEntrepot);
        }

    }

    /**
     * Détérmine une position horizontale sur la timeline pour un instant donné
     * @param instant L'instant dont on veut calculer la position horizontale
     * @return
     */
    protected int getXFromInstant(Heure instant) {

        return (int) ((instant.getTotalSeconds() - debutTimeline.getTotalSeconds()) * canvas.getWidth() / (finTimeline.getTotalSeconds() - debutTimeline.getTotalSeconds()));

    }

    /**
     * Dessine la plage horraire d'une demande de livraison
     * @param itineraireHorodate La livraison dont on souhaite dessiner la plage horraire
     */
    protected void dessinerLivraison(ItineraireHorodate itineraireHorodate) {

        boolean isEntrepot = itineraireHorodate.getItineraire().getLivraisonArrivee() instanceof Entrepot;

        // Desin d'une boite (Plage en rouge)
        Heure debutPlageLivraison = itineraireHorodate.getPlageLivraison().getHeureDeb();
        Heure finPlageLivraison = itineraireHorodate.getPlageLivraison().getHeureFin();

        if (isEntrepot) {
            try {
                finPlageLivraison = finPlageLivraison.clone();
            } catch (CloneNotSupportedException e) {
            }
            finPlageLivraison = new Heure(debutPlageLivraison.getTotalSeconds() + dureeEntrepot);

            context.setFill(Palette.Black);
        } else {
            context.setFill(Palette.Red);
        }

        int debutBoitePlage = getXFromInstant(debutPlageLivraison);
        int finBoitePlage = getXFromInstant(finPlageLivraison);

        context.fillRect(debutBoitePlage, 1, finBoitePlage-debutBoitePlage, hauteurBoite);

        // Dessin des horraires de livraison (sous boite en saumon)
        Heure debutLivraison = itineraireHorodate.getHeureDeLivraison();
        int dureeLivraison = isEntrepot ? dureeEntrepot : (int) (long) itineraireHorodate.getItineraire().getLivraisonArrivee().getDureeLivraisonSec();
        Heure finLivraison = new Heure(itineraireHorodate.getHeureDeLivraison().getTotalSeconds() + dureeLivraison);

        int debutBoiteLivraison = getXFromInstant(debutLivraison);
        int finBoiteLivraison = Math.min(getXFromInstant(finLivraison), finBoitePlage-2);

        //Coloration en blanc au survol ou non
        if(itineraireHorodate.getItineraire().getLivraisonArrivee() == controleurFenetreGestionTournee.getLivraisonCourante()) {
            context.setFill(Palette.White);
        } else {
            context.setFill(isEntrepot ? Palette.LightGrey : Palette.LightRed);
        }

        final int padding = 2;
        context.fillRect(debutBoiteLivraison, 1 + padding, finBoiteLivraison - debutBoiteLivraison, hauteurBoite - padding * 2);

    }

    /**
     * Met à jour le dessin sur le canvas
     */
    public void redessiner() {

        // On redessine le fond du canvas
        context.setFill(Palette.Background);
        context.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

        context.setFill(Palette.Black);
        context.setStroke(Palette.Black);

        //On dessine le repère temporell (chaque heure)

        context.strokeLine(0, 0, canvas.getWidth(), 0);

        for(int i = 1; i < 24; i++) {

            int x = getXFromInstant(new Heure(i, 0, 0));

            context.setTextAlign(TextAlignment.CENTER);
            context.setTextBaseline(VPos.BASELINE);

            if(0 <= x && x <= canvas.getWidth()) {
                context.strokeLine(x, 0, x, hauteurGraduation);
                context.fillText(String.valueOf(i) + "h", x, canvas.getHeight());
            }

        }

        if (tournee == null) {
            return;
        }

        // On dessine les boites de chaque livraison

        for(ItineraireHorodate itineraireHorodate : tournee.getListeTournee()) {

            dessinerLivraison(itineraireHorodate);

        }
    }


}
