package ControleurVue;

import Modele.Heure;
import Modele.PlageHoraire;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ControleurFenetreAjoutLivraison extends ControleurFenetre {

    /**
     * Spécialisation de la méthode d'ouverture de fenêtre
     */
    public static void ouvrir() {
        ControleurFenetre.creer("LogistiX - Ajout d'une livraison", "/fxml/AjoutLivraison.fxml");
    }

    /**
     * Spécialisation de la méthode de déclaration du contrôleur de fenêtre auprès du contrôleur principal
     */
    public void informerControleurPrincipal(){
        controleurPrincipal.setControleurFenetreAjoutLivraison(this);
    }

    /**
     * Champ texte contenant le numéro du point de livraison
     */
    @FXML
    protected TextField fieldNumeroPointLivraison;

    /**
     * Containeur des contrôles pour la plage horraire
     */
    @FXML
    protected Pane panePlageHorraire;

    /**
     * Bouton d'incrément de l'heure de début
     */
    @FXML
    protected Button buttonHeureDebutUp;

    /**
     * Bouton de décrément de l'heure de début
     */
    @FXML
    protected Button buttonHeureDebutDown;

    /**
     * Bouton d'incrément des minutes de début
     */
    @FXML
    protected Button buttonMinuteDebutUp;

    /**
     * Bouton de décrément des minutes de début
     */
    @FXML
    protected Button buttonMinuteDebutDown;

    /**
     * Bouton de décrément de l'heure de fin
     */
    @FXML
    protected Button buttonHeureFinDown;

    /**
     * Bouton d'incrément de l'heure de fin
     */
    @FXML
    protected Button buttonHeureFinUp;

    /**
     * Bouton de décrément des minutes de fin
     */
    @FXML
    protected Button buttonMinuteFinDown;

    /**
     * Bouton d'incrément des minutes de fin
     */
    @FXML
    protected Button buttonMinuteFinUp;

    /**
     * Label "Ajout d'une livrasion"
     */
    @FXML
    protected Label labelTitre;

    /**
     * Bouton "Annuler"
     */
    @FXML
    protected Button buttonAnnuler;

    /**
     * Bouton "Tester"
     */
    @FXML
    protected Button buttonTester;

    /**
     * Bouton "Valider"
     */
    @FXML
    protected Button buttonValider;

    /**
     * Champ texte contenant l'heure de début
     */
    @FXML
    protected TextField fieldHeureDebut;

    /**
     * Champ texte contenant les minutes de début
     */
    @FXML
    protected TextField fieldMinuteDebut;

    /**
     * Case à cocher pour préciser une plage horraire
     */
    @FXML
    protected CheckBox checkCreneauLivraison;

    /**
     * Champ texte contenant l'heure de fin
     */
    @FXML
    protected TextField fieldHeureFin;

    /**
     * Champ texte contenant les minutes de fin
     */
    @FXML
    protected TextField fieldMinuteFin;

    /**
     * Label contenant le résultat du test
     */
    @FXML
    protected Label labelResultatTest;

    /**
     * Initialisation de la fenêtre
     */
    @Override
    public void initialisation() {

        labelResultatTest.setText("Tester votre modification avant de valider");

        fieldHeureDebut.setText(String.valueOf(Heure.DEBUT_JOURNEE.getHeures()));
        fieldMinuteDebut.setText(String.valueOf(Heure.DEBUT_JOURNEE.getMinutes()));

        fieldHeureFin.setText(String.valueOf(Heure.FIN_JOURNEE.getHeures()));
        fieldMinuteFin.setText(String.valueOf(Heure.FIN_JOURNEE.getMinutes()));

        buttonValider.setDisable(true);

        checkCreneauLivraison.setSelected(false);
        panePlageHorraire.setVisible(false);

        buttonAnnuler.getScene().getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent event) {
                controleurPrincipal.annulerAjout();
            }
        });

        numericField(fieldNumeroPointLivraison, null, null, null);
        numericField(fieldHeureDebut, 24L, buttonHeureDebutUp, buttonHeureDebutDown);
        numericField(fieldMinuteDebut, 60L, buttonMinuteDebutUp, buttonMinuteDebutDown);
        numericField(fieldHeureFin, 24L, buttonHeureFinUp, buttonHeureFinDown);
        numericField(fieldMinuteFin, 60L, buttonMinuteFinUp, buttonMinuteFinDown);

        onFieldChanged(fieldNumeroPointLivraison);
        onFieldChanged(fieldHeureDebut);
        onFieldChanged(fieldMinuteDebut);
        onFieldChanged(fieldHeureFin);
        onFieldChanged(fieldMinuteFin);
    }

    /**
     * Action lors d'un clic sur "Annuler"
     */
    public void onButtonAnnulerClicked() {

        controleurPrincipal.annulerAjout();

    }

    /**
     * Action d'un clic sur "Tester"
     */
    public void onButtonTesterClicked() {

        controleurPrincipal.testerAjout();

    }

    /**
     * Action lors d'un clic sur "Valider"
     */
    public void onButtonValiderClicked() {

        controleurPrincipal.validerAjout();

    }

    /**
     * Action lors d'un clic sur le case à cocher "Définir une plage horraire"
     * @param actionEvent L'événement lancé lors du clic
     */
    public void onAffichagePlageChanged(ActionEvent actionEvent) {

        panePlageHorraire.setVisible(checkCreneauLivraison.isSelected());
        buttonValider.setDisable(true);

    }

    /**
     * Grise le bouton "Valider" lors d'une modification d'un champ texte
     * @param textField Le champ texte invoquant le grisement
     */
    protected void onFieldChanged(TextField textField) {
        textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                if(oldValue != newValue) {
                    buttonValider.setDisable(true);
                }

            }
        });
    }

    /** Modifie un champ texte comme champ numérique (ajoute des contraintes sur son contenu, active l'in/décrémentation grâce ux flèches du clavier, et bind des boutons UP et DOWN pour in/décrementer)
     * @param textField Le champ texte dont on veut modifier le comportement
     * @param max La valeur numérique maximum que le champ peut prendre (null si non définie)
     * @param buttonUp Un bouton up associé (null si indéfini)
     * @param buttonDown Un bouton down associé (null si indéfini)
     */
    protected void numericField(TextField textField, Long max, Button buttonUp, Button buttonDown) {

        textField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {

                String clearValue = newValue.replaceAll("[^\\d]", "");

                if (!newValue.equals(clearValue)) {
                    textField.setText(clearValue);
                }

                long value;

                try {
                    value = Long.valueOf(textField.getText());
                } catch (Exception e) {
                    value = 0;
                }

                if (max != null && value > max) {
                    value = Long.valueOf(oldValue);
                }

                textField.setText(String.valueOf(value));

            }
        });

        textField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent keyEvent) {

                long value;

                try {
                    value = Long.valueOf(textField.getText());
                } catch (Exception e) {
                    value = 0;
                }

                boolean modified = false;

                if (keyEvent.getCode() == KeyCode.UP) {
                    value++;
                    modified = true;
                }

                if (keyEvent.getCode() == KeyCode.DOWN) {
                    value--;
                    modified = true;
                }

                if (modified) {
                    if (max != null) {
                        value = (max + value) % max;
                    }
                    textField.setText(String.valueOf(value));
                }

            }
        });

        if(buttonUp != null && buttonDown != null) {

            buttonUp.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent actionEvent) {

                    long value = Long.valueOf(textField.getText());

                    value++;

                    if(max != null) {
                        value = (max + value) % max;
                    }

                    textField.setText(String.valueOf(value));
                }
            });

            buttonDown.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent actionEvent) {

                    long value = Long.valueOf(textField.getText());

                    value--;

                    if(max != null) {
                        value = (max + value) % max;
                    }

                    textField.setText(String.valueOf(value));
                }
            });

        }
    }


    /**
     * Ferme la fenêtre
     */
    public void fermerFenetre() {
        ((Stage) labelTitre.getScene().getWindow()).close();

    }

    /**
     * Modifie l'affichage du résultat comme un échec
     * @param erreur L'erreur à afficher
     */
    public void mettreAJourEchec(String erreur) {

        labelResultatTest.setText("Cette modification est impossible : \n" + erreur);

    }

    /**
     * Modifie l'affichage du résultat comme un succès et dégrise le bouton "Valider"
     * @param message Le message à afficher
     */
    public void mettreAJourSuccess(String message) {

        labelResultatTest.setText("Cette modification est réalisable\n" + message);

        buttonValider.setDisable(false);

    }


    /**
     * Renvoit la plage horraire définie dans la fenêtre
     * @return La plage horraire définie
     */
    public PlageHoraire getplageHoraire() {

        Heure debut = new Heure(Integer.parseInt(fieldHeureDebut.getText()), Integer.parseInt(fieldMinuteDebut.getText()), 0);
        Heure fin = new Heure(Integer.parseInt(fieldHeureFin.getText()), Integer.parseInt(fieldMinuteFin.getText()), 0);

        return checkCreneauLivraison.isSelected() ? new PlageHoraire(debut, fin) : new PlageHoraire(Heure.DEBUT_JOURNEE, Heure.FIN_JOURNEE);
    }


    /**
     * Renvoit le numéro du noeud défini dans la fenêtre
     * @return Le numéro du noeud défini
     */
    public Long getNumNoeud() {
        return fieldNumeroPointLivraison.getText().length() == 0 ? 0 : Long.parseLong(fieldNumeroPointLivraison.getText());
    }

    /**
     * Signale à l'utilisateur que le noeud est inexistant
     */
    public void mettreAJourNoeudInexistant() {
        labelResultatTest.setText("Ce noeud est inexistant");
    }

    /**
     * Signale à l'utilisateur que l'ajout n'est pas valide
     */
    public void mettreAJourEchecAjoutInvalide() {
        labelResultatTest.setText("Ajout invalide");
    }

    /**
     * Renvoit la durée de la livraison
     * @return La durée de la livraison
     */
    public Long getDuree() {
        return (long) 10*60;
    }
}
