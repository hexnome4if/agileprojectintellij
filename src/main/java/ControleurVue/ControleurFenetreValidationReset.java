package ControleurVue;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ControleurFenetreValidationReset extends ControleurFenetre{

    /**
     * Label du titre "Validation de réinitialisation"
     */
    @FXML
    protected Label labelTitre;

    /**
     * Spécialisation de la méthode d'ouverture de fenêtre
     */
    public static void ouvrir() {
        ControleurFenetre.creer("LogistiX - Validation de la réinitialisation", "/fxml/ValidationReset.fxml");
    }

    /**
     * Initialisation de la fenêtre
     */
    @Override
    public void initialisation() {
        labelTitre.getScene().getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent event) {
                controleurPrincipal.annulerReset();
            }
        });
    }

    /**
     * Spécialisation de la méthode de déclaration du contrôleur de fenêtre auprès du contrôleur principal
     */
    public void informerControleurPrincipal(){
        controleurPrincipal.setControleurFenetreValidationReset(this);
    }

    /**
     * Action lors d'un clic sur "Valider"
     */
    public void onButtonValiderResetClicked() {
        controleurPrincipal.validerReset();
    }

    /**
     * Action lors d'un clic sur "Annuler"
     */
    public void onButtonAnnulerResetClicked() {
        controleurPrincipal.annulerReset();
    }

    /**
     * Ferme la fenêtre
     */
    public void fermerFenetre() {
        ((Stage) labelTitre.getScene().getWindow()).close();
    }

}
