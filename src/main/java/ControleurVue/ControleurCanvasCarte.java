package ControleurVue;


import Modele.*;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class ControleurCanvasCarte {

    /**
     * Une référence vers le contrôleur de fenêtre de la gestion de tournée
     */
    protected ControleurFenetreGestionTournee controleurFenetreGestionTournee;

    /**
     * Le canvas de la carte
     */
    protected Canvas canvas;
    /**
     * Le contexte de dessin sur le canvas de la carte
     */
    protected GraphicsContext context;

    /**
     * La liste des troncons de la carte à dessiner
     */
    protected ArrayList<Troncon> troncons;
    /**
     * La tournée à dessiner sur la carte
     */
    protected Tournee tournee;

    /**
     * L'ensemble de tout les noeuds de la carte
     */
    protected HashSet<Noeud> noeuds;
    /**
     * Une map associant une demande de livraison à un noeud de la carte
     */
    protected HashMap<Noeud, DemandeLivraison> livraisons;

    /**
     * Le noeud actuellement survolé
     */
    protected Noeud noeudSurvole;

    /**
     * La position horizontale de la souris sur le canvas
     */
    protected double moveX;
    /**
     * La position verticale de la souris sur le canvas
     */
    protected double moveY;

    /**
     * La position horizontale à partir de laquelle on trouve des éléments intéressants à déssiner
     */
    protected int minX;
    /**
     * La position horizontale jusqu'à laquelle on trouve des éléments intéressants à déssiner
     */
    protected int maxX;
    /**
     * La position verticale à partir de laquelle on trouve des éléments intéressants à déssiner
     */
    protected int minY;
    /**
     * La position verticale jusqu'à laquelle on trouve des éléments intéressants à déssiner
     */
    protected int maxY;

    /**
     * Le décalage horizontal pour dessiner les éléments sur le canvas
     */
    protected int offsetX;
    /**
     * Le décalage vertical pour dessiner les éléments sur le canvas
     */
    protected int offsetY;
    /**
     * Le ratio de zoom pour dessiner les éléments sur le canvas
     */
    protected double ratio;

    /**
     * Le type de focalisation sur la carte
     */
    protected TypeFocus typeFocus;
    ;
    /**
     * L'action déclenchée au clic sur le canvas de la carte
     */
    EventHandler<MouseEvent> onMouseClicked = new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent event) {

            // On set la livraison courante si on est sur un point de la tournée

            if (noeudSurvole != null) {

                DemandeLivraison livraison = livraisons.get(noeudSurvole);

                if (livraison != null) {
                    controleurFenetreGestionTournee.setLivraisonCourante(livraison);
                }
            }

        }
    };
    ;

    /**
     * Constructeur
     * @param _controleurFenetreGestionTournee Le contrôleur de fenêtre de la gestion de tournée
     * @param _canvas Le canvas sur lequel on dessine la carte
     */
    public ControleurCanvasCarte(ControleurFenetreGestionTournee _controleurFenetreGestionTournee, Canvas _canvas) {

        controleurFenetreGestionTournee = _controleurFenetreGestionTournee;
        canvas = _canvas;
        context = canvas.getGraphicsContext2D();

        canvas.setOnMouseClicked(onMouseClicked);
        canvas.setOnMouseMoved(onMouseMoved);

        offsetX = offsetY = 10;
        ratio = 1;

        troncons = null;

        moveX = -100;
        moveY = -100;

        typeFocus = TypeFocus.FOCUSTOURNEE;

        redessiner();
    }
    /**
     * L'action déclenchée au au survol de la souris sur le canvas de la carte
     */
    EventHandler<MouseEvent> onMouseMoved = new EventHandler<MouseEvent>() {

        @Override
        public void handle(MouseEvent event) {

            // On enregistre la position de la souris

            moveX = event.getX();
            moveY = event.getY();

            redessiner();

        }
    };

    /**
     * Calcul la zone de focus minimale pour afficher le point de livraison actuellement séléctioné et ses deux voisins dans la tournée
     */
    protected void focusPointLivraison() {

        DemandeLivraison livraisonSuivante = null;
        DemandeLivraison livraisonPrecedente = null;

        for (ItineraireHorodate itineraireHorodate : tournee.getListeTournee()) {

            if (controleurFenetreGestionTournee.getLivraisonCourante() == itineraireHorodate.getItineraire().getLivraisonOrigine()) {
                livraisonSuivante = itineraireHorodate.getItineraire().getLivraisonArrivee();
            }

            if (controleurFenetreGestionTournee.getLivraisonCourante() == itineraireHorodate.getItineraire().getLivraisonArrivee()) {
                livraisonPrecedente = itineraireHorodate.getItineraire().getLivraisonOrigine();
            }
        }

        minX = Math.min(livraisonPrecedente.getAdresse().getCoord().getCoordX(),
                livraisonSuivante.getAdresse().getCoord().getCoordX());

        minX = Math.min(minX,
                controleurFenetreGestionTournee.getLivraisonCourante().getAdresse().getCoord().getCoordX());

        maxX = Math.max(livraisonPrecedente.getAdresse().getCoord().getCoordX(),
                livraisonSuivante.getAdresse().getCoord().getCoordX());

        maxX = Math.max(maxX,
                controleurFenetreGestionTournee.getLivraisonCourante().getAdresse().getCoord().getCoordX());

        minY = Math.min(livraisonPrecedente.getAdresse().getCoord().getCoordY(),
                livraisonSuivante.getAdresse().getCoord().getCoordY());

        minY = Math.min(minY,
                controleurFenetreGestionTournee.getLivraisonCourante().getAdresse().getCoord().getCoordY());

        maxY = Math.max(livraisonPrecedente.getAdresse().getCoord().getCoordY(),
                livraisonSuivante.getAdresse().getCoord().getCoordY());

        maxY = Math.max(maxY,
                controleurFenetreGestionTournee.getLivraisonCourante().getAdresse().getCoord().getCoordY());

        centrer(false);
    }

    /**
     * Injecte dans le contrôleur de carte les tronçons à dessiner
     * @param _troncons Les tronçons de carte à dessiner
     */
    public void injecterTroncons(ArrayList<Troncon> _troncons) {

        troncons = _troncons;

        noeuds = new HashSet<Noeud>();

        for (Troncon t : troncons) {

            noeuds.add(t.getOrigine());
            noeuds.add(t.getArrivee());

        }

    }

    /**
     * Injecte dans le contrôleur la tournée à dessiner
     * @param _tournee La tournée à dessiner
     */
    public void injecterTournee(Tournee _tournee) {
        tournee = _tournee;

        livraisons = new HashMap<Noeud, DemandeLivraison>();

        for(ItineraireHorodate itineraireHorodate : tournee.getListeTournee()) {

            livraisons.put(itineraireHorodate.getItineraire().getLivraisonOrigine().getAdresse(), itineraireHorodate.getItineraire().getLivraisonOrigine());
            livraisons.put(itineraireHorodate.getItineraire().getLivraisonArrivee().getAdresse(), itineraireHorodate.getItineraire().getLivraisonArrivee());

        }

    }

    /**
     * Calcul la zone de focus minimale pour afficher tout les tronçons et points de livraison d'une tournée
     */
    protected void focusTournee() {

        if (tournee == null) {
            return;
        }

        minX = maxX = tournee.getListeTournee().get(0).getItineraire().getListTroncon().get(0).getOrigine().getCoord().getCoordX();
        minY = maxY = tournee.getListeTournee().get(0).getItineraire().getListTroncon().get(0).getOrigine().getCoord().getCoordY();

        for(ItineraireHorodate itineraireHorodate : tournee.getListeTournee()) {

            for(Troncon t : itineraireHorodate.getItineraire().getListTroncon()) {

                minX = Math.min(minX, t.getOrigine().getCoord().getCoordX());
                minX = Math.min(minX, t.getArrivee().getCoord().getCoordX());

                maxX = Math.max(maxX, t.getOrigine().getCoord().getCoordX());
                maxX = Math.max(maxX, t.getArrivee().getCoord().getCoordX());

                minY = Math.min(minY, t.getOrigine().getCoord().getCoordY());
                minY = Math.min(minY, t.getArrivee().getCoord().getCoordY());

                maxY = Math.max(maxY, t.getOrigine().getCoord().getCoordY());
                maxY = Math.max(maxY, t.getArrivee().getCoord().getCoordY());

            }

        }

        centrer(false);

    }

    /**
     * Switch entre une focalisation sur une demande de livraison et focalisation sur la tournée
     *
     * @return Le type de focus désormais défini
     */
    public TypeFocus switchFocus() {
        switch (typeFocus) {
            case FOCUSTOURNEE:
                focusTournee();
                typeFocus = TypeFocus.FOCUSDEMANDELIVRAISON;
                break;
            case FOCUSDEMANDELIVRAISON:
                focusPointLivraison();
                typeFocus = TypeFocus.FOCUSTOURNEE;
                break;
            default:
                break;
        }
        redessiner();
        return typeFocus;
    }

    /**
     * Met à jour le dessin sur le canvas
     */
    public void redessiner() {

        switch (typeFocus) {
            case FOCUSTOURNEE:
                focusTournee();
                break;
            case FOCUSDEMANDELIVRAISON:
                focusPointLivraison();
                break;
            default:
                break;
        }

        determinerNoeudSurvolee();

        // Dessin du fond de la canvas

        context.setFill(Palette.LightGrey);
        context.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

        context.setStroke(Palette.White);
        context.setLineWidth(2);
        context.setLineDashes(null);

        if (troncons == null) {
            return;
        }

        // Dessin des troncons de la carte de la ville

        for (Troncon t : troncons) {

            dessinerTroncon(t, false);

        }

        // Dessin de la tournée

        context.setStroke(Palette.LightRed);
        context.setLineWidth(3);

        boolean tronconExplore = true;

        for (ItineraireHorodate itineraireHorodate : tournee.getListeTournee()) {

            if(controleurFenetreGestionTournee.getLivraisonCourante() == itineraireHorodate.getItineraire().getLivraisonOrigine()) {

                tronconExplore = false;
            }

            context.beginPath();

            if(itineraireHorodate.getItineraire().getListTroncon().size() > 0)
            {
                double Ox = offsetX + (itineraireHorodate.getItineraire().getListTroncon().get(0).getOrigine().getCoord().getCoordX() - minX) * ratio;
                double Oy = offsetY + (itineraireHorodate.getItineraire().getListTroncon().get(0).getOrigine().getCoord().getCoordY() - minY) * ratio;

                context.moveTo(Ox, Oy);

                // Première passe : Dessin des troncons de chaque itinéraire de la tournée

                for (Troncon t : itineraireHorodate.getItineraire().getListTroncon()) {

                    context.setStroke(Palette.LightRed);

                    if(tronconExplore) {
                        context.setLineDashes(null);
                    } else {
                        context.setLineDashes(10, 15);
                    }

                    dessinerTroncon(t, true);
                }

                context.stroke();

                int indexTroncon = 0;
                int indexMiChemin = itineraireHorodate.getItineraire().getListTroncon().size() / 2;

                double Dx = offsetX + (itineraireHorodate.getItineraire().getListTroncon().get(itineraireHorodate.getItineraire().getListTroncon().size() - 1).getArrivee().getCoord().getCoordX() - minX) * ratio;
                double Dy = offsetY + (itineraireHorodate.getItineraire().getListTroncon().get(itineraireHorodate.getItineraire().getListTroncon().size() - 1).getArrivee().getCoord().getCoordY() - minY) * ratio;

                // Deuxième passe : Dessin des flèches à mi chemin sur les itinéraires de la tournée

                for (Troncon t : itineraireHorodate.getItineraire().getListTroncon()) {

                    indexTroncon++;

                    if (indexTroncon == indexMiChemin) {

                        context.setFill(Palette.Black);

                        Noeud noeudA = t.getOrigine();
                        Noeud noeudB = t.getArrivee();

                        double Ax = offsetX + (noeudA.getCoord().getCoordX() - minX) * ratio;
                        double Ay = offsetY + (noeudA.getCoord().getCoordY() - minY) * ratio;

                        double Bx = offsetX + (noeudB.getCoord().getCoordX() - minX) * ratio;
                        double By = offsetY + (noeudB.getCoord().getCoordY() - minY) * ratio;

                        double Mx = (Ax + Bx) / 2;
                        double My = (Ay + By) / 2;

                        double Vx = Dx - Ox;
                        double Vy = Dy - Oy;

                        double distance = Math.sqrt(Math.pow(Vx, 2) + Math.pow(Vy, 2));

                        Vx = Vx / distance * 12;
                        Vy = Vy / distance * 12;

                        context.beginPath();

                        context.moveTo(Mx, My);
                        context.lineTo(Mx + Vx - Vy, My + Vy + Vx);
                        context.lineTo(Mx + Vx + Vy, My + Vy - Vx);
                        context.closePath();

                        context.fill();


                    } else {
                        context.setStroke(Palette.LightRed);
                    }

                }
            }

        }

        // Dessin des points de livraisons

        for (ItineraireHorodate itineraireHorodate : tournee.getListeTournee()) {

            dessinerLivraison(itineraireHorodate.getItineraire().getLivraisonArrivee());
        }

    }

    /**
     * Dessiner un point de livraison sur le canvas
     * Le dessin prend en compte le type de point de livraison (entrepôt ou classique), si le noeud associé est survolé ou non, et si la livraison est séléctionée ou non
     *
     * @param livraison Le point de livraison à dessiner
     */
    protected void dessinerLivraison(DemandeLivraison livraison) {

        if (livraison instanceof Entrepot) {
            context.setFill(Palette.Black);
        } else {
            context.setFill(Palette.Red);
        }

        Noeud noeud = livraison.getAdresse();

        double x = offsetX + (noeud.getCoord().getCoordX() - minX) * ratio;
        double y = offsetY + (noeud.getCoord().getCoordY() - minY) * ratio;

        // Su le point est survolé ou séléctionné, il apparait plus gros

        int diameter = (noeudSurvole == noeud || livraison == controleurFenetreGestionTournee.getLivraisonCourante()) ? 24 : 16;

        context.fillOval(x - diameter / 2, y - diameter / 2, diameter, diameter);

        // Si le point est séléctionné, il est marqué en blanc

        if (livraison == controleurFenetreGestionTournee.getLivraisonCourante()) {
            context.setFill(Palette.White);

            diameter /= 2;

            context.fillOval(x - diameter / 2, y - diameter / 2, diameter, diameter);
        }
    }

    /**
     * Calcul le décalage horizontal/vertical et le ratio de zoom pour afficher de manière centrée la zone minimale des éléments à dessiner
     * @param cover Mode d'affichage des éléments dans la zone de focus (false = contenu en entier dans le canvas, true = couvert par le canvas)
     */
    protected void centrer(boolean cover) {

        offsetX = offsetY = (typeFocus == TypeFocus.FOCUSDEMANDELIVRAISON) ? 20 : 10;

        double deltaX = maxX - minX;
        double deltaY = maxY - minY;

        final double ratioW = (canvas.getWidth() - offsetX * 2) / deltaX;
        final double ratioH = (canvas.getHeight() - offsetY * 2) / deltaY;

        if (cover) {
            ratio = Math.max(ratioH, ratioW);
        } else {
            ratio = Math.min(ratioH, ratioW);
        }

        offsetX += (canvas.getWidth() - offsetX * 2 - deltaX * ratio) / 2;
        offsetY += (canvas.getHeight() - offsetY * 2 - deltaY * ratio) / 2;
    }

    /**
     * Détérmine le noeud survolé par la souris s'il existe
     */
    protected void determinerNoeudSurvolee() {

        noeudSurvole = null;

        if(tournee == null) {
            return;
        }

        Double minDistance = null;
        Noeud minDistanceNoeud = null;

        for(Noeud noeud : noeuds) {

            if(livraisons.get(noeud) == null) {
                continue;
            }

            double x = offsetX + (noeud.getCoord().getCoordX() - minX) * ratio;
            double y = offsetY + (noeud.getCoord().getCoordY() - minY) * ratio;

            double distance = Math.sqrt(Math.pow(x - moveX, 2) + Math.pow(y - moveY, 2));

            if(distance < 30 && (minDistance == null || distance < minDistance)) {
                minDistance = distance;
                minDistanceNoeud = noeud;
            }

        }

        noeudSurvole = minDistanceNoeud;

    }

    /**
     * Un type de focalisation sur la carte
     */
    protected enum TypeFocus {
        FOCUSTOURNEE, FOCUSDEMANDELIVRAISON
    }

    /**
     * Dessine un tronçon sur le canvas
     * @param troncon Le tronçon à dessiner
     */
    protected void dessinerTroncon(Troncon troncon, boolean pathed) {

        Noeud noeudA = troncon.getOrigine();
        Noeud noeudB = troncon.getArrivee();

        double Ax = offsetX + (noeudA.getCoord().getCoordX() - minX) * ratio;
        double Ay = offsetY + (noeudA.getCoord().getCoordY() - minY) * ratio;

        double Bx = offsetX + (noeudB.getCoord().getCoordX() - minX) * ratio;
        double By = offsetY + (noeudB.getCoord().getCoordY() - minY) * ratio;

        if(pathed) {
            context.lineTo(Bx, By);
        } else {
           context.strokeLine(Ax, Ay, Bx, By);
        }


    }

}