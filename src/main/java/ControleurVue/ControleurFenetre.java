package ControleurVue;

import Controller.Controller;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.IOException;

public abstract class ControleurFenetre {

    /**
     * Référence statique vers le contrôleur principal
     */
    protected static Controller controleurPrincipal;

    /**
     * Méthode d'ouverture de la fenêtre à spécialiser
     */
    public static void ouvrir() {
        throw new NotImplementedException();
    }

    /**
     * Donne une référence du controlleur principal de l'application au contrôleur de fenêtre primaire (première fenêtre créée)
     * @param controleurPrincipal Une référence vers le contrôleur principal
     */
    public static void setControleurPrincipal(Controller controleurPrincipal) {
        ControleurFenetre.controleurPrincipal = controleurPrincipal;
    }

    /**
     * Méthode de déclaration du contrôleur de fenêtre auprès du contrôleur principal à spécialiser
     */
    public abstract void informerControleurPrincipal();

    /**
     * Méthode d'initialisation de la fenêtre à spécialiser
     */
    public abstract void initialisation();

    /**
     * Créer une fenêtre avec
     * @param title Le titre de la fenêtre
     * @param fxml Le chemin vers le template FXML de la fenêtre
     */
    protected static void creer(String title, String fxml) {

        FXMLLoader loader = new FXMLLoader(ControleurFenetre.class.getResource(fxml));
        Scene scene;

        try {
            scene = new Scene(loader.load());
            Stage stage = new Stage();

            stage.setScene(scene);
            stage.setTitle(title);

            ControleurFenetre controller = loader.getController();

            //Laison des contrôleurs entre eux.
            ControleurFenetre.setControleurPrincipal(controleurPrincipal);
            controller.informerControleurPrincipal();

            controller.initialisation();

            stage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
