package ControleurVue;

import Modele.DemandeLivraison;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class ControleurFenetreValidationSuppression extends ControleurFenetre{


    /**
     * Label du titre "Validation suppresion"
     */
    @FXML
    protected Label labelTitre;

    /**
     * Bouton "Annuler"
     */
    @FXML
    protected Button buttonAnnuler;

    /**
     * Bouton "Valider"
     */
    @FXML
    protected Button buttonValider;

    /**
     * Champ texte contenant le numéro du noeud de la livraison
     */
    @FXML
    protected TextField fieldNumeroPointLivraison;

    /**
     * Champ texte contenant le numéro de la livraison dans la tournée
     */
    @FXML
    protected TextField fieldNumeroLivraison;

    /**
     * Label contenant un message d'erreur
     */
    @FXML
    protected Label labelErreur;

    /**
     * Spécialisation de la méthode d'ouverture de fenêtre
     */
    public static void ouvrir() {
        ControleurFenetre.creer("LogistiX - Validation d'une suppression de livraison", "/fxml/ValidationSuppression.fxml");
    }

    /**
     * Initialisation de la fenêtre
     */
    @Override
    public void initialisation() {

        DemandeLivraison livraisonCourante = controleurPrincipal.getLivraisonSelected();

        fieldNumeroLivraison.setText(String.valueOf(livraisonCourante.getId()));
        fieldNumeroPointLivraison.setText(String.valueOf(livraisonCourante.getAdresse().getId()));

        labelTitre.getScene().getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent event) {
                controleurPrincipal.annulerSuppression();
            }
        });

        labelErreur.setText("");
    }

    /**
     * Spécialisation de la méthode de déclaration du contrôleur de fenêtre auprès du contrôleur principal
     */
    public void informerControleurPrincipal(){
        controleurPrincipal.setControleurFenetreValidationSuppression(this);
    }

    /**
     * Action lors d'un clic sur "Valider"
     */
    public void onButtonValiderSuppressionClicked() {
        controleurPrincipal.validerSuppression();
    }

    /**
     * Action lors d'un clic sur "Annuler"
     */
    public void onButtonAnnulerSuppressionClicked() {
        controleurPrincipal.annulerSuppression();
    }

    /**
     * Ferme la fenêtre
     */
    public void fermerFenetre() {
        ((Stage) labelTitre.getScene().getWindow()).close();

    }


    /**
     * Masque tout message d'erreur
     */
    public void mettreAJourSuccess() {

        labelErreur.setText("");
    }

    /**
     * Affiche un message d'erreur
     */
    public void mettreAJourEchec() {

        labelErreur.setText("Impossible de supprimer cette livraison");

    }
}
