package Controller;

import ApplicationException.ApplicationError;
import Controller.Commandes.Commande;
import Logger.InternalLogger;

import java.util.LinkedList;

/**
 * Gère la liste des commandes executées par l'application. Permet de défaire et refaire des commandes passées.
 * Voir Design Pattern Commande.
 */
public class ListeCommandes {
    private LinkedList<Commande> listHistCmd;
    private int indiceCrt;

    /**
     * Méthode pour créer la liste des commandes
     */
    public ListeCommandes() {
        indiceCrt = -1;
        this.listHistCmd = new LinkedList<Commande>();
    }

    /**
     * Méthode pour récupérer le nombre de commandes qui sont empilées
     *
     * @return : le nombre de commandes qui peuvent être undoable.
     */
    public int getNbUndoableCmd() {
        return indiceCrt + 1;
    }

    /**
     * Méthode pour récupérer le nombre de commandes qui peuvent être dépilées
     *
     * @return : le nombre de commandes qui peuvent être redoable.
     */
    public int getNbRedoableCmd() {
        return this.getCommandeListeLength() - (indiceCrt +1);
    }

    /**
     * Ajout de la commande cmd a la liste this. (Note, cette méthode devrait être protected.
     * Cependant pour des raisons de "rangement" dans des packets, elle doit être définie comme public pour être
     * appellée depuis les différents états).
     * @param cmd : la commande à ajouter et executer
     */
    public void ajouterCmd(Commande cmd) throws Exception {
        int i = indiceCrt+1;
        //Nettoyage de la liste si on était revenu en arrière
        while(i<listHistCmd.size()){
            listHistCmd.remove(i);
        }
        indiceCrt++;

        //Ajout de la commande
        listHistCmd.add(indiceCrt, cmd);

        //Execution de la commande
        try {
            cmd.doCmd();
        } catch (Exception e) {
            //On log l'erreur
            InternalLogger.getInstance().logSevere("Une erreur s'est produite à l'execution de la commande.", cmd);
            e.printStackTrace();

            //On retire la commande de la liste (puisque elle ne s'est pas executée correctement)
            if (indiceCrt >= 0){
                listHistCmd.remove(indiceCrt);
                indiceCrt--;
            }
            //On relance l'exeception au dessus.
            throw e;
        }
    }

    /**
     * Annule temporairement la derniere commande ajoutee (cette commande pourra etre remise dans la liste avec redo)
     */
    public void undoCmd() throws Exception {
        if (indiceCrt >= 0){
            Commande cde = listHistCmd.get(indiceCrt);
            indiceCrt--;

            //Execution de la commande
            try {
                cde.undoCmd();
            } catch (Exception e) {
                //On log l'erreur
                InternalLogger.getInstance().logSevere("Une erreur s'est produite au undo de l'execution de la commande.", cde);
                e.printStackTrace();

                //On remet la commande de la liste (puisque elle ne s'est pas executée correctement)
                indiceCrt++;
                throw e;
            }
        } else {
            throw new ApplicationError("Pas de commande à défaire.");
        }
    }

    /**
     * Supprime definitivement la derniere commande ajoutee (cette commande ne pourra pas etre remise dans la liste avec redo)
     */
    public void annule() throws Exception {
        if (indiceCrt >= 0){
            Commande cde = listHistCmd.get(indiceCrt);
            listHistCmd.remove(indiceCrt);
            indiceCrt--;
            try{
                cde.undoCmd();
            } catch (Exception e) {
                //On log l'erreur
                InternalLogger.getInstance().logSevere("Une erreur s'est produite au annuler de l'execution de la commande.", cde);
                e.printStackTrace();

                //On remet la commande de la liste (puisque elle ne s'est pas executée correctement)
                listHistCmd.add(indiceCrt, cde);
                indiceCrt++;
                throw e;
            }
        } else {
            throw new ApplicationError("Pas de commande à annuler.");
        }
    }

    /**
     * Remet dans la liste la derniere commande annulee avec undo
     */
    public void redo() throws Exception {
        if (indiceCrt < listHistCmd.size()-1){
            indiceCrt++;
            Commande cde = listHistCmd.get(indiceCrt);

            //Execution de la commande
            try {
                cde.doCmd();
            } catch (Exception e) {
                //On log l'erreur
                InternalLogger.getInstance().logSevere("Une erreur s'est produite à l'execution de la commande.", cde);
                e.printStackTrace();

                //On undo la commande de la liste (puisque elle ne s'est pas executée correctement)
                if (indiceCrt >= 0){
                    indiceCrt--;
                }
                //On relance l'exeception au dessus.
                throw e;
            }
        } else {
            throw new ApplicationError("Pas de commande à refaire.");
        }
    }

    /**
     * Supprime definitivement toutes les commandes de liste
     */
    public void reset(){
        indiceCrt = -1;
        listHistCmd.clear();
    }

    /** Récupérer le nombre de commande dans la liste
     * @return : le nombre de commande
     */
    public int getCommandeListeLength(){
        return listHistCmd.size();
    }

}
