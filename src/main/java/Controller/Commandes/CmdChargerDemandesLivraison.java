package Controller.Commandes;

import Controller.Controller;
import GestionIO.ParseurXML;
import Modele.CarteVille;
import Modele.PlanLivraison;

public class CmdChargerDemandesLivraison implements Commande{
    ParseurXML parseur = new ParseurXML();
    String cheminFichier = "";
    CarteVille carte;
    PlanLivraison planLivraison;
    Controller controller;

    /** Création d'une commande qui permet de charger un plan d'une ville.
     * @param cheminFichier : le chemin du fichier à charger
     * @param carte : La carte de la ville sur laquelle vérifier que les demandes de livraisons sont correctes.
     * @param controller  : la référence vers le controller où il existe une référence vers la carte de la ville à écraser une fois chargée.
     */
    public CmdChargerDemandesLivraison(String cheminFichier, CarteVille carte, Controller controller) {
        this.cheminFichier = cheminFichier;
        this.carte = carte;
        this.controller = controller;
    }

    /** Procède au chargment des demandes de livraison
     * @throws Exception : Exception qui a pu se produire lors du chargement (parser)
     */
    public void doCmd() throws Exception {
        //Met à jour ~ setCheminFichierPlan
        parseur.chargerFichierDemande(cheminFichier);

        //Parsing du fichier et enregistrement local dans la commande
        planLivraison = parseur.obtenirPlanLivraison(carte);

        //Copie de la commande vers le controller principal
        controller.planLivraison = planLivraison;
    }

    /**
     * Undo impossible (vide) sur cette commande
     */
    public void undoCmd() {
        //TODO : Throw error ?
    }
}
