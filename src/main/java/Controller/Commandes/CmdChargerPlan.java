package Controller.Commandes;

import ApplicationException.ApplicationError;
import Controller.Controller;
import GestionIO.ParseurXML;
import Modele.CarteVille;

public class CmdChargerPlan implements Commande{
    ParseurXML parseur = new ParseurXML();
    String cheminFichier = "";
    CarteVille carteville;
    Controller controller;
    boolean commandeExecute = false;

    /** Création d'une commande qui permet de charger un plan d'une ville.
     * @param cheminFichier : le chemin du fichier à charger
     * @param controller : la référence vers le controller où il existe une référence vers la carte de la ville à écraser une fois chargée.
     */
    public CmdChargerPlan(String cheminFichier, Controller controller) {
        this.cheminFichier = cheminFichier;
        this.controller = controller;
    }

    /** Procède au chargement du plan
     * @throws Exception : Exception qui a pu se produire lors du chargement (parser)
     */
    public void doCmd() throws Exception {
        //Met à jour ~ setCheminFichierPlan
        parseur.chargerFichierPlan(cheminFichier);

        //Parsing du fichier et enregistrement local dans la commande
        this.carteville = parseur.obtenirCarteVille();
        //Copie de la commande vers le controller principal
        controller.carteVille = carteville;
        //La commande s'est correctement executée
        this.commandeExecute = true;
    }

    /**
     * Undo impossible (vide) sur cette commande
     */
    public void undoCmd() {
        //TODO : Throw error ?
    }
}
