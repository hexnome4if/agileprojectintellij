package Controller.Commandes;

public interface Commande {
    /** Méthode de l'interface qui execute la commande
     * @throws Exception
     */
    void doCmd() throws Exception;

    /** Méthode de l'interface qui undo la commande
     * @throws Exception
     */
    void undoCmd() throws Exception;
}
