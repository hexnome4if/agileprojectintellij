package Controller.Commandes;

import Algo.AlgoGrapheComplet;
import Algo.Dijkstra;
import Algo.TSPTemplate;
import Algo.TSP_BnB_MST;
import Controller.Controller;
import Modele.CarteVille;
import Modele.CarteVilleSimplifiee;
import Modele.PlanLivraison;
import Modele.Tournee;

public class CmdCalculerTournee implements Commande{
    AlgoGrapheComplet algoDijkstra;
    TSPTemplate algoTSP;
    CarteVille carteVille;
    PlanLivraison plan;

    CarteVilleSimplifiee carteSimplifie;
    Tournee tourneeCalculated;

    Controller controller;
    boolean commandeExecute = false;


    /** Création d'une commande qui permet de calculer la tournée d'un plan de livraison sur une ville
     * @param carteVille : La carte de la ville sur laquelle vérifier la tournée sera claculée
     * @param plan : Le plan de livraison qui sera suivi pour la tournée
     * @param controller : la référence vers le controller où il existe une référence vers la tournée à calculer.
     */
    public CmdCalculerTournee(CarteVille carteVille, PlanLivraison plan, Controller controller) {
        this.carteVille = carteVille;
        this.plan = plan;
        this.controller = controller;
        algoDijkstra = new Dijkstra(carteVille, plan);
        //On set l'algoDjikstra avec les informations du premier chargement.
        controller.algoDijkstra = algoDijkstra;
        controller.planLivraisonOriginal = plan;
    }

    /** Procède au calcul de la tournée
     * @throws Exception : Exception qui a pu se produire lors du chargement (parser)
     */
    public void doCmd() throws Exception {
        //Calcul du djikstra de la carte et enregistrement local dans la commande
        this.carteSimplifie = algoDijkstra.CalculItineraires();
        //Copie de la carte vers le controller principal
        controller.carteVilleSimplifiee = carteSimplifie;
        controller.carteVilleSimplifieeOriginale = carteSimplifie;

        //Création du TSP
        algoTSP = new TSP_BnB_MST(carteSimplifie);
        controller.algoTSP = algoTSP;

        //Calcul de la tournée
        algoTSP.chercheSolution();
        tourneeCalculated = algoTSP.getSolution();

        //Sauvegarde de la tournée dans le controller
        controller.tournee = tourneeCalculated;
        controller.tourneeEditable = tourneeCalculated;
    }

    /**
     * Undo impossible (vide) sur cette commande
     */
    public void undoCmd() {
        //TODO : Throw error ?
    }
}
