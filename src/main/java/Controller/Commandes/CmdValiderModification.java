package Controller.Commandes;

import ApplicationException.ApplicationError;
import Controller.Controller;
import Modele.CarteVilleSimplifiee;
import Modele.PlanLivraison;
import Modele.Tournee;

public class CmdValiderModification implements Commande{

    PlanLivraison ancienPlanLivraison;
    CarteVilleSimplifiee ancienneCarteSimplifiee;
    Tournee ancienneTournee;
    PlanLivraison nouveauPlanLivraison;
    CarteVilleSimplifiee nouvelleCarteSimplifiee;
    Tournee nouvelleTournee;
    Controller controller;
    boolean commandeExecute = false;


    /** Le constructeur de la commande pour valider une modification (peu importe le type)
     * @param ancienPlanLivraison : l'ancien plan de livraison à mémoriser (en cas de undo)
     * @param ancienneCarteSimplifiee : l'ancien carte simplifiée à mémoriser (en cas de undo)
     * @param ancienneTournee : l'ancienne tournée à mémoriser (en cas de undo)
     * @param nouveauPlanLivraison : le nouveau plan de livraison à setter et à mémoriser (en cas de redo)
     * @param nouvelleCarteSimplifiee  : l'ancien carte simplifiée à setter et à mémoriser (en cas de redo)
     * @param nouvelleTournee  : l'ancienne tournée à setter et à mémoriser (en cas de redo)
     * @param controller : le controller général de l'application
     */
    public CmdValiderModification(PlanLivraison ancienPlanLivraison, CarteVilleSimplifiee ancienneCarteSimplifiee, Tournee ancienneTournee, PlanLivraison nouveauPlanLivraison, CarteVilleSimplifiee nouvelleCarteSimplifiee, Tournee nouvelleTournee, Controller controller) {
        //On retient les anciennes et nouvelles versions.
        this.ancienPlanLivraison = ancienPlanLivraison;
        this.ancienneCarteSimplifiee = ancienneCarteSimplifiee;
        this.ancienneTournee = ancienneTournee;

        this.nouveauPlanLivraison = nouveauPlanLivraison;
        this.nouvelleCarteSimplifiee = nouvelleCarteSimplifiee;
        this.nouvelleTournee = nouvelleTournee;
        this.controller = controller;
    }

    /**
     * On valide les modifications qui ont été effectuées (On set les nouvelle valeurs dans les algos)
     */
    public void doCmd() throws Exception {
        if(ancienneCarteSimplifiee == null || ancienneTournee == null || ancienPlanLivraison == null){
            throw new ApplicationError("La modification ne peut pas être effectuée : les savegardes des objets sont invalides.");
        }
        if(nouveauPlanLivraison == null || nouvelleCarteSimplifiee == null || nouvelleTournee == null){
            throw new ApplicationError("La modification ne peut pas être effectuée : les savegardes des objets sont invalides.");
        }

        //On donne à Djikstra le nouveau ...
        controller.algoDijkstra.setPlanLivraison(nouveauPlanLivraison);

        //On donne à Algo ls nouveaux ...
        controller.algoTSP.setTournee(nouvelleTournee);
        controller.algoTSP.setGraphe(nouvelleCarteSimplifiee);

        controller.planLivraison = nouveauPlanLivraison;
        controller.carteVilleSimplifiee = nouvelleCarteSimplifiee;
        controller.tourneeEditable = nouvelleTournee;

        commandeExecute = true;
    }

    /**
     * Undo qui permet de retirer une modification (peu importe le type) seulement si la commande a été executée précédemment
     */
    public void undoCmd()  throws Exception {
        if(commandeExecute){
            //On donne à Djikstra le nouveau ...
            controller.algoDijkstra.setPlanLivraison(ancienPlanLivraison);

            //On donne à Algo ls nouveaux ...
            controller.algoTSP.setTournee(ancienneTournee);
            controller.algoTSP.setGraphe(ancienneCarteSimplifiee);

            controller.planLivraison = ancienPlanLivraison;
            controller.carteVilleSimplifiee = ancienneCarteSimplifiee;
            controller.tourneeEditable = ancienneTournee;
        } else {
            throw new ApplicationError("Une commande non executée ne peut pas être défaite.");
        }


    }

}
