package Controller;

import Algo.AlgoGrapheComplet;
import Algo.TSPTemplate;
import ApplicationException.ApplicationError;
import ControleurVue.*;
import Controller.Etats.*;
import Logger.InternalLogger;
import Modele.*;

/**
 * Controller a une méthode pour chaque événement utilisateur. Ce que font les métodes du controleur dépend de l'état de l'application.
 */
public class Controller {
    public ListeCommandes listCmd;
    private Etat etatCourant;

    /** Methode qui permet de récupérer le nombre de commande qui peuvent être undo.
     * @return : le nombre de commande qui peuvent être dépilées.
     */
    public int getNbUndoableCmd() {
        return listCmd.getNbUndoableCmd();
    }

    /** Methode qui permet de récupérer le nombre de commande qui peuvent être undo
     * @return : le nombre de commande qui peuvent être repilées.
     */
    public int getNbRedoableCmd(){
        return listCmd.getNbRedoableCmd();
    }

    // Instances associees a chaque etat possible du controleur. Note : problématique de public/protected nécessaire
    // à cause des sub-packages
    public final EtatInit etatInit = new EtatInit();
    public final EtatPlanLoaded etatPlanLodaded = new EtatPlanLoaded();
    public final EtatDemandesLivraisonLoaded etatDemandesLivraisonLoaded = new EtatDemandesLivraisonLoaded();
    public final EtatTourneeCalculated etatTourneeCalculated = new EtatTourneeCalculated();

    public final EtatLivraisonSelected etatLivraisonSelected = new EtatLivraisonSelected();
    public final EtatAjouterLivraison etatAjouterLivraison = new EtatAjouterLivraison();
    public final EtatModifierLivraison etatModifierLivraison = new EtatModifierLivraison();
    public final EtatSupprimerLivraison etatSupprimerLivraison = new EtatSupprimerLivraison();
    public final EtatResetTournee etatResetTournee = new EtatResetTournee();

    /** Ajout du controleur de la fenêtre de démarrage et principale de l'application
     * @param controleurFenetreGestionTournee : le controleur JFX de la fenêtre principale
     */
    public void setControleurFenetreGestionTournee(ControleurFenetreGestionTournee controleurFenetreGestionTournee) {
        this.controleurFenetreGestionTournee = controleurFenetreGestionTournee;
    }

    /** Ajout du controleur de la fenêtre d'édition des trajets
     * @param controleurFenetreEditionLivraison : le controleur JFX de la fenêtre d'édition
     */
    public void setControleurFenetreEditionLivraison(ControleurFenetreEditionLivraison controleurFenetreEditionLivraison) {
        this.controleurFenetreEditionLivraison = controleurFenetreEditionLivraison;
    }

    /** Ajout du controleur de la fenêtre d'édition des trajets
     * @param controleurFenetreAjoutLivraison : le controleur JFX de la fenêtre d'édition
     */
    public void setControleurFenetreAjoutLivraison(ControleurFenetreAjoutLivraison controleurFenetreAjoutLivraison) {
        this.controleurFenetreAjoutLivraison = controleurFenetreAjoutLivraison;
    }

    /** Ajout du controleur de la fenêtre d'édition des trajets
     * @param controleurFenetreValidationReset : le controleur JFX de la fenêtre de validation de reset
     */
    public void setControleurFenetreValidationReset(ControleurFenetreValidationReset controleurFenetreValidationReset) {
        this.controleurFenetreValidationReset = controleurFenetreValidationReset;
    }

    /** Ajout du controleur de la fenêtre d'édition des trajets
     * @param controleurFenetreValidationSuppression : le controleur JFX de la fenêtre de validation de suppression
     */
    public void setControleurFenetreValidationSuppression(ControleurFenetreValidationSuppression controleurFenetreValidationSuppression) {
        this.controleurFenetreValidationSuppression = controleurFenetreValidationSuppression;
    }

    //Objets de la vue (sous-controller)
    public ControleurFenetreGestionTournee controleurFenetreGestionTournee;
    public ControleurFenetreEditionLivraison controleurFenetreEditionLivraison;
    public ControleurFenetreAjoutLivraison controleurFenetreAjoutLivraison;
    public ControleurFenetreValidationReset controleurFenetreValidationReset ;
    public ControleurFenetreValidationSuppression controleurFenetreValidationSuppression;

    // Objets du Modèle
    public CarteVille carteVille;
    public CarteVilleSimplifiee carteVilleSimplifiee;
    public PlanLivraison planLivraison;

    public CarteVilleSimplifiee carteVilleSimplifieeOriginale;
    public PlanLivraison planLivraisonOriginal;

    public Tournee tournee;
    //Tournée éditable pour les modifications/ajouts/ ...
    public Tournee tourneeEditable;

    /** Méthode qui permet de récupérer la tournée, qui sera éditée si on fait des ajouts/modifier/supprimer, stockée dans le controlleur, précédemment calculée.
     * @return : la tournée calculée éditable
     */
    public Tournee getTournee() {
        return tourneeEditable;
    }

    /** Méthode qui permet de donner au controlleur la livraison sélectionnée sur l'IHM. Sert pour les ajout/modifier/supprimer ...
     * @param livraisonSelected : la demande de livraison sélectionnée sur l'IHM à notifier au controller
     */
    public void setLivraisonSelected(DemandeLivraison livraisonSelected) {
        this.livraisonSelected = livraisonSelected;
    }

    /** Méthode qui permet de changer la livraisonSelected de la classe controlleur principal (cette classe) en prenant la livraison just précédente
     * @param ancienneTournee : la tournée où on a retiré la livraisonSelected
     * @throws ApplicationError
     */
    public void setPrevLivraisonSelected(Tournee ancienneTournee) throws ApplicationError {
        //La livraison actuellement sélectionnée (son ID)
        Long idCurrentLivraisonSelected = this.livraisonSelected.getId();
        //La livraison qui le remplacera
        Long idFuturLivraisonSelected;


        try {
            // ***** On cherche la la demande de livraison qui était avant la livraison retirée *****
            idFuturLivraisonSelected = ancienneTournee.getPreviousDemandeLivraisonById(idCurrentLivraisonSelected);

            // ***** On récupère cette demande de livraison *****
            DemandeLivraison tmp = ancienneTournee.getDemandeByID(idFuturLivraisonSelected);
            if (tmp != null) {
                this.livraisonSelected = tmp;
            } else {
                InternalLogger.getInstance().logSevere("Pas de livraison précédente à la livraison actuelle. (affichage)", this);
            }
        } catch (Exception e) {
            e.printStackTrace();
            InternalLogger.getInstance().logSevere("Erreur lors de la détection de la livraison précédente. (affichage)", this);
        }

    }

    /** Méthode qui permet de récupérer depuis le controller la livraison sélectionnée sur l'IHM. Sert pour les ajout/modifier/supprimer ...
     * @return : la livraison selctionéne sur l'IHM, stockée par le controller
     */
    public DemandeLivraison getLivraisonSelected() {
        return livraisonSelected;
    }

    // Objets de la vue
    public DemandeLivraison livraisonSelected;

    //Sauvegarde des objets d'algorithmie
    public AlgoGrapheComplet algoDijkstra;
    public TSPTemplate algoTSP;

    /**
     * Cree le controleur de l'application
     */
    public Controller() {
        listCmd = new ListeCommandes();
        etatCourant = etatInit;
    }

    /**
     * Change l'etat courant du controleur. (Note, cette méthode devrait être protected.
     * Cependant pour des raisons de "rangement" dans des packets, elle doit être définie comme public pour être
     * appellée depuis les différents états).
     * @param etat le nouvel etat courant
     */
    public void setEtatCourant(Etat etat){
        etatCourant = etat;
    }

    // Methodes correspondant aux evenements utilisateur

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "Ajouter carte de la ville"
     */
    public void chargerPlan(){
        try {
            etatCourant.chargerPlan(this, listCmd );
            controleurFenetreGestionTournee.actualiseVue();
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
    }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "Ajouter demandes livraisons"
     */
    public void chargerDemandesLivraison(){
        try {
            etatCourant.chargerDemandesLivraison(this, listCmd  );
            controleurFenetreGestionTournee.actualiseVue();
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "calculer Tournée"
     */
    public void calculerTournee(){
        try {
            etatCourant.calculerTournee(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres la sélection d'une livraison
     */
    public void selectionerLivraison(){
        try {
            etatCourant.selectionerLivraison(this, listCmd  );
            controleurFenetreGestionTournee.actualiseVue();
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "modifier livraison"
     */
    public void modifierLivraison(){
        try {
            etatCourant.modifierLivraison(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "Tester modification" durant la modification d'une livraison
     */
    public void testerModification(){
        try {
            etatCourant.testerModification(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "annuler modification"  durant la modification d'une livraison
     */
    public void annulerModification(){
        try {
            etatCourant.annulerModification(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "valider modification"  durant la modification d'une livraison
     */
    public void validerModification(){
        try {
            etatCourant.validerModification(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "supprimer livraison" dans le contexte global de l'application
     */
    public void supprimerLivraison(){
        try {
            etatCourant.supprimerLivraison(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "annuler livraison"  durant la suppression d'une livraison
     */
    public void annulerSuppression(){
        try {
            etatCourant.annulerSuppression(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "valider livraison"  durant la suppression d'une livraison
     */
    public void validerSuppression(){
        try {
            etatCourant.validerSuppression(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "reset tournée" dans le contexte global de l'application
     */
    public void resetTournee(){
        try {
            etatCourant.resetTournee(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "annuler reset" durant le reset de la tournée
     */
    public void annulerReset(){
        try {
            etatCourant.annulerReset(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "valider reset" durant le reset de la tournée
     */
    public void validerReset(){
        try {
            etatCourant.validerReset(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "ajouter livraison" dans le contexte global de l'application
     */
    public void ajouterLivraison(){
        try {
            etatCourant.ajouterLivraison(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "valider ajout d'une livraison" durant l'ajout d'une livraison
     */
    public void validerAjout(){
        try {
            etatCourant.validerAjout(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "tester ajout d'une livraison" durant l'ajout d'une livraison
     */
    public void testerAjout(){
        try {
            etatCourant.testerAjout(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "annuler ajout d'une livraison" durant l'ajout d'une livraison
     */
    public void annulerAjout(){
        try {
            etatCourant.annulerAjout(this, listCmd  );
        } catch (ApplicationError applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "undo" dans le contexte global de l'application
     */
    public void undo(){
        try {
            etatCourant.undo(this, listCmd  );
            controleurFenetreGestionTournee.actualiseVue();
        } catch (Exception applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "redo" dans le contexte global de l'application
     */
    public void redo(){
        try {
            etatCourant.redo(this, listCmd  );
            controleurFenetreGestionTournee.actualiseVue();
        } catch (Exception applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode appelee par fenetre apres un clic sur le bouton "redo" dans le contexte global de l'application
     */
    public void exportTournee(){
        try {
            etatCourant.exportTournee(this, listCmd  );
        } catch (Exception applicationError) {
            applicationError.printStackTrace();
            controleurFenetreGestionTournee.afficherMessageEtat(applicationError.getMessage());
        }
    }

    /**
     * Methode de lancement de l'application (initialisation de la première fenêtre et des objets nécessaires)
     */
    public void launchApplication(){
        ControleurFenetreGestionTournee.ouvrir();
    }

}


