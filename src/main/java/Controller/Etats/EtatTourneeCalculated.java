package Controller.Etats;

import ApplicationException.ApplicationError;
import ControleurVue.ControleurFenetreAjoutLivraison;
import ControleurVue.ControleurFenetreValidationReset;
import Controller.Controller;
import Controller.ListeCommandes;
import Logger.InternalLogger;

public class EtatTourneeCalculated extends EtatDefaut{

    /** Permet de lancer la procédure d'ajout d'une livraison (ouvertue de la fenêtre modale, principalement)
     * @param controller : le controlleur principal de l'application, qui va lancer le controleur de la fenetre modale
     * @param listCmd : la liste des commandes de l'application
     */
    @Override
    public void ajouterLivraison(Controller controller, ListeCommandes listCmd){
        //Ouverturede la nouvelle fenêtre, passage dans l'état correspondant
        ControleurFenetreAjoutLivraison.ouvrir();
        controller.setEtatCourant(controller.etatAjouterLivraison);
    }

    /** Permet de lancer la procédure de reset d'une livraison (ouvertue de la fenêtre modale, principalement)
     * @param controller : le controlleur principal de l'application, qui va lancer le controleur de la fenetre modale
     * @param listCmd : la liste des commandes de l'application
     */
    @Override
    public void resetTournee(Controller controller, ListeCommandes listCmd){
        //Ouverturede la nouvelle fenêtre, passage dans l'état correspondant
        ControleurFenetreValidationReset.ouvrir();
        controller.setEtatCourant(controller.etatResetTournee);
    }

    /** Permet de définir qu'une tournée a été selctionnée. Dévérouille de nouvelles options (ajouter, supprimer, modifer ... pour cette tournée)
     * @param controller : le controlleur principal de l'application, qui va stocker la livraison sélectionnée
     * @param listCmd : la liste des commandes de l'application
     */
    @Override
    public void selectionerLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError {
        //On retient la livraison actuellement sélectionnée
        controller.livraisonSelected = controller.controleurFenetreGestionTournee.getLivraisonCourante();

        //Sanity Check
        if(controller.livraisonSelected == null){
            InternalLogger.getInstance().logSevere("La fenêtre a indiquée qu'une livraison a été sélectionnée. Cependant, la méthode 'getLivraisonCourant' ne renvoit pas de livraison.", this);
            throw new ApplicationError("La livraison sélectionnée est erronée.");
        }

        //On passe dans l'état où on sait qu'une livraison a été sélectionnée
        controller.setEtatCourant(controller.etatLivraisonSelected);
    }

}
