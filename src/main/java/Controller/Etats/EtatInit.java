package Controller.Etats;

import ApplicationException.ApplicationError;
import Controller.Commandes.CmdChargerDemandesLivraison;
import Controller.Commandes.CmdChargerPlan;
import Controller.Controller;
import Controller.ListeCommandes;
import Logger.InternalLogger;

public class EtatInit extends EtatDefaut{

    /** Execution de la procédure de chargement d'un plan d'une ville
     * @param controller : Permet d'accéder au chemin du fichier, et de stocker le plan une fois parsé
     * @param listCmd : la liste des commandes du controller
     */
    @Override
    public void chargerPlan(Controller controller, ListeCommandes listCmd) throws ApplicationError {
        // ANCIENNE VERSION : Ajout de la commande à la liste des commandes
        // listCmd.ajouterCmd(new CmdChargerPlan(controller.controleurFenetreGestionTournee.cheminFichierCarte, controller.carteVille, controller));

        // NOUVELLE VERSION : On charge un plan en créant et executant la commande, sans la stocker dans la liste des commandes.
        CmdChargerPlan chargementPlan = new CmdChargerPlan(controller.controleurFenetreGestionTournee.getCheminFichierCarte(), controller);
        try {
            chargementPlan.doCmd();
        } catch (Exception e) {
            e.printStackTrace();
            //On log l'erreur
            InternalLogger.getInstance().logSevere("Une erreur au chargement du plan.", this);
            throw new ApplicationError("Erreur lors du chargement du plan.");
        }

        controller.controleurFenetreGestionTournee.afficherMessageEtat("Plan chargé");
        controller.controleurFenetreGestionTournee.actualiseVue();
        controller.setEtatCourant(controller.etatPlanLodaded);
    }

    /** Execution de la procédure de chargement de la liste des demandes de livraison
     * @param controller : Permet d'accéder au chemin du fichier, et de stocker le plan une fois parsé
     * @param listCmd : la liste des commandes du controller
     */
    @Override
    public void chargerDemandesLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError {
        // ANCIENNE VERSION : Ajout de la commande à la liste des commandes
        // listCmd.ajouterCmd(new CmdChargerPlan(controller.controleurFenetreGestionTournee.cheminFichierCarte, controller.carteVille, controller));
        // listCmd.ajouterCmd(new CmdChargerDemandesLivraison(controller.controleurFenetreGestionTournee.cheminFichierPlanLivraison , controller.carteVille, controller.planLivraison, controller));

        //Chargement du plan par défaut
        CmdChargerPlan chargementPlan = new CmdChargerPlan(controller.controleurFenetreGestionTournee.getCheminFichierCarte(), controller);
        try {
            chargementPlan.doCmd();
        } catch (Exception e) {
            e.printStackTrace();
            //On log l'erreur
            InternalLogger.getInstance().logSevere("Une erreur au chargement du plan de la ville par défaut.", this);
            throw new ApplicationError("Erreur lors du chargement du plan par défaut.");
        }

        //Chargement du plan de livraison
        CmdChargerDemandesLivraison chargementDemandesLivraison = new CmdChargerDemandesLivraison(controller.controleurFenetreGestionTournee.getCheminFichierPlanLivraison() , controller.carteVille, controller);
        try {
            chargementDemandesLivraison.doCmd();
        } catch (Exception e) {
            e.printStackTrace();
            //On log l'erreur
            InternalLogger.getInstance().logSevere("Une erreur au chargement de la liste des demandes de livraison.", this);
            throw new ApplicationError("Erreur lors du chargement des demandes de livraisons par défaut.");
        }

        controller.controleurFenetreGestionTournee.afficherMessageEtat("Plan par défaut et demandes de livraison chargés");
        controller.controleurFenetreGestionTournee.actualiseVue();
        controller.setEtatCourant(controller.etatDemandesLivraisonLoaded);
    }

}
