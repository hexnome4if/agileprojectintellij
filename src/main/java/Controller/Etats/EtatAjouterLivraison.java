package Controller.Etats;

import ApplicationException.ApplicationError;
import Controller.Commandes.CmdValiderModification;
import Controller.Controller;
import Controller.ListeCommandes;
import Logger.InternalLogger;
import Modele.*;

public class EtatAjouterLivraison extends EtatDefaut {

    /**
     * Les plan de livraison, carte ville simplifiée et tournée, utilisées pour l'ajout d'une livraison, avant l'ajout.
     */
    PlanLivraison ancienPlanLivraison;
    CarteVilleSimplifiee ancienneCarteSimplifiee;
    Tournee ancienneTournee;

    /**
     * Les plan de livraison, carte ville simplifiée et tournée, calculés après l'ajout de la livraison
     */
    PlanLivraison nouveauPlanLivraison;
    CarteVilleSimplifiee nouvelleCarteSimplifiee;
    Tournee nouvelleTournee;

    // Boolean qui indique si l'ajout a été un succès. (=L'ajout est possible)
    boolean estTestedEtValide = false;

    /** Méthode qui permet de valider l'ajout fait à une tournée.
     * Fait une copie des anciens plan de livraison, carte simplifiée, et tournée. Applique les nouvelles versions aux objets du modèles.
     * @param controller : le controlleur principal de l'application (qui contient les objets du modèle instanciés)
     * @param listCmd : la liste des commandes du controlleur principal pour effectuer la validation
     */
    @Override
    public void validerAjout(Controller controller, ListeCommandes listCmd) throws ApplicationError {
        if(estTestedEtValide){
            //On valide l'ajout
            try {
                listCmd.ajouterCmd(new CmdValiderModification(ancienPlanLivraison, ancienneCarteSimplifiee, ancienneTournee, nouveauPlanLivraison, nouvelleCarteSimplifiee,  nouvelleTournee, controller));
            } catch (Exception e) {
                e.printStackTrace();
                throw new ApplicationError("Erreur lors du la validation de l'ajout d'une livraison.");
            }
            //On ferme la fenêtre modale
            controller.controleurFenetreAjoutLivraison.fermerFenetre();

            //On dit à la fenêtre principale de se mettre à jour
            controller.controleurFenetreGestionTournee.reinjecterDonnee();
            //controller.controleurFenetreGestionTournee.NotifierUser("Ajout d'une livraison effectué.");

            //On change d'état
            controller.setEtatCourant(controller.etatLivraisonSelected);
            estTestedEtValide = false;

        } else {
            InternalLogger.getInstance().logWarning("Une tentative d'ajout de livraison non testée a été faite. Comportement non prévu.", this);
            throw new ApplicationError("Ajout d'une livraison impossible sans avoir été testée.");
        }
    }

    /** Méthode qui permet de tester l'ajout d'une demande de livraison à une tournée.
     * Fonctionne sans toucher aux attributs du controller principal. (Copies des composants avant tout test)
     * @param controller : le controlleur principal de l'application (qui contient les objets du modèle instanciés)
     * @param listCmd : la liste des commandes du controlleur principal pour effectuer la validation
     */
    @Override
    public void testerAjout(Controller controller, ListeCommandes listCmd) throws ApplicationError {

        //Récupérer les informations de la fenêtre
        PlageHoraire plageHoraire = controller.controleurFenetreAjoutLivraison.getplageHoraire();
        Long dureeLivraison = controller.controleurFenetreAjoutLivraison.getDuree();
        Long numNoeud = controller.controleurFenetreAjoutLivraison.getNumNoeud();
        Noeud noeudAAjouter = controller.carteVille.getNoeudByID(numNoeud);

        //Si des informations sont non spécifiées, on throw une erreur
        if(dureeLivraison == null || plageHoraire == null || numNoeud == null) {
            throw new ApplicationError("Erreur, les informations ne sont pas spécifiées dans le fenêtre");
        }
        //Si des informations sont bizarres, on throw une erreur
        if(plageHoraire.getHeureDeb().getTotalSeconds() > plageHoraire.getHeureFin().getTotalSeconds()) {
            throw new ApplicationError("Erreur, l'ordre des horaires ne permet pas une livraison.");
        }

        if (noeudAAjouter == null) {
            // Le noeud spécifié n'existe pas.
            controller.controleurFenetreAjoutLivraison.mettreAJourNoeudInexistant();
        } else {//Le noeud existe

            try {
                // On fait une sauvegarde de nos propres données
                ancienPlanLivraison = controller.planLivraison.clone();
                ancienneCarteSimplifiee = controller.carteVilleSimplifiee.clone();
                ancienneTournee = controller.tourneeEditable.clone();

                // On fait une copie des données actuelles, pour les modifier.
                nouveauPlanLivraison = controller.planLivraison.clone();
                nouvelleCarteSimplifiee = controller.carteVilleSimplifiee.clone();
                nouvelleTournee = controller.tourneeEditable.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                throw new ApplicationError("Erreur, sauvegarde impossible des données avant l'ajout d'une livraison.");
            }

            //Création de la demande de livraison
            DemandeLivraison nouvelleDemandeLivraison = new DemandeLivraison(noeudAAjouter, ((int) ((long) (dureeLivraison))), plageHoraire);

            //Créer un nouveau plan de livraison
            nouveauPlanLivraison.ajouterDemandeLivraison(nouvelleDemandeLivraison);

            //Calculer le nouveau plan de ville
            nouvelleCarteSimplifiee = controller.algoDijkstra.CalculNewItineraires(nouveauPlanLivraison);

            //Calculer la modification
            try{
                controller.algoTSP.ajouterLivraison(nouvelleDemandeLivraison, nouvelleCarteSimplifiee);
                nouvelleTournee = controller.algoTSP.getSolution();
                estTestedEtValide = true;
            } catch (Exception e){
                e.printStackTrace();
                controller.controleurFenetreAjoutLivraison.mettreAJourEchecAjoutInvalide();
            } finally {
                //Nettoyer les changements dans le modèle (pour éviter en cas d'annulatin d'avoir des problèmes)
                controller.algoDijkstra.setPlanLivraison(ancienPlanLivraison);
                controller.algoTSP.setTournee(ancienneTournee);
                controller.algoTSP.setGraphe(ancienneCarteSimplifiee);
            }

            if(estTestedEtValide){
                try{
                    Long demandeId = nouvelleDemandeLivraison.getId();
                    PlageHoraire plageLivraison = nouvelleTournee.getDemandeByID(demandeId).getPlageLivraison();

                    controller.controleurFenetreAjoutLivraison.mettreAJourSuccess(plageLivraison.getHeureDeb().getFormattedTime(false) + " - " + plageLivraison.getHeureFin().getFormattedTime(false));
                }  catch (Exception e){
                    e.printStackTrace();
                    controller.controleurFenetreAjoutLivraison.mettreAJourSuccess("");
                }
            }
        }
    }

    /** Méthode qui annule la procédure d'ajout d'une demande de livraison.
     * @param controller : le controlleur principal de l'application (qui contient les objets du modèle instanciés)
     * @param listCmd : la liste des commandes du controlleur principal pour effectuer la validation
     */
    @Override
    public void annulerAjout(Controller controller, ListeCommandes listCmd){
        //L'ajout n'est donc plus valide.
        estTestedEtValide = false;

        //TODO : Choix de l'état de retour : Tournee calculée ou bien Livraison selected ? Comment est-ce géré niveau IHM ? (Il faudrait que l'objet sélectionné soit déselectionné)
        controller.setEtatCourant(controller.etatLivraisonSelected);

        //Les modifications ont déjà été corrigées. Par défaut, les modifications ne sont pas appliquées.
        controller.controleurFenetreAjoutLivraison.fermerFenetre();

    }
}
