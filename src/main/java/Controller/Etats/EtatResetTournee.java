package Controller.Etats;
import ApplicationException.ApplicationError;
import Controller.Commandes.CmdValiderModification;
import Controller.Controller;
import Controller.ListeCommandes;
import Modele.CarteVilleSimplifiee;
import Modele.PlanLivraison;
import Modele.Tournee;

public class EtatResetTournee extends EtatDefaut {

    PlanLivraison ancienPlanLivraison;
    CarteVilleSimplifiee ancienneCarteSimplifiee;
    Tournee ancienneTournee;

    PlanLivraison nouveauPlanLivraison;
    CarteVilleSimplifiee nouvelleCarteSimplifiee;
    Tournee nouvelleTournee;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "valider" dans la fenêtre modale de reset d'une tournée
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    @Override
    public void validerReset(Controller controller, ListeCommandes listCmd) throws ApplicationError {

        try {
            // On fait une sauvegarde de nos propres données
            ancienPlanLivraison = controller.planLivraison.clone();
            ancienneCarteSimplifiee = controller.carteVilleSimplifiee.clone();
            ancienneTournee = controller.tourneeEditable.clone();

            // On fait une copie des données actuelles, pour les modifier. LES ORIGINALES seulement
            nouveauPlanLivraison = controller.planLivraisonOriginal.clone();
            nouvelleCarteSimplifiee = controller.carteVilleSimplifieeOriginale.clone();
            nouvelleTournee = controller.tournee.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            throw new ApplicationError("Erreur, sauvegarde impossible des données avant la remise à l'état d'origine de la tournée.");
        }

        try {
            listCmd.ajouterCmd(new CmdValiderModification(ancienPlanLivraison, ancienneCarteSimplifiee, ancienneTournee, nouveauPlanLivraison, nouvelleCarteSimplifiee, nouvelleTournee, controller));
        } catch (Exception e) {
            e.printStackTrace();
            throw new ApplicationError("Erreur lors du la validation la remise à l'état d'origine de la tournée.");
        }

        //TODO : Choix de l'état de retour : Tournee calculée ou bien Livraison selected ? Comment est-ce géré niveau IHM ? (Il faudrait que l'objet sélectionné soit déselectionné)
        //Ouverturede la nouvelle fenêtre, passage dans l'état correspondant
        controller.setEtatCourant(controller.etatLivraisonSelected);

        // close la fenêtre de modification
        controller.controleurFenetreValidationReset.fermerFenetre();

        //On dit à la fenêtre principale de se mettre à jour
        controller.controleurFenetreGestionTournee.reinjecterDonnee();
    }

    /**
     * Methode appelee par controleur apres un clic sur le bouton "annuler" dans la fenêtre modale de reset d'une tournée
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    @Override
    public void annulerReset(Controller controller, ListeCommandes listCmd){
        //TODO : Choix de l'état de retour : Tournee calculée ou bien Livraison selected ? Comment est-ce géré niveau IHM ? (Il faudrait que l'objet sélectionné soit déselectionné)
        controller.setEtatCourant(controller.etatLivraisonSelected);

        // close la fenêtre de modification
        controller.controleurFenetreValidationReset.fermerFenetre();
    }
}
