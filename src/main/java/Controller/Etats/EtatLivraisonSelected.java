package Controller.Etats;

import ApplicationException.ApplicationError;
import ControleurVue.ControleurFenetreAjoutLivraison;
import ControleurVue.ControleurFenetreEditionLivraison;
import ControleurVue.ControleurFenetreValidationReset;
import ControleurVue.ControleurFenetreValidationSuppression;
import Controller.Controller;
import Controller.ListeCommandes;
import GestionIO.ExtracteurFeuilleRoute;
import Logger.InternalLogger;
import Modele.Entrepot;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class EtatLivraisonSelected extends EtatDefaut {

    /** Permet de lancer la procédure d'ajout d'une livraison (ouvertue de la fenêtre modale, principalement)
     * @param controller : le controlleur principal de l'application, qui va lancer le controleur de la fenetre modale
     * @param listCmd : la liste des commandes de l'application
     */
    @Override
    public void ajouterLivraison(Controller controller, ListeCommandes listCmd){
        //Note, il faut le faire avant le lancement de la fenêtre, car l'appel coupe la chaîne d'execution.
        controller.setEtatCourant(controller.etatAjouterLivraison);

        //Ouverturede la nouvelle fenêtre, passage dans l'état correspondant
        ControleurFenetreAjoutLivraison.ouvrir();
    }

    /** Permet de lancer la procédure de modification d'une livraison (ouvertue de la fenêtre modale, principalement)
     * @param controller : le controlleur principal de l'application, qui va lancer le controleur de la fenetre modale
     * @param listCmd : la liste des commandes de l'application
     */
    @Override
    public void modifierLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError {
        if(controller.livraisonSelected == null || controller.livraisonSelected instanceof Entrepot){
            throw new ApplicationError("L'entrepôt n'est pas une livraison modifiable.");
        }

        //Note, il faut le faire avant le lancement de la fenêtre, car l'appel coupe la chaîne d'execution.
        controller.setEtatCourant(controller.etatModifierLivraison);

        //Ouverturede la nouvelle fenêtre, passage dans l'état correspondant
        ControleurFenetreEditionLivraison.ouvrir();
    }

    /** Permet de lancer la procédure de suppression d'une livraison (ouvertue de la fenêtre modale, principalement)
     * @param controller : le controlleur principal de l'application, qui va lancer le controleur de la fenetre modale
     * @param listCmd : la liste des commandes de l'application
     */
    @Override
    public void supprimerLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError {
        if(controller.livraisonSelected == null || controller.livraisonSelected instanceof Entrepot){
            throw new ApplicationError("L'entrepôt n'est pas une livraison supprimable.");
        }

        //Note, il faut le faire avant le lancement de la fenêtre, car l'appel coupe la chaîne d'execution.
        controller.setEtatCourant(controller.etatSupprimerLivraison);

        //Ouverturede la nouvelle fenêtre, passage dans l'état correspondant
        ControleurFenetreValidationSuppression.ouvrir();
    }

    /** Permet de lancer la procédure de reset d'une livraison (ouvertue de la fenêtre modale, principalement)
     * @param controller : le controlleur principal de l'application, qui va lancer le controleur de la fenetre modale
     * @param listCmd : la liste des commandes de l'application
     */
    @Override
    public void resetTournee(Controller controller, ListeCommandes listCmd){
        //Note, il faut le faire avant le lancement de la fenêtre, car l'appel coupe la chaîne d'execution.
        controller.setEtatCourant(controller.etatResetTournee);

        //Ouverture de la nouvelle fenêtre, passage dans l'état correspondant
        ControleurFenetreValidationReset.ouvrir();
    }

    /** Permet de définir qu'une tournée a été selctionnée. Dévérouille de nouvelles options (ajouter, supprimer, modifer ... pour cette tournée)
     * @param controller : le controlleur principal de l'application, qui va stocker la livraison sélectionnée
     * @param listCmd : la liste des commandes de l'application
     */
    @Override
    public void selectionerLivraison(Controller controller, ListeCommandes listCmd){
        //On retient la livraison actuellement sélectionnée
        controller.livraisonSelected = controller.controleurFenetreGestionTournee.getLivraisonCourante();

        //Sanity Check
        if(controller.livraisonSelected == null){
            InternalLogger.getInstance().logSevere("La fenêtre a indiquée qu'une livraison a été sélectionnée. Cependant, la méthode 'getLivraisonCourant' ne renvoit pas de livraison.", this);
        }

        //On passe dans l'état où on sait qu'une livraison a été sélectionnée
        controller.setEtatCourant(controller.etatLivraisonSelected);
    }

    /** Méthode appellée par le controlleur après un clic sur un bouton undo
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws Exception
     */
    public void undo(Controller controller, ListeCommandes listCmd) throws Exception {
        //Sanity check
        if (controller.listCmd == null) {
            throw new ApplicationError("Liste des commandes vide");
        }

        if (controller.listCmd.getCommandeListeLength() == 0) {
            throw new ApplicationError("Liste des commandes vide");
        }

        controller.listCmd.undoCmd();
        controller.controleurFenetreGestionTournee.reinjecterDonnee();
    }

    /** Méthode appellée par le controlleur après un clic sur un bouton undo
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws Exception
     */
    public void redo(Controller controller, ListeCommandes listCmd) throws Exception {
        //Sanity check
        if (controller.listCmd == null) {
            throw new ApplicationError("Liste des commandes vide");
        }

        controller.listCmd.redo();
        controller.controleurFenetreGestionTournee.reinjecterDonnee();
    }

    /** Méthode appellée par le controlleur après un clic sur un bouton d'exportation de la tournée actuelle
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws Exception
     */
    public void exportTournee(Controller controller, ListeCommandes listCmd) throws Exception {
        //TODO : Récupérer la string du fichier d'export dans la fenêtre (PAS besoin)

        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showSaveDialog(new Stage());

        try {
            //appeller l'export dans la gestion IO
            ExtracteurFeuilleRoute extracteurFeuilleRoute = new ExtracteurFeuilleRoute();

            if (file != null) {
                extracteurFeuilleRoute.setFichierExport(file.getAbsolutePath());
            }
            extracteurFeuilleRoute.creerFeuilleRoute(controller.tourneeEditable);
        } catch (Exception e) {
            controller.controleurFenetreGestionTournee.afficherMessageEtat("Erreur lors de l'exportation de la tournée.");
        }

        //Note, il faut le faire avant le lancement de la fenêtre, car l'appel coupe la chaîne d'execution.
        controller.setEtatCourant(controller.etatLivraisonSelected);
    }

}
