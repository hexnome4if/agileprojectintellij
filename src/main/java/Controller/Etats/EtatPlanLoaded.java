package Controller.Etats;

import ApplicationException.ApplicationError;
import Controller.Commandes.CmdChargerDemandesLivraison;
import Controller.Controller;
import Controller.ListeCommandes;
import Logger.InternalLogger;

public class EtatPlanLoaded extends EtatDefaut{

    /** Execution de la procédure de chargement de la liste des demandes de livraison
     * @param controller : Permet d'accéder au chemin du fichier, et de stocker le plan une fois parsé
     * @param listCmd : la liste des commandes du controller
     */
    @Override
    public void chargerDemandesLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError {
        // ANCIENNE VERSION : Ajout de la commande à la liste des commandes
        // listCmd.ajouterCmd(new CmdChargerDemandesLivraison(controller.controleurFenetreGestionTournee.cheminFichierPlanLivraison , controller.carteVille, controller.planLivraison, controller));

        //Chargement du plan de livraison
        CmdChargerDemandesLivraison chargementDemandesLivraison = new CmdChargerDemandesLivraison(controller.controleurFenetreGestionTournee.getCheminFichierPlanLivraison() , controller.carteVille, controller);
        try {
            chargementDemandesLivraison.doCmd();
        } catch (Exception e) {
            e.printStackTrace();
            //On log l'erreur
            InternalLogger.getInstance().logSevere("Une erreur au chargement de la liste des demandes de livraison.", this);
            throw new ApplicationError("Erreur lors du chargement des demandes de livraisons par défaut.");
        }

        controller.controleurFenetreGestionTournee.afficherMessageEtat("Demandes de livraison chargés");
        controller.controleurFenetreGestionTournee.actualiseVue();
        controller.setEtatCourant(controller.etatDemandesLivraisonLoaded);
    }

}
