package Controller.Etats;

import ApplicationException.ApplicationError;
import Controller.Controller;
import Controller.ListeCommandes;

public interface Etat {
    /**
     * Methode appelee par controleur apres un clic sur le bouton "Charger carte de la ville"
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     */
    void chargerPlan(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "Charger demandes de livraisons"
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     */
    void chargerDemandesLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "Calculer tournée "
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     */
    void calculerTournee(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * @param controller : le controleur de la fenêtre
     * @param listCmd  : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    void selectionerLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "Modifier livraison"
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     */
    void modifierLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "tester" dans la fenêtre modale de modification d'une livraison
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    void testerModification(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "annuler" dans la fenêtre modale de modification d'une livraison
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    void annulerModification(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "valider" dans la fenêtre modale de modification d'une livraison
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    void validerModification(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "Supprimer livraison"
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     */
    void supprimerLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "annuler" dans la fenêtre modale de suppression d'une livraison
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    void annulerSuppression(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "valider" dans la fenêtre modale de suppression d'une livraison
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    void validerSuppression(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "reset tournée"
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     */
    void resetTournee(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "annuler" dans la fenêtre modale de reset d'une tournée
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    void annulerReset(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "valider" dans la fenêtre modale de reset d'une tournée
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    void validerReset(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "Ajouter livraison"
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     */
    void ajouterLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "valider" dans la fenêtre modale d'ajout d'une livraison
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    void validerAjout(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "tester" dans la fenêtre modale d'ajout d'une livraison
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    void testerAjout(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /**
     * Methode appelee par controleur apres un clic sur le bouton "annuler" dans la fenêtre modale d'ajout d'une livraison
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws ApplicationError
     */
    void annulerAjout(Controller controller, ListeCommandes listCmd) throws ApplicationError;

    /** Méthode appellée par le controlleur après un clic sur un bouton undo
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws Exception
     */
    void undo(Controller controller, ListeCommandes listCmd) throws Exception;

    /** Méthode appellée par le controlleur après un clic sur un bouton undo
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws Exception
     */
    void redo(Controller controller, ListeCommandes listCmd) throws Exception;

    /** Méthode appellée par le controlleur après un clic sur un bouton d'exportation de la tournée actuelle
     * @param controller : le controleur de la fenêtre
     * @param listCmd : la liste des commandes du controlleur pour ajouter des actions
     * @throws Exception
     */
    void exportTournee(Controller controller, ListeCommandes listCmd) throws Exception;

}
