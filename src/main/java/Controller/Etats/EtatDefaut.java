package Controller.Etats;

import ApplicationException.ApplicationError;
import Controller.Controller;
import Controller.ListeCommandes;

public class EtatDefaut implements Etat {

    public void chargerPlan(Controller controller, ListeCommandes listCmd) throws ApplicationError {}

    public void chargerDemandesLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError {}

    public void calculerTournee(Controller controller, ListeCommandes listCmd) throws ApplicationError {}

    public void selectionerLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError {}

    public void validerAjout(Controller controller, ListeCommandes listCmd) throws ApplicationError {}

    public void annulerAjout(Controller controller, ListeCommandes listCmd) throws ApplicationError{}

    public void redo(Controller controller, ListeCommandes listCmd) throws Exception {
    }

    public void exportTournee(Controller controller, ListeCommandes listCmd) throws Exception {
    }

    public void undo(Controller controller, ListeCommandes listCmd) throws Exception {
    }

    public void testerAjout(Controller controller, ListeCommandes listCmd) throws ApplicationError {}

    public void ajouterLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError{}

    public void validerSuppression(Controller controller, ListeCommandes listCmd) throws ApplicationError {}

    public void validerReset(Controller controller, ListeCommandes listCmd) throws ApplicationError{}

    public void resetTournee(Controller controller, ListeCommandes listCmd) throws ApplicationError{}

    public void annulerReset(Controller controller, ListeCommandes listCmd) throws ApplicationError{}

    public void supprimerLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError {}

    public void annulerSuppression(Controller controller, ListeCommandes listCmd) throws ApplicationError{}

    public void validerModification(Controller controller, ListeCommandes listCmd) throws ApplicationError {}

    public void modifierLivraison(Controller controller, ListeCommandes listCmd) throws ApplicationError {}

    public void annulerModification(Controller controller, ListeCommandes listCmd) throws ApplicationError{}

    public void testerModification(Controller controller, ListeCommandes listCmd) throws ApplicationError {}

}
