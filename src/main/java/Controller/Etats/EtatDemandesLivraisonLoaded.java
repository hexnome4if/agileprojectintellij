package Controller.Etats;

import ApplicationException.ApplicationError;
import Controller.Commandes.CmdCalculerTournee;
import Controller.Controller;
import Controller.ListeCommandes;
import Logger.InternalLogger;

public class EtatDemandesLivraisonLoaded extends EtatDefaut{

    /** Execution de la procédure de chargement de la calcul de la tournée
     * @param controller : Permet d'accéder au plan de la ville, aux demandes de livraison, et de stocker la tournée une fois calculée.
     * @param listCmd : la liste des commandes du controller
     */
    @Override
    public void calculerTournee(Controller controller, ListeCommandes listCmd) throws ApplicationError {
        //Ajout de la commande à la lliste des commandes
        CmdCalculerTournee computationTournee = new CmdCalculerTournee(controller.carteVille, controller.planLivraison, controller);
        try {
            computationTournee.doCmd();
        } catch (Exception e) {
            e.printStackTrace();
            //On log l'erreur
            InternalLogger.getInstance().logSevere("Une erreur au calcul de la tournée.", this);
            throw new ApplicationError("Erreur lors du chargement du calcul de la tournée.");
        }

        controller.controleurFenetreGestionTournee.afficherMessageEtat("Tournée calculée avec succès.");
        controller.setEtatCourant(controller.etatTourneeCalculated);
    }
}
