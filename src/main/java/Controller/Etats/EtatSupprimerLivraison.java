package Controller.Etats;

import ApplicationException.ApplicationError;
import Controller.Commandes.CmdValiderModification;
import Controller.Controller;
import Controller.ListeCommandes;
import Modele.CarteVilleSimplifiee;
import Modele.DemandeLivraison;
import Modele.PlanLivraison;
import Modele.Tournee;

public class EtatSupprimerLivraison extends EtatDefaut {

    PlanLivraison ancienPlanLivraison;
    CarteVilleSimplifiee ancienneCarteSimplifiee;
    Tournee ancienneTournee;
    PlanLivraison nouveauPlanLivraison;
    CarteVilleSimplifiee nouvelleCarteSimplifiee;
    Tournee nouvelleTournee;

    /** Méthode qui permet de valider la suppresion d'une demande de livraison, faites à une tournée.
     * Fait une copie des anciens plan de livraison, carte simplifiée, et tournée. Applique les nouvelles versions aux objets du modèles.
     * @param controller : le controlleur principal de l'application (qui contient les objets du modèle instanciés)
     * @param listCmd : la liste des commandes du controlleur principal pour effectuer la validation
     */
    @Override
    public void validerSuppression(Controller controller, ListeCommandes listCmd) throws ApplicationError {
        //Récupérer les informations de la fenêtre (la demande de livraison actuellement sélectionnée)
        DemandeLivraison livraisonSelected = controller.getLivraisonSelected();

        if(livraisonSelected == null){
            // Il n'y a pas de livraison actuellement sélectionnée
            throw new ApplicationError("Erreur, aucune livraison n'est sélectionnée");
        } else {
            try {
                // On fait une sauvegarde de nos propres données
                ancienPlanLivraison = controller.planLivraison.clone();
                ancienneCarteSimplifiee = controller.carteVilleSimplifiee.clone();
                ancienneTournee = controller.tourneeEditable.clone();

                // On fait une copie des données actuelles, pour les modifier.
                nouveauPlanLivraison = controller.planLivraison.clone();
                nouvelleCarteSimplifiee = controller.carteVilleSimplifiee.clone();
                nouvelleTournee = controller.tourneeEditable.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                throw new ApplicationError("Erreur, sauvegarde impossible des données avant la suppression d'une livraison.");
            }

            //Créer un nouveau plan de livraison
            nouveauPlanLivraison.supprimerDemandeLivraison(livraisonSelected);

            //Calculer le nouveau plan de ville
            nouvelleCarteSimplifiee = controller.algoDijkstra.CalculNewItineraires(nouveauPlanLivraison);

            //Calculer la modification
            try{
                controller.algoTSP.supprimerLivraison(livraisonSelected, nouvelleCarteSimplifiee);
                nouvelleTournee = controller.algoTSP.getSolution();
            } catch (Exception e){
                e.printStackTrace();
                controller.controleurFenetreValidationSuppression.mettreAJourEchec();
                throw new ApplicationError("Impossible de supprimer cette livraison.");
            } // Pas besoin de setter les anciennes valeurs, puisqu'on on finalise directement l'action

            controller.controleurFenetreValidationSuppression.mettreAJourSuccess();

            try {
                listCmd.ajouterCmd(new CmdValiderModification(ancienPlanLivraison, ancienneCarteSimplifiee, ancienneTournee, nouveauPlanLivraison, nouvelleCarteSimplifiee,  nouvelleTournee, controller));
            } catch (Exception e) {
                e.printStackTrace();
                throw new ApplicationError("Erreur lors du la validation de la suppression d'une livraison.");
            }

            //On ferme la fenêtre modale
            controller.controleurFenetreValidationSuppression.fermerFenetre();

            try {
                controller.setPrevLivraisonSelected(ancienneTournee);
            } catch (ApplicationError ae) {

            }

            //On dit à la fenêtre principale de se mettre à jour
            controller.controleurFenetreGestionTournee.reinjecterDonnee();

            controller.setEtatCourant(controller.etatLivraisonSelected);
        }
    }

    /**
     * Méthode pour annuler la suppression dans la fenêtre modale.
     *
     * @param controller :
     * @param listCmd    :
     */
    @Override
    public void annulerSuppression(Controller controller, ListeCommandes listCmd){
        //Ne rien faire
        controller.setEtatCourant(controller.etatLivraisonSelected);

        controller.controleurFenetreValidationSuppression.fermerFenetre();
    }
}
