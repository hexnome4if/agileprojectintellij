package Controller.Etats;

import ApplicationException.ApplicationError;
import Controller.Commandes.CmdValiderModification;
import Controller.Controller;
import Controller.ListeCommandes;
import Logger.InternalLogger;
import Modele.*;

public class EtatModifierLivraison extends EtatDefaut {

    PlanLivraison ancienPlanLivraison;
    CarteVilleSimplifiee ancienneCarteSimplifiee;
    Tournee ancienneTournee;
    PlanLivraison nouveauPlanLivraison;
    CarteVilleSimplifiee nouvelleCarteSimplifiee;
    Tournee nouvelleTournee;
    boolean estTestedEtValide = false;

    /** Méthode qui permet de valider les modifications faites à une tournée.
     * Fait une copie des anciens plan de livraison, carte simplifiée, et tournée. Applique les nouvelles versions aux objets du modèles.
     * @param controller : le controlleur principal de l'application (qui contient les objets du modèle instanciés)
     * @param listCmd : la liste des commandes du controlleur principal pour effectuer la validation
     */
    @Override
    public void validerModification(Controller controller, ListeCommandes listCmd) throws ApplicationError {
        if(estTestedEtValide) {

            //On valide l'ajout
            try {
                listCmd.ajouterCmd(new CmdValiderModification(ancienPlanLivraison, ancienneCarteSimplifiee, ancienneTournee, nouveauPlanLivraison, nouvelleCarteSimplifiee, nouvelleTournee, controller));
            } catch (Exception e) {
                e.printStackTrace();
                throw new ApplicationError("Erreur lors du la validation de la modification d'une livraison.");
            }
            //On ferme la fenêtre modale
            controller.controleurFenetreEditionLivraison.fermerFenetre();

            //On dit à la fenêtre principale de se mettre à jour
            controller.controleurFenetreGestionTournee.reinjecterDonnee();
            controller.setEtatCourant(controller.etatLivraisonSelected);
            estTestedEtValide = false;
        } else {
            InternalLogger.getInstance().logWarning("Une tentative de modification de livraison non testée a été faite. Comportement non prévu.", this);
            throw new ApplicationError("Modification d'une livraison impossible sans avoir été testée.");
        }
    }

    /** Méthode qui permet de tester la modification d'une demande de livraison à une tournée.
     * Fonctionne sans toucher aux attributs du controller principal. (Copies des composants avant tout test)
     * @param controller : le controlleur principal de l'application (qui contient les objets du modèle instanciés)
     * @param listCmd : la liste des commandes du controlleur principal pour effectuer la validation
     */
    @Override
    public void testerModification(Controller controller, ListeCommandes listCmd) throws ApplicationError {

        //Récupérer les informations de la fenêtre (la demande de livraison actuellement sélectionnée)
        DemandeLivraison livraisonSelected = controller.getLivraisonSelected();
        // CECI EST DONC L'ANCIENNE LIVRAISON

        //Récupérer les informations de la fenêtre
        PlageHoraire plageHoraire = controller.controleurFenetreEditionLivraison.getplageHoraire();
        Long dureeLivraison = (long) controller.getLivraisonSelected().getDureeLivraisonSec();
        Long numNoeud = controller.controleurFenetreEditionLivraison.getNumNoeud();
        Noeud noeudAAjouter = controller.carteVille.getNoeudByID(numNoeud);

        //Si des informations sont non spécifiées, on throw une erreur
        if(dureeLivraison == null || plageHoraire == null || numNoeud == null){
            throw new ApplicationError("Erreur, les informations ne sont pas spécifiées dans le fenêtre");
        }

        //Si des informations sont bizarres, on throw une erreur
         if(plageHoraire.getHeureDeb().getTotalSeconds() > plageHoraire.getHeureFin().getTotalSeconds()) {
             controller.controleurFenetreGestionTournee.afficherMessageEtat("L'ordre des horaires ne permet pas une livraison.");
             throw new ApplicationError("Erreur, l'ordre des horaires ne permet pas une livraison.");
         }

        if (livraisonSelected == null) {
            // Il n'y a pas de livraison actuellement sélectionnée
            throw new ApplicationError("Erreur, aucune livraison n'est sélectionnée");
        } else if (noeudAAjouter == null) {
            // Le noeud spécifié n'existe pas.
            controller.controleurFenetreGestionTournee.afficherMessageEtat("Erreur : le noeud spécifié n'existe pas");
        } else {
            try {
                // On fait une sauvegarde de nos propres données
                ancienPlanLivraison = controller.planLivraison.clone();
                ancienneCarteSimplifiee = controller.carteVilleSimplifiee.clone();
                ancienneTournee = controller.tourneeEditable.clone();

                // On fait une copie des données actuelles, pour les modifier.
                nouveauPlanLivraison = controller.planLivraison.clone();
                nouvelleCarteSimplifiee = controller.carteVilleSimplifiee.clone();
                nouvelleTournee = controller.tourneeEditable.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
                throw new ApplicationError("Erreur, sauvegarde impossible des données avant la modification d'une livraison.");
            }

            //Création de la demande de livraison
            DemandeLivraison nouvelleDemandeLivraison = new DemandeLivraison(noeudAAjouter, ((int) ((long) dureeLivraison)), plageHoraire);

            //Créer un nouveau plan de livraison
            nouveauPlanLivraison.supprimerDemandeLivraison(livraisonSelected);
            nouveauPlanLivraison.ajouterDemandeLivraison(nouvelleDemandeLivraison);

            //Calculer le nouveau plan de ville
            nouvelleCarteSimplifiee = controller.algoDijkstra.CalculNewItineraires(nouveauPlanLivraison);

            //Calculer la modification
            try{
                controller.algoTSP.supprimerLivraison(livraisonSelected, nouvelleCarteSimplifiee);
                controller.algoTSP.ajouterLivraison(nouvelleDemandeLivraison, nouvelleCarteSimplifiee);
                nouvelleTournee = controller.algoTSP.getSolution();
                estTestedEtValide = true;
            } catch (Exception e){
                e.printStackTrace();
                controller.controleurFenetreEditionLivraison.mettreAJourEchec("Livraison non modifiable");
            } finally {
                //Nettoyer les changements dans le modèle (pour éviter en cas d'annulatin d'avoir des problèmes)
                controller.algoDijkstra.setPlanLivraison(ancienPlanLivraison);
                controller.algoTSP.setTournee(ancienneTournee);
                controller.algoTSP.setGraphe(ancienneCarteSimplifiee);
            }

            if(estTestedEtValide){
                try{
                    Long demandeId = nouvelleDemandeLivraison.getId();
                    PlageHoraire plageLivraison = nouvelleTournee.getDemandeByID(demandeId).getPlageLivraison();
                    controller.controleurFenetreEditionLivraison.mettreAJourSuccess(plageLivraison.getHeureDeb().getFormattedTime(false) + " - " + plageLivraison.getHeureFin().getFormattedTime(false));
                }  catch (Exception e){
                    e.printStackTrace();
                    controller.controleurFenetreEditionLivraison.mettreAJourSuccess("");
                }

                //On dit à la fenêtre principale de se mettre à jour
                controller.controleurFenetreGestionTournee.reinjecterDonnee();
            }
        }

    }

    /** Méthode qui annule la procédure d'ajout d'une demande de livraison.
     * @param controller : le controlleur principal de l'application (qui contient les objets du modèle instanciés)
     * @param listCmd : la liste des commandes du controlleur principal pour effectuer la validation
     */
    @Override
    public void annulerModification(Controller controller, ListeCommandes listCmd){
        //La modification n'est donc plus valide.
        estTestedEtValide = false;

        controller.setEtatCourant(controller.etatLivraisonSelected);

        //Les modifications ont déjà été corrigées. Par défaut, les modifications ne sont pas appliquées.
        controller.controleurFenetreEditionLivraison.fermerFenetre();
    }
}
