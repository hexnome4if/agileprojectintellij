package Logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.*;

/**
 * Méthode permettant de logger les warning de l'application (notamment)
 * Créé depuis : http://cyberzoide.developpez.com/tutoriels/java/logging/
 */
public class InternalLogger {

    private static InternalLogger INSTANCE = null;
    private static Logger standardLogger;
    private static int maxSize = 10000000; //Fichier de log de 10Mo MAX
    private static int maxFileLogNb = 10;

    private InternalLogger() {
        //Récupération du logger standard de Java
        standardLogger = Logger.getLogger("Logger LogistiX");

        //Récupération de la date
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

        Handler fh = null;
        try {
            String fileName = "./ApplicationLog_" + sdf.format(cal.getTime()) + "_";
            fileName = fileName.replace(":", "-");
            fh = new FileHandler(fileName, maxSize, maxFileLogNb, false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Suivant si on veut un formatage en TXT ou en XML
        fh.setFormatter(new SimpleFormatter());
        //fh.setFormatter(new XMLFormatter());

        standardLogger.addHandler(fh);
    }

    /**
     * Point d'accès pour l'instance unique du singleton
     */
    public static synchronized InternalLogger getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new InternalLogger();
        }
        return INSTANCE;
    }

    public void logWarning(String string, Object obj) {
        standardLogger.log(Level.WARNING, getString(string, obj));
    }

    public void logSevere(String string, Object obj) {
        standardLogger.log(Level.SEVERE, getString(string, obj));
    }

    public void logConfig(String string, Object obj) {
        standardLogger.log(Level.CONFIG, getString(string, obj));
    }

    private String getString(String string, Object obj) {
        String retour = obj.getClass().toString() + " - " + string;
        return retour;
    }

}
